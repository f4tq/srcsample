require 'jruby-openssl'
# A port from python version -- https://github.com/mitsuhiko/python-pbkdf2/blob/
# This is a mirror of the python implementation though a complete write.  It is intended for use with Devise
# yours, CLF

module Devise
  module Encryptable
    module Encryptors
      class Pbkdf2 < Base
        @@keylen = 24
        @@iterations = 1000

        def self._pseudorandom(x, mac)
          h = mac.clone()
          h.update(x)
          h.digest.split('').map{|t| t.ord}
        end

        def self.digest(password, stretches, salt_encoded, pepper)
          hashfunc =  OpenSSL::Digest::Digest.new('sha1')

          mac = OpenSSL::HMAC.new(password,  hashfunc)
          x = [salt_encoded]
          salt = x.pack('H*')
          #salt.encode('UTF-8')
          buf = []
          puts "pbkdf2 digest:  salt: #{salt} salt_encoded: #{salt.encoding} password: #{password}"
          
          for block in (1..-(-@@keylen / mac.digest.size).floor ) 
            rv = u = self._pseudorandom(salt + [block].pack('I>'),mac)
            #puts "block: #{block} u: #{u}"
            for i in (0..(@@iterations - 2))
              arg = u.map{|t| t.chr}.join('')
              u = self._pseudorandom(arg,mac)
              #puts"block: #{block} i: #{i}  arg: #{arg}"
              
              rv = rv.zip(u).map{|t| t[0] ^ t[1]}

            end
            buf << rv
            #puts "block: #{block} buf: #{buf}"
          end
          buf.flatten!
          #puts "raw buf: #{buf}"
          #puts "raw buf flattened: #{buf.flatten!}"
          str =  buf.flatten.map{|t| t.chr}.join('')[0..@@keylen-1]
          str2 = str.unpack('H*')[0]
          puts "Pbkdf2.digest: password_encrypted:#{str} salt: #{salt}  str2: #{str2[0..39]} #{str2.length}"
          #raise "purposeful exception"
          
          str2[0..39]
        end
      end
    end
  end
end

