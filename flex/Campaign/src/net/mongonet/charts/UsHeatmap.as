package net.mongonet.charts
{
	import mx.core.UIComponent;

	public class UsHeatmap extends UIComponent
	{
		public function UsHeatmap()
		{
			super();
		}
		        override protected function createChildren():void
        {
            super.createChildren();

            m_rendererEngine = new BasicRenderEngine();
            
            m_scene = new Scene3D();
            
            m_camera = new Camera3D();
            m_camera.focus = 1000;
            m_camera.zoom = 1; 
            m_camera.target = m_zero;
            m_camera.x = Model.DISTANCE;
            m_camera.orbit( m_rotationX, m_rotationY, true, m_zero ); 
            
            m_viewport = new Viewport3D();
            m_viewport.mouseChildren = true;
            m_viewport.mouseEnabled = true;
            m_viewport.interactive = true;

            addAxis();            
            
            addChild(m_viewport);
            
            m_viewport.containerSprite.sortMode = ViewportLayerSortMode.INDEX_SORT;
        }
        
      

        override protected function commitProperties():void
        {
            super.commitProperties();
            if( m_sceneDirty )
            {
                m_sceneDirty = false;
                renderScene();
            }
        }
        
        override protected function updateDisplayList(unscaledWidth:Number, unscaledHeight:Number):void
        {
            super.updateDisplayList( unscaledWidth, unscaledHeight );
            
            clipRect.x = 0;
            clipRect.y = 0;
            clipRect.width = unscaledWidth;
            clipRect.height = unscaledHeight;
            scrollRect = clipRect;
            
            m_viewport.x = 0;
            m_viewport.y = 0;            
            m_viewport.viewportWidth = unscaledWidth;
            m_viewport.viewportHeight = unscaledHeight;
            
            graphics.clear();
            graphics.beginFill( 0x000000, 1.0 );
            graphics.drawRect( 0, 0, unscaledWidth, unscaledHeight );
            graphics.endFill();
            
            renderScene();            
        }
        
        protected function renderScene() : void
        {
            m_rendererEngine.renderScene(m_scene, m_camera, m_viewport);                        
        }

	}
}