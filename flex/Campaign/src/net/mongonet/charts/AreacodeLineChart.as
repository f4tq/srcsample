package net.mongonet.charts {

import flash.utils.Dictionary;

import mx.binding.utils.*;
import mx.charts.*;
import mx.charts.chartClasses.Series;
import mx.charts.events.*;
import mx.charts.renderers.*;
import mx.charts.series.*;
import mx.collections.*;

import net.mongonet.beans.ejb.vo.AreacodeRollup;

[DefaultBindingProperty(destination="dataProvider")]

public class AreacodeLineChart extends LineChart
{
		var strokes:ArrayCollection ;
		var colors:ArrayCollection;
//	private var _data:ArrayCollection=null;
	private function init():void{
		this.addEventListener("dataProviderChanged",observeDataChange2);
/*
        var hAxis:CategoryAxis = new CategoryAxis();
        hAxis.categoryField = "transDate" ;
        hAxis.dataProvider =  this.dataProvider;
        hAxis.displayName = "When";
        this.horizontalAxis = hAxis;

        var vAxis:CategoryAxis = new CategoryAxis();
        vAxis.categoryField = "totalEmails" ;
        vAxis.dataProvider =  this.dataProvider;
        vAxis.displayName = "Impressions";
        this.verticalAxis = hAxis;

		colors = new ArrayCollection();
		
		// orange
		colors.addItem( new SolidColor(0xFFCC00,.3))
		// green1
		colors.addItem( new SolidColor(0x99ff00,.3))
		// yellow1
		colors.addItem( new SolidColor(0xFFFF33,.3))
		// hot pink
		colors.addItem( new SolidColor(0xFF00cc,.3))
		// gray
		colors.addItem( new SolidColor(0xCCCCCC,.3))
		// purple1
		colors.addItem( new SolidColor(0xcc99ff,.3))
		// purple2
		colors.addItem( new SolidColor(0x9933ff,.3))
		// lt blue1
		colors.addItem( new SolidColor(0x66ffff,.3))
		//  blue1
		colors.addItem( new SolidColor(0x3333ff,.3))
		//  blue2
		colors.addItem( new SolidColor(0x0000ff,.3))
		
		strokes = new ArrayCollection();
		
		for each (var s:SolidColor in colors)
		{		
			strokes.addItem( new Stroke(s.color, 1));
		}		
*/
	}

	public function AreacodeLineChart()
	{
		trace('AreacodeLineChart created'); 
		super();
		init();
	}
	override protected function updateDisplayList(unscaledWidth:Number,
													  unscaledHeight:Number):void
	{
		observeDataChange();
		super.updateDisplayList(unscaledWidth,unscaledHeight);
	}

	public function observeDataChange2():void
	{
		trace('dataProviderChanged event noticed!');
	}	
	public function observeDataChange():void
	{
		
		/*
		if(_lineChart != null)
			delete _lineChart;
		_lineChart = null;
		
		
		if(_series)
			delete _series;
		_series = null;
		*/
		
		var dict:Dictionary = new Dictionary();
			
		for  each (var i:AreacodeRollup in  this.dataProvider)
		{
			//var cur:LineSeries; 
			if (dict[i.areacode] == null)
			{
				dict[i.areacode]={ areacode: i.areacode 
									//series: cur,min: 0
									,dataProviderSubset : new ArrayCollection() 
									,max: i.totalClicks };
			}
			else
			{
				if (dict[i.areacode]['max'] < i.totalClicks)
					dict[i.areacode]['max'] = i.totalClicks + 1;
			}
			var cur2:ArrayCollection = dict[i.areacode]['dataProviderSubset'] as ArrayCollection;
			cur2.addItem(i);
		}
		
		/*
		_lineChart = new LineChart();
		_lineChart.dataProvider = _data;
		_lineChart.showDataTips = true;
		*/
		 // Define the category axis.
/*
        var hAxis:CategoryAxis = new CategoryAxis();
        hAxis.categoryField = "transDate" ;
        hAxis.dataProvider =  this.dataProvider;
        hAxis.displayName = "When";
        this.horizontalAxis = hAxis;

        var vAxis:CategoryAxis = new CategoryAxis();
        vAxis.categoryField = "totalEmails" ;
        vAxis.dataProvider =  this.dataProvider;
        vAxis.displayName = "Impressions";
        this.horizontalAxis = hAxis;
*/		
		
//		var s:Series = new Series();
		var s:Array = [];	
		
		for each ( var j:Object in dict)
		{
				var cur:LineSeries = new LineSeries();
				cur.displayName = ' Areacode - ' + j['areacode'];
				cur.xField='transDate';
				cur.yField='totalEmails';
				var a:ArrayCollection = j['dataProviderSubset'] as ArrayCollection;
				cur.dataProvider = a;
//				s.addChild(cur);
				s.push(cur);
		}
		this.series = s;
//		this.invalidateSeries();

	}

}
}