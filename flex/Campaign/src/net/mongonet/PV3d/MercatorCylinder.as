package net.mongonet.PV3d
{
         	import flash.geom.Point;
         	import flash.geom.Rectangle;
         	
         	import org.vanrijkom.shp.ShpPoint;
         	import org.vanrijkom.shp.ShpPolygon;

	public class MercatorCylinder
	{
// Geodetic Models
//  These models differ primarily by their measurement of the polar
//  and equatorial radius of the earth

			/**
    	     * Meters per degree (using 6371000 as radius).
        	 */
        	public static var METERS_PER_DEGREE : Number = 111195;
        	/**
         	* Meters per pixel at 96 dpi.
         	*/
        	public static var INCHESPERMETER : Number = 39.3700787;
		  	/** Radius of the earth, in meters, at the equator. */
		  	public static var GLOBE_RADIUS_EQUATOR:Number = 6378000;   

	  		/** Radius of the earth, in meters, at the poles. */
  			public static var GLOBE_RADIUS_POLES:Number = 6357000;   

//
			private var modelRadius:Array= [ {P:3432.275, Q:3443.950}, // Clarke 1866 
					{P:3432.370, Q:3443.917}, //WGS-72 
					{P:3432.371, Q:3443.918} ]; //WGS-84
					 
			private var e2:Number; // eccentricity
			private var e: Number; // eccentricity squared
			private var minLL:Point;
			private var maxLL:Point;

			private var fMin:Number;
			private var _curModel:Number=2;
        	private var _DPI:Number=96;
        	
        	// clip out any ring that's outside this rectangle
			public function MercatorCylinder( n:Number)
			{
				_curModel = 2;
				computeModel();
			}
            private var _trueScaleLatitude:Number = 45;
            // computed scale is derived from the current geodetic model
            // and the _mapScale provided by the user
			private var _computedScale:Number;
			// user supplied scale;
			private var _mapScale:Number;
			
			public function set DPI(n:Number):void	{	_DPI = n; }
			public function get DPI():Number{return _DPI;}


			[Bindable] 
			public function get curModel():Number{return _curModel;}
			
			public function set curModel(n:Number):void
			{
				trace('curModel set');
				
								// set up map variables
   			 	_curModel = n;
   			 	computeModel();

			}
			private function computeModel():void
			{
   			 	var p:Number = this.modelRadius[_curModel].P;
   			 	var q:Number = this.modelRadius[_curModel].Q;
   			 	
   			 	e2 = 1 - (p * p) / (q * q); // eccentricity squared
   			 	e = Math.sqrt( e2 );        // eccentricity of spheroid

			}			
			protected function set mapScale(n:Number):void
			{
				// newScale computes _computedScale
				
				newScale(_trueScaleLatitude, n);
			}
			[Bindable] 
			protected function get mapScale():Number { return _mapScale;}


			public function set trueScaleLatitude(n:Number):void
			{
				newScale(n, this.mapScale);
			}
			public function get trueScaleLatitude():Number
			{
				return _trueScaleLatitude;
			}      	

			public static function degrees(radians:Number) :Number {
    			return radians * 360 / (2*Math.PI);
  			}
			public static function radians(degrees:Number):Number {
			    return degrees * (2*Math.PI) / 360;
  			}
			public static function square(d :Number) :Number {
			    return d*d;
  			}

        	public  function get PIXELS_PER_METER() : Number { return   DPI  / _computedScale;}
        	
        	public  function get METERS_PER_PIXEL() : Number { return  1.0 / PIXELS_PER_METER;}
        	
        	public  function get PIXELS_PER_DEGREE() : Number { return PIXELS_PER_METER * METERS_PER_DEGREE;}
        	public  function get DEGREES_PER_PIXEL() : Number { return  1.0 / PIXELS_PER_DEGREE; }
        	public  function get RADIANS_PER_PIXEL() : Number { return  radians(DEGREES_PER_PIXEL)  ; }

			// www.ddj.com/cpp/184403028?pgno=3
			// SetBounds - Change the parameters for the projection 
			// 
			public function setBounds( min:Point, max:Point, scale:Number ) :void
			{ 
				
				// save the extrema 
				minLL = min; 
				maxLL = max;

				// recalculate the scale 
				newScale( (minLL.y + maxLL.y) / 2, scale );

				fMin = projection(radians(minLL.y)); // compute once for later use 

				trace('setBounds: fMin=' +  fMin + ' minLL(x,y)='+ minLL.x +','+minLL.y +')' 
					+ ' maxLL(x,y)='+ maxLL.x +','+maxLL.y +')');

			}

			// // SetOrigin - Change the origin for the projection // 
			
			public function setOrigin( origin :Point, scale:Number ):void
			{
				trace('origin set to :', origin,' @scale Map Inches/Screen Inch ratio', scale);
				// save the origin 
				minLL = origin; 
				maxLL = origin;

				// calculate the scale 
				newScale( origin.y, scale );

				fMin = projection( radians(minLL.y) ); // compute once for later use
				trace('setOrigin: fMin=' +  fMin + ' minLL(x,y)='+ minLL.x +','+minLL.y +')' 
					+ ' maxLL(x,y)='+ maxLL.x +','+maxLL.y +')');
			}
			
			public function get origin() :Point { return minLL;}
			
			public function newScale( lat :Number, inScale:Number ):void 
			{ // compute scale factor 

				_trueScaleLatitude = lat;
				_mapScale = inScale;
				
				trace(' true scale Latitude set to ' + lat + ' with scale map inches/display inches=' + _mapScale);
					 
				var sinLat:Number = Math.sin(radians(lat));

				var Kpt:Number = Math.sqrt( 1.0 - e2 * sinLat * sinLat) / Math.cos(radians(lat) );

				var So:Number = _mapScale * Kpt;

				// Conversion factor for radians to inches 
				_computedScale = 72913.2 * // number of inches in a nautical mile 
						modelRadius[curModel].Q / // equatorial radius 
						So;
				trace('newScale: scale = ' + _computedScale);
			}

			// dr. dobbs projection
			// latitude should already be in radians 
			private function projection(latitude:Number) : Number
			{
				
				var sinLat:Number = Math.sin( Math.abs(latitude));
				 var eSinLat:Number = this.e * sinLat;

				
				var newLat:Number  = 0.5 * Math.log(  
	   						             (( 1 + sinLat ) / ( 1 - sinLat ))  
							*  Math.pow( ( 1 - eSinLat ) / ( 1 + eSinLat ), this.e));


				// result must assume the sign of the input latitude
				if( (latitude < 0 && newLat > 0) ||
					(latitude > 0 && newLat < 0 ) )
      				newLat = - newLat;


				return newLat; 
			}
			public function XY2( x:Number,y:Number ) :Point
			{
				var p:ShpPoint = new ShpPoint(); p.x = x, p.y = y;
				return XY(p);
			}
			public function XY( ll:ShpPoint ) :Point
			{ 
				// X is easy, its just the delta in longitude times the scale
				var xyResult:Point = new Point();
				
				xyResult.x = radians(ll.x - minLL.x); 

				xyResult.x *= _computedScale;
				xyResult.x *= this.DPI;

				// Y is harder 
				xyResult.y = projection(radians(ll.y )) - fMin; 
				xyResult.y *= _computedScale;
				xyResult.y *= this.DPI;

				//var trans:Point = this.LatLon(xyResult);
				return xyResult; 
			}
			public function LatLon2( x:Number,y:Number ):Point 
			{ 
				return LatLon(new Point(x,y));
			}
			public function LatLon( xy:Point ):Point 
			{ 
				var llResult:Point = new Point;
			  	// Once again, X is easy. Compute the longitude delta 
  				llResult.x = degrees(xy.x / this._computedScale ); 
  				// un-scale the X value 
  				llResult.x += this.minLL.x; 
  				// add minimum longitude

			  // As you might suspect, Latitude is harder. We must use 
  			  // successive approximations to approach a suitable answer 
  				var Ym:Number = xy.y / this._computedScale + this.fMin; 
  				var t:Number = Math.exp( -Ym );

			  // initial latitude approximation
			  	var lat:Array = [0,0]; 
			  	var M_PI_2:Number = Math.PI /2;
			  	lat[1] = M_PI_2 - 2.0 * Math.atan( t );

				do { 
    				// save the old latitude 
    				lat[0] = lat[1];

				    // Use old latitude to compute a more accurate answer 
    				var eSinLat:Number = e * Math.sin( lat[0] ); 
    				var f:Number = Math.pow((1-eSinLat)/(1+eSinLat), e/2 ); 
    				lat[1] = M_PI_2 - 2.0 * Math.atan( t * f );

				    // continue until the difference is acceptably small 
  				} while( Math.abs(lat[1]-lat[0]) > 0.1e-5 );

				// convert from radians to degrees 
				llResult.y = degrees(lat[1]);

			  return llResult; 
			}

			
			public function printShape ( polygon:ShpPolygon):void
			{
				var s0:ShpPoint  = new ShpPoint();
				s0.x = polygon.box.x;
				s0.y = polygon.box.y;
				 				
				var m1 : Point = XY(s0);
				s0.x += polygon.box.width;
				s0.y += polygon.box.height;
				
				var m2 : Point = XY(s0);
				
				trace('min(x,y) = (' + m1.x + ', ' + m1.y + ')' +
					'max(x,y) = (' + m2.x + ', ' + m2.y + ')' );
					
				trace('compare aspect ratios original in degrees=' + polygon.box.height/polygon.box.width + ' mercator=' + (m2.y-m1.y)/(m2.x-m1.x) ); 

			}
			  			 /** Computes the earth's radius of curvature at a particular latitude,
			   * assuming that the earth is a squashed sphere with
   			   * elliptical cross-section.  Since we supposedly have latitude 
   			   * and longitude to lots of decimal places, I decided to worry
   			   * about the squashing, just for fun.
   			   *
   			   * The radius of curvature at a latitude is NOT the same as the
   			   * actual radius.  The actual radius is smaller at the poles
   			   * than at the equator, but the earth is less curved there, as
   			   * if it were the surface of a *bigger* sphere!  
               *
			   * The actual radius could be computed by 
   				*   return Math.sqrt(square(GLOBE_RADIUS_EQUATOR*Math.cos(lat)) 
			   *                    + square(GLOBE_RADIUS_POLES*Math.sin(lat)));
			   *
   				* The radius of curvature depends not only on the latitude you're at
   				* but also on the direction you are traveling.  But I'll use the 
   				* approximate formula recommended at
			   *    http://www.census.gov/cgi-bin/geo/gisfaq?Q5.1
			   * which ignores the direction.  There is a whole range of possible
			   * answers depending on direction; the formula returns the geometric
			   * mean of the max and min of that range. 
			   *
   				* @param lat - latitude in radians.  This is the angle
			   *    that a point at this latitude makes with the horizontal.
		   */
		  	public function distanceTo(p1:Point, p2:Point):Number {
			    // WARNING: These two lines of code are duplicated in another method.
    			var lat1:Number = radians(p1.y)
    			var lat2:Number = radians(p2.y)
    			var dlat:Number = lat2-lat1;
			    var dlong:Number = radians(p2.x)-radians(p1.x);

		    	// Formula from http://williams.best.vwh.net/avform.htm#Dist
	    		// See http://mathforum.org/library/drmath/view/51879.html for a derivation.
			    //
			    // I've adapted the formula slightly to deal with the squashed
    			// earth.  We still make an approximation by taking the radius of
	    		// curvature r to be constant throughout the route.  It actually
    			// changes, so we should integrate over the whole route.  But such
    			// "elliptic integrals" don't have a closed form and can't be
	    		// found using trigonometry.

			    var a:Number = square(Math.sin(dlat/2)) 
        	       + Math.cos(lat1)*Math.cos(lat2)*square(Math.sin(dlong/2));
		    	var c:Number = 2*Math.atan2(Math.sqrt(a),Math.sqrt(1-a));  // angle in radians formed by start point, earth's center, & end point
	    		var r:Number = globeRadiusOfCurvature((lat1+lat2)/2);      // radius of earth at midpoint of route
    			return r*c;
  			}

			/** The direction that you have to go from this point to get to p.
   				*
	   			* Answer is returned in degrees, between -180.0 and 180.0.
		   		* Here -180, -90, 0, 90, and 180 correspond to west, south, east,
   				* north, and west again -- just like theta in polar coordinates.
   				*
		   		* It would be tempting to just return atan2(dy,dx); see
   				* the comment in distanceTo() for why we don't do that.
   			*/
			public function directionTo(p1:Point , p2:Point ):Number {
		    	// WARNING: These two lines of code are duplicated in another method.
	    		var lat1  : Number = radians(p1.y), lat2 :Number= radians(p2.y), dlat:Number = lat2-lat1;
    			var dlong:Number = radians(p2.x)-radians(p1.x);

			    // Formula from http://williams.best.vwh.net/avform.htm#GCF
	    		var radians:Number = Math.atan2(Math.sin(-dlong)*Math.cos(lat2),
				        Math.cos(lat1)*Math.sin(lat2)
				        - Math.sin(lat1)*Math.cos(lat2)*Math.cos(-dlong));

			    var deg:Number = degrees(radians);
      
			    // That formula has 0 degrees being due north.  Rotate it
    			// so that 90 degrees is due east.
    			deg += 90;
	    		if (deg > 180) deg -= 360;
				    return deg;
  			}

			protected function globeRadiusOfCurvature(lat : Number) :Number {
			    var a:Number = GLOBE_RADIUS_EQUATOR;  // major axis
    			var b:Number = GLOBE_RADIUS_POLES;    // minor axis
    			var e:Number = Math.sqrt(1 - square(b/a));  // eccentricity
    			return a*Math.sqrt(1-square(e)) / (1-square(e*Math.sin(lat)));
  			}
  			protected function radiusOfEarthAtLatitude( lat : Number) : Number
  			{
  				return Math.sqrt(square(GLOBE_RADIUS_EQUATOR*Math.cos(lat)) 
   *                    + square(GLOBE_RADIUS_POLES*Math.sin(lat)));
  			}


	}
}