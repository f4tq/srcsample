package net.mongonet.beans.ejb.vo
{

	[Bindable]
	[RemoteClass(alias="net.mongonet.beans.ejb.Lla")]
	public class Lla
	{

        public var id:LlaId;
        
        public var xidsByOrigXidRef:Xids;
        
        public var xidsByNewXidRef:Xids;
           
	}
}