package net.mongonet.beans.ejb.vo
{

	[Bindable]
	[RemoteClass(alias="net.mongonet.beans.ejb.ReservationEmails")]
	public class ReservationEmails
	{

        public var reservationEmailSeqid:Number;
        
        public var emailAddresses:EmailAddresses;
        
        public var reservation:Reservation;
        
        public var emailType:String;
           
	}
}