package net.mongonet.beans.ejb.vo
{

	[Bindable]
	[RemoteClass(alias="net.mongonet.beans.ejb.GlobalPhoneExclusions")]
	public class GlobalPhoneExclusions
	{

        public var phoneRef:Number;
        
        public var phonenumbers:Phonenumbers;
        
        public var reason:String;
           
	}
}