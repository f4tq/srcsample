package net.mongonet.beans.ejb.vo
{

    import mx.collections.ArrayCollection;
        
	[Bindable]
	[RemoteClass(alias="net.mongonet.beans.ejb.HintQuestions")]
	public class HintQuestions
	{

        public var questionId:Number;
        
        public var question:String;
        
        public var userses:ArrayCollection;
           
	}
}