package net.mongonet.beans.ejb.vo
{

	[Bindable]
	[RemoteClass(alias="net.mongonet.beans.ejb.Cannedresponse")]
	public class Cannedresponse
	{

        public var cannedSeqid:Number;
        
        public var subject:String;
        
        public var body:String;
           
	}
}