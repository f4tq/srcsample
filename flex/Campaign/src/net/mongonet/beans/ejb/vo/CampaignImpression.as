package net.mongonet.beans.ejb.vo
{

	[Bindable]
	[RemoteClass(alias="net.mongonet.beans.ejb.CampaignImpression")]
	public class CampaignImpression
	{

        public var cuid:Number;
        
        public var xids:Xids;
        
        public var campaign:Campaign;
        
        public var requested:Date;
        
        public var ip:String;
        
        public var uid:Number;
        
        public var emailAddresses:EmailAddresses;
        
        public var emailType:String;
        
        public var phonenumber:Phonenumbers;
           
	}
}