package net.mongonet.beans.ejb.vo
{

    import mx.collections.ArrayCollection;
        
	[Bindable]
	[RemoteClass(alias="net.mongonet.beans.ejb.PaymentInfo")]
	public class PaymentInfo
	{

        public var payinfoSeqid:Number;
        
        public var name:String;
        
        public var address1:String;
        
        public var address2:String;
        
        public var city:String;
        
        public var state:String;
        
        public var country:String;
        
        public var phone:String;
        
        public var zip:String;
        
        public var chargeses:ArrayCollection;
        
        public var reservations:ArrayCollection;
        
        public var cookieses:ArrayCollection;
        
        public var creditcardInfo:CreditcardInfo;
           
	}
}