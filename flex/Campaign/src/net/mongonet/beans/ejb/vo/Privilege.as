package net.mongonet.beans.ejb.vo
{

	[Bindable]
	[RemoteClass(alias="net.mongonet.beans.ejb.Privilege")]
	public class Privilege
	{

        public var privNo:int;
        
        public var itemName:String;
           
	}
}