package net.mongonet.beans.ejb.vo
{

	[Bindable]
	[RemoteClass(alias="net.mongonet.beans.ejb.AccountOption")]
	public class AccountOption
	{

        public var accountOptionSeqid:Number;
        
        public var account:Account;
        
        public var nvSeqid:Number;
        
        public var nameId:Number;
        
        public var value:String;
           
	}
}