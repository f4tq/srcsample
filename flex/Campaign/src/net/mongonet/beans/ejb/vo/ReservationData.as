package net.mongonet.beans.ejb.vo
{

	[Bindable]
	[RemoteClass(alias="net.mongonet.beans.ejb.ReservationData")]
	public class ReservationData
	{

        public var resid:Number;
        
        public var reservationDataName:ReservationDataName;
        
        public var reservation:Reservation;
        
        public var value:String;
           
	}
}