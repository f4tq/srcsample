package net.mongonet.beans.ejb.vo
{

	[Bindable]
	[RemoteClass(alias="net.mongonet.beans.ejb.Keystore")]
	public class Keystore
	{

        public var keySeqid:Number;
        
        public var fingerPrint:String;
        
        public var armorPubkey:String;
           
	}
}