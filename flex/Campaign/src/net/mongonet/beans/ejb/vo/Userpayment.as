package net.mongonet.beans.ejb.vo
{

	[Bindable]
	[RemoteClass(alias="net.mongonet.beans.ejb.Userpayment")]
	public class Userpayment
	{

        public var id:UserpaymentId;
        
        public var paymentInfo:PaymentInfo;
        
        public var users:Users;
           
	}
}