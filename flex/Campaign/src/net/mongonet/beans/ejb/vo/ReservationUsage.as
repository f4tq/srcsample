package net.mongonet.beans.ejb.vo
{

	[Bindable]
	[RemoteClass(alias="net.mongonet.beans.ejb.ReservationUsage")]
	public class ReservationUsage
	{

        public var reservationUsageSeqid:Number;
        
        public var charges:Charges;
        
        public var reservation:Reservation;
        
        public var faxjob:Faxjob;
        
        public var trackingNumber:String;
        
        public var trackingOpaque:String;
           
	}
}