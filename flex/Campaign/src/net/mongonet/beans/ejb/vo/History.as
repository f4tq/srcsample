package net.mongonet.beans.ejb.vo
{

	[Bindable]
	[RemoteClass(alias="net.mongonet.beans.ejb.History")]
	public class History
	{

        public var historySeqid:Number;
        
        public var xids:Xids;
        
        public var whenProcessed:Date;
           
	}
}