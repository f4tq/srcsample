package net.mongonet.beans.ejb.vo
{

	[Bindable]
	[RemoteClass(alias="net.mongonet.beans.ejb.AccountUsers")]
	public class AccountUsers
	{

        public var acctuserid:Number;
        
        public var users:Users;
        
        public var account:Account;
        
        public var established:Date;
        
        public var active:Boolean;
           
	}
}