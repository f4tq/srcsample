package net.mongonet.beans.ejb.vo
{

	[Bindable]
	[RemoteClass(alias="net.mongonet.beans.ejb.FaxjobEmailaddresses")]
	public class FaxjobEmailaddresses
	{

        public var emListSeqid:Number;
        
        public var emailAddresses:EmailAddresses;
        
        public var emailType:String;
        
        public var faxjob:Faxjob;
           
	}
}