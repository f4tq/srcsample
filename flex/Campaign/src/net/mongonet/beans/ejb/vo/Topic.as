package net.mongonet.beans.ejb.vo
{

    import mx.collections.ArrayCollection;
        
	[Bindable]
	[RemoteClass(alias="net.mongonet.beans.ejb.Topic")]
	public class Topic
	{

        public var topicSeqid:Number;
        
        public var descr:String;
        
        public var suggestions:ArrayCollection;
        
        public var notifylists:ArrayCollection;
           
	}
}