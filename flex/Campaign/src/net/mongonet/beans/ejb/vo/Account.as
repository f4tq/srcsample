package net.mongonet.beans.ejb.vo
{

    import mx.collections.ArrayCollection;
        
	[Bindable]
	[RemoteClass(alias="net.mongonet.beans.ejb.Account")]
	public class Account
	{

        public var acctSeqid:Number;
        
        public var companyName:String;
        
        public var accountOptions:ArrayCollection;
        
        public var campaigns:ArrayCollection;
        
        public var accountUserses:ArrayCollection;

        public var accountRollups:ArrayCollection;
           
	}
}