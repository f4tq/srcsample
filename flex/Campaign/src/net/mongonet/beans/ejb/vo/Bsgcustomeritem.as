package net.mongonet.beans.ejb.vo
{

    import mx.collections.ArrayCollection;
        
	[Bindable]
	[RemoteClass(alias="net.mongonet.beans.ejb.Bsgcustomeritem")]
	public class Bsgcustomeritem
	{

        public var bsgitemSeqid:Number;
        
        public var applied:Date;
        
        public var charge:Number;
        
        public var pages:Number;
        
        public var bsgCharges:ArrayCollection;
           
	}
}