package net.mongonet.beans.ejb.vo
{

	[Bindable]
	[RemoteClass(alias="net.mongonet.beans.ejb.BsgInfo")]
	public class BsgInfo
	{

        public var sourcePhone:String;
        
        public var checked:Date;
        
        public var ocn:int;
        
        public var customerType:String;
        
        public var lec:int;
        
        public var onnet:String;
        
        public var rao:int;
        
        public var replycode:int;
        
        public var flags:Number;
        
        public var differentAddress:Boolean;
           
	}
}