package net.mongonet.beans.ejb.vo
{

	[Bindable]
	[RemoteClass(alias="net.mongonet.beans.ejb.CampaignRollup")]
	public class CampaignRollup
	{

        public var campaignRollupId:Number;
        
        public var campaign:Campaign;
        
        public var transDate:Date;
        
        public var totalClicks:Number;
        
        public var totalTx:Number;
        
        public var totalEmails:Number;
        
        public var totalUniqEmails:Number;
        
        public var totalCharges:Number;
        
        public var totalMinutes:Number;
        
        public var totalPages:Number;
        
        public var rollupType:String;
           
	}
}