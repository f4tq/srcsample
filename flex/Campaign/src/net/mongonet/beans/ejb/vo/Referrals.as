package net.mongonet.beans.ejb.vo
{

	[Bindable]
	[RemoteClass(alias="net.mongonet.beans.ejb.Referrals")]
	public class Referrals
	{

        public var referralSeqid:Number;
        
        public var emailAddresses:EmailAddresses;
        
        public var enterpriseXid:String;
        
        public var reftype:String;
        
        public var referralDate:Date;
        
        public var url:Url;
           
	}
}