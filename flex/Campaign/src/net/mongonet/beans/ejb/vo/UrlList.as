package net.mongonet.beans.ejb.vo
{

	[Bindable]
	[RemoteClass(alias="net.mongonet.beans.ejb.UrlList")]
	public class UrlList
	{

        public var ulistSeqid:Number;
        
        public var url:Url;
        
        public var listType:String;
           
	}
}