package net.mongonet.beans.ejb.vo
{

	[Bindable]
	[RemoteClass(alias="net.mongonet.beans.ejb.UserPix")]
	public class UserPix
	{

        public var pixSeqid:Number;
        
        public var users:Users;
        
        public var contentType:String;
        
        public var contentDesc:String;
        
        public var contentCreated:Date;
           
	}
}