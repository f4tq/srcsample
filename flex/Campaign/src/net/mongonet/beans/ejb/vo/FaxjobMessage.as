package net.mongonet.beans.ejb.vo
{

    import mx.collections.ArrayCollection;
        
	[Bindable]
	[RemoteClass(alias="net.mongonet.beans.ejb.FaxjobMessage")]
	public class FaxjobMessage
	{

        public var faxjobMessageSeqid:Number;
        
        public var lvalueLu:LvalueLu;
        
        public var faxjob:Faxjob;
        
        public var logTime:Date;
        
        public var faxjobMessageNvs:ArrayCollection;
           
	}
}