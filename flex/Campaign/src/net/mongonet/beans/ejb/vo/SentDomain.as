package net.mongonet.beans.ejb.vo
{

    import mx.collections.ArrayCollection;
        
	[Bindable]
	[RemoteClass(alias="net.mongonet.beans.ejb.SentDomain")]
	public class SentDomain
	{

        public var sentDomainSeqid:int;
        
        public var theDomain:String;
        
        public var clientSmtpservers:ArrayCollection;
           
	}
}