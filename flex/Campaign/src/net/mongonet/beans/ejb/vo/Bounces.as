package net.mongonet.beans.ejb.vo
{

	[Bindable]
	[RemoteClass(alias="net.mongonet.beans.ejb.Bounces")]
	public class Bounces
	{

        public var bounceSeqid:Number;
        
        public var xids:Xids;
        
        public var emailAddresses:EmailAddresses;
        
        public var fromEmailRef:Number;
        
        public var bounceProcessed:Date;
        
        public var data:String;
           
	}
}