package net.mongonet.beans.ejb.vo
{

	[Bindable]
	[RemoteClass(alias="net.mongonet.beans.ejb.Veryonlylist")]
	public class Veryonlylist
	{

        public var veryonlylistSeqid:Number;
        
        public var lvalueLuByTripletRef:LvalueLu;
        
        public var lvalueLuByAttributeRef:LvalueLu;
           
	}
}