package net.mongonet.beans.ejb.vo
{

    import mx.collections.ArrayCollection;
        
	[Bindable]
	[RemoteClass(alias="net.mongonet.beans.ejb.EmailAddresses")]
	public class EmailAddresses
	{

        public var emailSeqid:Number;
        
        public var emailAddress:String;
        
        public var md5:String;
        
        public var smtpPieces:ArrayCollection;
        
        public var faxjobEmail:ArrayCollection;
        
        public var reservationEmailses:ArrayCollection;
        
        public var reservations:ArrayCollection;
        
        public var cookieses:ArrayCollection;
        
        public var userses:ArrayCollection;
        
        public var suggestionThreads:ArrayCollection;
        
        public var signupses:ArrayCollection;
        
        public var suggestions:ArrayCollection;
        
        public var notifylists:ArrayCollection;
           
	}
}