package net.mongonet.beans.ejb.vo
{

    import mx.collections.ArrayCollection;
        
	[Bindable]
	[RemoteClass(alias="net.mongonet.beans.ejb.ReservationDataName")]
	public class ReservationDataName
	{

        public var nameId:Number;
        
        public var nameType:String;
        
        public var nameDesc:String;
        
        public var reservationDatas:ArrayCollection;
           
	}
}