package net.mongonet.beans.ejb.vo
{

	[Bindable]
	[RemoteClass(alias="net.mongonet.beans.ejb.Useroles")]
	public class Useroles
	{

        public var roleId:Number;
        
        public var users:Users;
        
        public var role:String;
           
	}
}