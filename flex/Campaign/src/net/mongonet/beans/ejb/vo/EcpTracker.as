package net.mongonet.beans.ejb.vo
{

	[Bindable]
	[RemoteClass(alias="net.mongonet.beans.ejb.EcpTracker")]
	public class EcpTracker
	{

        public var missingEcpSeqid:Number;
        
        public var url:Url;
        
        public var confirmedEcp:Boolean;
        
        public var captured:Date;
           
	}
}