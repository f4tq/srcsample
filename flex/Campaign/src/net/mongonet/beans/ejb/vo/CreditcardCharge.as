package net.mongonet.beans.ejb.vo
{

    import mx.collections.ArrayCollection;
        
	[Bindable]
	[RemoteClass(alias="net.mongonet.beans.ejb.CreditcardCharge")]
	public class CreditcardCharge
	{

        public var chargeDate:Date;
        
        public var firstpageCharge:Number;
        
        public var totalCharge:Number;
        
        public var referenceId:Number;
        
        public var transactionId:String;
        
        public var errorCode:String;
        
        public var avsResult:String;
        
        public var authResponseText:String;
        
        public var cvv2Result:String;
        
        public var authCode:String;
        
        public var wasPreauthed:Boolean;
        
        public var reconciled:Date;
        
        public var reconcileAmt:Number;
        
        public var creditcardChargeReversalses:ArrayCollection;
        
        public var creditcardInfo:CreditcardInfo;
           
	}
}