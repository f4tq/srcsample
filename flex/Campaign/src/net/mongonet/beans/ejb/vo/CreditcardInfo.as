package net.mongonet.beans.ejb.vo
{

	[Bindable]
	[RemoteClass(alias="net.mongonet.beans.ejb.CreditcardInfo")]
	public class CreditcardInfo
	{

        public var cc:String;
        
        public var ccenc:String;
        
        public var expmonth:int;
        
        public var expyear:int;
        
        public var cvv2:String;
        
        public var authCode:String;
        
        public var ccType:String;
        
        public var verified:Date;
        
        public var ccExpires:Date;
        
        public var preauthAmt:Number;
        
        public var transactionId:String;
        
        public var referenceId:int;
        
        public var errorCode:String;
        
        public var authResponseText:String;
        
        public var avsResult:String;
        
        public var cvv2Result:String;
           
	}
}