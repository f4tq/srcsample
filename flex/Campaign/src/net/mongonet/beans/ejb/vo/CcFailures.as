package net.mongonet.beans.ejb.vo
{

	[Bindable]
	[RemoteClass(alias="net.mongonet.beans.ejb.CcFailures")]
	public class CcFailures
	{

        public var failureSeqid:Number;
        
        public var who:String;
        
        public var transType:String;
        
        public var ccMasked:String;
        
        public var transDate:Date;
        
        public var toaddresses:String;
        
        public var ccaddresses:String;
        
        public var fromaddress:String;
        
        public var networkAddress:String;
        
        public var referenceId:Number;
        
        public var transactionId:String;
        
        public var errorCode:String;
        
        public var authResponseText:String;
        
        public var avsResult:String;
        
        public var cvv2Result:String;
        
        public var authCode:String;
           
	}
}