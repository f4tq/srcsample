package net.mongonet.beans.ejb.vo
{

	[Bindable]
	[RemoteClass(alias="net.mongonet.beans.ejb.FaxjobData")]
	public class FaxjobData
	{

        public var faxjobDataSeqid:Number;
        
        public var faxjob:Faxjob;
        
        public var nvSeqid:Number;
        
        public var nameId:Number;
        
        public var value:String;
           
	}
}