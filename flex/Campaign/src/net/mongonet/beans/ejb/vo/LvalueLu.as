package net.mongonet.beans.ejb.vo
{

    import mx.collections.ArrayCollection;
        
	[Bindable]
	[RemoteClass(alias="net.mongonet.beans.ejb.LvalueLu")]
	public class LvalueLu
	{

        public var nameId:Number;
        
        public var name:String;
        
        public var onlylists:ArrayCollection;
        
        public var faxjobMessages:ArrayCollection;
        
        public var namevaluelists:ArrayCollection;
        
        public var veryonlylistsForTripletRef:ArrayCollection;
        
        public var veryonlylistsForAttributeRef:ArrayCollection;
           
	}
}