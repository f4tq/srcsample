package net.mongonet.beans.ejb.vo
{

	[Bindable]
	[RemoteClass(alias="net.mongonet.beans.ejb.DomainRollup")]
	public class DomainRollup
	{

        public var domainRollupSeqid:Number;
        
        public var campaign:Campaign;
        
        public var rollupType:String;
        
        public var transDate:Date;
        
        public var domainName:String;
        
        public var totalClicks:Number;
        
        public var totalTx:Number;
        
        public var totalEmails:Number;
        
        public var totalUniqEmails:Number;
        
        public var totalCharges:Number;
        
        public var totalMinutes:Number;
        
        public var totalPages:Number;
           
	}
}