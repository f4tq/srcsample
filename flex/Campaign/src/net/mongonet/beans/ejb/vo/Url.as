package net.mongonet.beans.ejb.vo
{

    import flash.utils.ByteArray;
    
    import mx.collections.ArrayCollection;
        
	[Bindable]
	[RemoteClass(alias="net.mongonet.beans.ejb.Url")]
	public class Url
	{

        public var urlSeqid:Number;
        
        public var url:String;
        
        public var data:ByteArray;
        
        public var istransient:Boolean;
        
        public var isencrypted:Boolean;
        
        public var urlLists:ArrayCollection;
        
        public var ecpTrackers:ArrayCollection;
        
        public var referrals:ArrayCollection;
           
	}
}