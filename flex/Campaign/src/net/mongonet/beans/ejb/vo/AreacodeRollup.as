package net.mongonet.beans.ejb.vo
{

	[Bindable]
	[RemoteClass(alias="net.mongonet.beans.ejb.AreacodeRollup")]
	public class AreacodeRollup
	{

        public var areacodeRollupId:Number;
        
        public var campaign:Campaign;
        
        public var transDate:Date;
        
        public var areacode:String;
        
        public var totalClicks:Number;
        
        public var totalTx:Number;
        
        public var totalEmails:Number;
        
        public var totalUniqEmails:Number;
        
        public var totalCharges:Number;
        
        public var totalMinutes:Number;
        
        public var totalPages:Number;
        
        public var rollupType:String;
           
	}
}