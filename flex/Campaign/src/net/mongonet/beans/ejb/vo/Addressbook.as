package net.mongonet.beans.ejb.vo
{

	[Bindable]
	[RemoteClass(alias="net.mongonet.beans.ejb.Addressbook")]
	public class Addressbook
	{

        public var id:AddressbookId;
        
        public var emailAddresses:EmailAddresses;
        
        public var users:Users;
           
	}
}