package net.mongonet.beans.ejb.vo
{

	[Bindable]
	[RemoteClass(alias="net.mongonet.beans.ejb.Namevaluelist")]
	public class Namevaluelist
	{

        public var nvSeqid:Number;
        
        public var lvalueLu:LvalueLu;
        
        public var value:String;
           
	}
}