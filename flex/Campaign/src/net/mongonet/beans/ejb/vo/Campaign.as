package net.mongonet.beans.ejb.vo
{

    import mx.collections.ArrayCollection;
        
	[Bindable]
	[RemoteClass(alias="net.mongonet.beans.ejb.Campaign")]
	public class Campaign
	{

        public var cid:Number;
        
        public var account:Account;
        
        public var publicId:String;
        
        public var name:String;
        
        public var description:String;
        
        public var startTime:Date;
        
        public var link:String;
        
        public var presentation:String;
        
        public var domainRollups:ArrayCollection;
        
        public var campaignUsages:ArrayCollection;
        
        public var areacodeRollups:ArrayCollection;
        
        public var campaignImpressions:ArrayCollection;

        public var campaignRollups:ArrayCollection;
           
	}
}