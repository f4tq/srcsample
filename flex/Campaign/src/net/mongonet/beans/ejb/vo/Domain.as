package net.mongonet.beans.ejb.vo
{

    import mx.collections.ArrayCollection;
        
	[Bindable]
	[RemoteClass(alias="net.mongonet.beans.ejb.Domain")]
	public class Domain
	{

        public var domainId:Number;
        
        public var domain:String;
        
        public var domainroles:ArrayCollection;
        
        public var userses:ArrayCollection;
           
	}
}