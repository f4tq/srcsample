package net.mongonet.beans.ejb.vo
{

	[Bindable]
	[RemoteClass(alias="net.mongonet.beans.ejb.Raves")]
	public class Raves
	{

        public var raveSeqid:Number;
        
        public var suggestion:Suggestion;
        
        public var strength:int;
           
	}
}