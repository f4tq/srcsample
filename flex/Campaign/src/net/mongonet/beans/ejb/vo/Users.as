package net.mongonet.beans.ejb.vo
{

    import mx.collections.ArrayCollection;
        
	[Bindable]
	[RemoteClass(alias="net.mongonet.beans.ejb.Users")]
	public class Users
	{

        public var userSeqid:Number;
        
        public var emailAddresses:EmailAddresses;
        
        public var hintQuestions:HintQuestions;
        
        public var screenId:String;
        
        public var password:String;
        
        public var created:Date;
        
        public var verified:Date;
        
        public var hintAnswer:String;
        
        public var userkeyses:ArrayCollection;
        
        public var useroleses:ArrayCollection;
        
        public var active:Boolean;
        
        public var userName:String;
        
        public var userPix:ArrayCollection;
        
        public var accountUserses:ArrayCollection;
           
	}
}