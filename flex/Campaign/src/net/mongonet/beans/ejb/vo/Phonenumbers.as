package net.mongonet.beans.ejb.vo
{

	[Bindable]
	[RemoteClass(alias="net.mongonet.beans.ejb.Phonenumbers")]
	public class Phonenumbers
	{

        public var phoneSeqid:Number;
        
        public var phoneNumber:String;
           
	}
}