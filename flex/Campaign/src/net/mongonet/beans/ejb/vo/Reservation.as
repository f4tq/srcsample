package net.mongonet.beans.ejb.vo
{

    import mx.collections.ArrayCollection;
        
	[Bindable]
	[RemoteClass(alias="net.mongonet.beans.ejb.Reservation")]
	public class Reservation
	{

        public var rid:Number;
        
        public var emailAddresses:EmailAddresses;
        
        public var paymentInfo:PaymentInfo;
        
        public var publicRid:String;
        
        public var ridDate:Date;
        
        public var expireDate:Date;
        
        public var networkAddress:String;
        
        public var reservationEmailses:ArrayCollection;
        
        public var reservationUsages:ArrayCollection;
        
        public var reservationDatas:ArrayCollection;
        
        public var lead:String;
        
        public var parentRid:Number;
        
        public var webPublicRid:String;
           
	}
}