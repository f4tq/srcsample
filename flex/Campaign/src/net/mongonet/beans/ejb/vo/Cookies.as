package net.mongonet.beans.ejb.vo
{

	[Bindable]
	[RemoteClass(alias="net.mongonet.beans.ejb.Cookies")]
	public class Cookies
	{

        public var cookieId:Number;
        
        public var opaque:String;
        
        public var expiration:Date;
        
        public var paymentInfo:PaymentInfo;
        
        public var emailAddresses:EmailAddresses;
           
	}
}