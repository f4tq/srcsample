package net.mongonet.beans.ejb.vo
{

	[Bindable]
	[RemoteClass(alias="net.mongonet.beans.ejb.Stacker")]
	public class Stacker
	{

        public var id:StackerId;
        
        public var xidsByOrigXidRef:Xids;
        
        public var xidsByNewXidRef:Xids;
           
	}
}