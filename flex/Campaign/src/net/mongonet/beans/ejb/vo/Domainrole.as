package net.mongonet.beans.ejb.vo
{

	[Bindable]
	[RemoteClass(alias="net.mongonet.beans.ejb.Domainrole")]
	public class Domainrole
	{

        public var id:DomainroleId;
        
        public var domain:Domain;
           
	}
}