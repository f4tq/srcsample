package net.mongonet.beans.ejb.vo
{

    import mx.collections.ArrayCollection;
        
	[Bindable]
	[RemoteClass(alias="net.mongonet.beans.ejb.Charges")]
	public class Charges
	{

        public var chargeSeqid:Number;
        
        public var paymentInfo:PaymentInfo;
        
        public var reservationUsages:ArrayCollection;
        
        public var asCreditcardCharge:CreditcardCharge;
        
        public var asBsgCharge:BsgCharge;
           
	}
}