package net.mongonet.beans.ejb.vo
{

    import mx.collections.ArrayCollection;
        
	[Bindable]
	[RemoteClass(alias="net.mongonet.beans.ejb.Xids")]
	public class Xids
	{

        public var xidSeqid:Number;
        
        public var xid:String;
        
        public var xidDate:Date;
        
        public var smtplogs:ArrayCollection;
        
        public var histories:ArrayCollection;
        
        public var faxjobs:ArrayCollection;
        
        public var campaignUsages:ArrayCollection;
           
	}
}