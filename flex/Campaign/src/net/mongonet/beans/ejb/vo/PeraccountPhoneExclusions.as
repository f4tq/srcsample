package net.mongonet.beans.ejb.vo
{

	[Bindable]
	[RemoteClass(alias="net.mongonet.beans.ejb.PeraccountPhoneExclusions")]
	public class PeraccountPhoneExclusions
	{

        public var id:PeraccountPhoneExclusionsId;
        
        public var phonenumbers:Phonenumbers;
        
        public var account:Account;
        
        public var reason:String;
           
	}
}