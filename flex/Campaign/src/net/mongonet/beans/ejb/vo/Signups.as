package net.mongonet.beans.ejb.vo
{

	[Bindable]
	[RemoteClass(alias="net.mongonet.beans.ejb.Signups")]
	public class Signups
	{

        public var signupSeqid:Number;
        
        public var emailAddresses:EmailAddresses;
        
        public var hintQuestions:HintQuestions;
        
        public var userName:String;
        
        public var md5:String;
        
        public var domain:String;
        
        public var confirmSent:Date;
        
        public var confirmExpires:Date;
        
        public var hintAnswer:String;
        
        public var password:String;
           
	}
}