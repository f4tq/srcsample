package net.mongonet.beans.ejb.vo
{

    import mx.collections.ArrayCollection;
        
	[Bindable]
	[RemoteClass(alias="net.mongonet.beans.ejb.SmtpPiece")]
	public class SmtpPiece
	{

        public var smtpPieceSeqid:Number;
        
        public var smtplog:Smtplog;
        
        public var emailAddresses:EmailAddresses;
        
        public var subXid:String;
        
        public var fromRef:Number;
        
        public var smtplogDatas:ArrayCollection;
           
	}
}