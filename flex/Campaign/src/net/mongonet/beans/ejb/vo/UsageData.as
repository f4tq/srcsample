package net.mongonet.beans.ejb.vo
{

	[Bindable]
	[RemoteClass(alias="net.mongonet.beans.ejb.UsageData")]
	public class UsageData
	{

        public var id:Number;
        
        public var usageDate:Date;
        
        public var usageName:String;
        
        public var usageCount:Number;
        
        public var usageCountTotal:Number;
        
        public var usagePercent:Number;
           
	}
}