package net.mongonet.beans.ejb.vo
{

	[Bindable]
	[RemoteClass(alias="net.mongonet.beans.ejb.SuggestionThread")]
	public class SuggestionThread
	{

        public var threadSeqid:Number;
        
        public var emailAddresses:EmailAddresses;
        
        public var suggestion:Suggestion;
        
        public var responseTime:Date;
        
        public var subject:String;
        
        public var body:String;
           
	}
}