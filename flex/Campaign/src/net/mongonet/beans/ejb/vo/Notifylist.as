package net.mongonet.beans.ejb.vo
{

	[Bindable]
	[RemoteClass(alias="net.mongonet.beans.ejb.Notifylist")]
	public class Notifylist
	{

        public var notifySeqid:Number;
        
        public var emailAddresses:EmailAddresses;
        
        public var topic:Topic;
           
	}
}