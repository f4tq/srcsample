package net.mongonet.beans.ejb.vo
{

    import mx.collections.ArrayCollection;
        
	[Bindable]
	[RemoteClass(alias="net.mongonet.beans.ejb.Faxjob")]
	public class Faxjob
	{

        public var faxjobSeqid:Number;
        
        public var xids:Xids;
        
        public var noticeSent:Date;
        
        public var adopted:Date;
        
        public var renbrooked:Date;
        
        public var startTime:Date;
        
        public var endTime:Date;
        
        public var cvpPages:Number;
        
        public var cntPages:Number;
        
        public var curPage:Number;
        
        public var gotGlp:Boolean;
        
        public var acctSeqid:Number;
        
        public var stacker:Boolean;
        
        public var hasBounce:Boolean;
        
        public var pageResX:int;
        
        public var pageResY:int;
        
        public var neededDarkening:Boolean;
        
        public var neededResizing:Boolean;
        
        public var handwritten:Boolean;
        
        public var tranxCode:int;
        
        public var lastPageArrival:Date;
        
        public var ltr:Boolean;
        
        public var mailSentSuccessfully:Boolean;
        
        public var faxjobEmailaddresseses:ArrayCollection;
        
        public var faxjobMessages:ArrayCollection;
        
        public var reservationUsages:ArrayCollection;
        
        public var faxjobErrorses:ArrayCollection;
        
        public var dest:Phonenumbers;
        
        public var src:Phonenumbers;
           
	}
}