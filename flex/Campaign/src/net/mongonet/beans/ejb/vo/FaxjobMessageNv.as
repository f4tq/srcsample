package net.mongonet.beans.ejb.vo
{

    import mx.collections.ArrayCollection;
        
	[Bindable]
	[RemoteClass(alias="net.mongonet.beans.ejb.FaxjobMessageNv")]
	public class FaxjobMessageNv
	{

        public var faxjobMessageNvSeqid:Number;
        
        public var faxjobMessage:FaxjobMessage;
        
        public var nvSeqid:Number;
        
        public var nameId:Number;
        
        public var value:String;
        
        public var faxjobErrorses:ArrayCollection;
           
	}
}