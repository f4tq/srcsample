package net.mongonet.beans.ejb.vo
{

	[Bindable]
	[RemoteClass(alias="net.mongonet.beans.ejb.UsageDataName")]
	public class UsageDataName
	{

        public var id:Number;
        
        public var name:String;
           
	}
}