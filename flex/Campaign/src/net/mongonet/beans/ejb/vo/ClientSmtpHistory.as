package net.mongonet.beans.ejb.vo
{

	[Bindable]
	[RemoteClass(alias="net.mongonet.beans.ejb.ClientSmtpHistory")]
	public class ClientSmtpHistory
	{

        public var clientSmtpHistorySeqid:Number;
        
        public var clientSmtpserver:ClientSmtpserver;
        
        public var changed:Date;
        
        public var oldval:Boolean;
        
        public var newval:Boolean;
           
	}
}