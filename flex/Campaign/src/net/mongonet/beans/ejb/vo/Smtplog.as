package net.mongonet.beans.ejb.vo
{

    import mx.collections.ArrayCollection;
        
	[Bindable]
	[RemoteClass(alias="net.mongonet.beans.ejb.Smtplog")]
	public class Smtplog
	{

        public var smtpSeqid:Number;
        
        public var xids:Xids;
        
        public var created:Date;
        
        public var smtpPieces:ArrayCollection;
           
	}
}