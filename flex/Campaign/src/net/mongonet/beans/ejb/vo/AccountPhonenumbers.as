package net.mongonet.beans.ejb.vo
{

	[Bindable]
	[RemoteClass(alias="net.mongonet.beans.ejb.AccountPhonenumbers")]
	public class AccountPhonenumbers
	{

        public var id:AccountPhonenumbersId;
        
        public var phonenumbers:Phonenumbers;
        
        public var account:Account;
           
	}
}