package net.mongonet.beans.ejb.vo
{

	[Bindable]
	[RemoteClass(alias="net.mongonet.beans.ejb.Userkeys")]
	public class Userkeys
	{

        public var userRef:Number;
        
        public var users:Users;
        
        public var verified:Date;
        
        public var pubkey:String;
           
	}
}