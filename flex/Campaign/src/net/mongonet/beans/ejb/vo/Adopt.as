package net.mongonet.beans.ejb.vo
{

	[Bindable]
	[RemoteClass(alias="net.mongonet.beans.ejb.Adopt")]
	public class Adopt
	{

        public var adoptId:Number;
        
        public var xidRef:Number;
        
        public var adopted:Date;
        
        public var content:Date;
        
        public var adoptedBy:String;
        
        public var markContentBy:String;
           
	}
}