package net.mongonet.beans.ejb.vo
{

	[Bindable]
	[RemoteClass(alias="net.mongonet.beans.ejb.SmtplogData")]
	public class SmtplogData
	{

        public var smtplogDataSeqid:Number;
        
        public var clientSmtpserver:ClientSmtpserver;
        
        public var smtpPiece:SmtpPiece;
        
        public var fingerprint:String;
        
        public var prettyData:String;
        
        public var created:Date;
        
        public var encrypted:Boolean;
        
        public var success:Boolean;
        
        public var logTime:Date;
           
	}
}