package net.mongonet.beans.ejb.vo
{

	[Bindable]
	[RemoteClass(alias="net.mongonet.beans.ejb.FaxjobErrors")]
	public class FaxjobErrors
	{

        public var faxjobErrorsSeqid:Number;
        
        public var faxjobMessageNv:FaxjobMessageNv;
        
        public var faxjob:Faxjob;
           
	}
}