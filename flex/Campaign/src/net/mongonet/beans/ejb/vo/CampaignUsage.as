package net.mongonet.beans.ejb.vo
{

	[Bindable]
	[RemoteClass(alias="net.mongonet.beans.ejb.CampaignUsage")]
	public class CampaignUsage
	{

        public var cuid:Number;
        
        public var xids:Xids;
        
        public var campaign:Campaign;
        
        public var clicked:Date;
        
        public var ip:String;
        
        public var uid:Number;
        
        public var emailAddresses:EmailAddresses;
        
        public var emailType:String;
           
	}
}