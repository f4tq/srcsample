package net.mongonet.beans.ejb.vo
{

	[Bindable]
	[RemoteClass(alias="net.mongonet.beans.ejb.CreditcardChargeReversals")]
	public class CreditcardChargeReversals
	{

        public var ccRevSeqid:Number;
        
        public var creditcardCharge:CreditcardCharge;
        
        public var reversalDate:Date;
        
        public var referenceId:Number;
        
        public var transactionId:String;
        
        public var errorCode:String;
        
        public var avsResult:String;
        
        public var authResponseText:String;
        
        public var cvv2Result:String;
        
        public var reconciled:Date;
        
        public var reconcileAmt:Number;
        
        public var authCode:String;
        
        public var amt:Number;
        
        public var transactionType:String;
        
        public var reason:String;
           
	}
}