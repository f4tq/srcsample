package net.mongonet.beans.ejb.vo
{

	[Bindable]
	[RemoteClass(alias="net.mongonet.beans.ejb.Ecpversion")]
	public class Ecpversion
	{

        public var ecpversionSeqid:Number;
        
        public var version:Number;
        
        public var faxjob:Faxjob;
        
        public var notice:Date;
           
	}
}