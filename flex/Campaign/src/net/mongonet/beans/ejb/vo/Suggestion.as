package net.mongonet.beans.ejb.vo
{

    import mx.collections.ArrayCollection;
        
	[Bindable]
	[RemoteClass(alias="net.mongonet.beans.ejb.Suggestion")]
	public class Suggestion
	{

        public var suggestSeqid:Number;
        
        public var emailAddresses:EmailAddresses;
        
        public var topic:Topic;
        
        public var submittal:Date;
        
        public var thecomment:String;
        
        public var wasread:Boolean;
        
        public var importance:int;
        
        public var state:Number;
        
        public var name:String;
        
        public var phone:String;
        
        public var allowPublish:Boolean;
        
        public var suggestionThreads:ArrayCollection;
        
        public var raveses:ArrayCollection;
           
	}
}