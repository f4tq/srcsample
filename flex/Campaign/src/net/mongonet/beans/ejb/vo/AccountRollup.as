package net.mongonet.beans.ejb.vo
{

	[Bindable]
	[RemoteClass(alias="net.mongonet.beans.ejb.AccountRollup")]
	public class AccountRollup
	{

        public var accountRollupId:Number;
        
        public var account:Account;
        
        public var transDate:Date;
        
        public var totalClicks:Number;
        
        public var totalTx:Number;
        
        public var totalEmails:Number;
        
        public var totalUniqEmails:Number;
        
        public var totalCharges:Number;
        
        public var totalMinutes:Number;
        
        public var totalPages:Number;
        
        public var rollupType:String;
           
	}
}