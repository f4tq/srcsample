package net.mongonet.beans.ejb.vo
{

    import mx.collections.ArrayCollection;
        
	[Bindable]
	[RemoteClass(alias="net.mongonet.beans.ejb.ClientSmtpserver")]
	public class ClientSmtpserver
	{

        public var clientSmtpserverSeqid:Number;
        
        public var sentDomain:SentDomain;
        
        public var clientSmtpName:String;
        
        public var encrypted:Boolean;
        
        public var recorded:Date;
        
        public var clientSmtpHistories:ArrayCollection;
        
        public var smtplogDatas:ArrayCollection;
           
	}
}