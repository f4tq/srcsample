package net.mongonet.flamingo.flex.model
{
	import flash.events.EventDispatcher;
	
	import net.mongonet.beans.ejb.vo.Account;
	import net.mongonet.beans.ejb.vo.Campaign;
	import net.mongonet.beans.ejb.vo.Users;
	import net.mongonet.flamingo.flex.events.ViewEvent;

	[Bindable]
	public class ApplicationData extends EventDispatcher
	{
		private static var s_application : ApplicationData;
		public var curAccount : Account;
		public var curCampaign: Campaign;
		public var curUser : Users = new Users();
		public var isAdmin: Boolean;
		public var pageSize:int = 10;
		
		public static const LOGGED_IN_STATE:String = "logged_in";
		public static const LOGGED_OUT_STATE:String = "logged_out";

		public static const OVERALL_CHART_STATE:String="overallChartState";

		public static const ACCOUNT_LIST_STATE:String="accountListState";
		public static const ACCOUNT_DRILL_STATE:String="accountDrillState";
		public static const ACCOUNT_CHART_STATE:String="accountChartState";

		public static const CAMPAIGN_LIST_STATE:String="campaignListState";
		public static const CAMPAIGN_DRILL_STATE:String="campaignDrillState";
		public static const CAMPAIGN_CHART_STATE:String="campaignChartState";

		public static const XIDS_LIST_STATE:String="xidsListState";
		public static const XIDS_DRILL_STATE:String="xidsDrillState";
		public static const EMAIL_LIST_STATE:String="emailListState";
		public static const EMAIL_DRILL_STATE:String="emailDrillState";
		
		public var pageState:String=ACCOUNT_LIST_STATE;

		
		public function ApplicationData()
		{
			this.addEventListener(ViewEvent.LOGIN,observeLogin);
			this.addEventListener(ViewEvent.LOGOUT,observeLogout);

		}
		public static function instance() : ApplicationData
		{
			if (s_application == null)
			{
				s_application = new ApplicationData();
			}
			return s_application;
		}
		
		public  function observeLogin():void
		{
			trace("ApplicationData.observeLogin");	
		}
		public  function observeLogout():void
		{
			trace("ApplicationData.observeLogout");	
			
		}

	}
}