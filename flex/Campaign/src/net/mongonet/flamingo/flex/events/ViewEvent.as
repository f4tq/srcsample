package net.mongonet.flamingo.flex.events
{
	import flash.events.Event;

	public class ViewEvent extends Event
	{
		public static const REGISTRATION_START:String = "registrationStart";
		public static const REGISTER:String = "register";
		public static const REGISTER_CANCEL:String = "registerCancel";
		public static const AUTHENTICATE:String = "authenticate";
		
		public static const SEARCH_ACCOUNTS:String = "searchAccounts";
		public static const SEARCH_ACCOUNTS_NEXT_PAGE:String = "searchAccountsNextPage";
		public static const SEARCH_ACCOUNTS_PREV_PAGE:String = "searchAccountsPrevPage";
		public static const SELECT_ACCOUNT:String = "selectAccount";

		public static const SELECT_CAMPAIGN:String = "selectCampaign";
		public static const SELECT:String = "select";
		
		public static const SEARCH_CAMPAIGN_NEXT_PAGE:String = "searchCampaignNextPage";
		public static const SEARCH_CAMPAIGN_PREV_PAGE:String = "searchCampaignPrevPage";

		public static const GENERATE_DOMAIN_RPT:String = "domainReportPDF";
		public static const GENERATE_AREACODE_RPT:String = "areacodeReportPDF";

		
		public static const SEARCH_SHOW:String = "searchShow";
		public static const SETTINGS_SHOW:String = "settingsShow";
		public static const CHANGE_PASSWORD:String = "changePassword";
		public static const LOGOUT:String = "logout";
		public static const LOGIN:String = "login";
		public static const CANCEL:String = "cancel";
		
		public static const FLAMINGO_QUESTION:String = "flamingoQuestion";
		public static const LOGIN_QUESTION:String = "loginQuestion";
		public static const REGISTER_QUESTION:String = "registerQuestion";
		public static const SEARCH_QUESTION:String = "searchQuestion";
		public static const WIZARD_QUESTION:String = "wizardQuestion";
		public static const WORKSPACE_QUESTION:String = "workspaceQuestion";
		public static const CONVERSATION_QUESTION:String = "conversationQuestion";
		
		public var payLoad:Object;
		
		public function ViewEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
		
	}
}