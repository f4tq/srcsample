/*
 * Copyright (C) 2008 Exadel, Inc.
 *
 * The GNU Lesser General Public License, Version 3
 *
 */

package com.exadel.flamingo.flex.view.components
{
	import mx.controls.VRule;
	import flash.geom.Matrix;
	import flash.display.Graphics;
	import flash.display.GradientType;

	public class CustomVRule extends VRule
	{
		override protected function updateDisplayList(w:Number, h:Number):void {
			var g:Graphics = graphics;
			var color:Number = getStyle("strokeColor");
			var lineWidth:Number = getStyle("strokeWidth");
			g.clear();
			var matrix:Matrix = new Matrix();
			matrix.createGradientBox(w,100,Math.PI/2,0,0);
			g.lineStyle(lineWidth);
			g.lineGradientStyle(GradientType.LINEAR, [color, color], [0, 100], [0,255], matrix);
			g.moveTo(0,0);
			g.lineTo(0,h);
		}
	}
}