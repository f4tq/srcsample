//package net.mongonet.flamingo.flex.view.components
package screens.components
{
	import mx.validators.DateValidator;
	import mx.validators.ValidationResult;
//	import com.exadel.flamingo.flex.utils.DateUtils;

	public class MinDateValidator extends DateValidator
	{
		// Define Array for the return value of doValidation().
		private var results:Array;

		public var theMin:Date;

		// Constructor.
		public function MinDateValidator() {
			// Call base class constructor.
			super();
		}
		
		// Define the doValidation() method.
		override protected function doValidation(value:Object):Array {
			// Clear results Array.
			results = [];
			// Call base class doValidation().
			results = super.doValidation(value);
			// Return if there are errors.
			if (results.length > 0)
				return results;
			
			var dateToValidate:Date = new Date(value);
			trace('MinDateValidator: theMin = ' + theMin + ' -vs- the user value ' + dateToValidate);
			
			// If input value is not a future date

			if (dateToValidate < theMin) 
			{
				results.push(new ValidationResult(true, null, "NaN", "Your date cannot proceed " + theMin));
			}
			
			return results;
		}
	}
}