/*
 * Copyright (C) 2008 Exadel, Inc.
 *
 * The GNU Lesser General Public License, Version 3
 *
 */

package com.exadel.flamingo.flex.view.components
{
	import mx.validators.DateValidator;
	import mx.validators.ValidationResult;

	public class CheckOutDateValidator extends DateValidator
	{
		public var checkInDate:Date;
		
		// Define Array for the return value of doValidation().
		private var results:Array;
		
		public function CheckOutDateValidator()
		{
			super();
		}
		
		// Define the doValidation() method.
		override protected function doValidation(value:Object):Array {
			// Clear results Array.
			results = [];
			// Call base class doValidation().
			results = super.doValidation(value);
			// Return if there are errors.
			if (results.length > 0)
				return results;
			
			var dateToValidate:Date = new Date(value);
			
			// If input value is not a future date
			if (dateToValidate <= checkInDate)
			{
				results.push(new ValidationResult(true, null, "NaN", "Check out date must be later than check in date."));
				return results;
			}
			
			return results;
		}
	}
}