/*
 * Copyright (C) 2008 Exadel, Inc.
 *
 * The GNU Lesser General Public License, Version 3
 *
 */

package com.exadel.flamingo.flex.view.components
{
	import mx.validators.DateValidator;
	import mx.validators.ValidationResult;
	import com.exadel.flamingo.flex.utils.DateUtils;

	public class CheckinDateValidator extends DateValidator
	{
		// Define Array for the return value of doValidation().
		private var results:Array;
		
		// Constructor.
		public function CheckinDateValidator() {
			// Call base class constructor.
			super();
		}
		
		// Define the doValidation() method.
		override protected function doValidation(value:Object):Array {
			// Clear results Array.
			results = [];
			// Call base class doValidation().
			results = super.doValidation(value);
			// Return if there are errors.
			if (results.length > 0)
				return results;
			
			var dateToValidate:Date = new Date(value);
			var today:Date = new Date();
			DateUtils.clearTime(today);
			
			// If input value is not a future date
			if (dateToValidate < today)
			{
				results.push(new ValidationResult(true, null, "NaN", "Check in date must be a future date."));
				return results;
			}
			
			return results;
		}
	}
}