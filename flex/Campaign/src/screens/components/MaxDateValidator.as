//package net.mongonet.flamingo.flex.view.components
package screens.components
{
	import mx.validators.DateValidator;
	import mx.validators.ValidationResult;
//	import com.exadel.flamingo.flex.utils.DateUtils;

	public class MaxDateValidator extends DateValidator
	{
		// Define Array for the return value of doValidation().
		private var results:Array;

		public var theMax:Date;

		// Constructor.
		public function MaxDateValidator() {
			// Call base class constructor.
			super();
		}
		
		// Define the doValidation() method.
		override protected function doValidation(value:Object):Array {
			// Clear results Array.
			results = [];
			// Call base class doValidation().
			results = super.doValidation(value);
			// Return if there are errors.
			if (results.length > 0)
				return results;
			
			var dateToValidate:Date = new Date(value);
			trace('MaxDateValidator: theMax = ' + theMax + ' -vs- user value ' + dateToValidate);
			
			if (dateToValidate > theMax) 
			{
				results.push(new ValidationResult(true, null, "NaN", "Your date cannot exceed " + theMax));
			}
			
			return results;
		}
	}
}