#!/usr/bin/perl -w
use CGI; # qw/:standard :cgi :html4 gradient/;
use DBI;
use Data::Dumper;
use File::Basename;
use DateTime;
use JSON::XS;


$installDir = dirname($0);
$scriptName = basename($0);

chomp $installDir;
chdir $installDir;

$installDir="/var/lib/rpt";

my $cgih = new CGI;

#$cgih->gradient({speed=>fast,start=>red,end=>blue});

print $cgih->header ,"\n";

my $delimiter="\@\@";
my %eles;
my %sqls;
my %js;
my %dbjs;
my $verbose=0;
my $section;
my $inifile = "$installDir/$scriptName.ini";

my $sqlfile = "$installDir/mongo_state.sql";
my $jsfile = "$installDir/per-report.js";
my $dbjsfile = "$installDir/global.js";



$inifile || die "You must define an ini file\n";

#$self= sprintf("%s://%s%s%s", $cgih->https() ? "https" : "http", $cgih->virtual_host(), ($cgih->virtual_port() != 80 && $cgih->port() != 443 ? (":" . $cgih->port()) : ""), $cgih->script_name());
$self = $cgih->url();
#print "self=$self\n";
&read_ini($inifile, \%eles);

$sqlfile = $eles{'setup'}->{'sqlfile'} if (exists $eles{'setup'} and exists $eles{'setup'}->{'sqlfile'});
$dbjsfile = $eles{'setup'}->{'globalJS'} if (exists $eles{'setup'} and exists $eles{'setup'}->{'globalJS'});
$jsfile = $eles{'setup'}->{'perRptJS'} if (exists $eles{'setup'} and exists $eles{'setup'}->{'perRptJS'});


&read_sql($sqlfile, \%sqls);
&read_sql($jsfile, \%js);
&read_sql($dbjsfile, \%dbjs);

#print "$sql\n";
#$sqlname = $requestParams->{SQLNAME};
#defined $sqlname || die "Must define SQLNAME";

if (!$cgih->param())
  {
    $section = 'index';
    defined $section && exists $eles{$section} || die "Must define ini section ($section)";
    defined $section && exists $js{$section} || die "No 'index' jquery sections defined for this query";
    $requestParams = $eles{$section};
    $js_global = $js{$section};

    $templatebody = $requestParams->{TEMPLATEBODY};
    defined $templatebody || die "Must define TEMPLATEBODY";
    $templatebody = "$installDir/" . $templatebody if $requestParams->{TEMPLATEBODY} !~ m/\//;

    open (TMPL,"< $templatebody ") || die "Can't open TEMPLATEBODY \n";
    my $templateBodyContents;
    while (<TMPL>)
      {
	$templateBodyContents .= $_ ;
      }
    close TMPL;

    my $ii=1;      
    $txt .= '<thead><tr class="queryheader"><th class="header">Report Name</th></tr></thead>' . "\n";



    foreach $v (sort { (lc ($eles{$a}->{TITLE} ) ) cmp (lc ($eles{$b}->{TITLE})) }  keys %eles  ){
#      print "title: $eles{$v}->{TITLE}\n";
      next if $v =~ /default/ ;
      next if $v =~ /index/ ;
      next if $v =~ /drilldown/ ;
      next if $v =~ /setup/ ;
      my $rowType = $ii++ % 2 ? 'odd' : 'even';
      my $row = "<tr class=\"$rowType,querydata\">";
      $row .= "<td class=\"report\">" . (exists $eles{$v}->{TITLE} ? "$v/$eles{$v}->{TITLE}" : $v) . "</td>";
      $row .="</tr>\n";
      $txt .= $row;
    }
    foreach $v (keys %sqls){
      next if exists $eles{$v};
      my $rowType = $ii++ % 2 ? 'odd' : 'even';
      my $row = "<tr class=\"$rowType,querydata\">";
      $row .= "<td class=\"report_default\">$v</td>";
      $row .="</tr>\n";
      $txt .= $row;
    }

    $cgih->Vars->{script} = $self;


    $requestParams->{RESULT} = $txt;
    $requestParams->{SCRIPTS} = &replace($js_global, \%{$cgih->Vars}) ; #$script;

    
    print &fixup($templateBodyContents, $requestParams);    
  }
else {
  $section = $cgih->param('rpt');

  defined $section && exists $eles{$section} || die "Must define ini section ($section)";
  defined $section && exists $js{$section} || warn "No global jquery sections defined for this query: ($section)";

  if ($section !~ /default/){
    defined $section && exists $sqls{$section} || die "Must define sql section ($section)";
    $sql = $sqls{$section};
  }
  else {
    $cgih->param('sql') || do {
      print "<p class=error>You must reference a sql block</p>\n";
      exit(-1);
    };
    exists $sqls{$cgih->param('sql')} || do {
      print "<p class=error>You must reference a sql block previously defined</p>\n";
      exit(-1);
    };

    #print "<p>sql: " . $cgih->param('sql') . "</p>\n";
    $sql = $sqls{$cgih->param('sql')};


  }
  $requestParams = $eles{$section};
  $js_global = $js{$section} if exists $js{$section};
#  $js_global = 
  &handleReport();
}
exit(0);



sub handleReport()
  {
    my $txt='';
    my $other='';
    my $cerror='';
    $verbose= (exists $eles{'setup'} ? $eles{'setup'}->{'verbose'} : 0);
    $fields = $requestParams->{FIELDS} if exists $requestParams->{FIELDS};

    
    $templatebody = $requestParams->{TEMPLATEBODY};
    defined $templatebody || die "Must define TEMPLATEBODY";
    
    $templaterow = $requestParams->{TEMPLATEROW} if defined  $requestParams->{TEMPLATEROW};
    defined $templaterow || defined $fields || do {
      $cerror .= "<p class=warn>TEMPLATEROW not defined</p>";
    };

    #$sql ="";
    #open SQL,"< $sqlname" || die "Couldn't find SQLNAME";
    #while (<SQL>){
    #    $sql .= $_;
    #}
    #close SQL;
    
    $templatebody = "$installDir/" . $templatebody if $templatebody !~ m/\//;
    open (TMPL,"< $templatebody ") || die "Can't open TEMPLATEROW \n";
    my $templateBodyContents;
    while (<TMPL>)
      {
	$templateBodyContents .= $_ ;
      }
    close TMPL;
    
    defined $templaterow && do {
      $templaterow = "$installDir/" . $templaterow if $templaterow !~ m/\//;
      open (TMPL,"< $templaterow ") || die "Can't open TEMPLATEROW \n";
      my $templateRowContents;
      while (<TMPL>)
	{
	  $templateRowContents .= $_ ;
	}
      close TMPL;
    };

    $other .="<p class=verbose>Params:\n";
    foreach $v (keys %{$cgih->Vars}) {
      $other .= "'$v'= " . $cgih->Vars->{$v} . " \n";  
    }
    $other .= "</p>";


    $h = $cgih->Vars;
    $sql = replace($sql, $h);


    $host="sf1-pdb";
    $user="Collate_Client";
    $pass = "g0m0ng0";

    $database= "DBI:Pg:dbname=Mongo_State;host=$host";
    $dbh = DBI->connect($database, $user,$pass) or die DBI::errstr;
    $sth = $dbh->prepare($sql) 	|| do {
      $cerror .= "<p class=error>Couldn't prepare select statement " . $dbh->errstr . "</p>\n";
      print "$cerror\n";
      exit(-3);

    };

    $sth->execute() || do {
      	  $cerror .= "<p class=error><pre class=error>-->" . $sth->{'Statement'} . "\nFailed<---\n" . $dbh->errstr . "</pre></p>\n";
	  print "$cerror\n";
	  exit(-4);

    };
  

    my $script='';
    

    my %fieldMap = map{ lc($_) => $l } @{$sth->{NAME}};
    
    $other .= "<p class=verbose>Query FieldMap:\n";
    $other .=  "$_\n" foreach (keys %fieldMap);
    $other .=  "</p>\n";

    $other .= "<p class=sql>Executing: $sql</p>\n";     

    if (defined $fields){
      $other .=  "<p color=green>Checking Fields</p>\n";

      foreach $v (split(/ *, */,$fields)) {
	
	exists $fieldMap{lc($v)} || do {
	  $cerror .=  "<p class=error>No such query result: $v</p>\n";
	  
	}
      }
    }


    if (defined $fields){
      $other .= "<p>Filter Fields: $fields</p>\n";
      $txt .= "<thead><tr class=\"queryheader\">";
      foreach $v (split(/ *, */,$fields)) {
	$txt .= '<th class="header">' . $v . '</th>'; 
	$other .= "<p>Checking for field-level script: '$v'</p>\n" if $debug;
	$script .= $dbjs{$v} if exists $dbjs{$v};    
      }
      $txt .= '</tr></thead>';
    }
    else{
      $other .= "<p>Filter Fields: none</p>\n";
      $txt .= '<thead><tr class="queryheader">';
      foreach $v ( @{$sth->{NAME}}) {
	$txt .= '<th class="header">' . $v . '</th>';
	$script .= $dbjs{$v} if exists $dbjs{$v};
	
      }
      $txt .= '</tr></thead>';
    }

    my $ii=1;      
    while ($data = $sth->fetchrow_hashref()) {
      if (defined $fields){
	my $rowType = $ii++ % 2 ? 'odd' : 'even';
	my $row = "<tr class=\"$rowType,querydata\">";
	foreach $v (split(/ *, */,$fields)) {
	  $row .= "<td class=\"$v\">$data->{$v}</td>";
	}
	$row .="</row>";
	$txt .= $row;
      }
      elsif (! defined $templateRowContents ){
	my $row = "<tr class=\"querydata\">";
	my $rowType = $ii++ % 2 ? 'odd' : 'even';
	foreach $v (@{$sth->{NAME}}){
	  $row .= "<td class=\"$v\">$data->{$v}</td>";
	}
	$row .="</row>";
	$txt .= $row;
      }
      else{
	my $rez = &fixup($templateRowContents, $data);
	#	print $rez;
	$txt .= $rez;
      }

    }

    $other .="<p class=verbose>Environment:<ul>\n";
    foreach $v (keys %{ENV}) {
      $other .= "<li>'$v'= " . $ENV{$v} . "</li>";  
    }
    $other .= "</ul></p>";

    my $dt = DateTime->now->set_time_zone("America/Los_Angeles");
    my $now = $dt->ymd . " " . $dt->hms . " " . $dt->time_zone_short_name;

    $script .= ("// per-script section\n" . $js_global) if defined $js_global;
    # stuff the script name into the params
    $cgih->Vars->{script} = $self;

    $requestParams->{TIME} = $now;
    $requestParams->{TITLE} = &replace($requestParams->{TITLE}, \%{$cgih->Vars});
    $requestParams->{RESULT} = $txt;
    $requestParams->{SCRIPTS} = &replace($script, \%{$cgih->Vars}) ; #$script;
    $requestParams->{OTHER} = $other if  $verbose>0;
    $requestParams->{ERROR} = $cerror if $verbose>0;
    
#    map {$requestParams->{$_} => $cgih->Vars->{$_} } (keys %{$cgih->Vars});

    print &fixup($templateBodyContents, $requestParams);
    $sth->finish;
    $dbh->disconnect;
  }


sub fixup()
  {
    my ($tmplt, $acctvals)=@_;

    $tmplt =~ s/$delimiter([^$delimiter]+)$delimiter/exists $acctvals->{$1} ? $acctvals->{$1} : ($verbose>0 ? "** Couldn't find $1  **" :"")/ge;
    return $tmplt;
  }


sub replace()
  {
    my ($tmplt, $acctvals)=@_;
    
    $tmplt =~ s/\$([\w_\.]+)/exists $acctvals->{$1} ? $acctvals->{$1} : ($verbose>0 ? "** Couldn't find $1  **" : '$' . "$1")/ge;
    return $tmplt;
  }


sub read_ini()
  {

    my ($conf,$eles) = @_;
    if ( ! open(DATFILE,"< $conf")) 
      {
	    print STDERR "Couldn't open $conf\n";
	    &usage();
      }
    
    
    my $head;
    while (<DATFILE>)
      {
	# file is the form of a windows ini
	# file with each section headed by the message
	# triplet and the contents name=value pairs.
	# leading whitespace on both name and value are ignored
	
	next if /^$/;
	next if /^[\#]/;
	chomp;
	SWITCH : 
	    {
	      /[\s]*\[([^\]]+)\]/ && do {
		$head = $1;
		$eles->{$head}={};
		#	       print "found heading : $head\n";
		last SWITCH;
	      };
	      /\s*([^\s=]+)[\s]*=[\s]*(.*)/ && do 
		{
		  #	       print "found detail: $1 = $2\n";
		  
		  
		  my $name = $1;
		  my $val = $2;
		  $val =~ s/\"//g ;
		  last SWITCH if (!length($name) or !length($val)) ;
		  
		  $eles->{$head}{$name} = $val;
		  last SWITCH;
		};
	    } # SWITCH
      } # while 
    
    &dump($eles) if $verbose>0;
  }

sub read_sql()
  {

    my ($conf,$eles) = @_;
    if ( ! open(DATFILE,"< $conf")) 
      {
	    print STDERR "Couldn't open $conf\n";
	    &usage();
      }
    
    
    my $head;
    while (<DATFILE>)
      {
	# file is the form of a windows ini
	# file with each section headed by the message
	# triplet and the contents name=value pairs.
	# leading whitespace on both name and value are ignored
	
	next if /^$/;
	next if /^[\#]/;
	chomp;
	SWITCH : 
	    {
	      if (/^\[([^\]]+)\]/)  {
		$head = $1;
		$eles->{$head}="";
		#	       print "found heading : $head\n";
		last SWITCH;
	      }
	      else{

		  #	       print "found detail: $1 = $2\n";
		  $eles->{$head} .="$_\n";
		};
	    } # SWITCH
      } # while 
    
    &dump($eles) if $verbose>0;
  }

sub usage()
{
    print <<EOF
<p color="red">Error:
EOF
;
    scalar(@_) > 0  && do { print foreach @_; } ;

print <<EOF
</p>
EOF
;
}

      #    Dumper($data);
      #      foreach my $key  (keys %{$data})
      #	{
      #	print "key=$key value=$data->{$key}";
      #	  printf ("%20.s|",$key) if defined $data[$ii];
#	}
#      print "\n";

    #for $field (0 .. $sth->{NUM_OF_FIELDS}){
    #  print "Field: $field\n";
    #verify user spec'd fields are in query result

# make table header
