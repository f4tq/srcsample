[index]
// maps 
$(".report").wrapInner(function() {
                           var v=$(this).text().split("/");
                           if (v.length > 1)
                               $(this).text(v[1]);
                           return "<a href='$script?rpt=" + v[0] + "'/>";
});
$(".report_default").wrapInner(function() {
                         return "<a href='$script?rpt=default&sql=" + $(this).text() + 	"'/>";
});

[broken users - today]
/* global broken users */
$(".src_phone").wrapInner(function() {
                         return "<a href='https://trans-ng.mongonet.net/display_trans_detail_new.php?interval=1day&src_num=" + $(this).text() + 	"'/>";
});

[broken users - 365 days ago]
$(".src_phone").wrapInner(function() {
                         return "<a href='$script?rpt=broken users - drilldown&start=365 days&end=364 days&src_phone=" + $(this).text() + 	"'/>";
});

[broken users - drilldown]
$(".src_phone").wrapInner(function() {
                         return "<a href='$script?rpt=broken users - drilldown&start=365 days&end=364 days&src_phone=" + $(this).text() + 	"'/>";
});

[today transactions - hourly top]
// find & light up transactions 

$("td.hour").wrapInner(function() {

//                         var period= $(this).closest("tr");
//                         alert("period = " + period);
//                        if (period) $(this).(".hour").html().text();
                         
      return "<a href='$script?rpt=today transactions - hourly list drilldown&order=xid desc&start=" + $(this).text() + "'/>";
});


[today transactions - hourly list drilldown]

$(".dst_phone").wrapInner(function() {
                              var ss = $(this).closest("tr").find(".xid_epoch").text();
                              var cvt = new Date( new Number(ss) * 1000);
                              var start_new = new Date(cvt.getTime() - 3600*1000);
                              var end_new = new Date( cvt.getTime() + 3600*1000);
                              var ur = "<a href='$script?rpt=destination - list drilldown&dst_phone=" 
                                  + $(this).text() 
                                  + "&start=" 
                                  + start_new.getTime()/1000
                                  + "&end="
                                  + end_new.getTime()/1000
                                  +  "'/>";
                              return ur;
});

$(".src_phone").wrapInner(function() {
                              var ss = $(this).closest("tr").find(".xid_epoch").text();

                              var start= new Date(new Number(ss) * 1000);
                              
                              start_new = new Date(start.getTime() - 3600*1000);
                              end_new = new Date(start.getTime() + 3600*1000);
                              return "<a href='$script?rpt=source - list drilldown&src_phone=" 
                                  + $(this).text() 
                                  + "&start=" 
                                  + start_new.getTime()/1000
                                  + "&end="
                                  + end_new.getTime()/1000
                                  +  "'/>";
});

//$(".src_phone").wrapInner(function() {
//                         return "<a href='$script?rpt=source - list drilldown&src_phone=" + $(this).text() + 	"&start=$start'/>";
//});

//$(".dst_phone").wrapInner(function() {
//                         return "<a href='$script?rpt=destination - list drilldown&dst_phone=" + $(this).text() + 	"&start=$start'/>";
//});


$("td.sent").each(function() {
                                        if ( $(this).text()==="0" )
                                            $(this).closest("tr").each("td").addClass("bad");
                                        else 
                                            $(this).closest("tr").each("td").addClass("good");
                                    });

[today stacker - list]

$(".orig_xid").wrapInner(function() {
                         return "<a href='https://trans-ng.mongonet.net/fax_detail.php?xid=" + $(this).text() + 	"'/>";
});

$(".other_xid").wrapInner(function() {
                         return "<a href='https://trans-ng.mongonet.net/fax_detail.php?xid=" + $(this).text() + 	"'/>";
});
$(".orig_xid_epoch").text(function(index,old){
                              var cvt = new Date( new Number($(this).text() ) * 1000);
                              return cvt.toString("yyyy-MM-dd h:MM:ss GMT");
                          });
$(".other_xid_epoch").text(function(index,old){
                              var cvt = new Date( new Number($(this).text() ) * 1000);
                              return cvt.toString("yyyy-MM-dd h:MM:ss GMT");
                          });



[today - summary performance]

$(".dropped").wrapInner(function() {
                            if ($(this).text() != "0")
                                return "<a href='$script?rpt=today dropped calls - drilldown&faxhost=" + $(this).closest("tr").find(".faxhost").text() + 	"'/>";
                            else
                                return $(this).text();
});

$(".nopages").wrapInner(function() {
                            if ($(this).text() != "0")
                                return "<a href='$script?rpt=today nopage calls - drilldown&faxhost=" + $(this).closest("tr").find(".faxhost").text() + 	"'/>";
                            else
                                return $(this).text();
});




[today dropped calls - drilldown]

$(".dst_phone").wrapInner(function() {
                              var ss = $(this).closest("tr").find(".xid_epoch").text();
                              var cvt = new Date( new Number(ss) * 1000);
                              var start_new = new Date(cvt.getTime() - 3600*1000);
                              var end_new = new Date( cvt.getTime() + 3600*1000);
                              var ur = "<a href='$script?rpt=destination - list drilldown&dst_phone=" 
                                  + $(this).text() 
                                  + "&start=" 
                                  + start_new.getTime()/1000
                                  + "&end="
                                  + end_new.getTime()/1000
                                  +  "'/>";
                              return ur;
});

$(".src_phone").wrapInner(function() {
                              var ss = $(this).closest("tr").find(".xid_epoch").text();

                              var start= new Date(new Number(ss) * 1000);
                              
                              start_new = new Date(start.getTime() - 3600*1000);
                              end_new = new Date(start.getTime() + 3600*1000);
                              return "<a href='$script?rpt=source - list drilldown&src_phone=" 
                                  + $(this).text() 
                                  + "&start=" 
                                  + start_new.getTime()/1000
                                  + "&end="
                                  + end_new.getTime()/1000
                                  +  "'/>";
});

$(".xid_epoch").text(function(index,old){
                              var cvt = new Date( new Number($(this).text() ) * 1000);
                              return cvt.toString("yyyy-MM-dd h:MM:ss GMT");
                          });


[oneyear - summary performance]
$(".dropped").wrapInner(function() {
                            return "<a href='$script?rpt=today dropped calls - drilldown&faxhost=" + $(this).closest("tr").find(".faxhost").text() + 	"'/>";
});

[campaign list]
$(".cid").wrapInner(function() {
                         return "<a href='$script?rpt=campaign drilldown&company="+  $(this).closest("tr").find(".company").text()  +"&cid=" + $(this).text() +         "'/>";
});



[test]

$(".dst_phone").wrapInner(function() {
                              var ss = $(this).closest("tr").find(".xid_epoch").text();
                              var cvt = new Date( new Number(ss) * 1000);
                              var start_new = new Date(cvt.getTime() - 3600*1000);
                              var end_new = new Date( cvt.getTime() + 3600*1000);
                              var ur = "<a href='$script?rpt=destination - list drilldown&dst_phone=" 
                                  + $(this).text() 
                                  + "&start=" 
                                  + start_new.getTime()/1000
                                  + "&end="
                                  + end_new.getTime()/1000
                                  +  "'/>";
                              return ur;
});

$(".src_phone").wrapInner(function() {
                              var ss = $(this).closest("tr").find(".xid_epoch").text();

                              var start= new Date(new Number(ss) * 1000);
                              
                              start_new = new Date(start.getTime() - 3600*1000);
                              end_new = new Date(start.getTime() + 3600*1000);
                              return "<a href='$script?rpt=source - list drilldown&src_phone=" 
                                  + $(this).text() 
                                  + "&start=" 
                                  + start_new.getTime()/1000
                                  + "&end="
                                  + end_new.getTime()/1000
                                  +  "'/>";
});

$(".xid_epoch").text(function(index,old){
                              var cvt = new Date( new Number($(this).text() ) * 1000);
                              return cvt.toString("yyyy-MM-dd h:MM:ss GMT");
                          });


[test-save]

$(".dst_phone").wrapInner(function() {
                              var xid_str = $(this).closest("tr").find(".xid_epoch").text();
                              var pieces = xid_str.split(" ");
                              var x1 = pieces[0].split("-");
                              var x2 = pieces[1].split(":");
                              var start= new Date(x1[0],x1[1]-1,x1[2],x2[0],x2[1],x2[2]);
                              var ss = start.toDateString() + " " + start.toTimeString() + " " + pieces[2] + " " + pieces[3];
                              var cvt = new Date( ss);

                              start_new = new Date(cvt.getTime() - 3600*1000);
                              end_new = new Date(svt.getTime() + 3600*1000);
                              return "<a href='$script?rpt=destination - list drilldown&dst_phone=" 
                                  + $(this).text() 
                                  + "&start=" 
                                  + start_new.format("yyyy-MM-dd h:MM:ss TT Z")  
                                  + "&end="
                                  + end_new.format("yyyy-MM-dd h:MM:ss TT Z")  
                                  +  "/>";
});

$(".src_phone").wrapInner(function() {
                              var xid_str = $(this).closest("tr").find(".xid_date_str").text();
                              var pieces = xid_str.split(" ");
                              var x1 = pieces[0].split("-");
                              var x2 = pieces[1].split(":");
                              var start= new Date(x1[0],x1[1]-1,x1[2],x2[0],x2[1],x2[2]);
                              
                              start_new = new Date(start.getTime() - 3600*1000);
                              end_new = new Date(start.getTime() + 3600*1000);
                              return "<a href='$script?rpt=source - list drilldown&src_phone=" 
                                  + $(this).text() 
                                  + "&start=" 
                                  + start_new.format("yyyy-MM-dd h:MM:ss TT Z")  
                                  + "&end="
                                  + end_new.format("yyyy-MM-dd h:MM:ss TT Z")  
                                  +  "/>";
});

