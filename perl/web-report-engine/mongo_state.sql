[broken users - today]
select * from  (
select  ( case when Success > 0 then ((Success )/(Success::float+Fail) *100.0) else 0.0 end) as SuccessRate
	, ( case when Success > 0 then (Renbrooked/Success::float * 100.0) else 0.0 end) as RenbrookRate 
	, ( case when Success > 0 then (Adopted/Success::float * 100.0 )else 0.0 end) as AdoptionRate
	, phone_number as src_phone
	,*
from (
select src_phone_ref
,	sum(case when mail_sent_successfully = true then 1 else 0 end) as Success
,	sum(case when mail_sent_successfully = false then 1 else 0 end) as Fail
,	sum(case when adopted is not null then 1 else 0 end) as Adopted
,	sum(case when renbrooked is not null then 1 else 0 end) as Renbrooked
,	avg (cur_page) as Pages
,       sum (extract( epoch from (end_time - start_time) / 60) )as RawMinutes
, count(*) as Transactions
 from 
	faxjob join xids on xid_ref=xid_seqid
 where xid_date > current_date
	group by src_phone_ref
	having sum(case when mail_sent_successfully = false then 1 else 0 end) > 0
	
) as B join phonenumbers on src_phone_ref=phone_seqid 
order by successrate,transactions desc
) as C where  successrate < 50

[broken users - 365 days ago]
select * from (
select  ( case when Success > 0 then ((Success )/(Success::float+Fail) *100.0) else 0.0 end) as SuccessRate
	, ( case when Success > 0 then (Renbrooked/Success::float * 100.0) else 0.0 end) as RenbrookRate 
	, ( case when Success > 0 then (Adopted/Success::float * 100.0 )else 0.0 end) as AdoptionRate
	, phone_number as src_phone
	,*
from (
select src_phone_ref
,	sum(case when mail_sent_successfully = true then 1 else 0 end) as Success
,	sum(case when mail_sent_successfully = false then 1 else 0 end) as Fail
,	sum(case when adopted is not null then 1 else 0 end) as Adopted
,	sum(case when renbrooked is not null then 1 else 0 end) as Renbrooked
,	avg (cur_page) as Pages
,       sum (extract( epoch from (end_time - start_time) / 60) )as RawMinutes
, count(*) as Transactions
 from 
	faxjob join xids on xid_ref=xid_seqid
 where xid_date > (current_date - '365 days'::interval)
	and xid_date < (current_date - '364 days'::interval)
	group by src_phone_ref
	having sum(case when mail_sent_successfully = false then 1 else 0 end) > 0
	
) as B join phonenumbers on src_phone_ref=phone_seqid 
order by successrate,transactions desc
) as C where  successrate < 50

[broken users - drilldown]
select * from display_trans_view where
	xid_date > current_date - '$start'::interval
	and xid_date < current_date - '$end'::interval
	and src_phone_ref=(select phone_seqid from phonenumbers where phone_number='$src_phone' limit 1)



[today - hourly performance]
select faxhost
,calltype
,hour
,extract(epoch from hour) as hour_epoch
, ((good-dropped)/(good::float+bad))*100.0 as SuccessRate
,Total
,Good
,Bad
,ContentOnly
,Dropped
,NoPages
,fixed
,adopted
,adoptedbutnomail
,renbrooked 
, (goodminutes + badminutes) as rawminutes 
from (

select networkip2host(faxhost) as faxhost
, phonetype as CallType
, date_trunc('hour',dayhour) as hour
, sum(total) as Total
, sum(good) as good
, sum(bad) as bad
, sum(fixed) as fixed
, sum(adopted) as adopted
, sum(adoptedbutnomail) as adoptedbutnomail
, sum(renbrooked) as renbrooked
, sum(nopages) as nopages
, sum(goodminutes) as goodminutes
, sum(badminutes) as badminutes
, sum(contentonly) as contentonly
, sum(dropped) as dropped
 from fax_perf_history 
where dayhour > date_trunc('day',now())
group by faxhost,calltype,hour

) as B
order by hour desc, total desc,SuccessRate desc

[today - summary performance]
select faxhost
,calltype
, ((good-dropped)/(good::float+bad))*100.0 as SuccessRate
,Total
,Good
,Bad
,ContentOnly
, (ContentOnly/(good::float+bad))*100.0 as "CntOnly%"
,Dropped
, (Dropped/(good::float+bad))*100.0 as "Drop%"
,NoPages
, (NoPages/(good::float+bad))*100.0 as "NoPages%"
,fixed
,adopted
,adoptedbutnomail
,renbrooked 
, (goodminutes + badminutes) as rawminutes 
from (

select networkip2host(faxhost) as faxhost
, phonetype as CallType
, sum(total) as Total
, sum(good) as good
, sum(bad) as bad
, sum(fixed) as fixed
, sum(adopted) as adopted
, sum(adoptedbutnomail) as adoptedbutnomail
, sum(renbrooked) as renbrooked
, sum(nopages) as nopages
, sum(goodminutes) as goodminutes
, sum(badminutes) as badminutes
, sum(contentonly) as contentonly
, sum(dropped) as dropped
 from fax_perf_history 
where dayhour > date_trunc('day',now())
group by faxhost,calltype
) as B
order by total desc,SuccessRate desc


[lasthour - performance]
select faxhost
,calltype
, ((good-dropped)/(good::float+bad))*100.0 as SuccessRate
,Total
,Good
,Bad
,ContentOnly
,Dropped
,NoPages
,fixed
,adopted
,adoptedbutnomail
,renbrooked 
,(goodminutes + badminutes) as rawminutes
from (

select networkip2host(faxhost) as faxhost
, phonetype as CallType
, sum(total) as Total
, sum(good) as good
, sum(bad) as bad
, sum(fixed) as fixed
, sum(adopted) as adopted
, sum(adoptedbutnomail) as adoptedbutnomail
, sum(renbrooked) as renbrooked
, sum(nopages) as nopages
, sum(goodminutes) as goodminutes
, sum(badminutes) as badminutes
, sum(contentonly) as contentonly
, sum(dropped) as dropped
 from fax_perf_history 
where dayhour = (select max(dayhour) from fax_perf_history)
group by faxhost,calltype
) as B
order by total desc,SuccessRate desc


[last2weeks - broad stats]
select day,faxhost
,calltype
, ((good-dropped)/(good::float+bad))*100.0 as SuccessRate
,Total
,Good
,Bad
,ContentOnly
, (ContentOnly/(good::float+bad))*100.0 as "CntOnly%"
,Dropped
, (Dropped/(good::float+bad))*100.0 as "Drop%"
,NoPages
, (NoPages/(good::float+bad))*100.0 as "NoPages%"
,fixed
,adopted
,adoptedbutnomail
,renbrooked 
,(goodminutes + badminutes) as rawminutes
from (
select networkip2host(faxhost) as faxhost
, phonetype as CallType
, date_trunc('day',dayhour) as day
, sum(total) as Total
, sum(good) as good
, sum(bad) as bad
, sum(fixed) as fixed
, sum(adopted) as adopted
, sum(adoptedbutnomail) as adoptedbutnomail
, sum(renbrooked) as renbrooked
, sum(nopages) as nopages
, sum(goodminutes) as goodminutes
, sum(badminutes) as badminutes
, sum(contentonly) as contentonly
, sum(dropped) as dropped
 from fax_perf_history 
where dayhour > (date_trunc('day',now()) - '2 weeks'::interval)
group by faxhost,calltype,day
) as B
order by day desc,total desc,SuccessRate desc




[destination - list drilldown]
select 
	SRC.phone_number as src_phone  
	,DST.phone_number as dst_phone
	,xid
	,extract (epoch from xid_date) as xid_epoch
	,to_char( xid_date,'YYYY-MM-DD HH:MI:SS AM TZ') as xid_date
	,faxjob.*
from xids join faxjob on xid_seqid=xid_ref 
	join phonenumbers DST on dest_phone_ref=DST.phone_seqid
	join phonenumbers SRC on src_phone_ref=SRC.phone_seqid
	where
xid_date >= to_timestamp($start)
and xid_date <= (to_timestamp($end) )
and dest_phone_ref=(select phone_seqid from phonenumbers where phone_number='$dst_phone' limit 1)

[source - list drilldown]
select 	DST.phone_number as dst_phone
	,SRC.phone_number as src_phone  
	,xid
	,extract (epoch from xid_date) as xid_epoch
	,xid_date
	,faxjob.*
from xids join faxjob on xid_seqid=xid_ref 
	join phonenumbers DST on dest_phone_ref=DST.phone_seqid
	join phonenumbers SRC on src_phone_ref=SRC.phone_seqid
	where
xid_date <= to_timestamp($end)
and xid_date >= to_timestamp($start)
and src_phone_ref=(select phone_seqid from phonenumbers where phone_number='$src_phone' limit 1)

[today transactions - hourly top]
select date_trunc('hour',xid_date ) as Hour,count(*) as Transactions
	from xids join faxjob on xid_seqid=xid_ref 
	where xid_date > current_date
	group by date_trunc('hour',xid_date )
	order by Hour desc, Transactions desc

[today transactions - hourly list drilldown]
select 	 xid
	, to_char(xid_date ,'YYYY-MM-DD HH:MI:SS AM TZ') as xid_date
	, extract (epoch from xid_date) as xid_epoch
	, networkip2host(substring(xid,21,10)) as faxhost
	,DST.phone_number as dst_phone
	, SRC.phone_number as src_phone
	,mail_sent_successfully as sent
	,got_glp as glp
	,acct_seqid
	,cur_page
	,(case when adopted is not null and mail_sent_successfully=true then true else false end) as Adopted
	,(case when adopted is not null 
		and mail_sent_successfully=false 
		and exists (select * from adopt D where D.xid_ref=xid_ref and D.content is not null)
		then 1
		else 0 
		end) as ContentOnly

	, ( case when mail_sent_successfully=false 
		and (cvp_pages + cnt_pages) > 0 
		and exists(select * from faxjob FF join xids XX on XX.xid_seqid=FF.xid_ref where ff.src_phone_ref=fj.src_phone_ref and ff.dest_phone_ref=fj.dest_phone_ref and FF.faxjob_seqid < fj.faxjob_seqid and  ff.last_page_arrival is not null and (xx.xid_date - ff.last_page_arrival) < '10 minutes'::interval)
		then 1 else 0 end
	) as Drop

	from xids join faxjob fj 	on xid_seqid=xid_ref
	join phonenumbers DST on dest_phone_ref=DST.phone_seqid
	join phonenumbers SRC on src_phone_ref=SRC.phone_seqid
	where xid_date > '$start'::timestamp and xid_date < ( '$start'::timestamp + '1 hours'::interval)


[today stacker - list]
select case when orig_pages <> orig_real or other_pages <> other_real then true else false end as TransSync , *
from (
select ORIGX.xid as orig_xid
, extract (epoch from origx.xid_date) as orig_xid_epoch
, orig.cur_page as orig_pages
, (select count(*) from faxjob_message where faxjob_ref=orig.faxjob_seqid and triplet_ref=10) as orig_real
, orig.mail_sent_successfully as orig_sent
, orig.got_glp as orig_glp
, orig.renbrooked as orig_renbrooked
, orig.adopted as orig_adopted
, orig.last_page_arrival as orig_last_page_arrival
, orig.start_time as orig_start_time
, orig.end_time as orig_start_time
, NEWX.xid as other_xid
, extract (epoch from newx.xid_date) as other_xid_epoch
, (newf.cvp_pages + newf.cnt_pages) as other_pages
, (select count(*) from faxjob_message where faxjob_ref=NEWF.faxjob_seqid and triplet_ref=10) as other_real
, newf.mail_sent_successfully as new_sent
, newf.got_glp as new_glp
, newf.renbrooked as new_renbrooked
, newf.adopted as new_adopted
, newf.last_page_arrival as new_last_page_arrival
, newf.start_time as new_start_time
, newf.end_time as new_start_time

from stacker join faxjob ORIG on ORIG.xid_ref = orig_xid_ref 
			join xids ORIGX on ORIG.xid_ref=ORIGX.xid_seqid
			join faxjob NEWF on NEWF.xid_ref = new_xid_ref 
			join xids NEWX on NEWF.xid_ref=NEWX.xid_seqid
where ORIG.start_time > current_date

) as X


[today dropped calls - drilldown]

select 	xid
	, extract (epoch from xid_date) as xid_epoch
, to_char( xid_date,'YYYY-MM-DD HH:MI:SS AM TZ') as xid_date_str
, networkip2host(substring(xid,21,10)) as faxhost 
, dst.phone_number as dst_phone
, src.phone_number as src_phone
,(case when dst.phone_number ~ '1415' then '415' else '800' end ) as PhoneType
,(case when mail_sent_successfully=true then 1 else 0 end) as Good
,(case when mail_sent_successfully = false then 1 else 0 end) as Bad
,(case when tranx_code <> 0 and mail_sent_successfully = true then 1 else 0 end) as Fixed
,(case when adopted is not null then 1 else 0 end) as Adopted
,(case when adopted is not null and mail_sent_successfully=false then 1 else 0 end) as AdoptedButNoMail
,(case when renbrooked is not null then 1 else 0 end) as Renbrooked
,(case when renbrooked is not null and mail_sent_successfully=false then 1 else 0 end) as RenbrookedButNoMail

from xids OX join faxjob fj on ox.xid_seqid=fj.xid_ref 
	join phonenumbers dst on dest_phone_ref=dst.phone_seqid
	join phonenumbers src on src_phone_ref=src.phone_seqid
where 
	xid_date < ((select max(dayhour) from fax_perf_history) + '1 hour'::interval)
	and xid_date < date_trunc('hour',now())
	and xid_date > date_trunc('day',now())
	and start_time is not null
	and mail_sent_successfully=false
	and networkip2host(substring(xid,21,10)) = '$faxhost'

	and ( (cnt_pages + cvp_pages) > 0 
		and exists (select * from faxjob FF join xids XX on XX.xid_seqid=FF.xid_ref where ff.src_phone_ref=fj.src_phone_ref and ff.dest_phone_ref=fj.dest_phone_ref and FF.faxjob_seqid < fj.faxjob_seqid and  ff.last_page_arrival is not null and (xx.xid_date - ff.last_page_arrival) < '10 minutes'::interval)
	)




[today nopage calls - drilldown]

select 	xid
, to_char( xid_date,'YYYY-MM-DD HH:MI:SS AM TZ') as xid_date

, networkip2host(substring(xid,21,10)) as faxhost 
, dst.phone_number as dst_phone
, src.phone_number as src_phone
,(case when dst.phone_number ~ '1415' then '415' else '800' end ) as PhoneType
,(case when mail_sent_successfully=true then 1 else 0 end) as Good
,(case when mail_sent_successfully = false then 1 else 0 end) as Bad
,(case when tranx_code <> 0 and mail_sent_successfully = true then 1 else 0 end) as Fixed
,(case when adopted is not null then 1 else 0 end) as Adopted
,(case when adopted is not null and mail_sent_successfully=false then 1 else 0 end) as AdoptedButNoMail
,(case when renbrooked is not null then 1 else 0 end) as Renbrooked
,(case when renbrooked is not null and mail_sent_successfully=false then 1 else 0 end) as RenbrookedButNoMail

from xids OX join faxjob fj on ox.xid_seqid=fj.xid_ref 
	join phonenumbers dst on dest_phone_ref=dst.phone_seqid
	join phonenumbers src on src_phone_ref=src.phone_seqid
where 
	xid_date < ((select max(dayhour) from fax_perf_history) + '1 hour'::interval)
	and xid_date < date_trunc('hour',now())
	and xid_date > date_trunc('day',now())
	and start_time is not null
	and mail_sent_successfully=false
	and networkip2host(substring(xid,21,10)) = '$faxhost'
	and (cur_page = 0 or (cnt_pages + cvp_pages)=0)





[oneyear - summary performance]
select to_char( dayhour,'YYYY-MM-DD HH:MI:SS AM TZ') as dayhour
,faxhost
,calltype
, ((good-dropped)/(good::float+bad))*100.0 as SuccessRate
,Total
,Good
,Bad
,ContentOnly
, (ContentOnly/(good::float+bad))*100.0 as "CntOnly%"
,Dropped
, (Dropped/(good::float+bad))*100.0 as "Drop%"
,NoPages 
, (NoPages/(good::float+bad))*100.0 as "NoPages%"
,fixed
,adopted
,adoptedbutnomail
,renbrooked
, (goodminutes + badminutes) as rawminutes 
from (

select 	
date_trunc('day',dayhour) as dayhour 
, networkip2host(faxhost) as faxhost
, phonetype as CallType
, sum(total) as Total
, sum(good) as good
, sum(bad) as bad
, sum(fixed) as fixed
, sum(adopted) as adopted
, sum(adoptedbutnomail) as adoptedbutnomail
, sum(renbrooked) as renbrooked
, sum(nopages) as nopages
, sum(goodminutes) as goodminutes
, sum(badminutes) as badminutes
,sum(contentonly) as contentonly
, sum(dropped) as dropped
 from fax_perf_history where 
	dayhour > (date_trunc('day',now()) - '1 year'::interval)
	and 
	dayhour < (date_trunc('day',now()) - '351 days'::interval)
group by date_trunc('day',dayhour),faxhost,calltype
) as B
order by dayhour ,total desc, successrate desc

[today campaign]
select a.company_name, b.link, e.c1 as CampaignId ,e.clicks,e.impression from
((select cid_ref as c1, count(*) as impression from campaign_impression  where requested > 'today' group by cid_ref) as d left join
 (select cid_ref as c2, count(*) as clicks from campaign_usage where clicked > 'today' group by cid_ref ) as c  on d.c1=c.c2 ) as e
,account as a
,campaign as b
where e.c1=b.cid and b.acct_id=a.acct_seqid order by c1 desc;


[test]
select xid
	, xid_date
	, to_char( xid_date,'YYYY-MM-DD HH:MI:SS AM TZ') as xid_date_str
	, extract (epoch from xid_date) as xid_epoch
	, dst.phone_number as dst_phone
	, src.phone_number as src_phone

 from xids join faxjob on xid_ref=xid_seqid 
	join phonenumbers dst on dest_phone_ref=dst.phone_seqid
	join phonenumbers src on src_phone_ref=src.phone_seqid
limit 2

[campaign list]
select a.cid, a.acct_id,company_name as company,a.link,a.start_time  
	from campaign a join 
	(select acct_id,max(cid) as cid from campaign group by acct_id order by cid desc) as b
        on a.cid=b.cid
	join account on a.acct_id=acct_seqid 
	order by a.cid desc

[campaign drilldown]
(select e.c1 as cid_ref, 'today' as trans_date, e.total_clicks,e.total_impressions 
	from 
	(
		(select cid_ref as c1, count(*) as total_impressions from campaign_impression  where cid_ref=$cid and requested > 'today' group by cid_ref) as d 
		left join
 		(select cid_ref as c2, count(*) as total_clicks from campaign_usage where cid_ref=$cid and clicked > 'today' group by cid_ref ) as c  
		on d.c1=c.c2 
	) as e
)
union all
(

select cid_ref,trans_date,sum(total_clicks) as total_clicks, sum(total_emails) as total_impressions 
from areacode_rollup 
        where
	 cid_ref = $cid
	and rollup_type='d' 
        group by cid_ref, trans_date
        order by trans_date desc
)





