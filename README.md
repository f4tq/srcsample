This folder contains code written various languages by Chris Fortescue.  I've made every effort to ensure
I'm not violating any copyright assignments herein.  Still, the purpose of the folder is for review ONLY and may not be reused in any way without express permission of Chris Fortescue. 

Here is a description of what lies herein.

# Ruby
##  Devise Pdkdf2 plugin.
   A password encryption scheme that I needed to replicate in ruby for legacy reasons coming from python.  
   Why? Unfortunately, not all pdkdf2 implementations are equal and this was true with the existing Ruby pbkdf2 implementation in Ruby. For more info, see pbkdf2 (http://en.wikipedia.org/wiki/PBKDF2) and the original in python that I needed to replicate exactly here: (https://github.com/mitsuhiko/python-pbkdf2/blob/master/pbkdf2.py) as the implemenation can vary slightly.

    See File: ruby/pbkdf2.rb  

# C++ 
## ConnectionMgr
   A template based, thread-safe connection manager.  A class I wrote for a self startup.  I used it to manage Sybase connections for Clearstation/E*Trade graph server then at MongoNet for both PostgreSQL & Oracle connections in various servers.  This class is battle tested and behind billions of stock symbol graphs and millions of processed faxes.

    See Files: c++/ConnectionMgr/ 

## pdf417 decoder
   A boost/opencv/spilltree rewrite for better skew handling.  

    See Files: c++/pdf417/

# Java
## Apache Lucene Directory 
   An Apache Lucene (http://lucene.apache.org ) Directory that provides Berkeley DB as the backing store for search indexes.  I integrated this into the JCR project (http://www.jboss.org/modeshape )    

    See Files: java/lucene-bdb-je

##  AdServer
   JBoss/Seam 4.2.3/Hibernate based backend server providing SOAP/AMF/HTTP/RestEasy API interfaces.  The service consumers were Flex(AMF), customer clicks (RestEasy), email rendering (SOAP), Flex (AMF). Also, using Tuckey Rewrite filter, I proxied geoserver tile requestes from both Flex and OpenLayers/jQuery based on data provided through Hibernate

    See Files: java/Campaign-server

# Flex
   AdServer Campaign Manager front-end Flex component seen by customers.

    See Files: ./flex/Campaign/src/ 

# Javascript
   
## PDF417 encoder
 My port of Paolo Soare's c-based PDF417 library into javascript. 

    See File: javascript/pdf417.js

##Coffeescript snippet drawn from FreeMongoFax.
   It was used in support of creating an online-fax-to-email coversheet with a pdf417  barcode

    See File: javascript/reservation.js.coffee

##OpenLayers/jQuery for manipulating tiles served by the CampaignServer

    See Files: java/Campaign-server/view/js

# Cloud
## VMWare 1 remote/local VM backup script 
   A pair of scripts that work together to facilitate backing up VMware 1.x virtual machines that may (usually) be running on a different host than the backup host.  The script invokes with perl and fires the helper on the target host.  These scripts were used for VM migration and backup. 

    See Files: cloud/backup-vm.pl & cloud/backup-vm-helper.sh

# Perl
##Report Engine      
   A report writer for mod-perl/Apache, PostgreSQL, jQuery, and jQuery Datatables.  It also contains a lot of sql.

    See Files: perl/web-report-engine/

# SQL
  Lots of SQL I wrote in support of the report writer.

    See File:  perl/web-report-engine/mongo_state.sql
 
Cheers,
CLF 
