#!/bin/bash

#
#  This helper script for backup-vm.pl
#

DATE=`date +%F_%H-%M-%S`


DEBUG=0
#_LOG=/dev/stderr
_LOG=/tmp/snap-${DATE}.log
_ERROR_LOG=/tmp/snap-${DATE}.log
_DEBUG_LOG=/tmp/snap-${DATE}.log
KEY=/root/.ssh/assbabykey2

function vmware-cmd1() {
    #mock vmware-cmdb for testing
    debug vmware-dryrun $*
#    $VCMD $*
}
function mv1() {
    debug mv-dryrun $*
}
function rm1() {
    debug rm-dryrun $*
}
function tar1() {
    debug tar-dryrun $*
}
function lvremove1() {
    debug lvremove-dryrun $*
}
function lvcreate1() {
    debug lvremove-dryrun $*
}
function mount1() {
    debug mount-dryrun $*
}
function umount1() {
    debug umount-dryrun $*
}

function error () {
    echo "$$|`date +%F_%H-%M-%S`|error| $*"  >> "${_ERROR_LOG}"
    exit -1; 
}
function notice () {
    echo "$$|`date +%F_%H-%M-%S`|notice| $*"  >> "${_LOG}"
}
function debug () {
    if [ "x$DEBUG" != x ]; then
	echo "$$|`date +%F_%H-%M-%S`|debug| $*"  >> "${_DEBUG_LOG}"
    fi
}

function run () {

    if [ "x$DRYRUN" != "x"   ]; then
	echo "$$|`date +%F_%H-%M-%S`|dryrun| $*" >> "${_LOG}"
    else
	$* 
    fi
}

function LVM_start () {
    _snap_dir="${snap_dir:=/mnt/snap}"
    debug "snap_dir = $_snap_dir"
    if [ ! -d $_snap_dir ]; then
	mkdir -p $_snap_dir
    fi

    _VMware_snap_dir="${VMware_snap_dir:=$_snap_dir}"
    if [ "x${mount_offset}" != x ]; then
	# this happens if 'Virtual Machines' is not itself the mount point of the partition
   
	_VMware_snap_dir=${_VMware_snap_dir}${mount_offset}
    fi

    debug "VMware_snap_dir = $_VMware_snap_dir"
    SNAPVOL=/dev/${VG_GROUP}/LogVol_vmsnap
    debug "SNAPVOL: $SNAPVOL"

    LVPATH=/dev/${VG_GROUP}/${LV}
    debug "LVPATH: $LVPATH"
    debug "VG_GROUP: $VG_GROUP"
    debug "LV: $LV"

    if ! test -L $LVPATH ; then
	error "$LVPATH is not a link"
    fi

    notice "Creating snapshot..."
    # Unmount the snapshot directory if it happened to be mounted from a prior run
    mount${DRYRUN} | grep -q $_snap_dir
    if [ $? -eq 0 ]; then
	umount${DRYRUN} $_snap_dir >> "${_LOG}" 2>&1
    fi
    if [ -e $SNAPVOL ]; then
	debug "lvremove -f $SNAPVOL"
	lvremove${DRYRUN} -f $SNAPVOL >> "${_LOG}" 2>&1
    fi
    lvcreate${DRYRUN} -L30G -s -n LogVol_vmsnap ${LVPATH} >> "${_LOG}" 2>&1
    if [ $? != 0 ]; then
	notice "Not enough space for snapshot.  Falling back to tar"
	return 5;
    fi
    notice "Mounting snapshot..."

    mount${DRYRUN} $SNAPVOL $_snap_dir
    if [ $? -ne 0 ]; then
	notice "Couldn't mount the snapshot, removing snapshot"
	umount${DRYRUN} $_snap_dir >> "${_LOG}" 2>&1
	lvremove${DRYRUN} -f $SNAPVOL >> "${_LOG}" 2>&1
	error "Couldn't mount the snapshot, removing snapshot. bye"
    fi
    return 0;
}

function VM_restart() {
    if [ $VM_on ]; then
	notice "Restarting VM..."
	vmware-cmd${MOCKVMCMD} "$_VM_config_file" start >> "${_LOG}" 2>&1
	VM_restart_time=`date +%s`
	VM_downtime=$(($VM_restart_time-$VM_stop_time))
	
	if [ $? -ne 0 ]; then
	
	    echo "Couldn't restart VM $VM on $HOSTNAME!  Please help." | mail -s "SnapBackVM Error" ops@mongonet.net
	    
	    error "Could not restart VM $VM"
	fi
	notice "VM downtime: $VM_downtime seconds"
    fi
}



function doTar() {

    if [ $isLVM -eq 1 ]; then
	   VMDIR="$_VMware_snap_dir"
    else
	   VMDIR="$_VMware_dir"
    fi

    if [ "x$_filename" != "x-" ]; then
	
	notice "Making tarball $_filename ... "
	debug cd $_tar_dir
	
	cd "$_tar_dir"

	TBALL="${_tar_dir}/${_filename}"

	TMPNAME="${_tar_dir}/.${_filename}"

	debug cd "$VMDIR"

	cd "$VMDIR"

	debug "tar${DRYRUN} c${COMPRESS_FLAG}f "$TMPNAME"  \"$VTMP\""

        tar${DRYRUN} c${COMPRESS_FLAG}f "$TMPNAME"  "$VM"
	TAR_STATUS=$?
	if [ ${TAR_STATUS} -ne 0 ]; then
	    notice "Problem making tar: $TMPNAME"

	    rm -f "$TMPNAME" # Remove the partial tar in case we filled up the disk

	    error "Problem backing up '$VM'.  $TBALL Removed."
	else
	    mv${DRYRUN} "$TMPNAME"  "$TBALL"
	    notice "$VM successfully saved to $TBALL"

	fi

	cd -  > /dev/null 2>&1
    else

	debug cd "$VMDIR"
	cd "$VMDIR"
    	debug tar${DRYRUN} c${COMPRESS_FLAG}f - \"$VTMP\"
	tar${DRYRUN} c${COMPRESS_FLAG}f - "$VM"
	TAR_STATUS=$?
	if [ ${TAR_STATUS} -ne 0 ]; then
	    notice "Problem backing up VM: $VM"
	else
	    notice "$VM successfully piped to stdout"
	fi
	
	cd -  > /dev/null 2>&1

    fi
}

function LVM_finish(){
    notice "Removing snapshot..."
    umount${DRYRUN} $_snap_dir >> "${_LOG}" 2>&1
    lvremove${DRYRUN} -f $SNAPVOL >> "${_LOG}" 2>&1
    LVM_STATUS=$?

    if [ ${LVM_STATUS} -ne 0 ]; then
	error "Problem removing snapshot"
    fi
}





usage="Usage: $0 -d -R <method> -v vm name
    -K <ssh priv key> for remote
    -c compress.  default z.  z: zip b: bzip
    -f <filename> default: \$VM-\$DATE.tg\${compress} if COMPRESS == bzip
    -L <log file>.  default: /dev/stderr
    -d debug
    -D destination
    -R remote user@host for use in ssh tar pipeline
    -n dryrun
    
    The following environment variables can be defined to alter the behavior

    VMware_partition (default: /dev/VolGroup_MONGO/LogVol_vmware )
    VMware_dir (default: /var/lib/vmware/Virtual Machine )
    snap_dir  (default: /mnt/snap )
    VMware_snap_dir (default: \$snap_dir/Virtual Machines )
    VM_config_file  (default: \$VMware_dir/\$VM/\$VM.vmx ) 
    tar_dir (default: /tmp )
    remote_script (default: /usr/local/bin/remote.sh )
"
DRYRUN=
COMPRESS=z

while getopts  "x:K:c:f:L:dhR:v:n" flag
do
    case $flag in
	T)  MOCKVMCMD=1;;
	x) # our invoker
	    invoker=$OPTARG;;
	n) 
	    DRYRUN=1 
	    MOCKVMCMD=1
	    ;;
	K)  KEY=$OPTARG
	    if [ ! -f $KEY ]; then
		error "$KEY is not readable or does not exist";
	    fi
	    ;;
	c) COMPRESS=$OPTARG;;
	d ) DEBUG=1;;
	f ) filename=$OPTARG;;
	h ) notice "$usage" 
	     exit 1;;
	R ) remote=$OPTARG
	    ;;
	v ) 
	    VM=$OPTARG
	;;
	L) _LOG="$OPTARG"
	    _DEBUG_LOG="$_LOG"
	    _ERROR_LOG="$_LOG"
	;;
	\? ) notice "$usage" 
	     exit 1;;
	* ) notice "$usage" >> "${_LOG}"
	     exit 1;;
    esac
done

MOCKVMCMD=

VCMD=`which vmware-cmd`;
if [ ! -x $VCMD ]; then
    error "No vmware-cmd found!"
fi

if [ "x$VM" == x ]; then
    notice "No vm name provided." 
    notice "$usage" 
    exit 1
fi

debug "DATE = $DATE"

notice "Backing up VM $VM"

debug "DEBUG ENABLED"
debug "VM = $VM"

# bash tests need unescaped whitespace
# while arguments to programs must be

# VTMP is the VM escaped for whitespace
VTMP=`echo "$VM" |sed 's/[ )(]/\\\&/g'`



if [ "$COMPRESS" == b ]; then
    COMPRESS_FLAG=j
    COMPRESS_EXT=tbz
else
    COMPRESS_FLAG=z
    COMPRESS_EXT=tgz
fi

_filename="${filename:=${VM}_${DATE}.${COMPRESS_EXT}}"

_tar_dir="${tar_dir:=/tmp}"
debug "tar_dir = $_tar_dir"
debug "filename = $_filename"

if [ "x${_filename}" != "x-" -a  ! -d $_tar_dir ] ; then
    error "${_tar_dir} does not exist"
fi

if [ "x$_filename" = x ]; then
    error "Must define a filename"
fi

if [ "x$remote" = x ] ; then
    notice "Local backup"

    _VMware_dir="${VMware_dir:=/var/lib/vmware/Virtual\ Machines}"
    debug "VMware_dir = $_VMware_dir"

    _VM_config_file="${VM_config_file=`find "$_VMware_dir/$VM" -iname \*.vmx`}"
    debug "VM_config_file = $_VM_config_file"
    
    _VM_partition="${VM_partition:=/dev/VolGroup_MONGO/LogVol_vmware}"
    debug "VMware_partition = $_VM_partition"


    if ! test -b $_VM_partition ; then
	error "$_VM_partition is not a device"
    fi
    
    if ! test -f "$_VM_config_file" ; then
	error "Couldn't find vmx at: '$_VM_config_file'"
    fi

    if [ ! -d "$_VMware_dir" ]; then
	error "Couldn't find vmware_dir at: $_VMware_dir"
    fi

    VG_GROUP=`perl -e "'x$_VM_partition' =~ m/x\/dev\/mapper\/([^\-]+)-.*/ && do { print \\$1; } ;"`
    LV=`perl -e "'x$_VM_partition' =~ m/x\/dev\/mapper\/[^\-]+-(.*)/ && do { print \\$1; } ;"`

    if [ "x$LV" = x ]; then
	isLVM=0
	debug "NON LVM backup"
    else
	isLVM=1
	if [ "x${VG_GROUP}" = x ] ; then
	    error  "Unknown VG_GROUP mapping.  Expecting /dev/mapper in partition ${_VM_partition}.  Cautiously exiting";
	fi
	debug "LVM backup"
    fi

    VM_downtime="${VM_downtime_default:=0}"
 
    #debug "storage_dest = $storage_dest"
    debug "vmx at: $_VM_config_file"


    debug vmware-cmd${MOCKVMCMD} "$_VM_config_file" getstate
    GETSTATE="`vmware-cmd${MOCKVMCMD} "$_VM_config_file" getstate`"
    GETSTATECODE=$?
    
    if [ "x$DRYRUN" != x   ] ; then 
	GETSTATE='getstate() = on'
    fi
    
    if [ $GETSTATECODE -ne 0 ]; then
	error "Error getting state for VM at $_VM_config_file"
    fi

    echo $GETSTATE | grep -q 'getstate() = on'
    
    if [ $? -eq 0 ]; then
	VM_on=1
	debug "VM is on"
    fi
    
    debug "VM state: $GETSTATE"

    if [ $VM_on ]; then
	VM_stop_time=`date +%s`
	if [ $PAUSE ]; then
	    notice "Pausing VM..."
	    debug "vmware-cmd${MOCKVMCMD} \"$_VM_config_file\" suspend soft"
	    vmware-cmd${MOCKVMCMD} "$_VM_config_file" suspend soft >> "${_LOG}" 2>&1
	    if [ $? -ne 0 ]; then
		error "Couldn't pause VM $VM"
	    fi
	else
	    notice "Stopping VM..."
	    debug "vmware-cmd${MOCKVMCMD} \"$_VM_config_file\" stop soft"
	    vmware-cmd${MOCKVMCMD} "$_VM_config_file" stop soft >> "${_LOG}" 2>&1
	    if [ $? -ne 0 ]; then
		error "Couldn't stop VM $VM"
	    fi
	fi
    fi

    run sync 
    run sync 
    run sync
 

    if [ $? -ne 0 ]; then
	if [ $VM_on ]; then
	    notice "Error creating snapshot.  Restarting VM."
	    vmware-cmd${MOCKVMCMD} "$_VM_config_file" start >> "${_LOG}" 2>&1
	    error "done"
	else
	    error "Error creating snapshot."
	fi
    fi
    
    if [ $isLVM -eq 1 ]; then
	run LVM_start
	if [ $? != 0 ] ; then
	    notice "Rolling back to simple tar"
	    isLVM=0;
	    # change the order.  leave the host down until we're down
	    doTar
	    run VM_restart
	else
	    run VM_restart
	    doTar
	    run LVM_finish
	fi
    else
	doTar
	run VM_restart
    fi;
    if [ ${TAR_STATUS} -ne 0 ]; then
	exit 3;
    fi

else
    if [ "x$DEBUG" != x ]; then
	DEBUGFLAG="-d 20"
#	DEBUGFLAG="-d $DEBUG"
    fi
    VMDIR=$VMware_dir
    VMDIR=`echo $VMDIR|sed 's/[ )(]/\\\&/g'`
    debug VMDIR "$VMDIR"
    VLOGTMP=`echo "${_LOG}" |sed 's/[ )(]/\\\&/g'`


    debug cd "$VMDIR"

    debug ssh -t -i $KEY $remote "$invoker $DEBUGFLAG -N "\"$VM\""  -V "\"$VMDIR\"" -L "${VLOGTMP}"  -C $COMPRESS -f - " 
    ssh  -i $KEY $remote "\"${invoker}\" $DEBUGFLAG -N "$VTMP"  -V "$VMDIR" -L "${VLOGTMP}"  -C $COMPRESS  -f - " 2>> "${_ERROR_LOG}" 1> "${_tar_dir}/.${_filename}" 
    SSH_STATUS=$?
    sync; sync; sync;

#    can't do this bit if using -t on ssh 
#
#    SSH_PID=$!
#    trap "kill -TERM $SSH_PID; rm -f .${_filename}" TERM
#    WAIT_STATUS=0
#    while [ "$WAIT_STATUS" -ne 127 ]; do
#         SSH_STATUS=$WAIT_STATUS
#         wait $SSH_PID 2>/dev/null
#         WAIT_STATUS=$?
#    done

    if [ $SSH_STATUS -eq 0 ]; then
	notice  " $VM remote backup finished successfully."
	mv${DRYRUN} "${_tar_dir}/.${_filename}" "${_tar_dir}/${_filename}"
    else
	notice " $VM remote backup failed spectacularly."

	rm${DRYRUN} -f "${_tar_dir}/.${_filename}"
    fi
    debug "$VM remote status $SSH_STATUS"
    exit $SSH_STATUS
fi




    
exit 0
