#!/usr/bin/perl -w
=head1 NAME
$0 

=head1 SYNOPSIS
A program for stopping/pausing a running {or not}, local or remote virtual machine and taring it up into a compressed tarball.  This program requires a support shell script that does a lot of the heavy lifting.

=head1 DESCRIPTION

At times, you want snapshot a virtual machine and save it off somewhere.  Sometimes the VM in question is running locally, sometimes not.  Sometimes the VM is running on a machine that support filesystem snapshots, sometimes not.  It is usually best to stop the VM as opposed to pausing it for consistencies sake.  It is also best to run your VM on a snapshotible filesystem so as to aid in getting backups with the least amount of disruption i.e.  the least amount of down time possible.

If your VM is on a snapshotible filesystem, the following process is followed:

Method A
-snapshotible filesystem
-downtime limited to start/stop of vm.  snapshot takes seconds.

1. Stop the VM with vmware-cmd
2. Snapshot the filesystem
3. Restart the VM
4. mount the snapshot
5. tar from the snapshot


Method B
-no snapshotbile filesystem
-downtime is as long as the tar of the VM takes

1. Stop the VM
2. tar from the VM directory
3. Start the VM

If you VM is remote, then this script invokes as follows:

Local:                                          Remote:

$0 invokes $-helper   --- ssh remote $0 -O - --> $0 invokes $0-helper.sh


In particular, output is set to stdout.  log files are set to redirect stderr to a specific filesystem logfile.

The default logfile follows the form:  /tmp/\${VM}-\${DATE}.log

=head1 USAGE
Usage: $0 
  -N <vm name>  No default.  Exclusive of -a
  -a   Backup all VMs found
  -V   VMware directory.  On linux, this is usually /var/lib/vmware/Virtual\ Machines/
  -t   <tar_dir>  Where the tarballs are put
  -C   b|z  The compression scheme to use.  Default is z
  -R   use@host remote string fed to ssh as the target host
  -K   The keyfile to use with -R. Default /root/.ssh/assbabykey2
  -L   The logfile to use.  Default: stderr. 
  -d   <level>  Default: 0
  -O  <offsite>   No default. This script will check that something is mounted here and error out if not.  If so, it
      will be used to copy via rsync all the resulting tar balls after the have all been compiled locally.
  -I <interface>  default: eth0
=head1 WHATFOR

Cause we needed a way to cheaply backup VMs.


=head1 AUTHOR

Chris Fortescue 2010

=cut

my $selfname = $0;


$0="backup-vm";

use POSIX qw(strftime getcwd);
use Net::SMTP;
use Data::Dumper;
use Getopt::Std;

my $keyfile = "/root/.ssh/assbabykey2";
my $VMware_dir='/var/lib/vmware/Virtual Machines';

my $debug = 0;
my $whereto ;  # Where to put the offsited files
my $all=0;
my $remote;
my $vm;
my $tar_dir;
my $buildVersion="1.0";
my $dryrun;
my $quiet=0;
my $logfile;
my $compress="z";
my $supportScript="/usr/local/bin/backup-vm-helper.sh";
my $tarfile;
my $iface='eth0';

my %opts;
$Getopt::Std::STANDARD_HELP_VERSION = 1;
sub VERSION_MESSAGE() {
  print STDERR "$selfname  version $buildVersion\n";
}

getopts('C:L:qnK:T:R:O:V:d:aN:x:f:I:',\%opts);

$quiet=1 if exists $opts{q};

$iface=$opts{I} if exists $opts{I};

$VMware_dir=$opts{V} if exists $opts{V};
$whereto=$opts{O} if exists $opts{O};
$tarfile=$opts{f} if exists $opts{f};
$debug=$opts{d} if exists $opts{d};
$all=1 if exists $opts{a};
$vm=$opts{N} if exists $opts{N};
$tar_dir=$opts{T} if exists $opts{T};
$remote=$opts{R} if exists $opts{R};
$keyfile=$opts{K} if exists $opts{K};
$dryrun=1 if exists $opts{n};
$logfile=$opts{L} if exists $opts{L};
$supportScript=$opts{x} if exists $opts{x};

if ( exists $opts{C} ){
  #make sure compress is zip of bzip
  die "-C options are z|b for zip or bzip" if $opts{C} !~ /[zb]/;
  $compress=$opts{C};
}
die "$tarfile contains path!\n" if defined $tarfile && $tarfile =~ m/\//;
die "$supportScript is not executable!" if ! -x $supportScript; 
die "You must define the tar dir with -T\n" if $tarfile ne '-' && ! defined $tar_dir ;
select STDOUT; $| = 1;

if (defined $logfile)  {

    open (OLDERR, ">&STDERR");
    open (STDERR, ">>$logfile")
        or die "can't redirect stderr: $!";
    
}
elsif ($quiet) {
  open (OLDERR, ">&STDERR");
  open (STDERR, ">/dev/null")
    or die "can't redirect stderr: $!";
}
print STDERR "$0|$$| self = $selfname\n";

( ($all==0 && (!defined $vm || $vm =~ m/^$/)) || ($all==1 && defined $vm && $vm !~ m/^$/) ) &&  die "You must specify a VM by name or use -a for all VMS ...\n";



my $vms;
my $lvm_dir;

if (!defined $remote ||  $remote =~/^$/ ){

#  $network = &getNetwork( $iface );
#  defined $network || warn "$0|$$|Couldn't determine network\n";
#  print STDERR "$0|$$| Network: $network\n" ;;

  $lvm_dir = &getVMPartition( $VMware_dir );

  $vms = &getVMs ( $VMware_dir );

  # prune the list if we only want one.
  delete @$vms { grep { $_ ne $vm } keys(%$vms) } if $all==0;

  print STDERR "$0|$$| $_ => " . $vms->{$_} ."\n" foreach (keys %{$vms}) ;

  print STDERR "$0|$$ VM=$vm\n" if defined $vm;

  warn "$0|$$| Not a snapable partition: $lvm_dir->[0]\n" if $lvm_dir->[0] !~ m/mapper/;
  my ($volume) = ($lvm_dir->[0] =~ m!/dev/mapper/([^\-]+)-.*!);

  if ($lvm_dir->[1] ne $VMware_dir){
    print STDERR "$0|$$| $lvm_dir->[0] mounted at $lvm_dir->[1] which is offset from VMware base\n" ;;
    $offset =  $VMware_dir; 
    $offset =~ s/$lvm_dir->[1]//;
    print STDERR "$0|$$| offset=$offset\n" ;;
  }

  print STDERR "$0|$$| volume: $volume\n" if defined $volume ;
}
else
 {
   &checkRemoteSelf($remote);
   $remotevms = &getRemoteVMs($remote,$VMware_dir);
   (keys(%$remotevms) > 0 ) or die "No VMs accessible via $remote\n";
   print STDERR "$0|$$| RemoteVM: $_ => " . $remotevms->{$_} ."\n" foreach (keys %{$remotevms}) ;
   if ($all == 0){
     (grep { $_ eq $vm } keys(%$remotevms))  or do{
       die "$vm doesn't exist on $remote\n";
     };
     $vms = ();
     $vms->{$vm} = $vm;
     print STDERR "$0|$$| Remote VM: $vm\n";
   }
   else {

     $vms = $remotevms;

   }
   
 }

if (!keys(%$vms)){
  print STDERR "$0|$$|No VMs\n";
  exit -1;
}

my %vmstats;
my $retval=0;
foreach my $vm (keys %$vms)
  {
    my $logger;
    $logger = $logfile if defined  $logfile;
    $logger = "/tmp/$vm" . ".log" if not defined $logfile;
 
  FORK:
    print STDERR "$0|$$| vm: $vm\n" ;;
    if ($pid = fork) {
      print STDERR "[$$] Waiting for pid $pid handling $vm\n" ;;
      waitpid($pid,0);
      $vmstats{$vm} = $?;
      $retval = -1 if $vmstats{$vm} != 0;

      print STDERR "[$$] reaped $pid status $vmstats{$vm} \n" ;;

      next;
    }
    elsif (defined $pid ){
      $ENV{"tar_dir"}= $tar_dir if defined $tar_dir;
      $ENV{"VMware_dir"}= $VMware_dir;

      (! defined $remote || $remote =~ /^$/) && do {
	$ENV{"VM_partition"}=$lvm_dir->[0];
	$ENV{"mount_offset"}= $offset if defined $offset;
      };
  
      my @sw=( "-v", $vm );
      push(@sw,"-c",$compress)  ;
      push(@sw,"-x",$selfname)  ;
      push(@sw,"-L",$logger)   ;
      push(@sw,"-R",$remote, "-K", $keyfile)  if (defined $remote && $remote !~ /^$/);
      push(@sw,"-f",$tarfile)  if (defined $tarfile && $tarfile =~ /\-/);
      push(@sw,"-n")  if (defined $dryrun && $dryrun eq 1);
      $debug > 0  && push @sw, "-d" ;
      $debug > 2     && print STDERR "$0|$$| arg: $_\n" foreach @sw ; 
      #exec "/bin/env";
      exec "$supportScript", @sw;
      die "Exec failed!\n";
    }
    elsif ($! == EAGAIN){
      sleep 3;
      redo FORK;
    }
    else{
      print STDERR "$0|$$| Couldn't fork $vm!\n" ;;
      }
    
}
print STDERR "$0|$$| vm: $_ backup status: $vmstats{$_}\n" foreach keys(%vmstats);


if ( defined $whereto  ){
  #`echo "rsync -av  $_ $whereto "` foreach (grep { $vmstats{$_} == 0 } keys(%$vms));

}


exit $retval;

sub checkRemoteSelf( $ )
  {
    my $host = shift;
    `ssh -i $keyfile $host "test -x \"$selfname\"" >/dev/null 2>&1 `;
    die "$selfname doesn't exist for $host\n" if ($?);
    print STDERR "$0|$$| $selfname exists on $host\n" ;
  }
    
sub getRemoteVMs( $ )
  {
    my $host = shift;
    my $VMware_dir = shift;

    $VMware_dir =~ s/([ \)\(])/\\$1/g;
    open (OTHER, "ssh -i $keyfile $host \"find ${VMware_dir} -iname \*.vmx\" | ") ;
    my @v;
    while (<OTHER>)
      {

	print STDERR "$0|$$| $_" ;
        chomp;
        push(@v,$_); 
      }
    close OTHER;

    %machines = map {
      chomp;
      my $old=$_;

      s/([ \)\(])/\\$1/g;
      $f=`basename $_`;
      chomp $f;
      $d=`dirname $_`;
      chomp $d;
      my ($d2) = ($d =~ m/([^\/]+)\/*$/);
      
      $f =~ s/.vmx$//g;
      if ($d2 ne $f)
	{
	  # the directory name has precidence
	  warn "VMX ($f) has different name than containing directory ($d2)\n";
	  $f = $d2
	}


      print STDERR "$0|$$| f=$f, d=$d\n" if $debug>5 ;
      $f => $d;
    }  @v;
    return \%machines;


  }

sub getVMPartition( $ )
  {
    my ($VMware_dir) = shift;
    -d $VMware_dir || die "Couldn't find '$VMware_dir'\n";
    my $save = $VMware_dir;

    $VMware_dir =~ s/([ \)\(])/\\$1/g;

    open (OUT, "/bin/df -P $VMware_dir |") || die "Couldn't open $VMware_dir\n";
    while (<OUT>){
      my (@line) = split /\s+/;
      next if @line < 4;
      next if $line[0] =~/Filesystem/i;
      # fancy clfism. some directories 
      splice(@line,5,0,join(' ', @line[5 .. $#line]));
      die "Not expected filesystem '" . $line[5] . "' =~ '$VMware_dir'\n" if $save !~ m!$line[5]!;
      print STDERR "$0|$$| Partition Verified: $line[0] => $line[5]\n" ;;
      return [ $line[0], $line[5] ] ;
    }
  }
    

sub getVMs( $ )
  {
    my ($VMware_dir) = shift;
    -d $VMware_dir || die "Couldn't find $VMware_dir \n";
    my %machines ;
    $VMware_dir =~ s/([ \)\(])/\\$1/g;
    %machines = map {
      chomp;
      s/([ \)\(])/\\$1/g;

      $f=`basename $_`;
      chomp $f;
      $d=`dirname $_`;
      chomp $d;
      my ($d2) = ($d =~ m/([^\/]+)\/*$/);

      $f =~ s/.vmx$//g;

      if ($d2 ne $f)
	{
	  # the directory name has precidence for naming 
	  warn "VMX ($f) has different name than containing directory ($d2)\n";
	  $f = $d2
	}

      print STDERR "$0|$$| f=$f, d=$d\n" if $debug>5 ;
      $f => $d;
    }  `find $VMware_dir -iname \*.vmx `;
    return \%machines;
  }
   
sub notifyHandlers()
  {
    my ($from, $notify, $content) = @_;
    print STDERR "$0|$$| NotifyHandlers: smtp_server=$smtp_server\n\tnotify=$notify\n\tfrom=$from\n\tcontent=$content\n" ;;

    return if !$smtp_server || !$notify;

    $smtp = Net::SMTP->new($smtp_server,
#                          Debug => 1,
                           Timeout => 30
                          );
    $smtp->mail($from);

    foreach  $recpto ( split(/[, ]/, $notify ) )
      {
        $smtp->to( $recpto );
      }
    $smtp->data();
    $smtp->datasend($content);
    $smtp->dataend();
    $smtp->quit;
  }





sub getNetwork( $ )
  {
    my ($iface) = shift;
    my $seen = 0;
    foreach ( `ifconfig -a` ){
      print STDERR "$0|$$| $_" if $debug > 5 ;
      /$iface\s+Link/ && do 
	{ 

	  $seen=1 ; next;
	}; 
       $seen==1 && m/^\w+\s+Link encap/ && do 
	{  
	  $seen=0;
	}; 
       $seen==1 && /\s*inet addr:\s*(\d+\.\d+\.\d+)/ && do 
	{
           print STDERR "$0|$$| Found $1\n" if $debug>5  ;
	  return $1;
	};
    }
    return undef;
  }

#$hostname = `hostname`;

#chomp $hostname;

#`mkdir -p $whereto/$hostname` || -d "$whereto/$hostname" || die "Couldn\'t make $whereto/$hostname.  status $?" ;

#`rsync -av 
