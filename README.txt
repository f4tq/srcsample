This folder contains code in various languages written by Chris Fortescue.  I've made every effort to ensure
I'm not violating any copyright assignments herein.  Still, the purpose of the folder is for review ONLY and may not be reused in any way without express permission of Chris Fortescue. 

The following lists selected source code written by Chris Fortescue.

o) Ruby
   a) Pdkdf2 is a password encryption scheme that I needed to replicate in ruby for legacy reasons.  Unfortunately, not all pdkdf2 implementations are equal and this was true with the existing Ruby pbkdf2 implementation in Ruby. For more info, see pbkdf2 (http://en.wikipedia.org/wiki/PBKDF2) and the original in python that I needed to replicate exactly here: (https://github.com/mitsuhiko/python-pbkdf2/blob/master/pbkdf2.py) as the implemenation can vary slightly.

    File: ruby/pbkdf2.rb  

   b) I have tons of ruby that look strange picked from its project which I would be happy to review if necessary.


o) C++ 
  - ConnectionMgr
	A template based, thread-safe connection manager.  A class I wrote for a self startup in 90s.  I used it to manage Sybase connections for Clearstation/Etrade graph server then at MongoNet for both Postgres & Oracle connections in various servers.  This class is battle tested and behind billions of stock symbol graphs and millions of processed faxes.
    File: c++/ConnectionMgr/ConnectionMgr_T.h/cpp 

  - pdf417 decoder
	A boost/opencv/spilltree rewrite for better skew handling.  
    File: c++/pdf417/symbol.h/cpp

  - Again, tons of c++ that I can show those who'd care to see.

o) Java
   -  A lucene (http://lucene.apache.org ) directory and allows the use of Berkeley DB as the backing store for search indexes.  I integrated this into the JCR project (http://www.jboss.org/modeshape )    

   -  Campaign-server is the back-end, JBoss 4.2.3 (before Weld) based server that acted as the both a web services front end - providing AMF Soap end-point for Flex and MongoNet's internal email renderer.  MongoNet's business was fax to email.  It provided ad serving (for email delivery), click processing, report using GeoServer, Openlayers, JQuery and Flex.

o) Flex
	Campaign front-end Flex component seen by customers.

o) Javascript
	- My port of Paolo Soares pdf417 library.  Obviously, I am omitting key pieces but this should help you see my javascript aptitude.  If you search the web for 'MongoFax' coversheet you will find many pdfs.  Each pdf contains the full source that you may access with Acrobat Pro -> Tools -> Javascript.  To see the generated bar code, use one of the coversheets found on the web (here's one: http://www.texasrealtors.com/ae/for_your_members/MongoFaxCoverSheet.pdf) then PRINT it.  The bar doesn't appear otherwise.
	File: pdf417.js

	- A piece of coffeescript (nee: javascript) from FreeMongoFax.  It was used in support of creating an online-fax-to-email coversheet with a barcode
        File: reservation.js.coffee

	- Lots of map javascript in
	  java/Campaign-server/view/js
o) Cloud
	A pair of scripts that work as a pair to facilitate backing up VMware 1.x virtual machines that may (usually) be running on a different host than the backup host.  The script invokes with perl and fires the helper on the target host.  These scripts were used for VM migration and backup. 
	cloud/backup-vm.pl & cloud/backup-vm-helper.sh

o) Perl
       A self contained report writer based for mod-perl/Apache.  It uses a jquery plugin datatables and is easily extended.  It also contains a lot of sql.
       File: perl/web-report-engine/
 
       Again, I've written tons and tons of perl which I have and will show but not release on demand.

 
