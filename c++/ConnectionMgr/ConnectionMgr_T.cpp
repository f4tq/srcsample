// Copyright (c) Virtuality, Inc
// ALL RIGHTS RESERVED
//
// ConnectionMgr --A class for managing a pool of
//  Connections.  Responsibilities
// include scheduling,creation,destruction.
// Thread-safe.
// A CLF original

/*
#ifdef ACE_NLOGGING
#undef ACE_NLOGGING
#endif
*/

#include <ace/Get_Opt.h>
#include <ace/ARGV.h>
#include "ConnectionMgr_T.h"
#include <algorithm>

    // the connection we wrap
template<class CONNECTION, class CONNECTION_CREATOR, class ACE_LOCK>
ConnectionMgr<CONNECTION,CONNECTION_CREATOR,ACE_LOCK>::ConnectionHandle::ConnectionHandle(META &real, SELF &parent):_real(real),_parent(parent)
{
    _real._handleRefs++;
    ACE_DEBUG((LM_DEBUG,"[%D](%t) (%x) ConnectionHandle( Connection& ) refs %d\n",this,_real._handleRefs));
}

    // ConnectionHandle's can only be created by the ConnectionMgr
template<class CONNECTION, class CONNECTION_CREATOR, class ACE_LOCK>
ConnectionMgr<CONNECTION,CONNECTION_CREATOR,ACE_LOCK>::ConnectionHandle::ConnectionHandle(const ConnectionHandle<CONNECTION,CONNECTION_CREATOR,ACE_LOCK> & cp):_real(cp._real),_parent(cp._parent)
{
    _real._handleRefs++;
    ACE_DEBUG((LM_DEBUG,"[%D](%t) (%x) ConnectionHandle(const ConnectionHandle& cp) refs %d\n",this,_real._handleRefs));
}

template<class CONNECTION, class CONNECTION_CREATOR, class ACE_LOCK>
ConnectionMgr<CONNECTION,CONNECTION_CREATOR,ACE_LOCK>::ConnectionHandle::~ConnectionHandle()
{
    destroy();
}

template<class CONNECTION, class CONNECTION_CREATOR, class ACE_LOCK>
ConnectionMgr<CONNECTION,CONNECTION_CREATOR,ACE_LOCK>::ConnectionHandle&
ConnectionMgr<CONNECTION,CONNECTION_CREATOR,ACE_LOCK>::ConnectionHandle::operator=(const ConnectionMgr<CONNECTION,CONNECTION_CREATOR,ACE_LOCK>::ConnectionHandle &)
{
//    ACE_DEBUG((LM_DEBUG,"[%D](%t) ConnectionHandle::operator=() ; this->refs==%d ; cp.refs %d\n",_real._handleRefs,cp._real._handleRefs));
    return *this;
}

template<class CONNECTION, class CONNECTION_CREATOR, class ACE_LOCK>
void
ConnectionMgr<CONNECTION,CONNECTION_CREATOR,ACE_LOCK>::ConnectionHandle::destroy()
{
    --_real._handleRefs;
    ACE_DEBUG((LM_DEBUG,"[%D](%t) ~ConnectionHandle(%x) refs==%d\n",this,_real._handleRefs));
    if (_real._handleRefs==0)
    {
        _parent.release(*this);
    }
}

template<class CONNECTION, class CONNECTION_CREATOR, class ACE_LOCK>
ACE_hthread_t 
ConnectionMgr<CONNECTION,CONNECTION_CREATOR,ACE_LOCK>::ConnectionHandle::ownerThread() 
{ 
    return _real._ownerThreadId;
}


template<class CONNECTION, class CONNECTION_CREATOR, class ACE_LOCK>
ConnectionMgr<CONNECTION,CONNECTION_CREATOR,ACE_LOCK>::ConnectionMgr(): _produceConnection(_connectionListLock), _consumeConnection(_connectionListLock),_min(1),_max(2),_verbose(false)
{
    ACE_DEBUG((LM_DEBUG,"ConnectionMgr::ConnectionMgr()\n"));

    init();
}


template<class CONNECTION, class CONNECTION_CREATOR, class ACE_LOCK>
int 
ConnectionMgr<CONNECTION,CONNECTION_CREATOR,ACE_LOCK>::init ()
{
    for (int ii=_connections.size(); ii< _min;ii++)
	{
        CONNECTION *c = _creator(); //  ? (*_creator)( _context ) : new CONNECTION;
        if (c)
        {
            _connections.push_back( new MetaConnection<CONNECTION, CONNECTION_CREATOR, ACE_LOCK> ( c ) );
        }
    }
    return 0;
}


template<class CONNECTION, class CONNECTION_CREATOR, class ACE_LOCK>
ConnectionMgr<CONNECTION,CONNECTION_CREATOR,ACE_LOCK>::~ConnectionMgr()
{
    ACE_TRACE("ConnectionMgr::~ConnectionMgr");
    ACE_Write_Guard<ACE_LOCK> guard(_connectionListLock,1);

    for (ConnectVectIter iter = _connections.begin(); iter != _connections.end(); iter++)
        {
            MetaConnection_Ptr &p = (*iter);
            if (p) delete p;
            p=0;
        }
    _connections.erase(_connections.begin(),_connections.end());
}


template<class CONNECTION, class CONNECTION_CREATOR, class ACE_LOCK>
ConnectionMgr<CONNECTION,CONNECTION_CREATOR,ACE_LOCK>::MetaConnection_Ptr
ConnectionMgr<CONNECTION,CONNECTION_CREATOR,ACE_LOCK>::findAvailableConnection()
{
    // the findAvailableConnection must be called with _connectionListLock mutex held!
    // I didn't make it a recursive mutex
    
    // first we make sure the calling thread does not already own
    // a connection.  If it does we simply return the existing connection
    ACE_hthread_t self ;
    ACE_Thread::self(self);
    for (ConnectVectIter iter = _connections.begin(); iter != _connections.end(); iter++)
	{
	    MetaConnection_Ptr ptr = (*iter);
        if (ptr->_ownerThreadId == self)
        {
            ACE_DEBUG((LM_ERROR,"[%D](%t) ConnectionMgr::findAvailable -- thread already owns connection!\n"));
            return ptr;

        }
    }
    for (ConnectVectIter iter = _connections.begin(); iter != _connections.end(); iter++)
	{
	    MetaConnection_Ptr ptr = (*iter);
	    if (ptr->status() == META::Available)
        {
            ACE_DEBUG((LM_DEBUG,"[%D](%t) ConnectionMgr::findAvailable -- found connection  %x!\n",ptr));
            return ptr;
        }
	}
    // this is where the max can be used to expand the number of connections in use
    // 
    if (_connections.size() < (size_t) _max)
    {

//	    CONNECTION *c = _creator ? (*_creator)(this->_context) : new CONNECTION;
	    CONNECTION *c = _creator();
        if (c)
        {
            MetaConnection_Ptr ptr = new MetaConnection<CONNECTION, CONNECTION_CREATOR, ACE_LOCK> ( c );
            _connections.push_back( ptr );
            return ptr;
        }
        return 0;
    }

    return 0;
}

template<class CONNECTION, class CONNECTION_CREATOR, class ACE_LOCK>
ConnectionMgr<CONNECTION,CONNECTION_CREATOR,ACE_LOCK>::HANDLE
ConnectionMgr<CONNECTION,CONNECTION_CREATOR,ACE_LOCK>::acquire()
{
    ACE_Write_Guard<ACE_LOCK> guard(_connectionListLock,1);
    
    MetaConnection_Ptr avail=0;
    while (!(avail=findAvailableConnection()))
    {
        ACE_DEBUG((LM_DEBUG,"[%D](%t) ConnectionMgr::acquire  -- waiting for connection\n" ));
        
        _consumeConnection.wait();
    }
    
    avail->_status = META::InUse;
    ACE_hthread_t self ;
    ACE_Thread::self(self);
    avail->_ownerThreadId = self;
    
    HANDLE h(*avail, *this);
    //  _produceConnection.broadcast();
    ACE_DEBUG((LM_DEBUG,"[%D](%t) ConnectionMgr::acquire  -- GOT connection\n" ));
  
    return h;
}

template<class CONNECTION, class CONNECTION_CREATOR, class ACE_LOCK>
void
ConnectionMgr<CONNECTION,CONNECTION_CREATOR,ACE_LOCK>::release(HANDLE &handle)
{
    ACE_Write_Guard<ACE_LOCK> guard(_connectionListLock,1);
    handle._real._status = META::Available;
    handle._real._ownerThreadId = (ACE_hthread_t) -1;
    ACE_DEBUG((LM_TRACE,"[%D](%t) ConnectionMgr::release\n" ));
    _consumeConnection.broadcast();
}


template<class CONNECTION, class CONNECTION_CREATOR, class ACE_LOCK>
class killConnection
{
    int _maxKill;
    int _curKill;
    
public:
    int totalKilled(){return _curKill;}
    killConnection(int mx):_maxKill(mx),_curKill(0){}
    killConnection(const killConnection&cp):_maxKill(cp._maxKill),_curKill(cp._curKill){}
    killConnection& operator=(const killConnection&cp){_maxKill=cp._maxKill;_curKill=cp._curKill; return *this;} 
    bool operator()(ConnectionMgr<CONNECTION,CONNECTION_CREATOR,ACE_LOCK>::META *& ptr) 
        {
            if (_curKill>= _maxKill) return false;
            
            if (ptr->status() == ConnectionMgr<CONNECTION,CONNECTION_CREATOR,ACE_LOCK>::META::Available)
            {
                _curKill++;
                delete ptr;
                return true;
            }
            return false;
        }
};
                        
//int remove_if<ConnectVectIter, killConnection&>(ConnectVectIter ,ConnectVectIter ,killConnection& );

template <class CONNECTION, class CONNECTION_CREATOR, class ACE_LOCK>
int 
ConnectionMgr<CONNECTION,CONNECTION_CREATOR,ACE_LOCK>::getMax() 
{ 
    ACE_Read_Guard<ACE_LOCK> guard(_connectionListLock,1);
    return _max;
}

template <class CONNECTION, class CONNECTION_CREATOR, class ACE_LOCK>
void
ConnectionMgr<CONNECTION,CONNECTION_CREATOR,ACE_LOCK>:: setMaxConnections(int newMax)
{
    ACE_Write_Guard<ACE_LOCK> guard(_connectionListLock,1);
    
    // can't have zero connections
    if (newMax<=0)  
    {
        return;
    }
    // can't have less than min
    if (newMax < _min) 
        newMax = _min;
    
    if (((size_t)newMax) > _connections.size())    
    {
        _max = newMax;
    }
    else if (((size_t)newMax) < _connections.size())
    {
        // attempt to reduce the number of connections in use.
        // we can only shrink things *if* they are connections
        // not in use
        killConnection<CONNECTION,CONNECTION_CREATOR,ACE_LOCK> k(_connections.size() - newMax);
        std::remove_if(_connections.begin(), _connections.end(),k);
        ACE_DEBUG((LM_DEBUG,"ConnectionMgr::setMaxConnections killed %d connections\n",k.totalKilled())); 
        _max = newMax;

    }
}

template <class CONNECTION, class CONNECTION_CREATOR, class ACE_LOCK>
int
ConnectionMgr<CONNECTION,CONNECTION_CREATOR,ACE_LOCK>::getMin() 
{ 
    ACE_Read_Guard<ACE_LOCK> guard(_connectionListLock,1);
    return _min;
}

template <class CONNECTION, class CONNECTION_CREATOR, class ACE_LOCK>
void
ConnectionMgr<CONNECTION,CONNECTION_CREATOR,ACE_LOCK>::setMinConnections(int newMin)
{
    ACE_Write_Guard<ACE_LOCK> guard(_connectionListLock,1);
    if (newMin < _min)
    {
        // attempt to kill connections back to newMin.
        // no guareentee though
        killConnection<CONNECTION,CONNECTION_CREATOR,ACE_LOCK> k(_connections.size() - _min);
        std::remove_if(_connections.begin(), _connections.end(),k);
        ACE_DEBUG((LM_DEBUG,"ConnectionMgr::setMinConnections killed %d connections\n",k.totalKilled())); 
        _min = newMin;
    }
    else if (((size_t)newMin) < _connections.size())
    {
        _min = newMin;
    }
    else
    {
        int n = newMin - _connections.size();
        ACE_DEBUG((LM_DEBUG,"ConnectionMgr::setMinConnections created %d connections\n",n)); 
        
        for (int ii = 0; ii < n;ii++)
        {
            CONNECTION *c = _creator() ;
            if (c)
            {
                _connections.push_back( new MetaConnection<CONNECTION, CONNECTION_CREATOR, ACE_LOCK>(c) );
            }
        }
    }
}



 

