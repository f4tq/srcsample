// Copyright (c) Virtuality, Inc 
// ALL RIGHTS RESERVED
//
// ConnectionMgr --A class for managing a pool of
//  Connections.  Responsibilities
// include scheduling,creation,destruction.
// Thread-safe.  Connection requests are delaged using a ConnectionHandle
// construct that reference counts the Connection.  When the count reaches
// zero, the Connection is freed.  
//
// This class takes a CONNECTION_CREATOR that is expected to define a method 
// of the form:
//      CONNECTION* operator()();
//
// FYI: Make the method virtual so you can re-use a single singleton code instance
// if you need to make a CONNECTION with special attributes.  otherwise, you'll
// end up generating more code.
//
// ConnectionMgr gives out connections via the acquire method.  This method 
// returns a handle who's scoping determines the life of connection ownership
// returning the connection to the connectionmgr via its destructor
// a CLF original
#ifndef ConnectionMGR_T_INCLUDED
#define ConnectionMGR_T_INCLUDED


#include <ace/Synch_T.h>
#include <ace/Singleton.h>
#include <ace/Service_Object.h>
#include <ace/ARGV.h>

#include <vector>
#include <StrCollectionAdapter.hh>

template <class CONNECTION, class CONNECTION_CREATOR, class ACE_LOCK>
class ConnectionMgr : public ACE_Service_Object ,  public ACE_Cleanup
{
    typedef ConnectionMgr<CONNECTION, CONNECTION_CREATOR, ACE_LOCK> SELF;

    typedef CONNECTION* CONNECTION_Ptr;
    
    // MetaConnection wraps the CONNECTION so that its availablity, current (thread) owner
    // status can be tracked
    class MetaConnection
        {
            friend class ConnectionMgr<CONNECTION, CONNECTION_CREATOR, ACE_LOCK>;
        public:
            enum State {
                Available=0,
                InUse = 1
            };
            int _handleRefs;
            State _status;
            ACE_hthread_t _ownerThreadId;

            CONNECTION *_real;

            MetaConnection(CONNECTION *r) : _handleRefs(0),_status(Available), _ownerThreadId(-1),  _real(r){}
            ~MetaConnection()
                {
                    if (_real) delete _real;
                }
            State status(){ return _status;}
            CONNECTION &ref(){ return *_real;};
            CONNECTION *ptr(){ return _real;};
        };
    typedef SELF::MetaConnection  META;
    typedef SELF::MetaConnection* MetaConnection_Ptr;
    typedef ACE_Thread_Condition<ACE_LOCK> CONDITION;
    friend class ACE_Singleton<SELF , ACE_Recursive_Thread_Mutex>;
public:
    class ConnectionHandle 
        {
            friend class ConnectionMgr<CONNECTION, CONNECTION_CREATOR, ACE_LOCK>;

            META &_real;
            SELF &_parent;
            // no copying allowed! hence the copy/assigment are private
            ConnectionHandle& operator=(const ConnectionHandle& );
            void destroy();
            
        public:
            ConnectionHandle(META &real, SELF &parent);
            ConnectionHandle(const ConnectionHandle& cp);
            ~ConnectionHandle();
            // the destructor informs the ConnectionMgr of its demise.
            CONNECTION& ref() { return _real.ref();}
            CONNECTION* ptr() { return _real.ptr();}
            CONNECTION* operator->() { return _real.ptr();}
            ACE_hthread_t ownerThread() ;
        };

    typedef ConnectionHandle<CONNECTION, CONNECTION_CREATOR, ACE_LOCK> HANDLE;

    typedef std::vector < MetaConnection_Ptr > ConnectVect;
    typedef ConnectVect::iterator ConnectVectIter;
    typedef ConnectVect::const_iterator ConstConnectVectIter;
    typedef ConnectVect::value_type ConnectVectPair;


    // get the connectionMgr. If not created yet create 'count'
    // number of connections for its connection pool.
    
    HANDLE acquire();
    // acquire a connection.  The handle returned will release 
    // the hold on the connection when it goes out of scope.  Don't
    // stpre the contents of the Connection outside of the Connection class

    void release(HANDLE &);
    // generally the ConnectionHandle destructor calls this method when it
    // goes out of scope.  Use of the contents of the Connection AFTER it
    // release will likely cause a SEGV.  Only use a connection when you have
    // a handle to it.

    void setMaxConnections(int newMax);
    // setMaxConnections will introduce a new high end to the number
    // of connections that will be created on demand.  if newMax < _max
    // then an attempt is made to reduce the CtConnection pool to newMax
    // This method holds the _connectionListLock.

    int getMax() ;

    void setMinConnections(int newMin);
    // setMinConnections establishes a new lowend number of connections
    // if newMin < _min , then an attempt is made to reduce the connection pool
    // if newMin > _min, and the total connections in the pool is less than newmin,
    // newmin-_connectionList.size() new connections are created.
    int getMin();

    ACE_LOCK & lock(){return _connectionListLock;}
    
    ConnectionMgr();
    ~ConnectionMgr();

    // creator probably needs muxing
    void creator(CONNECTION_CREATOR newCreator) { _creator = newCreator;}
    CONNECTION_CREATOR& creator(){return _creator;}
protected:

private:

    int init ();
    void destroy();
    // findAvail requires that the mutex '_connectionListLock' be already held
    MetaConnection_Ptr findAvailableConnection();

    ConnectVect _connections;
    ACE_LOCK  _connectionListLock;

    CONDITION _produceConnection;
    CONDITION _consumeConnection;
    bool connectionAvail;
    int _min;
    int _max;
    bool _verbose;

    CONNECTION_CREATOR _creator;
};



#if defined (ACE_TEMPLATES_REQUIRE_SOURCE)
#include "ConnectionMgr_T.cpp"
// On Win32 platforms, this code will be included as template source
// code and will not be inlined. Therefore, we first turn off
// ACE_INLINE, set it to be nothing, include the code, and then turn
// ACE_INLINE back to its original setting. All this nonsense is
// necessary, since the generic template code that needs to be
// specialized cannot be inlined, else the compiler will ignore the
// specialization code. Also, the specialization code *must* be
// inlined or the compiler will ignore the specializations.
#if defined (ACE_WIN32)
#undef ACE_INLINE
#define ACE_INLINE
//#include "ConnectionMgr_T.i"
#undef ACE_INLINE
#if defined (__ACE_INLINE__)
#define ACE_INLINE inline
#else
#define ACE_INLINE
#endif /* __ACE_INLINE__ */
#endif /* ACE_WIN32 */
#endif /* ACE_TEMPLATES_REQUIRE_SOURCE */

#if defined (ACE_TEMPLATES_REQUIRE_PRAGMA)
#pragma implementation ("ConnectionMgr_T.cpp")
#endif /* ACE_TEMPLATES_REQUIRE_PRAGMA */

#endif


