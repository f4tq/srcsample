// copyright (c) Mongonet, Inc 2006
// ALL RIGHTS RESERVED.
// Confidential and Proprietary
//
// original: chris fortescue
//

#include <iostream>
#include <iomanip>
#include "symbol.h"
#include "bigE.h"
#include "barcode.h"
#include <math.h>
#include "pdf417rs.h"
#include "reed_solomon.h"
#include <vector>
#include <deque>
#include "bigETree.hpp"
#include <algorithm>
#include <boost/foreach.hpp>
#include <boost/lambda/lambda.hpp> 
#include <boost/lambda/bind.hpp>
#include <boost/lambda/algorithm.hpp> 
#include <boost/lambda/if.hpp> 
#include "statistics.h"
#include <boost/function_output_iterator.hpp>
#include "util.hpp"


#ifdef WIN32
#pragma warning(disable:4244) // disable float to int warnings
#endif
using std::ios_base;

int symbol::dlevel=0;

void 
barcode_filter::setCW(symbol &thesymbol, int trow,int tcol, int val)
{
    thesymbol.setCW(trow,tcol,val);
}
void 
barcode_filter::setRSError(symbol &thesymbol, int rez)
{
    thesymbol.setRSError(rez);
}
void 
barcode_filter::addRows(symbol &thesymbol, int delta)
{
    thesymbol.rows += delta;
}
void 
barcode_filter::addColumns(symbol &thesymbol, int delta)
{
    thesymbol.cols += delta;
}

void
symbol::copy(const symbol &cp)
{
    seenStart=cp.seenStart;
    seenStop=cp.seenStop;
    firstFull = cp.firstFull;
    lastFull=cp.lastFull;
    history= cp.history;
    stdCols = cp.stdCols;
    rows=cp.rows;
    cols=cp.cols;
    sec_level = cp.sec_level;
    _lastWidth = cp._lastWidth;
    _modWidth = cp._modWidth;
    symcount = cp.symcount;
    dataCW=cp.dataCW;
    padCW=cp.padCW;
    errorCW=cp.errorCW;
    _skew=cp._skew;
    mxWindow = cp.mxWindow;
    defaultModWidth = cp.defaultModWidth;

    for (int ii=0;ii<91;ii++)
        for (int jj=0;jj<40;jj++)
            raw_matrix[ii][jj] = cp.raw_matrix[ii][jj];

    rs_errors_corrected = cp.rs_errors_corrected;
    rowVotes= cp.rowVotes;
    colVotes = cp.colVotes;
    secVotes = cp.secVotes;
    outputs=cp.outputs;
    modErrorMin = cp.modErrorMin;
}

void symbol::successful(bigE &sym)

{  
    // measure success *starting* with a stop or start symbol because
    // the data gets used after that
//   if (symcount == 0 &&  SYMBOL_OK(sym) && sym.symbol_type != START && sym.symbol_type != STOP)
//     return 0;

    if (symcount == 0 ) 
    {
        if (!SYMBOL_OK(sym))
            // just don't measure until we have seen a stop or start.  don't throw
            return;
        else if (sym.width() > (MAX_PIXELS_PER_MODULE * 17) )
            throw FirstTooLong();
        //throw UnknownRow();
    }
 
    if (!seenStop && sym.symbol_type==STOP)
        seenStop = true;
    else if (!seenStart && sym.symbol_type == START)
        seenStart = true;
    float thisSymWidth=0.0;
    float thisModWidth=0.0;
    if (sym.symbol_type == STOP)
    {
        // stop symbols are the only symbol that is 18 long so adjust it to 17 
        thisModWidth = sym.width()/18;
        thisSymWidth = sym.width() - thisModWidth;
    }
    else 
    {
        // all other symbols are 17 modules
        thisModWidth = sym.width()/17;
        thisSymWidth = sym.width();
    }

    _modWidth  = symcount==0 ? thisModWidth : (symcount*_modWidth + thisModWidth)/(symcount+1);
    _lastWidth = symcount==0 ? thisSymWidth : (symcount*_lastWidth + thisSymWidth)/(symcount+1) ;

    symcount++;
}

#define CMP_NOMATCH 0
int
symbol::bwdCompare(const bigE &A, const bigE &B)
{
    if (!SYMBOL_OK(A) || !SYMBOL_OK(B)) // || A.cluster == B.cluster)
		return CMP_NOMATCH;
    //throw NoMatch();
    return bwdCompare(A.cluster,B.cluster);
}
int
symbol::bwdCompare(int K, int cluster)
{
    switch (K)
    {
    default: break;
    case 0:
        switch (cluster)
        {
        case 6: return -1; break;
        case 3: return -2; break;
        default: break;
        }
        break;
    case 3:
        switch (cluster)
        {
        case 0: return -1; break;
        case 6: return -2; break;
        default: break;
        }
        break;
        
    case 6:
        switch (cluster)
        {
        case 3: return -1; break;
        case 0: return -2; break;
        default: break;
        }
        break;
    };
	return CMP_NOMATCH;
    //throw NoMatch();
}

int 
symbol::fwdCompare(const bigE &A, const bigE &B)
{
    if (!SYMBOL_OK(A) || !SYMBOL_OK(B) ) 
		return CMP_NOMATCH;
    //throw NoMatch();
 
    return fwdCompare(A.cluster, B.cluster);
}

int 
symbol::fwdCompare(int K, int cluster)
{   
 
    switch (K)
    {
    default: break;
    case 0:
        switch (cluster)
        {
        case 3: return 1 ; break;
        case 6: return 2; break;
        default: break;
        }
        break;
    case 3:
        switch (cluster)
        {
        case 6: return 1; break;
        case 0: return 2; break;
        default: break;
        }
        break;
    case 6:
        switch (cluster)
        {
        case 0: return 1 ; break;
        case 3: return 2; break;
        default: break;
        }
        break;
    };

    //throw NoMatch();
	return CMP_NOMATCH;
}
   

int 
symbol::symbolCount(Row &symbol_row, int type)
{
    int tot[BAD+1];

    for (unsigned  ii=0;ii< (sizeof(tot)/sizeof(int));ii++)
        tot[ii]=0;
    for (unsigned int ii=0;ii< symbol_row.size();ii++)
    {
        bigE & b = *symbol_row[ii];
        tot[b.symbol_type]++;

    }
    int ret=0;
    for (int ii = static_cast<int>(QUIET) ; ii <= static_cast<int>(BAD); ii++)
    {
        if (type & (1 << ii))
            ret += tot[ii];
    }
    return ret;
}

void
symbol::processScanRow(Bitmap &bm, int row, int min_x, int max_x, Row & symbol_calc)
{
    bool pendingFullSymbol=false;    
    State cur = QUIET;
    cur = QUIET;
    
//    State last = cur;
    pixTrend trend=WHITE;
    //const Bitmap::cont_type & bits = bm.ref();    
	Bitmap & bits = bm;
    
    bigE sym;

    // rows start quietly with space
    
    int quietPix=0;
    int endQuiet=-1;
    int totSyms=0;
    
    for (int col=min_x ; col < max_x; col++)
    {
        switch(cur)
        {
        case QUIET:
            if (bits[row][col] && quietPix > 0)
            {
                if (dlevel>= 1)
                    std::cout << "QUIET ends at "<< row << "," << col  << std::endl; 
                cur = START;
                trend=BLACK;
                sym.start(col);
                quietPix=0;
                endQuiet = col;
                // fall through
            }
            else {
                ++quietPix;
                break;
            }
        case START:

        case LEFT_ROW:
        case SYMBOL:
        case RIGHT_ROW:
        case STOP:

        {
                
            // try {
            //    try {
            //    catch TooShort
            //    catch FullSymbol
            //      try {
            //      catch WrongState
            //      catch WeirdWidth
            //      catch BadCodeWord
            //      catch Rowdone
            //      catch ...
            //    catch WrongState
            //    catch ...
            // 
            // catch ...
            try {
                try {
                    if (pendingFullSymbol)
                    {
                        if (dlevel >= 1)
                            std::cout<<"pendingFullSymbol @" << col << std::endl;
                        //pendingFullSymbol=false;
                        throw FullSymbol();
                    }
                    else if (trend== WHITE && bits[row][col]) 
                    {
                        sym.newState(*this,col);
                        trend = BLACK;
                    }
                    else if (trend == BLACK && !bits[row][col])
                    {
                        sym.newState(*this,col);
                        trend = WHITE;
                    }
                    else if ( (col+1) == max_x)
                    {
                        throw FullSymbol();
                    }
                }
                catch (TooShort &)
                {
                    // keep rolling
                    if (dlevel >= 2)
                        std::cout << "Caught TooShort (row,col) @["<< row << "," << col  << "] trend:" << trend <<  " delta: " 
                                  << (col - sym.lastMark()) 
                                  << " overall symbol width: " << (col - sym.e[e1] )
                                  << " state=" << sym.current_integral_module
                                  << std::endl;
                        
                    if (sym.current_integral_module>e1)
                    {
                            
                        if (sym.current_integral_module == e8 &&
                            (this->symWidth() && ( (( fabs(col-sym.e[e1] - this->symWidth())) /this->symWidth()) <= SYMBOL_WIDTH_ERROR) ))
                        {
                            // tooshort when we are one bar from full
                            col--;
                            pendingFullSymbol=true;
                            sym.e[sym.current_integral_module=e9]=col;
                                
                            continue;
                        }
                        if ( (col - sym.lastMark())>1)
                        {
                            // the trend got switched and we are ignoring the bit
                            // that caused the trend switch.  This only happens if
                            // the more than one bit in this stretch has been collected.
                            // 
                        }
                        // look ahead to see if the change is big and justifies 
                        // a switch of just a jaggie

                        int sz = col;
                        while ( sz < max_x && 
                                (
                                    (trend == BLACK &&  !bits[row][sz])
                                    || (trend == WHITE &&  bits[row][sz]) 
                                    ) 
                            )
                            sz++;
                        ;
                        float wd = this->modWidth();
                        if ( wd && static_cast<float>(sz - col)/wd < .5) 
                        {
                            // if it less than half a symbol, consider it noise and continue
                            col = sz;
                        }
                        else
                        {
                            // otherwise, consider the current state noise and roll back to the previous state
                            // roll back
                            sym.e[sym.current_integral_module]=0;
                            trend = trend==BLACK?WHITE:BLACK;
                            reinterpret_cast<int&>(sym.current_integral_module)--;
                            if (dlevel >= 2)
                            {
                                std::cout << " TooShort Adjustment New (row,col) @["
                                          << row << "," << col  << "] trend:" << trend <<  " delta: "
                                          << (col - sym.lastMark()) ;
                                std::cout << " e[" ;
                                    
                                for (unsigned int ei=1;ei < 10 ; ei++)
                                {
                                    std::cout  << sym.e[ei] <<  (ei+1 == 10 ? "] " : ",");
                                }
                                std::cout  << "new state=" << sym.current_integral_module  << std::endl;
                            }
                        }
                    }
                    sym._flags |= MOD_WAS_TOOSHORT;
                }
                catch (FullSymbol & /*f*/)
                {
                    bigE clean;
                    
                    try {
                        if (col+1 == max_x && sym.current_integral_module != e9)
                            throw RowDone();
                        sym.convertMeasurements(*this,row,col);
                        if (cur == START && sym.symbol_type != START)
                         
                            if (cur == START && sym.symbol_type != START)
                                cur = SYMBOL;
                        // need to put BAD symbols for alignment
                        this->successful(sym);

                        pendingFullSymbol=false;

                        totSyms++;

                        bigE_ptr sym_ptr (new bigE(sym));
                        symbol_calc.push_back(sym_ptr);
                        
                        if (sym.symbol_type == STOP)
                        {
                            col = max_x;
                            continue;
                        }
                        sym = clean;

                        if (cur == LEFT_ROW && sym.symbol_type == BAD)
                            cur = SYMBOL;
                        else
                            cur = 
                                (cur == QUIET ? START 
                                 : (cur == START ? LEFT_ROW 
                                    : (cur == LEFT_ROW ? SYMBOL
                                       : (cur == SYMBOL ? SYMBOL
                                          : (cur==RIGHT_ROW ? STOP : BAD))))) ;
                        
                        sym.symbol_type = cur;
                        sym.start(col);
                        trend = BLACK;
                    }
                    catch (FirstTooLong &)
                    {
                        // reset because we have yet to find a first symbol that makes
                        // sense.  text can look like a barcode...  but don't throw out
                        // the whole set we just read. just reorient on the 2nd bar

                        if (dlevel>=1)
                            std::cout << "Caught FirstTooLong (row,col) @(" << row << "," << col << ") resetting col to " << sym.e[3] << std::endl;
                        trend = BLACK;
                        col = sym.e[3];
                        sym = clean;
                        trend = BLACK;
                        sym.start(col);

                    }
                    catch (WrongState &)
                    {
                        cur = BAD;

                        if (dlevel>=1)
                            std::cout << "Caught WrongState (row,col) @(" << row << "," << col << ")" << std::endl;
                        if (dlevel>=1)
                            sym.dump();                        
                    }
                    catch (WeirdWidth &)
                    {
                        // TODO: make attempt to resync.  for now, just go on
                        sym._flags |= COL_WAS_TOOSHORT;
                        if (dlevel >= 2)
                            std::cout << "Caught WeirdWidth (row,col) @(" << row << "," << col <<") len=" << sym.width() << " trend " << trend << std::endl;
                        if (dlevel >= 7)
                        {
                            std::cout  << " dump follows:>>"<< std::endl;
                            sym.dump();                        
                        }
                        // pendingFullSymbol will only be set if we already tried to handle
                        // a weirdwidth.  so, if its set, something went wrong and we punt
                        if (pendingFullSymbol

                            || 
                            // or
                            // no symbols this row.  we've never seen a start symbol so there 
                            // is no guareentee that the first black we found is a symbol start.  so 
                            // use e[3] as a new start and rescan forward.
                            (this->seenStop && ! this->seenStart && !totSyms)
                            // overshot but we're not aligned yet
                            || ( sym.width() > this->symWidth() && totSyms < 5))
                        {
                            if (dlevel >= 2)
                            {
                                std::cout  << " spin cycle: still got a weirdo(fuckit!).  throwing the whole symbol out and using the next black space e[3]=" << sym.e[3] << std::endl;
                            }
                            pendingFullSymbol=false;
                            col = sym.e[3];
                            sym = clean;
                            trend = BLACK;
                            sym.start(col);
                            continue;
                        }
                        else if (sym.width() < this->symWidth())
                        {
                            // suck up another pair
                            // but leave the scan line in the same spot.  in other words, ignore the last pair of bar space.
                            // and keep going
                            trend=WHITE;
                            sym.current_integral_module = e6;
                            col--;
                        }
                        else if (sym.width() > this->symWidth())
                        {

                            // ok we overshot.  did we throw out a small module
                            int changes=0;
                            int mins[9];
                            int ttrend=BLACK;
                            mins[0] = sym.e[1]; 
                            // this->symWidth()+1
                            for (int sc=sym.e[1]; sc <= sym.e[9]  ; sc++) 
                            {
                                if ( (ttrend== WHITE && bits[row][sc])
                                     || (ttrend == BLACK && !bits[row][sc]))
                                {
                                    mins[++changes]=sc;
                                    ttrend = (ttrend==BLACK?WHITE:BLACK);
                                }
                                if (changes==8) break;
                            }
                            // now check whether we threw out any small module by comparing what we have to the typical.
                            int s1= mins[8]-mins[0];
                            if ( fabs((this->symWidth() -s1)/this->symWidth() ) <= SYMBOL_WIDTH_ERROR)
                            {
                                // we did throw out one, so now calibrate
                                // using the small bits by converting them to at least the
                                // normal modWidth.  All the while
                                int newbits[9];
                                col=mins[8];
                                newbits[0] = mins[0];
                                pendingFullSymbol=true;
                                for (int ii=1;ii<9;ii++)
                                {
//                                        int msr = mins[ii]-mins[ii-1];
                                    int msr = mins[ii]-newbits[ii-1];
                                    int mods = msr / ((int) rint(this->modWidth()));
                                    int bits = msr % ((int) rint(this->modWidth()));
                                    if ( ( bits/this->modWidth()) >= .5)
                                    {
                                        mods++;
                                    }
                                    else if (mods==0) mods=1;

                                    newbits[ii]= newbits[ii-1] + ((int) rint(mods * this->modWidth()));
                                }
                                for (int ii=1;ii<=9;ii++)
                                    sym.e[ii]=newbits[ii-1];
                                if (fabs((this->symWidth()-sym.width())/this->symWidth()) <= SYMBOL_WIDTH_ERROR)
                                {
                                    for (int ii=sym.e[9]; !bits[row][ii];ii++) col = ii;
                                    col--;
                                }
                                else if (sym.width() < this->symWidth())
                                {
                                        
                                    while (fabs((this->symWidth()-sym.width())/this->symWidth()) > SYMBOL_WIDTH_ERROR)
                                        // this is bogus but *must* be done to prevent the rest of the code from 
                                        // looping when it re-evaluates the symbol and finds it too short, throws WeirdWeidth
                                        sym.e[9]++;

                                    // yes, i forced 
                                    int start = sym.e[9];
                                    for ( ;!bits[row][start]; start++);

                                    col = start;
                                    if (dlevel>=2)
                                    {                       
                                        std::cout << "WeirdWidth (row,col) @(" << row << "," << col <<") toobig stretched =" << sym.width()  << std::endl;

                                        sym.dump();
                                    }
                                        
                                }
                                trend=BLACK;
                                continue;
                            }
                            else 
                            {
                                // guess at where to place the stream then
                                // call the symbol bad.
                                // too long.  we need to re-align on the previous bar.
                                int last=1;
                                int delta=99999;
                                int targ = sym.e[1] + (int) rint(this->symWidth());
                                // zero in on bar closest to the overall symbol width

//                                 for (int ii=3;ii<=9; ii+=2)
//                                 {
//                                     if (sym.e[ii] < targ) 
//                                         last = ii; 
//                                 }

                                for (int ii=9;ii>=3; ii-=2)
                                {
                                    if (abs( sym.e[ii] -  targ) < delta)
                                    { 
                                        last = ii; 
                                        delta = abs( sym.e[ii] -  targ);
                                    }
                                }

                                if (last >= 3)
                                {
                                    // one last look at which is closer to the symbol width
                                    if (abs(sym.e[last] - targ) < abs(sym.e[last+2] - targ))
                                        sym.e[9] = sym.e[last];
                                    else
                                        sym.e[9] = sym.e[last+2];
                                }
                                // else the original e[9] is closest
                                col = sym.e[9];
                                if (dlevel>=2)
                                {
                                    std::cout << "weirdwidth: way too big. weird col=" << col << std::endl;
                                    sym.dump();
                                }

//                                State old=sym.symbol_type;
                                // mark it as bad
                                sym.symbol_type = BAD;
                                // push it!

                                bigE_ptr sym_ptr (new bigE(sym));
                                symbol_calc.push_back(sym_ptr);

                                // make a clean symbol
                                sym = clean;
                                // initialize the new symbol on the new boundary plucked from the old symbol
//                                sym.symbol_type = old;
// actually had to change this because I had a row with a bad left row column that
// falsely marked 2 rows in as the left row marker.
                                cur = 
                                    (cur == QUIET ? START 
                                     : (cur == START ? LEFT_ROW 
                                        : (cur == LEFT_ROW ? SYMBOL
                                           : (cur == SYMBOL ? SYMBOL
                                              : (cur==RIGHT_ROW ? STOP : BAD))))) ;
                                sym.symbol_type = cur;

                                // minus 1 because e[7] is the start
                                // of a bar and the for loop will ++ before handling
                                // the boundary.                                
                                sym.start(col--);
                                // and set the trend to BLACK since we've just
                                // initialized a new symbol on this
                                trend=BLACK;
                            }
                        }
                    }
                    catch (BadCodeWord & /*foo*/)
                    {
                        pendingFullSymbol=false;

                        if (dlevel>=1)
                        {
                            std::cout << "Caught BadCodeWord (row,col) @(" << row <<"," << col << ") symbol state=" << cur << std::endl;
                        }
                        if (dlevel>=2)
                        {
                            std::cout << ">>>" << std::endl;
                            sym.dump();
                            std::cout << "<<< " << std::endl;
                        }

                        if (this->seenStop && ! this->seenStart && !totSyms)
                        {
                            if (dlevel >= 2)
                            {
                                std::cout  << " spin cycle2: badcodeword, no start, probably bad monkey. re-positioning to e[3]=" << sym.e[3] << std::endl;
                            }
                            col = sym.e[3];
                            sym = clean;
                            trend = BLACK;
                            sym.start(col);
                            continue;
                        }
                        else if (cur == START )
                        {
                            // bad symbol when we are looking for a start symbol
                            quietPix=0;
//                                cur = QUIET;
                            if (!this->history.size() || !this->symWidth()) // ! SYMBOL_OK( (history[history.size()-1])[0] ))
                            {
                                // we have no history
                                // so inch along the first row until we find something
                                if (sym.e[3] > sym.e[1])
                                    col = sym.e[3];
                                sym = clean;
                                trend = BLACK;
                                sym.start(col);
                                continue;
                            }
                        }
                        else if (cur == SYMBOL)
                        {
                            if (this->modWidth() && this->symWidth())
                            {
                                // see if we can't 
                            }
                        }
                        // mark it as bad
                        sym.symbol_type = BAD;
                        // push it!
                        bigE_ptr sym_ptr (new bigE(sym));
                        symbol_calc.push_back(sym_ptr);

                        // make a clean symbol
                        sym = clean;
                        cur = 
                            (cur == QUIET ? START 
                             : (cur == START ? LEFT_ROW 
                                : (cur == LEFT_ROW ? SYMBOL
                                   : (cur == SYMBOL ? SYMBOL
                                      : (cur==RIGHT_ROW ? STOP : BAD))))) ;
                        sym.symbol_type = cur;
                        //sym.symbol_type = old;
                        // initialize the new symbol on the new boundary plucked from the old symbol
                        sym.start(col);
                        // this symbol is on a black boundary
                        trend = BLACK;
                    }
                    catch (RowDone &)
                    {
                        // probably a short row...  But we want to process if is symbol_calc has good symbols.
                        if (dlevel>1)
                            std::cout << "Caught RowDone @row,col=" << row <<"," << col  << " with incomplete symbol " << symbol_calc.size() << std::endl;
                    }
                    catch (UnknownRow &)
                    {
                        if (dlevel>1)
                            std::cout << "Caught Unknown @row,col=" << row <<"," << col  << " " 
                                      << " symcount=" << symcount << ". sym.e[9]=" << sym.e[9] << " ignoring symbol til START/STOP" << std::endl;
                        cur = QUIET;
                        trend=WHITE;
                        sym = clean;
                    }

                    catch (...)
                    { 
						if( dlevel > 0)
				            std::cout << "1. Caught unknown exception in strange place @row,col=" << row <<","<< col << std::endl;
                    }
                }
                catch (WrongState &)
                {
					if( dlevel > 0)
			            std::cout << "Caught WrongState at @row,col[" << row << "," << col << "]" << std::endl;
                }
                catch (...)
                {
					if( dlevel > 0)
		                std::cout << "2. Caught unknown execption at @row,col[" << row << "," << col << "]" << std::endl;
                    col=max_x;
                }
            }
            catch (...)
            { 
				if( dlevel > 0)
	                std::cout << "3. Caught unknown exception in strange place symbol @row,col=" << row <<"," << col  << std::endl;
            }
            break;
        }
        default:
        case BAD:
			if( dlevel > 0)
                std::cout << "Detected BAD state in @row,col=(" << row << "," << col << ")" << std::endl;
			break;
        };
    }
    // just cracks row indicators
    analyzeRow(symbol_calc,row);


}
//#define TRACKMEM 
void
symbol::pdf417_clf(SmartPointer<symbol> & symArg, Bitmap &bm, Geometry &box, int mxWindow, double defaultModWidth)
{
    int min_x = box.xOff();
    int min_y = box.yOff();
    int max_x = min_x +  box.width();
    int max_y = min_y + box.height();


	if( dlevel > 0)
	    std::cout << "pdf417_clf : minx="<<min_x << " min_y=" << min_y << " max_x=" << max_x << " max_y=" << max_y << std::endl;    

    if (min_x < 0 || min_y<0 || max_x < 0 || max_y < 0 || min_x >= max_x || min_y>=max_y)
        throw BadDimensions();

    double idealModWidth=defaultModWidth;
    double idealModError=0.65;
    int idealRow=0;
    int idealCol=0;
    int totMatches=0;
    int quickness=max_y-min_y;

    // try to pre-determine the module width of this bar.
    // this is exhaustive but attempts to short circuit if something reasonable is found
    // each row has a 1 possible START, and one possible STOP.
    // so if the quickness is 16 and cnt==30, that means that quickness * 2 = 32 possible START/STOPS
    
    {
        int save = symbol::dlevel;
        int save2 = bigE::dlevel;
        symbol::dlevel = 0;
        bigE::dlevel=0;
        double widths[]={ defaultModWidth,defaultModWidth+1,defaultModWidth-1,defaultModWidth+2,defaultModWidth-2,-1};
        double dialations[]={0.65,0.60,-1.0};
        bool best=false;

        for (int zz=0; widths[zz] >0; zz++)
            for (int yy=0; !best && dialations[yy] != -1; yy++)
            {
                int width=(int)widths[zz];
                symbol * idealBar = new symbol;
                idealBar->setDefaultModuleWidth(width);
                idealBar->setModuleErrorMin(dialations[yy]);

                SmartPointer<symbol> sym(idealBar);

                int firstRow=0;
                int cnt=0;
                int startCount=0;

                double modWidth=0.0;
                float firstCol=0.0;
                int row=min_y;
                Row save;
                for ( ; (row < max_y && cnt < 30 &&  (!firstRow || (row - firstRow) < quickness)); row++)
                {        

                    if (idealRow && !firstRow && (row-10) > idealRow)
                        break;
                    // stack of symbols encountered
                    Row symbol_calc;
                    int howmany = 0;
                    try 
                    {
                        sym.Reference().processScanRow(bm, row, min_x, max_x, symbol_calc);
                        try {
                            bigE &b = *symbol_calc[find(symbol_calc, START)];
                            float sz = b.e[9] - b.e[1];
                            modWidth = ((modWidth * cnt) + (sz/17.0))/(cnt+1);
                            firstCol = ((firstCol * startCount) + b.e[1])/(startCount+1);
                            cnt++;
                            startCount++;
                        
                            if (!firstRow)
                                firstRow = row;
                        }
                        catch (NoMatch &){}
                        try {
                            bigE &b = *symbol_calc[find(symbol_calc, STOP)];
                            float sz = b.e[9] - b.e[1];
                            modWidth = ((modWidth * cnt) + (sz/18.0))/(cnt+1);
                            cnt++;
                            if (!firstRow)
                                firstRow = row;
                        }
                        catch (NoMatch &){}
                        howmany = symbolCount(symbol_calc,F_LEFTROW | F_RIGHTROW | F_SYMBOL|F_START|F_STOP);
                        if (howmany > 0)
                            std::copy(symbol_calc.begin(),symbol_calc.end(),std::back_inserter(save));
                    }
                    catch (UnknownRow &)
                    {
                        if( dlevel > 0)
                            std::cout << "UnknownRow: > " << row << ". " << symbol_calc.size() << " symbols ignored" << std::endl;

                    }
                    catch (...)
                    {
                        if( dlevel > 0)
                            std::cout << "Unknown Exception processing > " << row << ". " << symbol_calc.size() << " symbols ignored" << std::endl;

                        continue;
                    }
                }
                if( dlevel > 0)
                    std::cout << "Iters: modWidth=" << width << " modError=" << dialations[yy] << " last row processed=" << row << " first Start/Stop=" << firstRow 
                              << "total Start/Stops=" << cnt << " pass quickness=" << (firstRow ? (row-firstRow) : -1) << " starts=" << startCount << std::endl;

                if ( cnt >= 30 && (row - firstRow) < quickness)
                {
                    quickness = (row - firstRow);
                
                    totMatches = cnt;
                    idealModWidth = modWidth;
                    idealModError = dialations[yy];
                    idealRow = firstRow;
                    idealCol = (startCount ? (int) firstCol   : min_x);

                    int curRow=0;
 
                    if( dlevel > 0)
                    std::cout << "best> (width, matches, idealRow ,firstCol, startCount, lastRow )=" << modWidth <<","<< totMatches <<"," << idealRow << "," << min_x << "," << startCount<< "," << row << std::endl;

                    if (  cnt/(quickness *2.0) >= .9)
                    {

                        best=true;
                        row = max_y;
                    }
                    // correct for rows we found *before* we found good ones as long as their mod widths were good. This allows us to 
                    // 
                    for (Row::iterator iter = save.begin(), theEnd=save.end(); iter != theEnd; iter++)
                    {
                        float curModWidth = (*iter)->p/17.0 ;
                        if (fabs(curModWidth - idealModWidth)/idealModWidth <= SYMBOL_WIDTH_ERROR)
                        {
                            firstRow = (*iter)->e[0];
                            idealRow = firstRow;
                            quickness = (row - firstRow);
                            break;
                        }
                    }
                }
            }
        symbol::dlevel = save;
        bigE::dlevel =save2;
    }
    if( dlevel > 0)
        std::cout << "Precalulated ideal width " << idealModWidth << " idealModError=" << idealModError << " quickness=" << quickness << " @row " << idealRow << ". " << totMatches << " START/STOP symbols"<< std::endl;

    symbol * allocBar = new symbol;
    if( ! allocBar){
        return;
    }
    // set the ideal module width based on what we learned
    allocBar->setDefaultModuleWidth(idealModWidth);
    allocBar->setModuleErrorMin(idealModError);
    if (idealCol)
        min_x= idealCol;

    // might as well start with the first good row we found
    if (idealRow)
        min_y = idealRow;

   SmartPointer<symbol> sym(allocBar);
   
	// assign the pointer to the passed in arg to bump the ref count.
	// this allows the calling function to use the symbol class object 
	// when this function throw an exception  which without this assignment would
	// delete the object when it loses scope
	symArg = sym;
    symbol & thebar = sym.Reference();
    thebar.setMaxInspectWindow(mxWindow);

#ifdef TRACKMEM
	_CrtMemState Before,After,Diff;	
	_CrtMemCheckpoint( &Before );
#endif

    int badRow=0;
    int foundRow=0;

    for (int row =min_y; row < max_y ; row++)
    {
        // badRows setting is arbitrary
        if (foundRow && badRow>=5)
            break;

        // stack of symbols encountered
        Row symbol_calc;
        int howmany = 0;

        try {

            thebar.processScanRow(bm, row, min_x, max_x, symbol_calc);
            howmany = symbolCount(symbol_calc,F_LEFTROW | F_RIGHTROW | F_SYMBOL|F_START|F_STOP);
            if (howmany>0)
            {
                badRow=0;
                foundRow++;
            }
        }
        catch (UnknownRow &)
        {

            howmany = symbolCount(symbol_calc,F_LEFTROW | F_RIGHTROW | F_SYMBOL|F_START|F_STOP);
            if (dlevel>=1)
                std::cout << "Caught UnknownRow @row" << row << " with " << howmany << " good symbols" << std::endl;
            if ( howmany >1) 
            {
                thebar.history.push_back(symbol_calc);
            }

        }
        catch (...)
        {
            if (dlevel>=2)
                std::cout << "bad row @" << row << std::endl;
            continue;
        }

        if (howmany>0)
            thebar.history.push_back(symbol_calc);
        else
            badRow++;

    }



    if (!thebar.good())
        throw BadBar();
#ifdef TRACKMEM
    _CrtMemCheckpoint( &After );
    if (_CrtMemDifference( &Diff,&Before,&After)){
        _CrtMemDumpStatistics(&Diff);
    }
    _CrtMemCheckpoint( &Before);
#endif

    // determine zones for columns
    thebar.computeColumnAvgs();
#ifdef TRACKMEM
    _CrtMemCheckpoint( &After );
    if (_CrtMemDifference( &Diff,&Before,&After)){
        _CrtMemDumpStatistics(&Diff);
        _CrtMemDumpAllObjectsSince(&Before);
    }
    _CrtMemCheckpoint( &Before);
#endif
    // calculate confidence/rows
    thebar.organizeMatrix();
//    thebar.processDelayedRows(history);
#ifdef TRACKMEM
    _CrtMemCheckpoint( &After );
    if (_CrtMemDifference( &Diff,&Before,&After)){
        _CrtMemDumpStatistics(&Diff);
        _CrtMemDumpAllObjectsSince(&Before);
    }
    _CrtMemCheckpoint( &Before);
#endif
    
    if (!thebar.rows)
    {
        std::cout << "No rows detected from row indicators but surmizing: " << thebar.history.size() << " rows." << std::endl;
        thebar.rows= thebar.history.size();
    }
    if (!thebar.cols)
    {
        float av=0.0;
        int amt=0;
        for (Table::iterator iter = thebar.history.begin(); iter != thebar.history.end();iter++)
        {
            Row &item = (*iter);
            amt++;
            av = (((amt-1) * av) + (item.size() - 4))/amt;
        }
        thebar.cols = (int) round(av);
        std::cout << "No columns detected from row indicators but surmizing: " << av << " cols." << std::endl;
    }
    if (!thebar.dataCW)
    {
        if (thebar.sec_level>=0)
     
            thebar.dataCW= (thebar.rows * thebar.cols) - (( 1 << thebar.sec_level) *2);
     
        else
            // lost hope
            thebar.dataCW = (thebar.rows * thebar.cols);
        // the first element is whacked so build a sane enough one for error correction to fix it.
        if (thebar.getCW(1,1) == -1)
            thebar.setCW(1,1,thebar.dataCW);
    }
    if (!thebar.errorCW)
    {
        if (thebar.sec_level>=0)
            thebar.errorCW=(1 << thebar.sec_level) * 2;
        else
            thebar.errorCW=0;
    }
#ifdef TRACKMEM
    _CrtMemCheckpoint( &After );
    if (_CrtMemDifference( &Diff,&Before,&After)){
        _CrtMemDumpStatistics(&Diff);
        _CrtMemDumpAllObjectsSince(&Before);
    }
    _CrtMemCheckpoint( &Before);
#endif

    if (dlevel>=1)
        thebar.result();

    if (thebar.getCW(1,1) != -1 )
        thebar.dataCW = thebar.getCW(1,1);
 
    reed_solomon fil;
#ifdef TRACKMEM
    _CrtMemCheckpoint( &After );
    if (_CrtMemDifference( &Diff,&Before,&After)){
        _CrtMemDumpStatistics(&Diff);
        _CrtMemDumpAllObjectsSince(&Before);
    }
    _CrtMemCheckpoint( &Before);
#endif
    thebar.process(fil);
#ifdef TRACKMEM
    _CrtMemCheckpoint( &After );
    if (_CrtMemDifference( &Diff,&Before,&After)){
        _CrtMemDumpStatistics(&Diff);
        _CrtMemDumpAllObjectsSince(&Before);
    }
#endif


    
}

bool
symbol::good()
{
//    return  history.size() && (firstFull != lastFull) && ((firstFull!= 0) || (getColumns() > 0));
  return  history.size() &&  getColumns() > 0;
}

void
symbol::analyzeRow(Row &symbol_row, int )
{
        
    int sz = symbol_row.size(); 
    int curCluster=-1;
    int curRow=-1;

    if (sz < 2) 
        throw UnknownRow(); 

    int good= symbolCount(symbol_row,F_START | F_STOP | F_LEFTROW | F_RIGHTROW | F_SYMBOL);
    
    int startPos = -1;
    try{ startPos =  find(symbol_row,START);  }
    catch (NoMatch &){}

    int stopPos = -1;
    try { stopPos = find(symbol_row,STOP); } catch(NoMatch &){}

    if (startPos ==-1 && stopPos==-1) 
        return;
        //throw UnknownRow();

    int rightPos=-1;
    int leftPos=-1;

    if (startPos != -1)
    {
        bigE &start = *symbol_row[startPos];

        if (SYMBOL_OK(start) && SYMBOL_OK( (*symbol_row[startPos + 1])) 
            && start.e[9] == (*symbol_row[startPos + 1]).e[1])
        {
            // edges touch
            (*symbol_row[startPos + 1]).symbol_type = LEFT_ROW;
            try {
                processRowIndicator( *symbol_row[startPos + 1]);
                if ( (*symbol_row[startPos + 1])._row > 0)
                    leftPos = 1;
            }
            catch (InvalidRowIndicator &)
            {}
        }
    }
    if (stopPos != -1)
    {
        bigE &stop = *symbol_row[stopPos];
        if (SYMBOL_OK(stop)  && SYMBOL_OK( (*symbol_row[stopPos-1]) )   && stop.e[1] == (*symbol_row[stopPos-1]).e[9])
        {
            // edges touch
            (*symbol_row[stopPos -1]).symbol_type = RIGHT_ROW;
            try {
                processRowIndicator(*symbol_row[stopPos-1]);
                if ( (*symbol_row[stopPos-1])._row > 0)
                    rightPos = stopPos-1;
            }
            catch (InvalidRowIndicator &)
            {}
        }
    }
    if (sz < 5 
        && !symbolCount(symbol_row,F_START|F_STOP) 
        && !symbolCount(symbol_row,F_LEFTROW|F_RIGHTROW ))   
        throw UnknownRow();

    if (leftPos>0 && rightPos > 0)
    {
        if ((*symbol_row[leftPos])._row > (*symbol_row[rightPos])._row)
            _skew--;
        else if ((*symbol_row[leftPos])._row < (*symbol_row[rightPos])._row)
            _skew++;
    }
}




void
symbol::computeColumnAvgs()
{
    int mxRow=0;
    int mxLen=0;
//    float avgStart=0.0;
//    float avgEnd=0.0;
    
    stdCols.resize(getColumns() + 4);
    
    
    float goodStart=0.0;
    float goodStop=0.0;    
    for (size_t ii=0; ii< history.size();ii++)
    {
        int howmany = symbolCount(history[ii],F_START|F_STOP);
        if (!howmany) continue;

        for (size_t jj=0;jj<history[ii].size();jj++)
        {
            bigE &b = *history[ii][jj];
            if (!SYMBOL_OK(b)) continue;
            if (b.symbol_type == START)
            {
                if (dlevel>1)
                    printf ("computeColumns START  : %d,%d\n", b.e[1],b.e[9]);

                stdCols[0].start = (goodStart * stdCols[0].start)/(goodStart+1)  + b.e[1]/(goodStart+1);
                stdCols[0].stop =  (goodStart * stdCols[0].stop)/(goodStart+1) + b.e[9]/(goodStart+1);
                goodStart++;
            }
            else if (b.symbol_type == STOP)
            {
                int pos = stdCols.size()-1;
                if (dlevel>1)
                    printf ("computeColumns STOP  : %d,%d\n", b.e[1],b.e[9]);
  
                stdCols[pos].start = (goodStop * stdCols[pos].start)/(goodStop+1)  + b.e[1]/(goodStop+1);
                stdCols[pos].stop =  (goodStop * stdCols[pos].stop)/(goodStop+1)  + b.e[9]/(goodStop+1);
                goodStop++;
            }
        }
    }
    if (goodStart >= goodStop)
    {
        float last=stdCols[0].stop;
        for (size_t ii=1; ii < (stdCols.size()-1); ii++)
        {
            stdCols[ii].start = last;
            stdCols[ii].stop = last + symWidth();
            last += symWidth();
        }
    }
    else if (goodStart < goodStop)
    {
        int pos = stdCols.size()-1;
        float last=stdCols[pos].start;
        for (size_t ii=pos-1; ii > 0 ; ii--)
        {
            stdCols[ii].stop = last;
            stdCols[ii].start = last - symWidth();
            last -= symWidth();
        }
    }
    else 

    {
	
        for (size_t ii=0; ii< history.size();ii++)
        {
            if (!history[ii].size()) continue;
            if (firstFull == -1 && static_cast<int>(history[ii].size()) == (getColumns() +4))
                firstFull=ii;
	    
            if (static_cast<int>(history[ii].size()) == (getColumns() +4))
                lastFull = ii;
	    
            if (static_cast<int>(history[ii].size()) > mxLen)
            {
                mxLen = static_cast<int>(history[ii].size());
                mxRow = ii;
            }
        }
        if (lastFull==-1)
        {
            firstFull = mxRow;
            lastFull = static_cast<int>(history.size());
        }
	
	
        for (int jj=0; jj < (getColumns()+4); jj++)
        {
            float good=0;
            for (int ii=firstFull; ii < lastFull;ii++)
            {
                //            float cnt = ii-firstFull;
                if (!SYMBOL_OK( (*history[ii][jj]) ) ) continue;
                if (dlevel >=3)
                    std::cout << "(hist,scan) row=(" << ii <<","<< (*history[ii][jj]).e[0]  << "): stdCol[" << jj << "] start=" << stdCols[jj].start << " stop=" << stdCols[jj].stop << " good=" << good << " candidate=" << (*history[ii][jj]).e[1] << "," << (*history[ii][jj]).e[9] << " cw=" << (*history[ii][jj]).codeword() << std::endl;
		
                stdCols[jj].start = (good * stdCols[jj].start)/(good+1) + (*history[ii][jj]).e[1]/(good+1);
                stdCols[jj].stop = (good * stdCols[jj].stop/(good+1)) +  (*history[ii][jj]).e[9]/(good+1);
                good++;
            }
            if (dlevel >=2)
                std::cout << "stdCol[" << jj << "] start=" << stdCols[jj].start << " stop=" << stdCols[jj].stop << std::endl;
        }
	
        if (firstFull==-1 || lastFull==-1)
        {
            std::cout << "NO row has enough elements!" << std::endl;
        }
    }

    /*    
          for (size_t ii=0; ii < stdCols.size();ii++) 
          {
          if ( fabsf((stdCols[ii].stop - stdCols[ii].start) - symWidth()) > (.15 * symWidth()))
          {
          if (ii>0)
	      {
          if ( fabsf((stdCols[ii-1].stop - stdCols[ii-1].start) - symWidth()) < (.15 * symWidth()))
		  {
          stdCols[ii].start = stdCols[ii-1].stop + 2;
          stdCols[ii].stop = stdCols[ii].start + symWidth();
		  }
          else if ( fabsf((stdCols[ii+1].stop - stdCols[ii+1].start) - symWidth()) < (.15 * symWidth()))
		  {
          stdCols[ii].stop = stdCols[ii+1].start - 2;
          stdCols[ii].start = stdCols[ii].stop - symWidth();
		  }
          else 
		  {
          std::cerr << "stdColumns have out of range boundary " <<std::endl;
		  }
	      }
          }
          }
    */
    if (dlevel>1)
    {
        printf("Column   Start   Stop\n");
        for (unsigned ii=0; ii < stdCols.size();ii++) 
            printf("%u  %6.2f  %6.2f \n",ii,stdCols[ii].start,stdCols[ii].stop);
    }
}

int 
symbol::determineColumn(bigE &b)
{
    // need raw closeness for when the numbers are small
    const float rawcloseness= symWidth() *.1;

    for ( size_t ii = 0 ; ii <  stdCols.size();ii++)
    {
        float d1 = fabsf( b.e[1]- stdCols[ii].start ); 
        float d2 = fabsf( b.e[9]- stdCols[ii].stop  );

        if ( (d1  < rawcloseness ) &&  (d2  < rawcloseness))
            return ii;
    }
    // still here?
    for (size_t ii = 0 ; ii < stdCols.size(); ii++)
    {
	
        float x1 = stdCols[ii].stop -  b.e[1];
        float x2 = stdCols[ii].stop -  b.e[9];
        if (x1 > 0)
        {
            if (fabsf(x2) > x1) continue;
            else
                return ii;
        }
    }
	    
    return -1;
}

void
symbol::recalibrate()
{
    for(unsigned zz = 0; zz < history.size(); zz++)
    {
        Row &symbol_calc = history[zz];
        
        unsigned sz = symbol_calc.size();

        for (unsigned yy=0;yy < sz; yy++)
        {
            bigE &b= *symbol_calc[yy];
            //  if (b._flags & NEEDS_RECALIBRATION)
            if (b.codeword() < 0)
            {
                try {
                    b.re_analyze(*this);
                    if (b.codeword())
                    {
                        if (dlevel>=2)
                            std::cout << "successfully recalibrated scanRow " << b.e[0] << " codeword=" << b.codeword() << std::endl;
                    } 
                }
                catch (BadCodeWord &)
                {
                    
                }
            }
        }
    }
}

bigE_ptr
symbol::find(Row &symbol_row, int col)
{
    for (size_t ii=0; ii <symbol_row.size();ii++)
    {
        if (symbol_row[ii]->_column == col)
            return symbol_row[ii];
    }
    throw NoMatch();
}

int
symbol::find(Row &symbol_row, State symT)
{
    for (size_t ii=0; ii <symbol_row.size();ii++)
    {
        if (symbol_row[ii]->symbol_type  == symT)
            return ii;
    }
    throw NoMatch();
}


void
symbol::assignConfidence( int start)
{
    // backwards
    for(int firstGood= start; firstGood>=0; firstGood--)
    {
        Row &symbol_row = history[firstGood];
        assignConfidence(symbol_row,firstGood);
    }

    // forwards
    for(int firstGood= start; firstGood< static_cast<int>(history.size()); firstGood++)
    {
        Row &symbol_row = history[firstGood];
        assignConfidence(symbol_row,firstGood);
    }
    for (size_t ii=0; ii < history.size(); ii++)
    {
        for (size_t jj=0; jj < history[ii].size(); jj++)
        {
            if (SYMBOL_OK( (*history[ii][jj]) ) && history[ii][jj]->_column >=0)
            {
                for (int kk=ii-1; kk >= 0 ; kk--)
                {
                    try {
                        bigE_ptr up = find(history[kk],history[ii][jj]->_column);
                        
                        if (SYMBOL_OK( (*up)))
                        {
                            if ( history[ii][jj]->codeword() == up->codeword() && history[ii][jj]->cluster == up->cluster )
                                history[ii][jj]->_confidence +=3;
                            else 
                                break;
                        }
                    }
                    catch (...)
                    {
                    }
                }
                for (int kk=ii+1; kk < static_cast<int>(history.size()) ;kk++)
                {
                    try {
                        bigE_ptr down = find(history[kk],history[ii][jj]->_column);
                        if (SYMBOL_OK( (*down) ))
                        {
                            if (history[ii][jj]->codeword() == down->codeword() && history[ii][jj]->cluster == down->cluster )
                                history[ii][jj]->_confidence +=3;
                            else
                                break;
                        }
                    }
                    catch (...)
                    {
                    }
                }
            }
        }
    }
}

void symbol::assignConfidence(Row &symbol_row, int/* row */)
{
    int sz = symbol_row.size();         
    for (int ii = 0; ii < sz;ii++)
    {
        bigE &b = *symbol_row[ii] ; //(*iter);
        //if (b.hasError) continue;
        if (!SYMBOL_OK(b)) continue;
        
        switch(b.symbol_type)
        {
        default:
        case QUIET:
        case BAD:
        case START:
        case STOP:    
            break;
            
            
        case LEFT_ROW:
        case RIGHT_ROW:
        case SYMBOL:
        {
            // previous cluster and post cluster are the same as this and have valid codewords
            // then the highest confidence
            
            // if one of them is the same, then rank it medium
            
            // otherwise, its better than nothing but not by much
            if (b.cluster != 0 && b.cluster != 3 && b.cluster !=6)
                continue;
            
            int prevCluster = ii-1 < 0 ? b.cluster 
                : ( ((symbol_row[ii-1]->cluster % 3) == 0  && symbol_row[ii-1]->_codeword>0) ? symbol_row[ii-1]->cluster : -1);
            
            int postCluster = ii+1 >= sz ? b.cluster 
                : ( ((symbol_row[ii+1]->cluster % 3) == 0  && symbol_row[ii+1]->_codeword>0) ? symbol_row[ii+1]->cluster : -1);
            
            b._confidence = (b.hasError || b._codeword < 0 || (b.cluster % 3)) ? 0 
                : (b.cluster == prevCluster && b.cluster == postCluster? 6 
                   : ((b.cluster == prevCluster || b.cluster == postCluster)  ? 3 : 1));
            
            
            break;
        }
        }
    }
}

void
symbol::grindRowIndicators(State indicator)
{

    // double check row indicators make sense
    // 2 adjacent rows should not be separated by more than one logical row number
    // 
    for (unsigned ii=0; ii < history.size(); ii++)
    {
        int leftPos = -1;
      
        try {
	
            leftPos = find(history[ii],indicator);
            bigE &thisRow = *history[ii][leftPos];
            if ( SYMBOL_OK(thisRow) && thisRow._row>0)
            {
                if (ii>0)
                {
                    try{
                        int l2 = find(history[ii-1],indicator);
                        bigE &prev = *history[ii-1][l2];
                        if (SYMBOL_OK(prev) && abs(prev._row - thisRow._row) > 1 && abs(prev.e[0] - thisRow.e[0]) > 5 && prev.confidence() > thisRow.confidence())
                        {
                            if (dlevel>=1)
                                printf("grindRowIndicator indicator discarded %-6.1d  %-6.1d  %-6.1d  %-6.1d  %-8.1d %-8.1d %-8.1d\n",  
                                                    thisRow.e[0] , thisRow.e[1], thisRow._codeword, thisRow._row, thisRow._column, thisRow.cluster,thisRow._confidence);
                            thisRow._codeword = -1;
                            thisRow.cluster = -1;
                            thisRow.symbol_type = BAD;
                            continue;
                        }
                    } catch(...){}
                }
                if ( (ii+1) < history.size())
                {
                    try{
                        int l2 = find(history[ii+1],indicator);
                        bigE &next = *history[ii+1][l2];
                        if (SYMBOL_OK(next) && abs(next._row - thisRow._row) > 1  && abs(next.e[0] - thisRow.e[0]) > 5  && next.confidence() > thisRow.confidence())
                        {
                            if (dlevel>=1)
                                printf("grindRowIndicator indicator discarded %-6.1d  %-6.1d  %-6.1d  %-6.1d  %-8.1d %-8.1d %-8.1d\n",  
                                                    thisRow.e[0] , thisRow.e[1], thisRow._codeword, thisRow._row, thisRow._column, thisRow.cluster,thisRow._confidence);
                            thisRow._codeword = -1;
                            thisRow.cluster = -1;
                            
                            thisRow.symbol_type = BAD;
                            continue;
                        }
                    } catch(...){}
                }
                    
            }
        }
        catch(...){}
    }
}

void
symbol::verifyRowIndicator()
{
    // sanity check row indicators
    grindRowIndicators(LEFT_ROW);
    grindRowIndicators(RIGHT_ROW);

    if (dlevel>=2){
        std::cout << "rowindicators "  << std::endl;
        
        std::cout << "     phys(scan)                logical    " << std::endl; 
        std::cout << "   row     col     cw        row      column   cluster   confidence" << std::endl; 
        for (int ii = 0; ii < (int) history.size();ii++)
        {
            int leftRow=-1;
            int rightRow=-1;
            int leftPos=-1;
            int rightPos=-1;
            bigE_ptr l,r;
            try {
                leftPos = find(history[ii],LEFT_ROW);
                l = history[ii][leftPos];
                printf("L> %-6.1d  %-6.1d  %-6.1d  %-6.1d  %-8.1d %-8.1d %-8.1d\n",  l->e[0] , l->e[1], l->_codeword, l->_row, l->_column, l->cluster,l->_confidence);
                
            }
            catch(...){}
            try {
                rightPos = find(history[ii],RIGHT_ROW);
                r = history[ii][rightPos];
                printf("R> %-6.1d  %-6.1d  %-6.1d  %-6.1d  %-8.1d %-8.1d %-8.1d\n\n",  r->e[0] , r->e[1], r->_codeword, r->_row, r->_column, r->cluster,r->_confidence);

            }
            catch(...){}

        }
    }

    // this next test was caused by many rows of good right
    // row indicators with good codewords and high confidences but that
    // we're totally wrong when viewed consequtively row over row
    // in the case, referred to, 10 rows of right row indicators indicating
    // row 4 followed by row 18.  a barcode starting with row 4 followed by
    // row 2.

    checkRowIndicator(LEFT_ROW);
    checkRowIndicator(RIGHT_ROW);

    

}

void
symbol::checkRowIndicator(State indicator)
{
    std::vector<stats> l;
    for (unsigned ii=0; ii < history.size(); ii++)
    {
        int leftPos=-1;
        int rightPos=-1;
        bool leftGood= false;
        bool rightGood= false;
        
        
        try {
            leftPos = find(history[ii],indicator);
            bigE &leftRow = *history[ii][leftPos];
            if (SYMBOL_OK(leftRow) &&  leftRow._row != -1 )
            {
                if (l.empty())
                {
                    l.push_back(std::vector<stats>::value_type(stats(leftRow.codeword(),leftRow._row,ii,leftRow.confidence())));
                }
                else if (l[l.size()-1].cw == leftRow.codeword())
                {
                    l[l.size()-1].stop = ii-1 ; //leftRow.row();
                    l[l.size()-1].cw_confidence += leftRow.confidence();
                }
                else{
                    l.push_back(std::vector<stats>::value_type(stats(leftRow.codeword(),leftRow._row,ii,leftRow.confidence())));
                }
            }
//             else
//                 l.push(std::stack<stats>::value_type(stats()));
        }
        catch(...)
        {
//            l.push(std::stack<stats>::value_type(stats()));
        }
    }
    for (size_t ii = 0 ; ii < l.size() ; ii++)
    {
        // look forward
        int runconfid=0;
        for (size_t jj =ii+1 ; jj < l.size() ; jj++)
            if ((l[ii].row + (int)(jj-ii))  == l[jj].row)
                runconfid++;
                
        for (int jj=ii-1; jj>=0 ; jj--)
            if ((l[ii].row - (int)(ii-jj))  == l[jj].row)
                runconfid++;
        l[ii].run_confidence = runconfid;
    }
    for (size_t ii = 0; ii < l.size(); ii++)
    {
        stats &c = l[ii];
        if (c.row == -1) continue;
        if (ii > 0)
        {
            stats &p = l[ii-1];
            if (p.row != -1 && p.row > c.row && p.run_confidence > c.run_confidence)
            {
                clearRowIndicator(c,indicator);
                continue;
            }
        }
        if ((ii+1) < l.size())
        {
            stats &n = l[ii+1];
            if (n.row != -1 && n.row < c.row && n.run_confidence > c.run_confidence)
            {
                clearRowIndicator(c,indicator);
                continue;
            }
        }
    }

    
}

void
symbol::clearRowIndicator(stats &s, State indicator)
{


    for (int ii=s.start; ii <= s.stop ; ii++)
    {
        int leftPos=-1;
        int rightPos=-1;
        bool leftGood= false;
        bool rightGood= false;
        
        
        try {
            leftPos = find(history[ii],indicator);
            bigE &leftRow = *history[ii][leftPos];
            if (SYMBOL_OK(leftRow))
            {

                if (dlevel >=1)
                    std::cout << "clearRowIndictoro:  clearing row indicator " << (indicator == LEFT_ROW? " LEFT_ROW ":" RIGHT_ROW ")
                              << "hist[" << ii << " , phys(r,c)=(" << history[ii][leftPos]->e[0] << "," << history[ii][leftPos]->e[1] <<"), (cw,row,cluster)=(" 
                              << history[ii][leftPos]->_codeword  << ","  <<  history[ii][leftPos]->_row << "," << history[ii][leftPos]->cluster << ")"    
                              << std::endl;

                leftRow._row = -1;
                leftRow._codeword=-1;
            }
        }
        catch (...)
        {}
    }
}



/*
struct arow2{
    int row;
    Statistics<double> left;
    Statistics<double> right;
    int start;
    int stop;
    int strength;
    int overallSymbolCount;
    arow2():row(0),start(9999),stop(0),strength(0),overallSymbolCount(0){}
    bool operator > (const arow &cp) { return strength > cp.strength || (strength == cp.strength && overallSymbolCount > cp.overallSymbolCount);}
    friend bool operator> (const arow2 &a , const arow2 &b){ return a.strength > b.strength;}
    arow2 &operator=(int r){ row = r; return *this;}
    arow2 &operator=(const arow &cp){ 
        row = cp.row; 
        start=cp.start; 
        stop=cp.stop; 
        strength=cp.strength; 
        overallSymbolCount=cp.overallSymbolCount; 
        return *this;
    }
};
*/


typedef boost::shared_ptr<cv::Mat> Mat_ptr;


class column_FeatureTree {


    CvFeatureTree* _tr;
    Mat_ptr colData;
    int _minX, _maxX, _minY, _maxY;
    const int K;
    const int dims;

public:
    int getK() const { return K;}
    int getDims() const { return dims ; }

    const CvFeatureTree* getFeatureTree(){ return _tr; }

    virtual ~column_FeatureTree(){
        if (_tr)
            cvReleaseFeatureTree(_tr);
    }

    column_FeatureTree(std::vector < ColumnAvgs  > &stdCols , int minX,int maxX,int minY, int maxY):
        _tr(0),_minX(minX),_maxX(maxX),_minY(minY),_maxY(maxY),K(1),dims(2)
        {

            colData.reset(new cv::Mat (stdCols.size(), 2, CV_32FC1));
            for (std::vector < ColumnAvgs  >::iterator begin = stdCols.begin(), iter = begin, end = stdCols.end(); iter!= end;iter++)
            {
                int pos = std::distance(begin,iter);
                ColumnAvgs &avg = (*iter);
                colData->at<float>(pos,0) = avg.start / ((float) (_maxX-_minX));
                colData->at<float>(pos,1) = avg.stop / ((float) (_maxX-_minX));
//                std::cout << "column_FeatureTree colData :" << pos << " = (" << colData->at<float>(pos,0) << " , " << colData->at<float>(pos,0) << ")" << std::endl;
            }
            CvMat colDesc = (*colData);
            _tr = cvCreateSpillTree( &colDesc );
        }
    int mapColumn (bigE_ptr ptr, int logicalColumns)
        {

            cv::Mat points( 1, dims, CV_32FC1 );
            points.at<float>(0,0) = ptr->e[1] / ((float) (_maxX-_minX));
            points.at<float>(0,1) = ptr->e[9] / ((float) (_maxX-_minX));
            
//            std::cout << "Mapping point " << (*ptr) << " to (" << points.at<float>(0,0) << "," << points.at<float>(0,1) << ")"<< std::endl;

            cv::Mat neighbors (points.rows, K, CV_32SC1);
            cv::Mat dist( points.rows, neighbors.cols, CV_64FC1);
            CvMat _dist = dist, _points = points, _neighbors = neighbors;
            int emax=16;
            cvFindFeatures( _tr, &_points, &_neighbors, &_dist, neighbors.cols, emax );
    
            int correctMatches = 0;

            int n = neighbors.at<int>(0, 0);
            if (n == 1 )
            {
                ptr->_column = 0;
                ptr->symbol_type = LEFT_ROW;
            }
            else if (n == (logicalColumns+2))
            {
                ptr->_column = logicalColumns + 1;
                ptr->symbol_type = RIGHT_ROW;
            }
            else if (n > 1 && n < (logicalColumns+2))
                ptr->_column = n - 1;
        }
};

typedef boost::shared_ptr<column_FeatureTree> column_FeatureTree_ptr;

class bigRC_FeatureTree
{
    bigRC_ptr box;
    int minX, maxX, minY, maxY;
    Mat_ptr data;
    CvFeatureTree* tr;
    column_FeatureTree_ptr columnMapper;
    int logicalRows, logicalColumns;
    float phyRowHeight, phyColWidth;
    const int dims;

protected:
    
    
public:
    void mapPoint(cv::Mat & data, int pos, bigE_ptr b)
        {

//            data.at<float>(pos,0) = b->_row != -1 ? (b->_row/logicalRows) : ((b->e[0] - minY) / phyRowHeight ) / logicalRows;
            data.at<float>(pos,0) = (b->e[0] - minY) / phyRowHeight ;
            data.at<float>(pos,1) = ((b->e[1] + (b->e[9] - b->e[1])/2.0f) - minX) / phyColWidth ;
            data.at<float>(pos,2) =  (b->symbol_type==LEFT_ROW ? 0.0 : ( b->symbol_type==RIGHT_ROW ? logicalColumns + 1 : b->_column)) / (logicalColumns + 2.0f);
            data.at<float>(pos,3) =  b->_row != -1 ? (b->_row/logicalRows) : 0.0;
            data.at<float>(pos,4) =  b->symbol_type==LEFT_ROW || b->symbol_type==RIGHT_ROW  ? 0.0 : 1.0 ;

//            data.at<float>(pos,3) =  b->cluster ==0 ? 0.0 : b->cluster == 3 ? 0.5 : 1.0;
        }

    bigRC_ptr getBox(){ return box;}
    column_FeatureTree_ptr getColumnMapper() { return columnMapper;}
    const CvFeatureTree* getFeatureTree(){ return tr;}
    int getMinY() const { return minY;}
    int getMinX() const { return minX;}
    int getMaxY() const { return maxY;}
    int getMaxX() const { return maxX;}

    float getPhyRowHeight() const { return phyRowHeight;}
    float getPhyColWidth() const { return phyColWidth;}

    virtual ~bigRC_FeatureTree(){
        if (tr)
            cvReleaseFeatureTree(tr);
    }

    bigRC_FeatureTree(Table &history, std::vector < ColumnAvgs  > &stdCols, int logRows,int logCols) : 
        minX(99999),maxX(0),minY(99999),maxY(0),tr(0),logicalRows(logRows)
        ,logicalColumns(logCols), dims(5)
        {
            bigRC_ptr tmp(new bigRC);

            for (Table::iterator iter=history.begin(); iter != history.end();iter++)
            {
                Row::iterator end = (*iter).end();
                for (Row::iterator i2 = (*iter).begin(); i2 != end; i2++){
                    bigE_ptr ptr = *i2;
                    if (!ptr.get())
                        continue;
                    
                    minX=std::min(minX, ptr->e[1]);
                    maxX=std::max(maxX, ptr->e[9]);
                    minY=std::min(minY, ptr->e[0]);
                    maxY=std::max(maxY, ptr->e[0]);
                }
            }
            columnMapper.reset(new  column_FeatureTree(stdCols,minX,maxX,minY,maxY));

            for (Table::iterator iter=history.begin(); iter != history.end();iter++)
            {
                Row::iterator end = (*iter).end();
                for (Row::iterator i2 = (*iter).begin(); i2 != end; i2++){
                
                    bigE_ptr ptr = *i2;
                
                    if (ptr->symbol_type == START || ptr->symbol_type==STOP || !SYMBOL_OK((*ptr)))
                        continue;

                    columnMapper->mapColumn(ptr,logicalColumns);
                    
                    if (ptr->_column >=0 && ptr->_column <=(logCols+2))
                        tmp->insert(ptr);

                }
            }

            box = tmp;


        }

    void computeTree()
        {
            if (!box->size())
                return;
            // assemble the spilltree

            phyRowHeight = (maxY-minY)/logicalRows;
            phyColWidth = (maxX-minX)/(logicalColumns + 4);

//            boxRC_by_RC &lu = boost::multi_index::get<by_row_column>( *box);
            //           boxRC_by_RC::iterator theEnd = lu.end();
            if (tr){
                cvReleaseFeatureTree(tr);
                tr = 0;
            }

            data.reset( new cv::Mat(box->size(), dims, CV_32FC1));

            bigRC::size_type pos=0;
            boxRC_by_random &rand_lu = boost::multi_index::get<by_random>( *box);

            for (boxRC_by_random::iterator begin= rand_lu.begin(), iter = begin ,randEnd = rand_lu.end(); iter != randEnd; iter++)
            {
                bigE_ptr b = (*iter);

                bigRC::size_type tpos = std::distance( begin , iter );
                if (tpos != pos)
                    std::cout << "Warning position not expected! pos=" << pos << " tpos=" << tpos << std::endl;

                mapPoint(*data,pos,b);

                pos++;
            }

            // build the spill tree
            CvMat desc = (*data);
            if (tr){
                cvReleaseFeatureTree(tr);
                tr = 0;
            }

            tr = cvCreateSpillTree( &desc );
        }


    Mat_ptr findSymbol( int x, int y, int logX, int logY, State symbol_type=SYMBOL, int K=7)
        {
            cv::Mat points ( 1, dims, CV_32FC1 );
            int pos=0;

            points.at<float>(pos,0) = (y - minY) / phyRowHeight ;
            points.at<float>(pos,1) = (x - minX) / phyColWidth ;
            points.at<float>(pos,2) =  logX / (logicalColumns + 2.0f);
            points.at<float>(pos,3) = logY != -1 ? (logY/logicalRows) : 0.0;
            points.at<float>(pos,4) =  symbol_type==LEFT_ROW || symbol_type==RIGHT_ROW  ? 0.0 : 1.0 ;

            Mat_ptr neighbor_ptr ( new cv::Mat( points.rows, K, CV_32SC1));
            cv::Mat &neighbors = *neighbor_ptr;

            cv::Mat dist( points.rows, neighbors.cols, CV_64FC1);
            CvMat _dist = dist, _points = points, _neighbors = neighbors;
            int emax=16;
            cvFindFeatures( tr, &_points, &_neighbors, &_dist, neighbors.cols, emax );
            return neighbor_ptr;

        }

    Mat_ptr find(bigE_ptr ptr, int K=7)
        {
            bigRC foo;
            foo.insert(ptr);
            return find(foo,K);
        }
    
    Mat_ptr find(bigRC & goodPoints, int K=7)
        {
//            bigRC &goodPoints = (*goodPoints_ptr);

            cv::Mat points ( goodPoints.size(), dims, CV_32FC1 );

            for (boxRC_by_random::iterator begin=get<by_random>(goodPoints).begin(), iter= begin, end = get<by_random>(goodPoints).end(); 
                 iter != end; iter++)
            {
                bigE_ptr ptr = (*iter);
                size_t pos = std::distance(begin, iter ); 

                mapPoint(points,pos,ptr);
            }
            Mat_ptr neighbor_ptr ( new cv::Mat( points.rows, K, CV_32SC1));
            cv::Mat &neighbors = *neighbor_ptr;

            cv::Mat dist( points.rows, neighbors.cols, CV_64FC1);
            CvMat _dist = dist, _points = points, _neighbors = neighbors;
            int emax=16;
            cvFindFeatures( tr, &_points, &_neighbors, &_dist, neighbors.cols, emax );
            return neighbor_ptr;
        }


};

    
void 
symbol::organizeMatrix_new()
{

}
    
void 
symbol::assignRowNumbers( bigRC_ptr box, int start , int stop)
{
        
    for (int ii=start; ii <= stop;ii++){
        
        boxRC_by_RC::iterator ic0,ic1;
        
        boost::tuples::tie(ic0,ic1) = get<by_row_column>( *box).equal_range(boost::make_tuple( ii));
        
        Row p;
        
        try {
            
            
            std::copy(ic0,ic1, boost::make_function_output_iterator(bigEptr_copy_appender((p))));
            
            assignRowNumbers(p);
            
            std::pair<boxRC_by_RC::iterator , Row::iterator> theDiff =
                std::mismatch(ic0,ic1,
                              p.begin(), bigE_row_differ);
            while (theDiff.first != ic1 && theDiff.second != p.end())
            {
                if (dlevel >= 1)
                    std::cout << "changing row " << (*theDiff.first) << "    to row " << (*theDiff.second)->_row << std::endl;
                
                get<by_row_column>( *box).modify( theDiff.first, change_row( (*theDiff.second)->_row) );
                
                theDiff = std::mismatch(++theDiff.first,ic1,++theDiff.second , bigE_row_differ);
            }
            
        }
        catch (UnknownCluster &)
        {
            if (dlevel >= 1)
                std::cout << " assignRowNumber threw UnknownCluster handling row " << ii << std::endl;
        }
        catch ( UnknownRow &)
        {
            if (dlevel >= 1)
                std::cout << " assignRowNumber threw UnknownRow handling row " << ii << std::endl;
        }
        catch (...)
        {
            if (dlevel >= 1)
                std::cout << " assignRowNumber threw exception handling row " << ii << std::endl;
        }
 
        if (dlevel >=1 )
            std::copy(p.begin(), p.end(),
                  boost::make_function_output_iterator(bigEptr_appender(std::cout)));


    }
}

void
symbol::dumpBigRC(bigRC_FeatureTree &tree)
{
    bigRC_ptr box = tree.getBox();    
    std::cout << std::setw(7) << "Row#|" ;
    for (int uu=0;uu < (cols+1);uu++){
        std::cout << std::setw(7) << uu;
        std::cout << '|' ;
    }
    std::cout << std::endl;
    int lastRow=tree.getMinY();

    for (int xx=tree.getMinY(); xx < tree.getMaxY();xx++){
        boxRC_by_RC::iterator ic0,ic1;
        boost::tuples::tie(ic0,ic1) = get<by_row_column>( *box).equal_range(boost::make_tuple( xx));
        int lastCol=0;

        while (lastRow < xx)
        {
            std::cout << std::setw(7) << lastRow << "|";
            while (lastCol < cols)
            {
                std::cout << std::setw(7) << '-';
                std::cout << '|' ;
                lastCol++;
            }
            lastRow++;
            std::cout << std::endl;
        }
        std::cout << std::setw(7) << xx << '|';
        lastCol=0;
        while (ic0 != ic1)
        {
            while (lastCol < (*ic0)->_column)
            {
                std::cout << std::setw(7) << '-';
                std::cout << '|' ;
                lastCol++;
            }
            std::cout << std::setw(5) << (*ic0)->_codeword << '/' << (*ic0)->cluster ;
            std::cout << '|' ;
            ic0++;
            lastCol++;
                
        }
        while (lastCol < cols)
        {
            std::cout << std::setw(7) << '-';
            std::cout << '|' ;
            lastCol++;
        }
        lastRow++;

        std::cout << std::endl;
    }

}


void 
symbol::organizeMatrix()
{   

    // build the initial tree given the physical results

    bigRC_FeatureTree tree (history, stdCols, rows ,cols) ;

    bigRC_ptr box = tree.getBox();

    struct foo{
        State typ;
        int col;
    } bar[] = {{LEFT_ROW,0},{RIGHT_ROW,cols+1}};



    for (int ii=0; ii < 2 ; ii++)
    {
        //
        // promote row indicators based on the known columns that indicate a row indicator
        // then crack the indicator 
        //

        boxRC_by_logical_column::iterator begin,end;
        boost::tuples::tie(begin,end) = get<by_logical_column>( *box ).equal_range(boost::make_tuple(bar[ii].col));
        while (begin != end)
        {
            bigE_ptr ptr = *begin;
            if (ptr->symbol_type == bar[ii].typ && ptr->_row == -1)
            {
                ptr->symbol_type = bar[ii].typ;
                try {

                    bigE_ptr ptr2 ( new bigE(*(ptr.get())));

                    processRowIndicator((*ptr2) );
                    if (ptr->_row != ptr2->_row)
                    {
                        get<by_logical_column>( *box).modify( begin, change_row( ptr2->_row) );

                    }
                }
                catch(...){}
                
            }
            begin++;
        }
    }
    if (dlevel >=1)
        dumpBigRC(tree);


    State items[2] = {LEFT_ROW, RIGHT_ROW};

    std::vector<arow> rowStrength(this->rows + 3);
    {
        //
        // initialize all the rowStrength logical values using physical row indicator
        // 
        //
        int r=0;
        std::for_each(rowStrength.begin(), rowStrength.end(), (lambda::_1 = lambda::var(r)++) );
    }


    for (size_t ii=0; ii < (sizeof items)/sizeof(State); ii++)
    {
        //
        // correct rows with LEFT/RIGHT row indicators in wrong columns
        //
        // build the logical row statistics using rows with good LEFT/RIGHT indicators
        //

        boxRC_by_symbol_type::iterator begin,ic0,end;
        boost::tuples::tie(begin,end) = get<by_symbol_type>( *box).equal_range(boost::make_tuple(items[ii]));
        
        int lastRow=0;
        ic0=begin;
        //for  ( ic0=begin; ic0 != end; ic0++){
        while (ic0 != end)
        {
            bigE_ptr cur = (*ic0);
            if (cur->_row == -1 || ( items[ii] == LEFT_ROW && cur->_column != 0) ||
                (items[ii] == RIGHT_ROW && cur->_column != (cols + 1)))
            {
                // something wrong with the indicator;
                if (dlevel >= 1)
                    std::cout << "BAD row indicator at: " << (*cur) << std::endl;
                // returns the following iterator
                if (( items[ii] == LEFT_ROW && cur->_column != 0) ||
                    (items[ii] == RIGHT_ROW && cur->_column != (cols + 1)))
                {
                    boxRC_by_symbol_type::iterator tmp = ic0;
                    tmp++;
                    
                    get<by_symbol_type>( *box).modify(ic0, change_symbol(SYMBOL) );
                    ic0 = tmp;
                }
                else {
                    boxRC_by_random &rand_lu = boost::multi_index::get<by_random>( *box);
                    
//                    boost::multi_index::index< bigRC,by_random>::iterator iter = box->project<by_random>(ic0);
                    boxRC_by_random::iterator iter = box->project<by_random>(ic0);

                    bigRC::size_type tpos = std::distance( boost::multi_index::get<by_random>( *box).begin() , iter );

                    if (dlevel >= 1)
                        std::cout << "Erasing slot " << tpos <<  " with undetermined row " << (**ic0) << std::endl;

                    (*ic0)->symbol_type = BAD;
                    ic0 = get<by_symbol_type>( *box).erase(ic0);
                }
                continue;
            }
            else{
                //
                // check that the indicator is valid by checking neighbors
                // 
                using namespace boost::lambda;
                
                int greater=0;
                int lesser=0;
                int from_start = std::distance( begin , ic0 );
                int from_end = std::distance( ic0, end );
                greater = std::count_if(begin, ic0, boost::bind( std::greater<int>()
                                                                 ,boost::bind(&bigE::_row, lambda::_1)
                                                                 ,boost::bind(&bigE::_row, (*ic0)) ));
                
                
                lesser = std::count_if( ic0, end, boost::bind( std::less<int>()
                                                               ,boost::bind(&bigE::_row, lambda::_1)
                                                               ,boost::bind(&bigE::_row, (*ic0)) ));
                
                if (((float) greater/from_start) > .20 || ((float) lesser/from_end) > .20)
                {
                    bigE_ptr ptr = *ic0;

                    if (dlevel > 1)
                        std::cout << "Bad row indicator: " << (*ic0);
                    ic0 = get<by_symbol_type>( *box).erase(ic0);

                    ptr->symbol_type = BAD;

                    continue;
                }
            }


            if (items[ii] == LEFT_ROW)
                rowStrength[cur->_row].left(cur->e[0]);
            else
                rowStrength[cur->_row].right(cur->e[0]);
            ic0++;
        }
    }


    // update the spilltree
    tree.computeTree();




    if (dlevel == 1)
        std::cout << "Dumping final row indicators" << std::endl;

    for (size_t ii=0; ii < (sizeof items)/sizeof(State); ii++)
    {
        // get rid of bad indicators

        boxRC_by_symbol_type::iterator begin,ic0,end;
        boost::tuples::tie(begin,end) = get<by_symbol_type>( *box).equal_range(boost::make_tuple(items[ii]));
        
        int lastRow=0;
        ic0=begin;
        //for  ( ic0=begin; ic0 != end; ic0++){
        while (ic0 != end)
        {
            bigE_ptr cur = (*ic0);
            
            arow &rowMaster = rowStrength[ cur->_row ];
            if ( (items[ii] == LEFT_ROW && (cur->e[0] < ( rowMaster.leftStart()) || cur->e[0] > rowMaster.leftStop()))
                 || (items[ii] == RIGHT_ROW && (cur->e[0] < ( rowMaster.rightStart()) || cur->e[0] > rowMaster.rightStop())))
            {
                boxRC_by_random &rand_lu = boost::multi_index::get<by_random>( *box);
                
                boxRC_by_random::iterator iter = box->project<by_random>(ic0);

                bigRC::size_type tpos = std::distance( boost::multi_index::get<by_random>( *box).begin() , iter );

                if (dlevel >= 1)
                    std::cout << "Erasing indicator falling beyond row stats:  " << tpos  << " " << (*cur) << std::endl;

                (*ic0)->symbol_type = BAD;

                ic0 = get<by_symbol_type>( *box).erase(ic0);
            }
            else {
                if (dlevel == 1)
                    std::cout << *cur;
                ic0++;
            }
        }
    }

    std::vector<missing> unmappedRows;
    // sort by strongest rows.

    // determine physical rows that outside of any logical row characterization.  ie. physical rows that have spans with no row indicators
    // such unmappedRows may still have valid codewords.
    std::copy(rowStrength.begin(), rowStrength.end(), boost::make_function_output_iterator(arow_appender(unmappedRows, tree.getMinY())));


    if (dlevel >=1){
        std::cout << "Mapped rows follow: " << std::endl;
        std::copy(rowStrength.begin(), rowStrength.end(), std::ostream_iterator<arow>(std::cout));
        std::cout << "Unmapped rows follow: " << std::endl;
        std::copy(unmappedRows.begin(), unmappedRows.end(), std::ostream_iterator<missing>(std::cout));
    }

    // sort according to which rows have the highest prospects of being solved
    std::sort(rowStrength.begin(), rowStrength.end(), (lambda::_1 > lambda::_2) );
    
    BOOST_FOREACH( arow&  slot, rowStrength )
    {
        // fix busted row indicators based on their expected position
        for (int ii=0; ii < 2 ; ii++)

        {
            // search for busted row indicator by checking LEFT/RIGHT symbol types of the current row
            if ( (bar[ii].typ == LEFT_ROW && (slot.left.count() <= 1 || slot.left.stddev() < 3)) 
                 || (bar[ii].typ == RIGHT_ROW && (slot.right.count() <= 1 || slot.right.stddev() < 3)) )
                continue;

            boxRC_by_logical_row::iterator begin1,end1;
            boost::tuples::tie(begin1, end1) = get<by_logical_row>( *box ).equal_range( boost::make_tuple(slot.row));

            if (begin1 == end1)
                continue;
            
            boxRC_by_symbol_type::iterator begin =  get<by_symbol_type>( *box).lower_bound(boost::make_tuple(bar[ii].typ,(*begin1)->e[0]));
            boxRC_by_symbol_type::iterator end =  get<by_symbol_type>( *box).upper_bound(boost::make_tuple(bar[ii].typ,(*end1)->e[0]));

            double low = 0.0;
            double high=0.0;
            Statistics<double> &old =  bar[ii].typ == LEFT_ROW ? slot.left : slot.right;

            low = old.mean() - old.stddev();
            high = old.mean() + old.stddev();

            bool changed=false;
            Statistics<double> newVal;
            while (begin != end)
            {
                bigE_ptr ptr = *begin;
                
                if (ptr->e[0] < low || ptr->e[0] > high)
                {
                    boxRC_by_random::iterator iter = box->project<by_random>(begin);
                    
                    bigRC::size_type tpos = std::distance( boost::multi_index::get<by_random>( *box).begin() , iter );

                    if (dlevel >= 1)
                        std::cout << "row: " << slot.row << " , Erasing indicator falling outside range (low,high) = (" << low << "," << high   << ")"  << std::endl;

                    begin = get<by_symbol_type>( *box).erase(begin);
                    changed = true;
                    continue;
                }
                newVal(ptr->e[0]);
                    
                begin++;
            }
            if (changed){
                if (dlevel >= 1)
                    std::cout << "Changed statistics. old " << slot << std::endl;

                old = newVal;

                if (dlevel >= 1)
                    std::cout << "Changed statistics. new " << slot << std::endl;
            }
        }
    }
    // determine skew from row indicators if possible.

    BOOST_FOREACH( arow&  slot, rowStrength )
    {
        
      for (int ii=static_cast<int>(slot.start()); ii < static_cast<int>(slot.stop());ii++){
            if (!slot.left.count() || ! slot.right.count())
                continue;
            else if (slot.left.mean() < slot.right.mean())
                _skew--;
            else if (slot.left.mean() < slot.right.mean())
                _skew++;

        }
    }


    if (dlevel >= 1)
        std::cout << "Assigning row numbers  " << std::endl;
 
    BOOST_FOREACH( arow&  slot, rowStrength )
    {
        if (dlevel >= 1)
            std::cout << "statistical row: " << slot.row << " range(y1,y2)= (" << slot.start() << "," << slot.stop() << ")" <<std::endl;

        if (!slot.count())
            continue;

        assignRowNumbers(box, static_cast<int>(slot.start()), static_cast<int>(slot.stop()));

    }


    BOOST_FOREACH( missing&  slot, unmappedRows)
    {
        assignRowNumbers(box, slot.start, slot.stop);
    }

    // update the spilltree
    tree.computeTree();



    // method 2: use row strength but start with the row indicators with *good* row#s for those rows in seeding the queue
    // then find neighbors with bad and pop them into the queue. 
    //
    bigRC goodPoints;
    BOOST_FOREACH( arow &  slot, rowStrength )
    {
      for (int ii=static_cast<int>(slot.start()); ii < static_cast<int>( slot.stop() );ii++){
            
            boost::multi_index::index<bigRC,by_row_column>::type::iterator ic0,ic1;
            for (int jj=0; jj < 2 ; jj++){
                if ( (bar[jj].typ == LEFT_ROW && !slot.left.count() ) 
                     || (bar[jj].typ == RIGHT_ROW && !slot.right.count()))
                    continue;
                
                boxRC_by_logical_row::iterator begin1,end1;
                boost::tuples::tie(begin1, end1) = get<by_logical_row>( *box ).equal_range( boost::make_tuple(slot.row));
                
                if (begin1 == end1) continue;

                while (begin1 != end1){

                    if ( (*begin1)->_row != -1){
                        std::cout << " Seeding goodPoint: " << (**begin1) ;
                        goodPoints.insert( *begin1);
                    }
                    begin1++;
                }
            }
        }
    }
    

    do {

        bigRC unsolved,solved;
/*
  for (boxRC_by_random::iterator beg=get<by_random>(goodPoints).begin(),end=get<by_random>(goodPoints).end(); beg!= end; beg++){
  // make good points look bad for spilltree search
  bigE_ptr foo(new bigE(**beg));

  if (foo->symbol_type == LEFT_ROW || foo->symbol_type == RIGHT_ROW){
  foo->symbol_type = SYMBOL;
  foo->_column =  foo->symbol_type == LEFT_ROW ? 1 : (foo->_column < cols ? foo->_column++: foo->_column);
  }
  std::cout << "Considering " << (**beg);
  foo->_row = -1;
  unsolved.insert(foo);
  }

  if (unsolved.size() == 0 ) continue;
*/
        //boxRC_as_inserted &lu = ;
//        bigE_ptr cu = box->front();
        bigE_ptr point = get<as_inserted>(goodPoints).front();
        get<as_inserted>(goodPoints).pop_front();

        

        Mat_ptr neighbors = tree.findSymbol(point->e[1] + (point->e[9]- point->e[1])/2, point->e[0], point->_column, -1 );

        int row=0;

/*
  std::vector<arow>::iterator iter = std::find_if(rowStrength.begin()
  ,rowStrength.end()
  , inrange(ii));
*/
        // the spill tree will return points that should have a physical & logical proximity.  row=-1 is a valid proximity 
        int pi=0;
        for (int pp=0; pp < neighbors->cols; pp++){
            int n = neighbors->at<int>(pi, pp);
            bigE_ptr target = get<by_random>( *box )[n];
            VoteRecorder rowMachine;
            if ( *point == *target || target->symbol_type==LEFT_ROW || target->symbol_type == RIGHT_ROW || target->_row != -1 ) {
                std::cout << "    ignoring good neighbor: " << *target;
                continue;
            }
            if (target->_row == -1 && abs(target->_column - point->_column) <= 1 && abs(target->e[0] - point->e[0]) <= (tree.getPhyRowHeight() *2 )){
                
                // now we want to find approximate neighbors that have *valid* row numbers
                // so we need to seed the search with an approximate row# 
                
                std::cout << "   Neighbor " << *target;
                int change=0;
                    

                if (target->_column <= point->_column)
                {
                    
                    if (target->e[0] > point->e[0])
                        // fwd
                        change = _skew <= 0 ? -fwdCompare( target->cluster,point->cluster)
                            : fwdCompare(point->cluster,target->cluster);
                    else if (target->e[0] <= point->e[0])
                        // bwd
                        change = _skew > 0 ? bwdCompare(point->cluster,target->cluster) 
                            : fwdCompare(point->cluster,target->cluster);
                    
                    int predict = point->_row + change;
                    tallyVotes(rowMachine, row, predict);
                    
                }
                else if (target->_column > point->_column)
                {
                    if (target->e[0] < point->e[0])
                        // sense if fwd
                        change = _skew <= 0 ? -fwdCompare( target->cluster,point->cluster)
                            : fwdCompare(point->cluster,target->cluster);
                    
                    else if (target->e[0] >= point->e[0])
                        // sense if forwards
                        change = _skew <= 0 ? -fwdCompare( target->cluster,point->cluster)
                            : fwdCompare(point->cluster,target->cluster);
                    
                    int predict = point->_row + change;
                    tallyVotes(rowMachine, row, predict);
                }
            }

            if (row != 0)
            {
                if (dlevel >= 1)
                    std::cout << "       Predicated row " << point->_row << ".  deduced row as " << row << std::endl; 
                
                boost::multi_index::index<bigRC,by_row_column>::type::iterator ra = get<by_row_column>( *box).find(boost::make_tuple( target->e[0] , target->e[1]));
                boost::multi_index::index<bigRC,by_row_column>::type::iterator theEnd = get<by_row_column>( *box).end();
                if (ra != theEnd)
                {
                    
                    get<by_row_column>( *box).modify( ra, change_row( row) );
                    solved.insert(  *ra  );
                    {
                        boxRC_by_logical_column::iterator iter=box->project<by_logical_column>(ra)
                            ,up=iter
                            ,down=iter
                            ,start = get<by_logical_column>(*box).begin()
                            ,end = get<by_logical_column>(*box).end();
                        
                        {
                            
                            int curRow=(*ra)->_row;
                            int curCluster = (*ra)->cluster;
                            int neighborIndex = curCluster/3;
                            
                            while (--up != start)
                            {
                                if ( curCluster == (*up)->cluster ){
                                    //&& (*ra)->_codeword == (*up)->_codeword){
                                    get<by_logical_column>( *box).modify( up, change_row( curRow) );
                                    solved.insert(*up);
                                }
                                else if ((*up)->cluster == adjacent[neighborIndex][1])
                                {
                                    curRow--;
                                    curCluster = (*up)->cluster;
                                    neighborIndex=curCluster/3;
                                    get<by_logical_column>( *box).modify( up, change_row( curRow) );
                                    solved.insert(*up);
                                }
                                else
                                    break;
                            }
                        }

                        {
                            
                            int curRow=(*ra)->_row;
                            int curCluster = (*ra)->cluster;
                            int neighborIndex = curCluster/3;
                            
                            while (++down != end)
                            {
                                if ( curCluster == (*down)->cluster ){
                                    //&& (*ra)->_codeword==(*down)->_codeword){
                                    get<by_logical_column>( *box).modify( down, change_row( curRow) );
                                    solved.insert(*down);
                                }
                                else if ((*down)->cluster == adjacent[neighborIndex][2])
                                {
                                    curCluster = (*down)->cluster;
                                    neighborIndex=curCluster/3;
                                    if ((*down)->_row == -1)
                                    {
                                        curRow++;
                                        get<by_logical_column>( *box).modify( down, change_row( curRow) );
                                        solved.insert(*down);
                                    }
                                    else {
                                        curRow = (*down)->_row;
                                    }
                                }
                                else
                                    break;
                            }
                        }
                    }
                    
                        
                    tree.computeTree();

                    if (dlevel >= 1){
                        //   boxRC_by_random::iterator iter = box->project<by_random>(ra);
                        //bigRC::size_type tpos = std::distance( boost::multi_index::get<by_random>( *box).begin() , iter );
                        std::cout << "Solved " << solved.size() << " slots "  << std::endl;
//
//                        std::copy(solved.begin(),solved.end(),boost::bind(std::ostream_iterator<bigE_ptr>(std::cout), lambda::ret<bigE>(lambda::_1)));
//std::copy(solved.begin(), solved.end(), boost::make_function_output_iterator( lambda::var(std::cout) << **lambda::_1) );
//                        std::copy(solved.begin(), solved.end(), std::ostream_iterator<bigE>(std::cout), boost::bind<bigE>(_1));

//                        std::for_each(solved.begin(), solved.end(), boost::bind(std::ostream_iterator<bigE>(std::cout), lambda::ret<bigE>();
//    std::copy(get<2>(solved).begin(),get<2>(solved).end(),std::ostream_iterator<bigE_ptr>(std::cout));
//    std::copy(solved.begin(),solved.end(),std::ostream_iterator<bigE_ptr>(std::cout));
//    std::copy(solved.begin(),solved.end(),boost::bind(std::ostream_iterator<bigE_ptr>(std::cout), lambda::ret<bigE>(lambda::_1)));

                        std::for_each(solved.begin(),solved.end(), (std::cout << *boost::lambda::_1));

                    }
                    
//                    std::copy(solved.begin(),solved.end(),std::back_inserter(goodPoints));

                }
                else
                    std::cerr << "Couldn't find row by position :" << *point;  
                
            }
            else {
                std::cout << "       Failed to deduce row :  " << *point ;
            }
            
        }

//        goodPoints.erase(goodPoints.begin(),goodPoints.end());
//        goodPoints = solved;

    } while (goodPoints.size());

    for (int ii=1; ii <= rows; ii++){
        for (int jj=1; jj <= cols; jj++){
            boxRC_by_logical_row::iterator ic0,ic1;
            boost::tuples::tie(ic0,ic1) = get<by_logical_row>(*box).equal_range(make_tuple(ii,jj));
            int codeword=0;
            VoteRecorder codewordVoter;
            while (ic0 != ic1){
                int t=(*ic0)->_codeword;
                tallyVotes(codewordVoter, codeword, t );
                ic0++;
            }
            VoteRecorder::iterator iter = codewordVoter.find(codeword);
            raw_matrix[ii][jj]._codeword = codeword;
            raw_matrix[ii][jj]._confidence = (*iter).second * 2;
            std::cout << "[" << ii << "][" << jj << "] cw=" << codeword << " , confid=" << (*iter).second << std::endl;
        }
    }
    

    if (dlevel >= 1)    std::cout << "organizeMatrix --> done" << std::endl;

}

void 
symbol::organizeMatrix_newer()
{}

void 
symbol::organizeMatrix2()
{
// figure out the logical row of the elements
    
    int sz = getColumns() + 4;
    bigE *lastLeft=0;
    bigE *lastRight=0;

    
    for (unsigned ii=0; ii < history.size(); ii++)
    {
        if (dlevel >=2)        std::cout << "row :"<< ii << "----------" << std::endl;

        bool hadBad=true;
        bool hadGood=true;
        for (unsigned jj=0; jj < history[ii].size(); jj++)
        {
            // returns physical column
            bigE &b = *history[ii][jj];
            if (!SYMBOL_OK(b)) continue;
            int col = determineColumn(b);

            if (col == -1)
            {
                hadBad=true;
                continue;
            }
            else
            {
                hadGood=true;
                b._column=col;
            }
            bool plainOle =  b.symbol_type == SYMBOL;

            if (col == 0)
                b.symbol_type = START;
            else if (col == 1)
                b.symbol_type = LEFT_ROW;
            else if (col == sz-1)
                b.symbol_type = STOP;
            else if (col == sz -2)
                b.symbol_type = RIGHT_ROW;
            else //if (SYMBOL_OK(history[ii][jj]))
            {
                if (dlevel >=2)
                    std::cout << "  " << col << ". codeword=" << b._codeword << " cluster=" << b.cluster << " row=" << b._row << " col=" << b._column << 
                        " e[0]=" << b.e[0] << " e[1]=" << b.e[1] << " e[9]=" << b.e[9] << std::endl;
                b.symbol_type = SYMBOL;
                b._row=-1;
            }

            if (b.symbol_type == LEFT_ROW || b.symbol_type == RIGHT_ROW) 
            {
                if (plainOle)
                {
                    // got a row indicator that was just promoted via positional data 
                    int curRow=-1;
                    int curCluster=-1;
                    try{
                        processRowIndicator(b);
                    }
                    catch (...){}
                    if (dlevel >=2) std::cout << "  " << (b.symbol_type == LEFT_ROW?"LR":"RR:") << jj << ". codeword=" << b._codeword << " row=" << b._row << " cluster=" << b.cluster << std::endl;
                }
                
            }
        }

        
        if (hadBad && hadGood)
        {
            // try to repair bad columns that have good codewords.
            // this can happen when the position that we start to look for 
            // codewords causes false positive codewords to generate bad statistics
            // to stdCols.  stdCols are used to average positions of successfully decoded codewords
            // This happened to me when i put a yellow gif in for a customer logo.  it caused a crazy
            // bar-space pattern.

            hadBad=false;
            for (unsigned jj=0; jj < history[ii].size(); jj++)
            {
                bigE &b = *history[ii][jj];
                if (!SYMBOL_OK(b) || b._column >=0 ) continue;
                if (jj>0 && history[ii][jj-1]->_column >=0)
                    history[ii][jj]->_column  = history[ii][jj-1]->_column + 1; 
                else{
                    hadBad = true;
                }
            }
            if (hadBad)
            {
                // go in reverse from the end
                for (int  jj=history[ii].size()-1; jj>=0; jj--)
                {
                    bigE &b = *history[ii][jj];
                    if (!SYMBOL_OK(b) || b._column >=0 ) continue;
                    if ( (size_t)(jj+1) < history[ii].size() && history[ii][jj+1]->_column >0)
                        history[ii][jj]->_column  = history[ii][jj+1]->_column - 1; 
                }
            }
        }
    }



    if (_skew == 0)
    {
        int lastLeft=-1; int lastRight=-1;
        for (unsigned jj=0; jj < history.size(); jj++)
            for (unsigned ii=0;ii<history[jj].size();ii++)
            {
                bigE &b = *history[jj][ii];
                if (SYMBOL_OK(b))
                {
                    if (lastLeft == -1)
                        lastLeft = b.e[1];
                    else if (lastLeft > b.e[1])
                    {
                        lastLeft = b.e[1];
                        _skew++;
                    }
                    if (lastRight == -1)
                        lastRight = b.e[9];
                    else if (lastRight < b.e[9])
                    {
                        lastRight = b.e[9];
                        _skew--;
                    }
                }
            }
    }


    // now go to the first row long enough
    // make sure that there is something usable in what we found

    int start = firstFull + (lastFull - firstFull)/2;
    start = start == -1 ? history.size()/2 : start;
    int firstGood=start;
    

    int f=0;


    // spin over everything looking for sanity.  bail as soon as we know we
    // have something good.
    
    for( firstGood= start; firstGood >= 0; firstGood--)
    {
        size_t s2 = history[firstGood].size();
        int howmany = symbolCount(history[firstGood],F_LEFTROW | F_RIGHTROW | F_START|F_STOP);
        if (howmany>=2)
            break;
    }
    if (firstGood<0)
    {
        for(firstGood= start; firstGood < static_cast<int>(history.size()); firstGood++)
        {
            int howmany = symbolCount(history[firstGood],F_LEFTROW | F_RIGHTROW | F_START|F_STOP);
            if (howmany>=2)
                break;
        }
    }
    if (firstGood == static_cast<int>(history.size()))
        throw NoAnswer();

    assignConfidence(start);
    // need confidence (horizontal) to help with a strenght compare
    verifyRowIndicator();

    for(size_t zz = 0; zz < history.size(); zz++)
    {
        Row &symbol_row = history[zz];
        int good= symbolCount(symbol_row,F_START | F_STOP | F_LEFTROW | F_RIGHTROW | F_SYMBOL);
        if (good <=0) 
            continue;
        try {
            assignRowNumbers(symbol_row);
        }
        catch(...)
        {}
    }


    int rowHeight=0;
    VoteRecorder rowHeightMachine;
    for (unsigned ii=0; ii < history.size(); ii++)
    {
        for (unsigned jj=0; jj < history[ii].size(); jj++)
        {
            bigE &b = *history[ii][jj];
            if (b.symbol_type == LEFT_ROW || b.symbol_type == RIGHT_ROW) 
            {
                switch(b.symbol_type)
                {
                case LEFT_ROW:{
                    if (lastLeft  &&  b._row != -1 && lastLeft->_row != b._row)
                    {
                        double f = (double) (lastLeft->e[0] - b.e[0])/((b._row - lastLeft->_row));
                        int f2 = (int) roundl(f);
                        tallyVotes(rowHeightMachine, rowHeight, f2);
                    }
                    if (b._row != -1)
                        lastLeft = &b;

                    break;
                }
                case RIGHT_ROW:{
                    if (lastRight  &&  b._row != -1 && lastRight->_row != b._row)
                    {
                        double f = fabs((double) (lastRight->e[0] - b.e[0])/((b._row - lastRight->_row)));
                        int f2 = (int) roundl(f);
                        tallyVotes(rowHeightMachine, rowHeight, f2);
                    }
                    if (b._row != -1)
                        lastRight = &b;

                }
                default:
                    break;
                }
            }
        }
    }
   if (dlevel >=1)
            std::cout << "  computed rowHeight: " << rowHeight << std::endl;




    int orphans=0;
        std::vector<mongo_item> goodwords(history.size());
        // pickup stats on the all row numbers
        for( firstGood= 0 ;  firstGood < (int) history.size(); firstGood++)
        {
            goodwords[firstGood].row = firstGood;
            for (size_t ii=0; ii <history[firstGood].size();ii++)
            {
                if (!SYMBOL_OK( (*history[firstGood][ii] )) || history[firstGood][ii]->symbol_type == START || history[firstGood][ii]->symbol_type == STOP )
                    continue;

                if (history[firstGood][ii]->_row > 0)
                    goodwords[firstGood].good++;
                else
                {
                    goodwords[firstGood].bad++;
                    orphans++;
                }
            }
        }

        // figure out close neighbors
        for (size_t ii=0; ii < goodwords.size();ii++)
        {
            int minBwd = ii>=3 ? ii-3 : 0;
            int maxFwd = (ii+3)< goodwords.size() ? ii+3 : goodwords.size();
            for (int jj= ii; jj >= minBwd; jj--)
            {
                goodwords[ii].closeGood += goodwords[jj].good;
                goodwords[ii].closeBad  += goodwords[jj].bad;
            }
            for (int jj= ii; jj < maxFwd; jj++)
            {
                goodwords[ii].closeGood += goodwords[jj].good;
                goodwords[ii].closeBad  += goodwords[jj].bad;
            }
        }
        if (dlevel>1)
        {
            std::cout <<"################ Starting Vertical Inspections ###################" << std::endl;
            printf("goodword    good    bad   closeGood  closeBad  row\n"); 
            for (int ii=0; ii < (int)goodwords.size();ii++)
                printf("goodword[%d] %5.2d %5.2d   %5.2d   %5.2d  %5.2d\n", ii,
                       goodwords[ii].good,
                       goodwords[ii].bad,
                       goodwords[ii].closeGood,
                       goodwords[ii].closeBad,
                       goodwords[ii].row);
        }


    for (int JJ=4 ; JJ <= mxWindow ;JJ++)
    {
        int changes=0;

        std::stable_sort(goodwords.begin(),goodwords.end(),less_most_action());

        // figure out close neighbors
        for (std::vector<mongo_item>::iterator iter = goodwords.begin(); iter!= goodwords.end();iter++)
        {
            if (dlevel>1)
            {
                size_t ii = (*iter).row;
                int row = history[ ii ][0]->e[0];
                printf("%d (in row) good:",row);
                for (int kk=0; kk < goodwords[ii].good; kk++) printf("+");
                printf("\n%d (in row) bad:",row);
                for (int kk=0; kk < goodwords[ii].bad; kk++) printf("-");
                printf("\n%d (near bad) :",row);
                for (int kk=0; kk < goodwords[ii].closeGood; kk++) printf("*");
                printf("\n%d (near bad):",row);
                for (int kk=0; kk < goodwords[ii].closeBad; kk++) printf("#");
                printf("\n");
            }

            if ((*iter).bad == 0) continue;
            size_t zz = (*iter).row;
            size_t rowSize = history[zz].size();

            for (size_t aa = 0 ; aa < rowSize; aa++)
            {
                if (SYMBOL_OK( (*history[zz][aa])) )
                {
                    if (  history[zz][aa]->_row < 0 && history[zz][aa]->symbol_type != START && history[zz][aa]->symbol_type != STOP)
                    {
                        try {
                            int ret =inspectLocation(zz, aa, JJ, true);
                            if (ret) 
                            {
                                changes += ret;
                                (*iter).good +=ret;
                                (*iter).bad-=ret;
                                orphans-=ret;
                            }

                            if (dlevel > 1)
		    
                                printf("inspectLocation[%d,%d].row = %d",(int) zz,(int) aa,history[zz][aa]->_row);

                        }
                        catch (NoMatch &)
                        {
                        }
                    }
                }
            }
        }
	if( dlevel >1)
	  printf("window size=%d  orphans %d -> rescued %d\n",JJ,orphans,changes);
        if (changes < 10)
            break;

    }
/*
#ifndef WIN32
    Table savehistory = history;
#endif
    
#ifndef WIN32
    if (dlevel > 1)
    {
        std::cout << "Post-Vertical Align changes" << std::endl;
        std::cout << "       phys                logical    " << std::endl; 
        std::cout << "    row    col    cw    row    column  cluster   confid" << std::endl; 
        
        for (size_t zz=0; zz < history.size(); zz++)
        {
            size_t rowSize = history[zz].size();
            if (rowSize<1) continue;
            for (size_t aa = 0 ; aa < rowSize; aa++)
            {
                bigE& a = history[zz][aa];
                bigE& b = savehistory[zz][aa];
                
                if ( a._row != b._row
                     ||  a._column != b._column
                     ||  a.cluster != b.cluster
                     ||  a._confidence != b._confidence)
                {
                    std::cout << "Old: " << b.e[0] << "    " << b.e[1] << "    " <<  b.codeword() << "    " << b._row  <<  "    " << b._column << "    "  << b.cluster << "    " << b._confidence <<std::endl;
                    std::cout << "New: " << a.e[0] << "    " << a.e[1] << "    " <<  a.codeword() << "    " << a._row  <<  "    " << a._column << "     " << a.cluster << "    " << a._confidence <<std::endl;
                    std::cout <<std::endl;
                }
            }
        }
    }

#endif
*/
    for (int ii=0; ii < static_cast<int>(history.size()); ii++)
    {
        try {

            symbol::FwdIter iter (history[ii]);
            assignRowsToMatrix(iter);
        }
        catch (...)
        {
            if (dlevel>= 1)
                std::cout << "caught exception trying to fill matrix" << std::endl;
        }
    }
    
}


int
symbol::determineRow(int zz, int slot)
{

//    int sz = static_cast<int>(history[zz].size());

    if (!SYMBOL_OK( (*history[zz][slot])))
        throw NoMatch();

    int slotMin=clf_max(slot-3,0);
    int slotMax=clf_min(slot+3,static_cast<int>(history[zz].size()));
    int rowMin=clf_max(zz-3,0);
    int rowMax=clf_min(zz+3,static_cast<int>(history.size()));

    for (int ii=slot-1; ii >= slotMin;ii--)
    {
        if (SYMBOL_OK((*history[zz][ii])) && history[zz][ii]->_row>0)
        {
            // try backwards in row
            try {
                int v = fwdCompare(*history[zz][ii],*history[zz][slot]);
                if( v){
                    history[zz][slot]->_row = history[zz][ii]->_row + v;
                    return 0;
                }
            }
            catch(NoMatch &)
            {
                
            }
        }
    }
    for (int ii=slot+1; ii < slotMax; ii++)
    {
        if (SYMBOL_OK( (*history[zz][ii])) && history[zz][ii]->_row>0)
        {
            // try backwards in row
            try {
                int v = bwdCompare(*history[zz][ii],*history[zz][slot]);
                if( v){
                    history[zz][slot]->_row = history[zz][ii]->_row + v;
                    return 0;
                }
            }
            catch(NoMatch &)
            {
                
            }
        }
    }
    // forward rows, right to left
    for (int ii=zz+1; ii < rowMax; ii++)
    {
        // look backwards are forwards
        for (int jj=slot; jj > slotMin;jj--)
        {
            try {
// don't need to find the same column, just a reasonable row
                // BUT we DO need to use the range checking access function at()
                // so that we don't attempt to get bogus memory
                Row & row_stuff = history.at(ii);
                bigE &b =  *row_stuff.at(jj);
                if ( SYMBOL_OK(b) && b._row > 0)
                {
                    // try forwards in column
                    int v = bwdCompare(b,*history[zz][slot]);
                    if( v){
                        history[zz][slot]->_row = b._row + v;
                        return 0;
                    }
                }
            }
            catch(NoMatch &)
            {
            }
            catch(std::out_of_range & ){
                // continue to get next lower
            }
        }
        for (int jj=slot+1; jj < slotMax; jj++)
        {
            try {
                //
                // bigE &b =  find(history[ii], jj);
                // BUT we DO need to use the range checking access function at()
                // so that we don't attempt to get bogus memory
                Row & row_stuff = history.at(ii);
                bigE &b =  *row_stuff.at(jj);

                if (SYMBOL_OK(b) && b._row>0)
                {
                    // try backwards in row
                  
                    int v = bwdCompare( b ,*history[zz][slot]);
                    if(v) {
                        history[zz][slot]->_row = b._row + v;
                        return 0;
                    }
                }
            }
            catch(NoMatch &)
            {
            }
            catch(std::out_of_range & ){
                // we're done
                jj = slotMax;
                break;
            }
        }
    }

    //still here
    // backwards rows
    for (int ii = zz-1; ii >= rowMin; ii--)
    {
        // look backwards are forwards
        for (int jj=slot; jj >= slotMin;jj--)
        {
            // try backwards in column
            try {
                
                Row & row_stuff = history.at(ii);
                bigE &b =  *row_stuff.at(jj);
                if (SYMBOL_OK(b) && b._row>0)
                {
                    int v = fwdCompare( b , *history[zz][slot]);
                    if( v){
                        history[zz][slot]->_row = b._row + v;
                        return 0;
                    }
                }
            }
            catch(NoMatch &)
            { }
            catch(std::out_of_range & ){
                // continue to next lower slot
            }
        }
        // look forward in back rows
        for (int jj=slot+1; jj < slotMax;jj++)
        {
            // try backwards in column
            try {
                //                bigE &b =  find(history[ii], jj);
                Row & row_stuff = history.at(ii);
                bigE &b =  *row_stuff.at(jj);
                if (SYMBOL_OK(b) && b._row > 0)
                {
                    int v = fwdCompare( b , *history[zz][slot]);
                    if( v){
                        history[zz][slot]->_row = b._row + v;
                        return 0;
                    }
                }
            }
            catch(NoMatch &)
            {
            }
            catch(std::out_of_range & ){
                // we're done
                jj = slotMax;
                break;
            }
        }
    }
    

    throw NoMatch();
}            



void
symbol::processRowIndicator(bigE &b)
{
    if ((b.cluster != 0 && b.cluster != 3 && b.cluster !=6) || b.codeword() < 0 )
    {
        b.symbol_type = BAD;
        throw InvalidRowIndicator();
    }
    
    int left = b._codeword/30;
    int right = b._codeword % 30;
  
    // can't make a global claim on the current overall row
    // because I have seen a scan row go from 
    // ( Row 0)cluster 0->cluster 3 -> cluster 6->cluster 0 (Row 4)!
    // with a skewed image.  concluding cur row without considering this
    // trend breaks a perfectly good analysis.  in the case mentioned above,
    // there was only 1 bad element between the transitions from one cluster to the next.

    
    int prospectiveRow= (left*3+1);
/* 
    bool bad=false;
    switch (prospectiveRow%3){
    case 0:
        if (b.cluster != 0) bad = true; break;
    case 1:
        if (b.cluster != 3) bad = true; break;
    case 2:
        if (b.cluster != 6) bad = true; break;
    default:
        bad=true; break;
    }
    if (bad){
        b.symbol_type = BAD;
        throw InvalidRowIndicator();
    }

*/
    b._row= prospectiveRow; 
    // the cluster for this row should be K.  if its not, then add only on
    // the road indicators

   int K = ((b._row - 1) % 3) * 3; 
    if (K != b.cluster)
    {
        b._row += fwdCompare(K,b.cluster);
    }
    int t = b.symbol_type == LEFT_ROW ? 0 : 1;
    
    switch (eq[ t ][ b.cluster/3])
    {
    case 0:
    {
        int newrows = (3 * right)   +1;
        if (tallyVotes(rowVotes,rows,newrows))
        {

        }
        break;
    }
    case 3:
    {

        int newlevel = right/3;

        if (tallyVotes(secVotes,sec_level,newlevel))
        {

        }
        int trowModulus = right % 3;
        if (tallyVotes(rowModulusVotes,_rowModulus,trowModulus))
        {

        
        }
        break;
    }
    case 6:
    {
        int newcol=right+1;

        if (tallyVotes(colVotes,cols,newcol))
        {

        }
        //this->cols = right + 1;
        break;
    }
        
    }
}

// 
// skew -
//   from left - left to right assumes decreasing row
//   from right - right to left assumes increasing row 
// 
// skew + 
//   from left - left to right assumes increasing row
//   from right - right to left traversal assumes increasing row

void
symbol::assignRowNumbers(Row &symbol_row)
{
    int curRow=-1;
    int curCluster=-1;
    int sz = symbol_row.size();
    
    int leftOK = 0;
    int rightOK = 0;
    int leftPos = -1;
    int rightPos = -1;
    int traversal=0;

    try {
        leftPos = find(symbol_row,LEFT_ROW);
    }
    catch(...){}

    try {
        rightPos = find(symbol_row,RIGHT_ROW);
    }
    catch(...){}

    
    if (leftPos != -1 && SYMBOL_OK( (*symbol_row[leftPos])) && symbol_row[leftPos]->symbol_type == LEFT_ROW && symbol_row[leftPos]->_row > 0)
    {
        leftOK = 1;
    }
    if (rightPos != -1 && symbol_row[rightPos]->symbol_type == RIGHT_ROW && symbol_row[rightPos]->_row > 0 && !(symbol_row[rightPos]->cluster % 3))
    {
        rightOK=1;
    }
    if (leftOK && rightOK)
    {
        traversal = symbol_row[leftPos]->confidence() >= symbol_row[rightPos]->confidence() ? 1 : 2;
    }
    else if (leftOK) traversal = 1;
    else if (rightOK) traversal = 2;

    if (traversal == 1 )
    {

        curRow = symbol_row[leftPos]->_row;
        curCluster = symbol_row[leftPos]->cluster;
        int neighborIndex = curCluster/3;
        int prevOff = symbol_row[leftPos]->e[9];
        
        for (int ii = leftPos+1; ii < sz;ii++)
        {
            bigE &b = *symbol_row[ii] ; //(*iter);

            if (!SYMBOL_OK(b))
            {
                b._row = curRow;
            }
            else if (b.symbol_type == START || b.symbol_type==STOP) continue;
            else if (b.codeword()==-1)
                b._row = curRow;
            else if (b.symbol_type==LEFT_ROW)
            {
                curRow=b._row;
                curCluster = b.cluster;
                neighborIndex = curCluster/3;
                prevOff = b.e[9];
            }
            else if (b.symbol_type==RIGHT_ROW) continue;
            else if (b.cluster == curCluster)
            {
                b._row = curRow;
                prevOff = b.e[9];
            }
            else if (  adjacent[neighborIndex][1] != b.cluster &&   adjacent[neighborIndex][2] != b.cluster  && prevOff == b.e[1]) {
                std::cout << "ignoring good codeword because of weird (but valid) cluster @row="<< b.e[0] << " codeword=" << b.codeword() 
                          << " column=" << b._column << std::endl;
                b._codeword=-1;
            }
            else 
            {
                int change = _skew <= 0 ? -fwdCompare(b.cluster,curCluster) : fwdCompare(curCluster,b.cluster);
                if (abs(change) > 1 && abs(b.e[9] - prevOff) < (int) (1.5*this->symWidth()) ) 
                {
                    // not right
                    if (dlevel>0)
                        std::cerr << "WARNING: assignRows -- left traversal phys/log row  (" 
                                  << b.e[0] << "/" << curRow << ").  adjacent columns differ are predicting change > 1! prevOff/cluster = " 
                                  << prevOff << "/" << curCluster << " newOff/cluster=" << b.e[9]  << "/" << b.cluster 
                                  << " change=" << change << " row=" << (curRow + change) 
                                  << std::endl;
	      
                    //TODO: do something about row change >1 , col change <2
                    // refuse to do it
                    continue;
                }
                curRow += change;
                b._row = curRow;
	      
                curCluster=b.cluster;
                neighborIndex = curCluster/3;
                prevOff = b.e[9];
            }
            
            if (b._row> this->rows && b._row <= 90 && b._row>0)
                this->tallyVotes(rowVotes,this->rows, b._row);
            //                this->rows = b._row;
        }
    }
    else if ( traversal == 2 )
    {

        curRow = symbol_row[rightPos]->_row;
        curCluster = symbol_row[rightPos]->cluster;
        int neighborIndex = curCluster/3;
        int prevOff = symbol_row[rightPos]->e[1];
        for (int ii=rightPos-1;ii>=0;ii--)
        {
            bigE &b = *symbol_row[ii] ; //(*iter);
            if (b.hasError) continue;
            if (!SYMBOL_OK(b))
            {
                b._row = curRow;
            }
            else if (b.symbol_type == START || b.symbol_type==STOP) continue;
            else if (b.codeword()==-1)
                b._row = curRow;
            else if (b.symbol_type==RIGHT_ROW)
            {
                curRow=b._row;
                curCluster = b.cluster;
                neighborIndex = curCluster/3;
                prevOff = b.e[1];
            }
            else if (b.symbol_type==LEFT_ROW) continue;
            else if (b.cluster == curCluster)
            {
                b._row = curRow;
                prevOff = b.e[1];
            }
            else if (  adjacent[neighborIndex][1] != b.cluster &&   adjacent[neighborIndex][2] != b.cluster  && prevOff == b.e[1]) {
                std::cout << "good codeword because of weird (but valid) cluster @row="<< b.e[0] << " codeword=" << b.codeword() 
                          << " column=" << b._column << std::endl;
                b._codeword=-1;
            }
            else 
            {
                // sanity check neighbor index first
                // -skew number means a neighborIndex 0 when we are 3 does not make sense.  it means +skew.

                int change = _skew > 0 ? bwdCompare(curCluster,b.cluster) : fwdCompare(curCluster,b.cluster);
                if (abs(change) > 1 && abs(b.e[1] - prevOff) < (int) (1.5  * this->symWidth()) ) 
                {
                    // not right
                    if (dlevel >0)
                        std::cerr << "WARNING: assignRows -- right traversal phys/log row (" 
                                  << b.e[1] << "/" << curRow << ").  adjacent columns differ are predicting change > 1! prevOff/cluster = " 
                                  << prevOff << "/" << curCluster << " b.e[1]/cluster=" << b.e[1]  << "/" << b.cluster 
                                  << " change=" << change << " row=" << (curRow + change) 
                                  << std::endl;
                    continue;
                }
	      

                curRow += change;
                b._row  = curRow;
                curCluster = b.cluster;
                neighborIndex = curCluster/3;
                prevOff = b.e[1];
            }
            if (b._row> this->rows && b._row <= 90 && b._row>0)
            {
                //		this->rows = b._row;
                this->tallyVotes(rowVotes,this->rows, b._row);
            }
        }
    }
    else
    {
        if (dlevel >= 1)
        {
            int good=0;
            printf("No Left/Right Row:\n");
            printf("   phys                logical    \n");
            printf("%-6.3s  %-6.3s  %-6.3s  %-6.3s  %-8.8s %-8.8s %10.10s\n","row","col","cw","row","column","cluster","confidence");
            for (int ii = 0; ii < sz;ii++)
            {
                bigE &b = *symbol_row[ii];
                if (b.codeword() >= 0) good++;
                printf("%-6.1d  %-6.1d  %-6.1d  %-6.1d  %-6.1d   %-6.1d   %d\n",  b.e[0] , b.e[1], b._codeword, b._row, b._column, b.cluster,b.confidence());
            }

            std::cout << "Can't determine the following row.  ignoring " << good << " symbols!" << std::endl;                    
        
        }
        throw UnknownRow();
    }
    int lastRow = -1;
    int lastCluster = -1;
    for (size_t ii=0; ii < symbol_row.size();ii++)
    {
        bigE &b = *symbol_row[ii] ; //(*iter);
        if (b.hasError ||!SYMBOL_OK(b) || b._row < 0) continue;
        if (lastRow == -1)
        {
            lastRow = b._row;
            lastCluster = b.cluster;
        }
        else if (b._row != lastRow)
        {
            if (b._row > lastRow)
                _skew++;
            else
                _skew--;

            lastRow = b._row;
            lastCluster = b.cluster;
        }
    }

    if (dlevel>=1){
        std::cout << "bar: rows=" << this->rows << ", cols=" << this->cols << ", security=" << this->sec_level << std::endl;
        std::cout << "curRow=" << curRow << ", curCluster=" << curCluster << " skew = " << _skew << std::endl;
        
        printf("   phys                logical    \n");
        printf("%-6.3s  %-6.3s  %-6.3s  %-6.3s  %-8.8s  %-8.8s  %10.10s\n","row","col","cw","row","column","cluster","confidence");
        for (int ii = 0; ii < sz;ii++)
        {
            bigE &b = *symbol_row[ii];
            printf("%-6.1d  %-6.1d  %-6.1d  %-6.1d  %-6.1d %-6.1d %-6.1d\n",  b.e[0] , b.e[1], b._codeword, b._row, b._column, b.cluster,b.confidence());

        }
    }

    if (curRow == -1)
    {
        throw UnknownRow();
    }
    if (curCluster == -1)
    {
        throw UnknownCluster();
    }
}

void
symbol::assignRowsToMatrix(symbol::Iter &iter)
{


    if (!this->cols)
        throw UnknownRow();


//    int sz = iter.symbol_row.size();

/*
  if (this->cols != (sz-4))
  if (dlevel>=1)
  std::cout << "WARNING: detected columns differ from embedded this->cols=" << this->cols 
  << " columns parsed " << sz << std::endl;        
*/
    int curRow=0;
    int dataCol=-1;

    for (int ii=iter.first();  iter.good()  ; ii=iter.next())
    {

        bigE &b = *(*iter); 

        if (b.symbol_type == START || b.symbol_type == STOP || b.symbol_type == LEFT_ROW || b.symbol_type == RIGHT_ROW)
        {
            continue;
        }
        
        dataCol = b._column - 1;

        curRow = b._row;

        // need to count the BAD
        if (!SYMBOL_OK(b) || b._row < 0 || b._row > 90 || dataCol < 0) {
            if (dlevel>=11)
                std::cout << "Skipping ii(" << ii << ") row(" << b._row << "," << dataCol <<") candidate code,confid,cluster(" << b.codeword() <<","<< b.confidence() << "," << b.cluster<<")"
                          << " is  BAD"
                          << std::endl;
            continue;
        }

        int effRow = b._row;
	
        if (raw_matrix[effRow][dataCol].codeword() != b.codeword())
        {
            if ( raw_matrix[effRow][dataCol].codeword()==-1)
            {
                if (dlevel>=1)
                    std::cout << "(" <<effRow << "," << dataCol <<")  code,confid,cluster(" << b.codeword() <<","<< b.confidence() 
                              << "," << b.cluster<<") inserted in empty slot" << std::endl;

                raw_matrix[effRow][dataCol] = b;
            }
            else{
                if (dlevel>=1)
                {
                    std::cout << "(" <<effRow << "," << dataCol <<") = mismatched codewords.  existing (codeword,confid,cluster)="
                              << raw_matrix[effRow][dataCol].codeword() << "," << raw_matrix[effRow][dataCol].confidence() << "," << raw_matrix[effRow][dataCol].cluster << ")"
                              << " new " << b.codeword() << "," << b.confidence() << "," << b.cluster << ")" << std::endl;
                    for (int xj=dataCol+1 ;xj<this->cols;xj++)
                    {
                        std::cout << "  here? >>> (" << effRow <<","<< xj <<") existing (codeword,confid,cluster)"
                                  << raw_matrix[effRow][xj].codeword() << "," << raw_matrix[effRow][dataCol].confidence() << "," << raw_matrix[effRow][dataCol].cluster << ")"
                                  << " new " << b.codeword() << "," << b.confidence() << "," << b.cluster << ")" << std::endl;                    
                    }
                }

                if ( raw_matrix[effRow][dataCol].confidence() < b.confidence() 
                     ||  ( raw_matrix[effRow][dataCol].confidence() == b.confidence() 
                           && (raw_matrix[effRow][dataCol]._flags & (COL_WAS_TOOSHORT|MOD_WAS_TOOSHORT))
                           && !(raw_matrix[effRow][dataCol]._flags & (COL_WAS_TOOSHORT|MOD_WAS_TOOSHORT))))
                           
                    //&& raw_matrix[effRow][dataCol]->_row == b._row
                    // && raw_matrix[effRow][dataCol]->cluster == b.cluster
                {
                    if (dlevel>=1)
                        std::cout << "Replace element (row,col)="<< effRow << "," << dataCol << std::endl;

                    raw_matrix[effRow][dataCol] = b;
                }
                else
                {
                    raw_matrix[effRow][dataCol]._confidence -= b.confidence() ;

                }
                continue;
            }
        }
        else{
            if (dlevel>=1)
                std::cout << "(" <<effRow << "," << dataCol <<  ">> matched codeword (curCluster,codeword)=" << b.cluster <<","<<b.codeword()<<")"<<std::endl;

            raw_matrix[effRow][dataCol]._confidence += b.confidence();
        }
    }
}






void symbol::data_outputs::dump()
{
    if (otype == Text)
    {
        std::cout << "Text length: " << pos << std::endl;
        std::cout << outBuf << std::endl;
    }
    else if (otype==pdf417_Byte)
    {
        for (int j = 0; j < pos ; ++j) 
            printf("%02X", outBuf[j]);
    }
}
    

void symbol::result()

{

    std::cout << "Dumping raw_matrix rows " << this->rows 
              << " columns=" << this->cols << std::endl
              << " sec_level=" << this->sec_level << std::endl
              << " dataCW (with symbol length and padding)"<< this->dataCW << std::endl
              << " paddingCW "<< this->padCW << std::endl
              << " errorCW "<< this->errorCW << std::endl
        //<< " decode: " << this->outBuf 
              << std::endl
              << std::endl;

    for (std::vector < data_outputs >::iterator iter = outputs.begin(); iter != outputs.end();iter++)
    {

        (*iter).dump();

    }


    for (int kl = 0 ; kl <= rows ; kl++)
    {
        std::cout << "row :"<< kl << "----------" << std::endl;
        for (int kc = 0; kc <= cols ; kc++)
        {  
            std::cout << "  " << kc << ". " 
                      << "  type=" << raw_matrix[kl][kc].symbol_type 
                      << "  code=" << std::dec << raw_matrix[kl][kc].codeword() << " (" << std::hex << raw_matrix[kl][kc].codeword() << ")" << std::dec
                      << "  confid=" << raw_matrix[kl][kc]._confidence << std::endl;
        }
    }
}

void symbol::dump()
{
    std::cout << "symbol avgs: modWidth=" << _modWidth << " , columnWidth=" 
              << _lastWidth << " symcount "<<symcount << std::endl;
}



//#define checkoff(av,x2,x1) { if ( av != 0 && (( (x2-x1) > (av + av * moderror)) || ( (x2-x1) < (av - av * moderror)))) 
//   std::cout << "x2-x1=" << (x2-x1) << " against average module width of throw TooShort();}


void
symbol::checkBSWidth(int x2,int x1)
{
    int wi = x2-x1;

    if ( !wi || ((symcount >= 1  && 
                  (wi < (modErrorMin * this->_modWidth))) || (!symcount && wi < (.65 * this->defaultModWidth))))
    {
        ////                 ( fabs(wi - this->_modWidth) / this->_modWidth < moderror)) 
        //	  wi <=1 ))
        //( wi < (this->_modWidth - (this->_modWidth * .25) ))))

//   std::cout << "x2-x1=" << (x2-x1) << " against average module width of throw TooShort();
        // try to normalize it
        
        throw TooShort();
    }
}


/*


# Reed Solomon codes are based on a polynomial equation where x power is 2s+1 with s = error correction level used. For sample with the level 1 we use an equation like this : a + bx + cx2 + dx3 + x4 The numbers a, b, c and d are the factors of the polynomial equation.
# For information the equation is : (x - 3)(x - 32)(x - 33).....(x - 3k) ( with k = 2s+1 ) We develop the polynomial equation and we apply a MOD 929 on each factor. These factors have been pre-computed for the 8 equations corresponding to the 8 correction levels. You can see the factors file.
# Rather than to draw the algorithm used to compute the correction CsW, I prefer to provide it to you in Basic.
Let s the correction level used, k = 2s+1 the number of correction CWs, a the factors array, m the number of data CWs, d the data CWs array and c the correction CWs array. We'll use a temporary variable t.
c and t are inited with 0. And let's go with the math fiddle :

For i = 0 To m - 1
t = (d(i) + c(k - 1)) Mod 929
For j = k - 1 To 0 Step -1
If j = 0 Then
c(j) = (929 - (t * a(j)) Mod 929) Mod 929
Else
c(j) = (c(j - 1) + 929 - (t * a(j)) Mod 929) Mod 929
End If
Next
Next
For j = 0 To k - 1
If c(j) <> 0 Then c(j) = 929 - c(j)
Next

*/

enum TextSubModeLatch
{
    // latch to upper case Alphabetic
    al ,
    // latch to lower case Alphabetic
    ll ,
    // latch to mixed punctuation & numeric
    ml ,
    // latch to punctuation 
    pl,
    // only make a byte latch submode to make the shift code work in textmode
    bl

};
enum TextShift
{
    // none
    none = ((int) bl) + 1,
    // punctuation
    ps,
    // shift to alpha
    as,
    // shift to byte compaction for 1 byte
    bs
};


struct tt
{
    int id;
    int modemap[5][2];
} textlatch [5] ={
    {al,{{27,ll},{28,ml},{29,ps},{0,0}}},
    // ll
    {ll ,{{27,as},{28,ml},{29,ps},{0,0}}},
    // ml 
    {ml, {{27,ll},{28,al},{29,ps},{25,pl},{0,0}}},
    // pl
    {pl, {{29,al},{0,0},{0,0},{0,0}}},

    {-1 ,{{0,0},{0,0},{0,0},{0,0}}}
};

static 
const char * texttable[]=
{
// uppercase (al)
    "ABCDEFGHIJKLMNOPQRSTUVWXYZ    ",
// lowercase (ll)
    "abcdefghijklmnopqrstuvwxyz    ",
// mixed 
    "0123456789&\r\t,:#-.$/+%*=^     ",
// punctuation
    ";<>@[\\]_`~!\r\t,:\n-.$/\"|*()?{}' "
};
/*
  enum TextSubModeLatch
  {

  Alpha,
  Lower,
  Mixed,
  Punctuation
  };
  enum LatchSubStates
  {
  // shifts to mode for 1 character
  ps,
  as
  };

*/

int debug=1;
int encfmt=1;

void 
symbol::convert_byte(int *cw, data_outputs &item, int len, int mode)
{
    unsigned long long codeval;
    int i, j;
    unsigned char b[6];

    codeval = 0;
    
    /* convert from base 900 to base 256 */
    
    if (len == 5 && mode == 924)
    {
        
        for (i = 0; i < 5; ++i) {
            codeval *= 900;
            codeval += *cw++;
        }
        
        if (debug > 1) 
            printf("codeval = %Lx, giving ", codeval);
        
        for (j = 0; j < 6; ++j) {
            b[5-j] = codeval % 256;
            codeval >>= 8;
        }
        for (j = 0; j < len+1; ++j) 
            item.putByte(b[j]);
    }
    else
    {
        for (int ii=0;ii<len;ii++)
        {
            item.putByte(cw[ii]);
        }
    }
}


int symbol::process( barcode_filter &filter)
{
    //unsigned char foo[121];
    //memset(foo,'A',120);
    //foo[120] = '\0';

    
    if (symbol::dlevel>0)
        std::cout << "data codewords=" << this->dataCW
                  << " padCW=" << this->padCW
                  << " errorCW=" << this->errorCW
                  << std::endl;


    filter.handle_symbol(*this);

    this->dataCW = raw_matrix[1][1].codeword();
    this->errorCW =  2 <<this->sec_level ; // (this->rows * this->cols) - this->dataCW;
    this->padCW = (this->cols * this->rows) - (this->dataCW + this->errorCW);

    if (dlevel>=1)
        result();

    LatchStates mode= Text;
    TextSubModeLatch submode = al;
    TextShift shift=none;
    data_outputs data;

    // push any pending data in case of throw
    AutoPush a(*this,data);
	int effectiveEnd = this->dataCW-1;
	for (int ii = effectiveEnd; ii >=0 &&  raw_matrix[(ii/this->cols) + 1][(ii % this->cols) + 1].codeword()==900;ii--, effectiveEnd--)
		;
	int byteMode = 901; // assumed
    for (int ii = 1; ii < effectiveEnd;ii++)
    {
        int trow = (ii/this->cols) + 1;
        int tcol = (ii % this->cols) +1 ;
        int cw = this->raw_matrix[trow][tcol].codeword();

        if (dlevel>=3)
            std::cout << "Top : absolute_cw,row,col,cw= " << ii << "," << trow << ","<<tcol << ","<< cw << std::endl;
        if (cw > 929 || cw < 0) continue;

//        if (cw==-1) { dataCW++; continue;}
        switch (cw) {
        case 900:  /* mode latch to Text Compaction mode */
            
            if (data.pos)
            {
                outputs.push_back(data);
            }
            mode= Text;
            data.reset(mode); // avoid constructor , then copy using reset

            submode = al;
            break;
        case 924:  /* mode latch to Byte Compaction (num of encoded bytes*/
        case 901:  /* mode latch to Byte Compaction */
			byteMode = cw;
            mode = pdf417_Byte;
            if (data.pos)
            {
                outputs.push_back(data);
            }
            data.reset(mode);
            
            break;
        case 902:  /* mode latch to Numeric Compaction */
            mode = Numeric;
            break;
            
        case 913:  /* mode shift to Byte Compaction */
            shift = bs;
            break;
            
        case 921:  /* reader initialization */
            break;
            
        case 922:  /* terminator codeword for Macro PDF control block */
            break;
            
        case 923:  /* Begin of optional fields in the Macro PDF control block */
            break;
        case 903:
        case 904:
        case 905:
        case 906:
        case 907:
        case 908:
        case 909:
        case 910:
        case 911:
        case 912:
        case 914:
        case 915:
        case 916:
        case 917:
        case 919:
            //reserved -- see ISO spec pg 11
            break;

//         case 924:  /* mode latch to Byte Compaction (num of encoded bytes
//                       is an integer multiple of 6) */
//             mode = pdf417_Byte;
//             break;
            
        case 925:  /* identifier for a user defined Extended Channel
                      Interpretation (ECI) */
        case 926:  /* identifier for a general purpose ECI format */
        case 927:  /* identifier for an ECI of a character set or code page */
            break;
            
        case 928:  /* Begin of a Macro PDF Control Block */
            break;
                
        default:
        {
            // Text,Numeric,Byte
            int mode_nibble[3]={1,6,5};
            int buf[6];
            memset(buf,0,6*sizeof(int));
            int jj=0;
            for ( ;jj<mode_nibble[mode] && (ii+jj) < dataCW;jj++)
            {
                int trow1 = ((ii+jj)/this->cols) + 1;
                int tcol1 = ((ii+jj) % this->cols) +1 ;
                buf[jj] =  this->raw_matrix[trow1][tcol1].codeword();
                if (buf[jj] >=900) 
                    break;
                if (dlevel>=3)
                    std::cout << "Nibble: row,col,cw= " << trow1 << ","<<tcol1 << ","<< buf[jj] << std::endl;                }
            if (jj == mode_nibble[mode])
                ii+=jj-1;
            else 
                // otherwise we came up short
                ii +=jj;

            switch (mode)
            {
            case Text:
            {
                int c[2];
                c[0] = buf[0] / 30;
                c[1] = buf[0] % 30;
                

                for (jj = 0; jj < 2; ++jj) 
                {
                    
                    switch (submode)
                    {
                    case al:
                    case ll:
                    case ml:
                    case pl:
                    {
                        bool modeShift=false;

                        for (int kk=0;kk<4 && shift == none && !modeShift && textlatch[submode].id !=-1 && textlatch[submode].modemap[kk][0] ;kk++)
                        {
                            if (textlatch[submode].modemap[kk][0]==c[jj])
                            {
                                switch(textlatch[submode].modemap[kk][1])
                                {
                                    // latches first.  they're permanent
                                case al:
                                case ll:
                                case ml:
                                case pl:
                                case bl:
                                    submode=(TextSubModeLatch)textlatch[submode].modemap[kk][1];
                                    modeShift=true;
                                    //continue;
                                    break;
                                    // shifts (one char)
                                case ps:
                                case as:
                                case bs:
                                    modeShift = true;
                                    shift = (TextShift) textlatch[submode].modemap[kk][1];
                                    //continue;
                                    break;
                                }
                            }
                        }
                        
                        
                        if (modeShift) continue;
                        if (shift!=none)
                        {
                            TextSubModeLatch old = submode;
                            submode = shift== none?  submode: 
                                (shift == ps ? pl : 
                                 (shift == as ? al : (shift==bs ? bl : submode)));


                            if (dlevel>1)
                                std::cout << "shift =" << int(submode) << " char=" <<  texttable[submode][c[jj]]<< " raw=" << c[jj]<< "  @pos=" <<jj<<std::endl;                            
                            if (shift == bs)
                                // see appendix B for the one-byte byte shift.  fyi: this is usually an error for our ecp.
                                data.putByte(c[jj]);
                            else
                                // table lookup
                                data.putByte(texttable[submode][c[jj]]);
                            
                            submode=old;
                            shift=none;
                        }
                        else
                        {
                            // turn off any shift that was on
                            if (!modeShift){
                                data.putByte(texttable[submode][c[jj]]);
//                                outBuf[pos++] = texttable[submode][c[jj]];
                                if (dlevel>1)
                                    std::cout << "submode=" << int(submode) << " char = " << texttable[submode][c[jj]]<< "  raw=" << c[jj]<< " @pos=" << jj<<std::endl;
                            }
                        }
                        break;
                    }
                    case bl:
                        // convert to byte
                        break;
                        
                        
                    };
                }
                break;
            }
            case pdf417_Byte:
            {
				int typConvert = (byteMode==924)?924:( (effectiveEnd -ii+jj) <= 5)?901:924;
				convert_byte(buf, data, jj, typConvert);
                // do something
                break;
            }
            case Numeric:
            {
                
            }
            }

        }
        };
    }

    return 0;
}


