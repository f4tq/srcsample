// copyright (c) Mongonet, Inc 2006
// ALL RIGHTS RESERVED.
// Confidential and Proprietary
//
// original: chris fortescue
//
#ifndef symbolINCLUDED
#define symbolINCLUDED
/*
  class symbol is the highest abstraction of a pdf417 barcode.  it includes both the raw symbol matrix after reading
  scan lines from a bitmap and the subsequently decoded data (getDecodedBytes).  Reed solomon correction and confidence
  ranking (see the patent) are to organizing scanrows into a raw matrix.  Reed solomon works on the raw matrix.

  This class was written by CLF with help from the patent and the ISO. 

*/

#include "bigE.h"
#include "bigETree.hpp"
#include "barcode.h"
#include <map>
#include <boost/shared_ptr.hpp>

#include <common/SmartPointer.h>


// symbol accumulates logical rows (vs- scanrows) and accumlates
// statistics about them that are applied against newer
// rows (vis. compared against prior accumulated, average stats )
// finally, will take its 


class bigRC_FeatureTree;
struct ColumnAvgs
{
    float start;
    float stop;
    ColumnAvgs():start(0.0),stop(0.0){}
    ColumnAvgs(const ColumnAvgs &cp):start(cp.start),stop(cp.stop){}
    ColumnAvgs& operator=(const ColumnAvgs &cp){ start=(cp.start),stop=(cp.stop) ; return *this;}
};

class barcode_filter;
class symbol
{
    friend class barcode_filter;
    friend class bigE;
 public:
    class Iter
        {
        public:
            Row &symbol_row;
            Iter(Row &in) : symbol_row(in){}
            Iter(const Iter &cp): symbol_row(cp.symbol_row){}
	      virtual ~Iter(){}
            virtual bool good()=0;
            virtual int first()=0;
            virtual int next()=0;
            virtual int last()=0;
            virtual bigE_ptr& operator *()=0;
            virtual int sense() = 0;
        };
    class FwdIter:public Iter
        {
        public:
            int index;
	    virtual ~FwdIter(){}
            FwdIter(Row &in) : Iter(in),index(symbol_row.size() ? 0 :-1){}
            FwdIter(const FwdIter &cp): Iter(cp),index(cp.index){}
            virtual bool good(){ return symbol_row.size() > 0 && static_cast<size_t>(index) < symbol_row.size();}
            virtual int first() { return 0;}
            virtual int last() { return symbol_row.size()-1;}
            virtual int next() { return static_cast<size_t>(index+1) < symbol_row.size() ?  ++index : (index=-1); }
            virtual bigE_ptr& operator *(){ if (!good())  throw BadReference(); return  symbol_row[index]; }
            virtual int sense() { return 1;}
        };
    class RevIter:public Iter
        {
        public:
            int index;
	    virtual ~RevIter(){}
            RevIter(Row &in) : Iter(in),index(symbol_row.size()-1){}
            RevIter(const RevIter &cp): Iter(cp),index(cp.index){}
            virtual bool good(){ return symbol_row.size() > 0 && index >= 0;}
            virtual int first() { return symbol_row.size()-1;}
            virtual int last() { return 0;}
            virtual int next() { return good() && index>0 ? --index: (index=-1); }
            virtual bigE_ptr& operator *(){ if (!good()) throw BadReference(); return symbol_row[index] ;}
            virtual int sense() {return -1;}
        };

    enum LatchStates
        {
            Text,
            Numeric,
            pdf417_Byte
        };

#define BARCODE_OUTPUT_BUFFER_SIZE 2000
    struct data_outputs{
        LatchStates otype;
        unsigned char outBuf[BARCODE_OUTPUT_BUFFER_SIZE];
        // pos -- current position in outBuf
        int pos;
        data_outputs(LatchStates o=Text):otype(o)
            {
                memset(outBuf,0,BARCODE_OUTPUT_BUFFER_SIZE);
                pos=0;
            }
       
        data_outputs(const data_outputs &cp){copy(cp);}
        data_outputs& operator=(const data_outputs &cp){ copy(cp);return *this;}
        void copy(const data_outputs &cp)
            {
                memcpy(outBuf,cp.outBuf,BARCODE_OUTPUT_BUFFER_SIZE);
                pos = cp.pos;
                otype = cp.otype;
            }
        void dump();
        void putByte(unsigned char c){ outBuf[pos++]=c;}
		void reset(LatchStates o=Text){
			otype = o;
            memset(outBuf,0,BARCODE_OUTPUT_BUFFER_SIZE);
            pos=0;
		}
    };
    class AutoPush
        {
        public:
            symbol &t;
            symbol::data_outputs &o;
            AutoPush(symbol &t1,symbol::data_outputs &o1):t(t1),o(o1){}
            ~AutoPush()
                {
                    if (o.pos)
                    {
                        t.outputs.push_back(o);
                    }
                }
        };


    struct stats{
        int cw;
        int row;
        int start;
        int stop;
        int cw_confidence;
        int run_confidence;
        
        stats():cw(-1),row(-1),start(-1),stop(-1),cw_confidence(-1),run_confidence(-1){}
        stats(int c, int r,int s, int cw_con):cw(c),row(r),start(s),stop(s),cw_confidence(cw_con),run_confidence(0){}
    };

 private:
    // vectors of average start/stop positions for a given column
    std::vector < ColumnAvgs  > stdCols;
    // all read rows with at least 1 good symbol
    Table history;
 

    int determineRow(int zz, int slot);
    int determineColumn(bigE &b);

    // do the dirty work grinding on the indicators
    void grindRowIndicators(State indicator);
   // verifies the LEFT/RIGHTrow indicators by making sure they make sense relative to the indicators above and below them
 
    void verifyRowIndicator();
 protected:
    bool seenStart;
    bool seenStop;
    // first row that has the number of columns + 4
    int firstFull;
    // last row that has the number of columns + 4
    int lastFull;
    // rows -- derived from decoding left/right row indicators
    int rows;
    // columns --derived from decoding left/right row indicators
    int cols;
    // security level -- derived from left/right row indicators
    int sec_level;
    // 
    double _lastWidth;
    // calculated (from successfully matched BS->codewords) average module width 
    double _modWidth;
    // total number of decoded BS->codewords.
    int symcount;
    // number of data codewords
    int dataCW;

    // the number of pad codewords -- calculates
    int padCW;
    // number of error codewords
    int errorCW;

    int _skew;
    // mxWindow is the mxSize inspectLocation looks to 
    // determine rows for good bigE who's row cannot be determined normally.  Default is 4 (horiz/vert) 
    int mxWindow;
    // determine the default module width used to bootstrap the first few modules.  very important or a whole bar can be missed.
    double defaultModWidth;

    // turns out that the right row indicator for K=6 is the correction to K=3. So they must be accumulated and avg'd before
    // after all rows have been read.  _rowModulus maintains that value.
    int _rowModulus;
 public:
    // VoteRecorder keeps track of running averages of 
    // rows,cols,sec_level since they can very in rare situations
    // need to be able to know one is bogus

    typedef std::map<int,int,std::less<int> > VoteRecorder;
    typedef std::vector<data_outputs> DataVector;
    // codeword matrix
    bigE  raw_matrix[91][40];
    // codeword decode buffer

    int rs_errors_corrected;

    // accumulator used to avg key 
    VoteRecorder rowVotes;
    VoteRecorder colVotes;
    VoteRecorder secVotes;
    VoteRecorder rowModulusVotes;

    std::vector<data_outputs> outputs;
    double modErrorMin;


	bool HasSeenStart() { return seenStart; }
	bool HasSeenStop() { return seenStop; }

    void setSkew(int s){ _skew=s;}
    int  getSkew() {return _skew;}

    void setRSError(int err) { rs_errors_corrected = err;}
    void setCW(int trow,int tcol, int val) { raw_matrix[trow][tcol]._codeword =  val; if (trow==1 && tcol == 1) dataCW = val;} 

    // takes up to 6 900 base codewords and converts them to 256 byte array
    void convert_byte(int *cw, data_outputs &item, int len, int mode);


    // stl help for determining highest vote getter
    struct MaxCount{
        bool operator() ( const VoteRecorder::value_type &a,const VoteRecorder::value_type &b)
            {
                return a.second < b.second;
            }
    };
    // keep track of successes 
    void successful(bigE &sym);    
    void result();
    VoteRecorder::iterator maxCount(VoteRecorder &m)
        {
            // find the highest vote count
        VoteRecorder::iterator f = std::max_element(m.begin(),m.end(),MaxCount());
        return f;
        }
    // tallyVotes keeps stats various values so as not to 
    // blow in the wind on left,right row indicator value (or mis-values)
    bool tallyVotes(VoteRecorder &m, int &master, int &challenger)
        {
            VoteRecorder::iterator iter;
            if ((iter=m.find(challenger)) != m.end())
                (*iter).second++;
            else
                m.insert(VoteRecorder::value_type(challenger,1));
        
            iter = this->maxCount(m);
            if (iter != m.end())
            {
                master = (*iter).first;
                if (master != challenger)
                {
                    return true;
                }
            }
            return false;
        }

    static int symbolCount(Row &symbol_row, int type=ALLSYM );
    
    //void
    void processScanRow(Bitmap &bm, int row, int startCol, int stopCol,Row & symbol_calc);

    // find the measured column position (not necessarily the indexed column)
    static bigE_ptr find(Row &symbol_row, int col);
    // find the symbol with a certain symbol type
    static int    find(Row &symbol_row, State symT);
    // compares clusters where A is before B.  
    static int fwdCompare(const bigE &A, const bigE &B);
    static int fwdCompare(int K, int cluster);

    // compares clusters where A is 'in front' of B (horizontal or vertical)
    static int bwdCompare(const bigE &A, const bigE &B);
    static int bwdCompare(int K, int cluster);
    // analyzeRow applies confidences, row numbers and inserts qualified
    // rows into the logical row matrix : raw_matrix.
    void analyzeRow(Row & therow, int row);
    // Decode left and right row indicators
    void processRowIndicator(bigE &b);
    void checkRowIndicator(State indicator);
    void clearRowIndicator(stats &s, State indicator);

    // assign row numbers by using left/right row numbers as starting point and the neighbor/cluster 
    // to adjust
    void assignRowNumbers(Row &symbol_row);
    // if we have good symbols but bad start/stop/rowstart/rowstop, then see if the neighbor above
    // can be used to assign the row number using the cluster/neighbor algo.
    void assignRowNumbersByHistory( Row & symbol_calc);
    // assignRowsToMatrix -- puts a cracked row of symbols into the logical row/column matrix
    void assignRowsToMatrix(symbol::Iter & symbolIter);
    // assign confidence starting in history row
    void assignConfidence(int row);
    // assign confidence to the given row.

    void assignConfidence(Row &symbol_row, int row);

    // take a given column and normalize the values, then try to lookup
    void recalibrate();
    // compute the average start/stop of all columns.  used to fill holes
    void computeColumnAvgs();
    // take all the rows, start in the middle where the first good row that
    // matches (in length) the number of elements taken from the row indicators
    // then try to find good start/stop combos and start assigning confidence
    // and row numbers.  use the good data to help less fortunate rows.
    void organizeMatrix();
    void organizeMatrix_new();
    void organizeMatrix_newer();
    void organizeMatrix2();

    void checkBSWidth(int x2,int x1);

    symbol():seenStart(false),seenStop(false),
        firstFull(-1),lastFull(-1),
        rows(0),cols(0),sec_level(0),_lastWidth(0.0),_modWidth(0.0), symcount(0),dataCW(0),padCW(0),errorCW(0),_skew(0),mxWindow(4),defaultModWidth(5.0),rs_errors_corrected(-1),
        modErrorMin(.65)
        {
        }

    void copy(const symbol &cp);
    int inspectLocation(int zz, int slot, int maxperimeter, bool follow);
public:
    symbol(const symbol &cp)
        {
            copy(cp);
        }
    symbol &operator=(const symbol &cp)
        {
            copy(cp);
            return *this;
        }


//std::vector<data_outputs>
    DataVector & output(){return this->outputs;}

    // rows int logical matrix
    int getRows() const {return rows;};
    // columns in logical matrix
    int getColumns() const {return cols;}
    // security level 0-8
    int getSecurityLevel() const {return sec_level;}
    // total data codewords
    int getDataCWCount() const {return dataCW; }
    // errorCW =  (1 << sec_level) - 2
    int getErrorCWCount() const {return errorCW;  }
    // padCW =  row*columns - dataCW - errorCW
    int getPadCWCount() const {return padCW;}
    // the position in the output buf o---o the number of bytes decoded
    // getCW -- gets codeword from logical matrix.  do not pass row > getRows() or col>getColumns.
    int getCW(int trow, int tcol) { return raw_matrix[trow][tcol].codeword();}
    bigE & ref(int trow, int tcol) { return raw_matrix[trow][tcol];}
    // the decode buffer
    // the number of errors corrected.  -1 if something went wrong (usually too many errors)
    int rsResult() const { return rs_errors_corrected;}

    int confidence(int trow, int tcol) { return raw_matrix[trow][tcol].confidence();}
    void confidence(int trow, int tcol,int newcon) {  raw_matrix[trow][tcol].confidence(newcon);}

    // the average data column width of successfull matched bs patterns

    double symWidth() { return symcount>=5 ?_lastWidth : modWidth() * 17 ;}

    // the average module width derived from successfully decoded data columns

    double modWidth() { return symcount>=5 ? _modWidth : defaultModWidth;}

    int count() {return symcount;}

    void setMaxInspectWindow(int e) { mxWindow=e;}
    int getMaxInpsectWindow(){ return mxWindow;}

    void setDefaultModuleWidth(double width){ defaultModWidth = width;}
    double  getDefaultModuleWidth(){ return defaultModWidth ;}

    void setModuleErrorMin(double m){modErrorMin=m;}
    double moduleErrorMin(){ return modErrorMin;}

    Table & getHistory(){return this->history;}

    void assignRowNumbers( bigRC_ptr box, int start , int stop);

    bool good();

    void dump();

    // The following methods should probably be protected.

    // after we've read all the keywords, chew on the raw matrix
    // apply error correct and decode the codewords.
    int process( barcode_filter &);

    // debug level
    static int dlevel;

    // this is 'main' for a given pdf417.  This should be the callers interface to using this class.

    static void pdf417_clf(SmartPointer<symbol> & sym, Bitmap &bm, Geometry &box, int mxWindow=4, double defaultModWidth=5.0);

    // blobfinder interface is another form of 'main'

//    static SmartPointer<symbol> processBarCodes(Bitmap &bm,  std::vector<BlobFinder::Blob> &blobs);
    
        

    void  dumpBigRC(bigRC_FeatureTree &tree);


};


class barcode_filter
{
 protected:
    // accessors granted access to protected methods
    // of symbol
    void setCW(symbol &thesymbol, int trow,int tcol, int val);
    void setRSError(symbol &thesymbol, int rez);
    void addRows(symbol &thesymbol,int delta);
    void addColumns(symbol &thesymbol,int delta);
public:
    virtual ~barcode_filter(){}
    virtual void handle_symbol(symbol & thebar)=0;
};

#ifdef WIN32
#include <math.h>
/*
round() --- rounds away from zero
trunc() --- rounds towards zero
floor() --- rounds towards negative infinity
ceil() --- rounds towards positive infinity

nearbyint() --- round to even (use current rounding mode)
rint() --- round to even (use current rounding mode)

*/
inline double rint(double roundMe){
	double ii;
	return ( modf(roundMe,&ii) >= 0.5)?(ii + 1.0):ii;
}
inline double round(double roundMe) { return (double)(rint(roundMe)); }
inline float roundf(float roundMe) { return (float)(rint((double)roundMe)); }
#endif

#define MAX_PIXELS_PER_MODULE 9

#endif
