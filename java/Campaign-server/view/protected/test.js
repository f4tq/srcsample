var webServices = new Object();
var groups = new Object();

ServiceParam = function(name, key)
{
  this.name = name;
  this.key = key; 
  this.value = "#{" + key + "}";
}

ServiceMetadata = function(name, group)
{
  this.name = name;
  this.group = group;
  this.parameters = new Array();
  this.conversational = false;

  webServices[name] = this;

  ServiceMetadata.prototype.setDescription = function(description) { this.description = description; };  
  ServiceMetadata.prototype.getDescription = function() { return this.description; };
  ServiceMetadata.prototype.addParameter = function(param) { this.parameters.push(param); };
  ServiceMetadata.prototype.setRequest = function(request) { this.request = request; };
  ServiceMetadata.prototype.getRequest = function() { return this.request; };
  ServiceMetadata.prototype.setConversational = function(val) { this.conversational = val; };
  ServiceMetadata.prototype.isConversational = function() { return this.conversational; };
}

// start of web service definitions

var svc = new ServiceMetadata("login", "Security");
svc.setDescription("Login");
svc.setRequest("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" " +
               "\n    xmlns:web=\"http://webservices.mongonet.net/\">\n  <soapenv:Header/>" +
               "\n  <soapenv:Body>" +
               "\n    <web:login>" +
               "\n      <arg0>#{username}</arg0>" +
               "\n      <arg1>#{password}</arg1>" +
               "\n    </web:login>" +
               "\n  </soapenv:Body>" +
               "\n</soapenv:Envelope>");
svc.addParameter(new ServiceParam("Username", "username"));
svc.addParameter(new ServiceParam("Password", "password"));    

svc = new ServiceMetadata("saveCampaign", "Save Campaign");
svc.setDescription("Sets the current campaign used in outbound email");
svc.setRequest("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:web=\"http://webservices.mongonet.net/\">" + 
               "<soapenv:Header/>" + 
	       "<soapenv:Body>" + 
	       "<web:saveCampaign>" + 
	           "<arg0>#{acctId}</arg0>" +
	           "<arg1>#{companyName}</arg1>" +
               "<arg2>#{targetUrl}</arg2>" + 
               "</web:saveCampaign>" +
               "</soapenv:Body>" + 
               "</soapenv:Envelope>" );
svc.addParameter(new ServiceParam("Account", "acctId"));
svc.addParameter(new ServiceParam("CompanyName", "companyName"));
svc.addParameter(new ServiceParam("Url", "targetUrl"));

svc = new ServiceMetadata("getPublicId", "Get Campaign Id");
svc.setDescription("Gets the current campaign's publicId for use in URLs");
svc.setRequest("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:web=\"http://webservices.mongonet.net/\">" + 
               "<soapenv:Header/>" + 
	       "<soapenv:Body>" + 
	       "<web:getPublicId>" + 
	           "<arg0>#{acctId}</arg0>" +
               "</web:getPublicId>" +
               "</soapenv:Body>" + 
               "</soapenv:Envelope>" );
svc.addParameter(new ServiceParam("Account", "acctId"));

svc = new ServiceMetadata("getRedirect", "Get Campaign's target URL");
svc.setDescription("Gets the current campaign's target URL");
svc.setRequest("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:web=\"http://webservices.mongonet.net/\">" + 
               "<soapenv:Header/>" + 
	       "<soapenv:Body>" + 
	       "<web:getRedirect>" + 
	           "<arg0>#{acctId}</arg0>" +
               "</web:getRedirect>" +
               "</soapenv:Body>" + 
               "</soapenv:Envelope>" );
svc.addParameter(new ServiceParam("Account", "acctId"));


svc = new ServiceMetadata("acquireImpression", "Acquire Impression");
svc.setDescription("Retrieves an impression for the given acct_id.  Email,Email Type, Phonenumber and keywords are all used to decide on the impression served");
svc.setRequest("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:web=\"http://webservices.mongonet.net/\">" + 
               "<soapenv:Header/>" + 
	       "<soapenv:Body>" + 
	       "<web:acquireImpression>" + 
	           "<arg0>#{acctId}</arg0>" +
	           "<arg1>#{xid}</arg1>" +
               "<arg2>#{email}</arg2>" + 
               "<arg3>#{emailType}</arg3>" + 
               "<arg4>#{phoneNumber}</arg4>" + 
               "<arg5>#{keywords}</arg5>" + 
           "</web:acquireImpression>" +
           "</soapenv:Body>" + 
           "</soapenv:Envelope>" );
svc.addParameter(new ServiceParam("Account", "acctId"));
svc.addParameter(new ServiceParam("Xid", "xid"));
svc.addParameter(new ServiceParam("Email", "email"));
svc.addParameter(new ServiceParam("EmailType", "emailType"));
svc.addParameter(new ServiceParam("SrcPhone", "phoneNumber"));
svc.addParameter(new ServiceParam("Keywords", "keywords"));


// end of web service definitions

function getEndpoint()
{
  return document.getElementById("endpoint").value; 
}

var selectedService = null;         

function setAllParams()
{
  if (!selectedService)
    return;
  
  var request = selectedService.request;  
  
  for (var i = 0; i < selectedService.parameters.length; i++)
  {
    var param = selectedService.parameters[i];
    var search = "#{" + param.key + "}";
    
    request = request.replace(search, param.value);
  } 
  
  // Set the conversation ID
  request = request.replace("#{conversationId}", document.getElementById("conversationId").value);
  
  document.getElementById("serviceRequest").value = request;  
}

function setParamValue(event)
{
  var ctl = null
  if (event.target)
    ctl = event.target;
  else if (window.event.srcElement)
    ctl = window.event.srcElement;
    
  var key = ctl.id;
  
  for (var i = 0; i < selectedService.parameters.length; i++)
  {
    var param = selectedService.parameters[i];
    if (param.key == key)
    {
      param.value = ctl.value;
      break;
    }
  } 
  
  setAllParams();
}

function selectService(serviceName)
{
  var svc = webServices[serviceName];
  
  if (!svc)
  {
    alert("Unknown service");
    return;
  }
  
  selectedService = svc;
  
  document.getElementById("selectedService").innerHTML = svc.getDescription();
  document.getElementById("serviceResponse").value = null;
  
  var ctl = document.getElementById("parameters");
  for (var i = ctl.childNodes.length - 1; i >= 0; i--)
  {
     ctl.removeChild(ctl.childNodes[i]);
  }

  var tbl = document.createElement("table");
  tbl.cellspacing = 0;
  tbl.cellpadding = 0;
  
  ctl.appendChild(tbl);
    
  for (var i = 0; i < svc.parameters.length; i++)
  {
     var row = tbl.insertRow(-1);
     
     var td = document.createElement("td");
     row.appendChild(td);
     td.innerHTML = svc.parameters[i].name;
          
     var inp = document.createElement("input");

     inp.id = svc.parameters[i].key;
     inp.value = svc.parameters[i].value;
     inp.onchange = setParamValue;
     inp.onkeyup = setParamValue;
     
     td = document.createElement("td");
     row.appendChild(td);
     td.appendChild(inp);
  }
  
  document.getElementById("conversationId").readOnly = !svc.isConversational();
  
  setAllParams();
}

function sendRequest()
{
  if (!selectedService)
  {
    alert("Please select a service first");
    return;
  }
  
  document.getElementById("serviceResponse").value = null;
  
  var req;
  if (window.XMLHttpRequest)
  {
    req = new XMLHttpRequest();
    if (req.overrideMimeType)
      req.overrideMimeType("text/xml");
  }
  else
    req = new ActiveXObject("Microsoft.XMLHTTP");
    
  req.onreadystatechange = function() { receiveResponse(req); };
  req.open("POST", getEndpoint(), true);
  req.setRequestHeader("Content-type", "text/xml");
  req.send(document.getElementById("serviceRequest").value);
}

function receiveResponse(req)
{
  if (req.readyState == 4)
  {
    if (req.responseText)
      document.getElementById("serviceResponse").value = req.responseText;
      
    if (req.responseXML)
    {
      var cid = extractConversationId(req.responseXML);
      
      if (cid)
      {
        document.getElementById("conversationId").value = cid;
      }
    }
      
    if (req.status != 200)
    {
      alert("There was an error processing your request.  Error code: " + req.status);      
    }
  }  
}

function extractConversationId(doc)
{
  var headerNode;

  if (doc.documentElement)
  {
    for (var i = 0; i < doc.documentElement.childNodes.length; i++)
    {
      var node = doc.documentElement.childNodes.item(i);
      if (node.localName == "Header")
        headerNode = node;
    }
  }

  if (headerNode)
  {
    for (var i = 0; i < headerNode.childNodes.length; i++)
    {
      var node = headerNode.childNodes.item(i);
      if (node.localName == "conversationId")
      {
        return node.firstChild.nodeValue;
      }
    }
  }    
}

function initServices()
{
  for (var i in webServices)
  {    
    var ws = webServices[i];
    
    var anchor = document.createElement("a");
    anchor.href = "javascript:selectService('" + ws.name + "')";  
    anchor.appendChild(document.createTextNode(ws.getDescription()));

    if (!groups[ws.group])
    {
      groups[ws.group] = document.createElement("div");
      var groupTitle = document.createElement("span");
      groupTitle.appendChild(document.createTextNode(ws.group));
      groups[ws.group].appendChild(groupTitle);
      document.getElementById("services").appendChild(groups[ws.group]); 
    }
    
    groups[ws.group].appendChild(anchor);    
  }
}

initServices();
