	        jQuery.noConflict(); // Avoid conflicts with the RichFaces/Prototype library
			
			// The following 9 parameters must be filled out prior to init running!
			
			var baseLayerURL;
			var baseLayerName;
		
    	    var drillableLayerURL;
			var drillableLayerName;
		
			var queryLayer;
			var drillTarget;

			var mapWidth;
			var mapHeight;
		
			var conversationID;			

            var map;
            var tiled;
            var areacodesTiled=[];
			var periods = ['5day','30day','mtd','year','ytd'];
			//var curPeriod='30day';
			var curPeriod=null;
			

            // pink tile avoidance
            OpenLayers.IMAGE_RELOAD_ATTEMPTS = 5;
            // make OL compute scale according to WMS spec
            OpenLayers.DOTS_PER_INCH = 25.4 / 0.28;


        		
            function init(){
                // if this is just a coverage or a group of them, disable a few items,
                // and default to jpeg format

                if (baseLayerURL == null || baseLayerName==null || drillableLayerURL == null || drillableLayerName==null || queryLayer==null || drillTarget==null 
                	|| mapWidth==null || mapHeight==null || conversationID == null)
                {
                	alert("init missing parameter");
                	return;
                }
                format = 'image/png';
                var bounds = new OpenLayers.Bounds(
                    -127.61950065, 23.7351786,
                    -64.08177035, 50.592523400000005
                );
                var options = {
                    controls: [],
                    maxExtent: bounds,
                    maxResolution: 0.248194258984375,
                    projection: "EPSG:4326",
                    units: 'degrees'
                };
                
                map = new OpenLayers.Map('map', options);
            
                // setup tiled layer
                tiled = new OpenLayers.Layer.WMS(
                    "States - Tiled", baseLayerURL,
                    {
                        width: mapWidth,
                        srs: 'EPSG:4326',
                        layers: baseLayerName,
                        height: mapHeight,
                        styles: '',
                        format: format,
                        tiled: 'false',
                        tilesOrigin : "-127.61950065, 23.7351786" 
                    },
                    {singleTile: true, ratio: 1}
                );
                        // "-180.83519804999997,15.255426699999997"
                var tilesOriginXY = "-127.61950065, 23.7351786";

				map.addLayer(tiled);
				for (ii =0; ii < periods.length;ii++)
				{
					var urlB = drillableLayerURL + "-" + periods[ii] + "?cid="+ conversationID + "&conversationPropagation=join&";
					if (accountAcctSeqid != null)
						urlB += "accountAcctSeqid=" + accountAcctSeqid + "&";
					if (campaignCid != null)
						urlB += "campaignCid=" + campaignCid + "&";

					
					var layer = new OpenLayers.Layer.WMS(
	                    periods[ii],
	                    urlB, 
    	                {
        	                width: mapHeight,
            	            srs: 'EPSG:4326',
                	        layers: drillableLayerName + periods[ii],
                    	    height: mapHeight,
                        	styles: '',
	                        format: format,
    	                    tiled: 'false',
        	                tilesOrigin : "-127.61950065, 23.7351786" ,
            	            transparent: 'true'
	                    },
    	                {
        	                isBaseLayer: false,
            	            opacity: 0.9,
            	            singleTile: true, 
            	            ratio: 1 
                	    }
                	);
					layer.setVisibility(false);
					map.addLayer(layer);
					
                }
                
                map.addControl(new OpenLayers.Control.PanZoomBar({
                    position: new OpenLayers.Pixel(2, 15)
                }));
                map.addControl(new OpenLayers.Control.Navigation());
                map.addControl(new OpenLayers.Control.Scale($('scale')));
                map.addControl(new OpenLayers.Control.MousePosition({element: $('location')}));
                map.addControl(new OpenLayers.Control.LayerSwitcher());
                
                map.zoomToExtent(bounds);

                tiled.setVisibility(true);
                map.setBaseLayer(tiled);
                
                switchLayer('5day');
                map.events.register('click', map, handleClick);

           }

           function handleClick(e)
           {
               document.getElementById('nodelist').innerHTML = "Loading... please wait...";
               var params = {
                        REQUEST: "GetFeatureInfo",
                        EXCEPTIONS: "application/vnd.ogc.se_xml",
                        BBOX: map.getExtent().toBBOX(),
                        X: e.xy.x,
                        Y: e.xy.y,
                        INFO_FORMAT: "text/html",
                        QUERY_LAYERS: queryLayer + curPeriod,
                        FEATURE_COUNT: 50,
                        Srs: 'EPSG:4326',
                        Layers: drillableLayerName + curPeriod,
                        Styles: '',
                        WIDTH: map.size.w,
                        HEIGHT: map.size.h,
                        format: new OpenLayers.Format.XML() ,
                        cid : conversationID,
                        accountAcctSeqid: (accountAcctSeqid != null ? accountAcctSeqid : ''),
                        campaignCid: (campaignCid != null ? campaignCid : ''),
                        conversationPropagation:'join'
                        };
                OpenLayers.loadURL(drillTarget + curPeriod, params, this, setHTML, setHTML);
                OpenLayers.Event.stop(e);
           }
           function switchLayer(lay)
           {
	           setPeriod(lay);
	           var viewing = "Viewing ";
	           if (lay == '1day')
	           		viewing += '1-day';
	           else if (lay == '5day')
	           		viewing += '5-day';
	           else if (lay == '30day')
	           		viewing += '30-day';
	           else if (lay == 'mtd')
	           		viewing += 'Month-To-Date';
	           else if (lay == 'ytd')
	           		viewing += 'Year-To-Date';
	           else if (lay == 'year')
	           		viewing += '1 Year';
			   else viewing += lay;
	           jQuery('#notice').html( viewing );
	       }
           function setPeriod( newPeriod)
           {
                if (curPeriod != null)
                {
	                var oldLayer = map.getLayersByName(curPeriod);

					if (oldLayer.length > 0)
	    	            oldLayer[0].setVisibility(false);
					else {
	            		alert("Couldn't find layer: " + curPeriod);
					}
				}
                var newLayer = map.getLayersByName(newPeriod);
                if (newLayer.length > 0)
	                newLayer[0].setVisibility(true);
                curPeriod=newPeriod;
                document.getElementById('nodelist').innerHTML = 'Click the map';
            }
            function remap(table,nameMap,colMap)
            {
				jQuery(table).find("tr th").each(function(tr){
					var t = jQuery(this).text();
					colMap[tr] = t;
					nameMap[t] = tr;
				});
            }
            
           	function setHTML(response){
				var nodelist = document.getElementById('nodelist');
				jQuery(nodelist).hide();

                nodelist.innerHTML = response.responseText;

				var table = jQuery("table.featureInfo", nodelist);
				if (table != null && table[0].rows.length > 1)
				{
					// remove the fid column
					jQuery(table).find('th:first-child, td:first-child' ).remove();
				
					var colMap = [];
					var nameMap=[];
					// remove the polluted colums taking calling remap before proceeding
					var pollution= ['color','fid','fips'];
					for (var zz=0; zz < pollution.length;zz++)
					{
						colMap = [];
						nameMap=[];

						remap(table,nameMap,colMap);
							
						if (nameMap[pollution[zz]]){
							var stmt = 'th:nth-child(' + (nameMap[pollution[zz]] + 1) + '), td:nth-child(' 
								+ (nameMap[pollution[zz]] +1) +  ')';
							jQuery(table).find( stmt ).remove();
						}
					}
					remap(table,nameMap,colMap);
					
					// the geoserver result is missing thead which is required for sorting
					jQuery('<thead></thead>').prependTo(table);
					
//					jQuery('caption',table).prependTo(table); 
					jQuery('caption',table).remove(); 

					jQuery('tr:has(th)',table).prependTo(jQuery('thead',table)); 

					jQuery(table).find("tr th:contains('acode')").each(function(index){
						jQuery(this).text('AreaCode');
						});
					jQuery(table).find("tr th:contains('state')").each(function(index){
						jQuery(this).text('State');
						});
					jQuery(table).find("tr th:contains('acct_id')").each(function(index){
						jQuery(this).text('Acct#');
						});
					jQuery(table).find("tr th:contains('cid')").each(function(index){
						jQuery(this).text('Campaign#');
						});
					jQuery(table).find("tr").each(function(tr){
						if (jQuery(this).find('td').size() > 0)
						{
							var id = null;
							if (nameMap['acct_id']){
								id = jQuery(this.cells[ nameMap['acct_id'] ]).text();
								jQuery("<td class='link'><a href=\"/campaign/ManagedAccount.mongo?cid=" + conversationID + "&accountAcctSeqid=" +  id  + '\">' + id + "</a></td>").replaceAll(this.cells[ nameMap['acct_id'] ]);
							}
							
							if (nameMap['cid']){
							
								var cid = jQuery(this.cells[ nameMap['cid'] ]).text();

								jQuery("<td class='link'><a href=\"/campaign/ManagedCampaign.mongo?cid=" + conversationID  + '&campaignCid=' + cid +'\">' + cid + "</a></td>" + (id != null ? ( "&accountAcctSeqid=" +  id ) : '')).replaceAll(this.cells[ nameMap['cid'] ]);
							}
							jQuery(':nth-child(' + (nameMap['acode']+1) + ')',this).wrapInner('<b></b>');
						}
					});
					
					jQuery(table).tablesorter({widthFixed: true,
											widgets: ['zebra'],
											sortList: [[nameMap['total_minutes'],1]]});
//						.tablesorterPager({container: jQuery("#pager")});
				}
				else {
					jQuery(nodelist).innerHTML = '<p>No minutes for this areacode</p>';
				}
				jQuery('tbody tr:has(.odd)',table).removeClass('odd');
				jQuery('tbody tr:has(.even)',table).removeClass('even');
//				jQuery('tbody tr:odd',table).addClass('odd');
//				jQuery('tbody tr:even',table).addClass('even');

				jQuery(table).removeClass("featureInfo").addClass("tablesorter");

				jQuery(nodelist).show();
		
            }

			function transformRichTab(item)
			{
				var table = jQuery('div#accountUsersesChildren table', item);
				jQuery(table).addClass("tablesorter");
				jQuery(table).tablesorter({widthFixed: true,
											widgets: ['zebra']});
				;
				alert('transformRichTab completed :' + jQuery(table).attr('id'));
			}

			