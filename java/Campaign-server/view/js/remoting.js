            var map;
            var untiled;
            var tiled;
            var areacodesTiled;
            var pureCoverage = false;
            var acctIds="";
            
            // pink tile avoidance
            OpenLayers.IMAGE_RELOAD_ATTEMPTS = 5;
            // make OL compute scale according to WMS spec
            OpenLayers.DOTS_PER_INCH = 25.4 / 0.28;
            
            function init()
            {
	            var campaign = Seam.Component.getInstance("CampaignJS");
            	alert("Calling accountList :" + campaign);
	        	campaign.getAccountList( paintItCallback);
	        }
            function init2(){
                // if this is just a coverage or a group of them, disable a few items,
                // and default to jpeg format
                format = 'image/png';
                if(pureCoverage) {
                    document.getElementById('filterType').disabled = true;
                    document.getElementById('filter').disabled = true;
                    document.getElementById('antialiasSelector').disabled = true;
                    document.getElementById('updateFilterButton').disabled = true;
                    document.getElementById('resetFilterButton').disabled = true;
                    document.getElementById('jpeg').selected = true;
                    format = "image/jpeg";
                }
            /*
                var bounds = new OpenLayers.Bounds(
                    -180.83519804999997, 15.255426699999997,
                    -59.764400949999995, 74.0288733
                );
                var options = {
                    controls: [],
                    maxExtent: bounds,
                    maxResolution: 0.4729328011718749,
                    projection: "EPSG:4326",
                    units: 'degrees'
                };
				*/                
                var bounds = new OpenLayers.Bounds(
                    -127.61950065, 23.7351786,
                    -64.08177035, 50.592523400000005
                );
                var options = {
                    controls: [],
                    maxExtent: bounds,
                    maxResolution: 0.248194258984375,
                    projection: "EPSG:4326",
                    units: 'degrees'
                };
                
                map = new OpenLayers.Map('map', options);
            
                // setup tiled layer
                tiled = new OpenLayers.Layer.WMS(
                    "States - Tiled", "#{geoserverPath}/wms",
                    {
                        width: '800',
                        srs: 'EPSG:4326',
                        layers: 'topp:JustAnOutline',
                        height: '364',
                        styles: '',
                        format: format,
                        tiled: 'true',
                        tilesOrigin : "-127.61950065, 23.7351786" 
                    }
                );
                        // "-180.83519804999997,15.255426699999997"

                areacodesTiled = new OpenLayers.Layer.WMS(
                    "Areacode", 
                    "#{geoserverPath}/wms",
                    {
                        width: '800',
                        srs: 'EPSG:4326',
                        layers: 'mongonet:areacode_account_30day',
                        height: '364',
                        styles: '',
                        format: format,
                        tiled: 'true',
                        tilesOrigin : "-127.61950065, 23.7351786" ,
                        CQL_FILTER: 'acct_id=41147', 
                        transparent: 'true'
                        
                    },
                    {
                        isBaseLayer: false,
                        opacity: 1.0,
                    }
                );
                //tilesOrigin : "-180.83519804999997,15.255426699999997",
            
                // setup single tiled layer
                untiled = new OpenLayers.Layer.WMS(
                    "States - Untiled", "#{geoserverPath}/wms",
                    {
                        width: '800',
                        srs: 'EPSG:4326',
                        layers: 'topp:JustAnOutline',
                        height: '364',
                        styles: '',
                        format: format
                    },
                    {singleTile: true, ratio: 1} 
                );
        
                map.addLayers([untiled, tiled, areacodesTiled]);

                // build up all controls            
                map.addControl(new OpenLayers.Control.PanZoomBar({
                    position: new OpenLayers.Pixel(2, 15)
                }));
                map.addControl(new OpenLayers.Control.Navigation());
                map.addControl(new OpenLayers.Control.Scale($('scale')));
                map.addControl(new OpenLayers.Control.MousePosition({element: $('location')}));
                map.addControl(new OpenLayers.Control.LayerSwitcher({'ascending':true}));
                map.zoomToExtent(bounds);
                
                // wire up the option button
                var options = document.getElementById("options");
                options.onclick = toggleControlPanel;
                
                // support GetFeatureInfo
                map.events.register('click', map, function (e) {
                    document.getElementById('nodelist').innerHTML = "Loading... please wait...";
                    var params = {
                        REQUEST: "GetFeatureInfo",
                        EXCEPTIONS: "application/vnd.ogc.se_xml",
                        BBOX: map.getExtent().toBBOX(),
                        X: e.xy.x,
                        Y: e.xy.y,
                        INFO_FORMAT: 'text/html',
                        QUERY_LAYERS: areacodesTiled.params.LAYERS,
                        FEATURE_COUNT: 50,
                        Srs: 'EPSG:4326',
                        Layers: 'mongonet:areacode_account_30day',
                        Styles: '',
                        WIDTH: map.size.w,
                        HEIGHT: map.size.h,
                        CQL_FILTER: 'acct_id=41147', 
                        format: format};
                    updateFeatureInfoFilters(params);
                    OpenLayers.loadURL("#{geoserverPath}/wms", params, this, setHTML, setHTML);
                    OpenLayers.Event.stop(e);
                });
            }
            
            // sets the HTML provided into the nodelist element
            function setHTML(response){
                document.getElementById('nodelist').innerHTML = response.responseText;
            };
            
            // shows/hide the control panel
            function toggleControlPanel(event){
                var toolbar = document.getElementById("toolbar");
                if (toolbar.style.display == "none") {
                    toolbar.style.display = "block";
                }
                else {
                    toolbar.style.display = "none";
                }
                event.stopPropagation();
                map.updateSize()
            }
            
            // Tiling mode, can be 'tiled' or 'untiled'            
            function setTileMode(tilingMode){
                if (tilingMode == 'tiled') {
                    untiled.setVisibility(false);
                    tiled.setVisibility(true);
                    areacodesTiled.setVisibility(true);

                    map.setBaseLayer(tiled);
                }
                else {
                    untiled.setVisibility(true);
                    tiled.setVisibility(false);

                    map.setBaseLayer(untiled);
                    areacodesTiled.setVisibility(true);

                }
            }
            
            // changes the current tile format
            function setImageFormat(mime){
                // we may be switching format on setup
                if(tiled == null)
                  return;
                  
                tiled.mergeNewParams({
                    format: mime
                });
                untiled.mergeNewParams({
                    format: mime
                });
                /*
                var paletteSelector = document.getElementById('paletteSelector')
                if (mime == 'image/jpeg') {
                    paletteSelector.selectedIndex = 0;
                    setPalette('');
                    paletteSelector.disabled = true;
                }
                else {
                    paletteSelector.disabled = false;
                }
                */
            }
            
            function setAntialiasMode(mode){
                tiled.mergeNewParams({
                    format_options: 'antialias:' + mode
                });
                untiled.mergeNewParams({
                    format_options: 'antialias:' + mode
                });
            }
            
            function setPalette(mode){
                if (mode == '') {
                    tiled.mergeNewParams({
                        palette: null
                    });
                    untiled.mergeNewParams({
                        palette: null
                    });
                }
                else {
                    tiled.mergeNewParams({
                        palette: mode
                    });
                    untiled.mergeNewParams({
                        palette: mode
                    });
                }
            }
            
            function setWidth(size){
                var mapDiv = document.getElementById('map');
                var wrapper = document.getElementById('wrapper');
                
                if (size == "auto") {
                    // reset back to the default value
                    mapDiv.style.width = null;
                    wrapper.style.width = null;
                }
                else {
                    mapDiv.style.width = size + "px";
                    wrapper.style.width = size + "px";
                }
                // notify OL that we changed the size of the map div
                map.updateSize();
            }
            
            function setHeight(size){
                var mapDiv = document.getElementById('map');
                
                if (size == "auto") {
                    // reset back to the default value
                    mapDiv.style.height = null;
                }
                else {
                    mapDiv.style.height = size + "px";
                }
                // notify OL that we changed the size of the map div
                map.updateSize();
            }
            
            function updateFilter(){
                // merge the new filter definitions
                var filterParams = getFilterParams();
                mergeNewParams(filterParams);
            }
            
            function getFilterParams() {
              if(pureCoverage)
                  return null;
            
                var filterType = document.getElementById('filterType').value;
                var filter = document.getElementById('filter').value;
                
                // by default, reset all filters                
                var filterParams = {
                    filter: null,
                    cql_filter: null,
                    featureId: null
                };
                if (OpenLayers.String.trim(filter) != "") {
                    if (filterType == "cql") 
                        filterParams["cql_filter"] = filter;
                    if (filterType == "ogc") 
                        filterParams["filter"] = filter;
                    if (filterType == "fid") 
                        filterParams["featureId"] = filter;
                }
                return filterParams;
            }
            
            function updateFeatureInfoFilters(featureInfoParams){
                var filterParams = getFilterParams();
                if(!filterParams)
                  return;
                  
                featureInfoParams["cql_filter"] = filterParams["cql_filter"];
                featureInfoParams["filter"] = filterParams["filter"];
                featureInfoParams["featureId"] = filterParams["featureId"];
            }
            
            function resetFilter() {
                if(pureCoverage)
                  return;
            
                document.getElementById('filter').value = "";
                updateFilter();
            }
            
            function mergeNewParams(params){
                tiled.mergeNewParams(params);
                untiled.mergeNewParams(params);
            }

function paintItCallback(result)
{
	alert("paintItCallback result:" + result);
    for (var ii=0; ii < result.length;ii++)
    {
    	if (acctIds.length > 0)
    		acctIds += ",";
		acctIds += result[ii].acctSeqid;	
	}
	alert("acctIds = " + acctIds);
    init2();
}
