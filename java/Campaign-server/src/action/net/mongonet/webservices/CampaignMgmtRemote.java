package net.mongonet.webservices;
import javax.ejb.Remote;

import net.mongonet.beans.ejb.CampaignWS.NotFound;
import net.mongonet.beans.ejb.CampaignWS.AlreadyExists;

import java.net.MalformedURLException;
import java.net.URL;

@Remote
public interface CampaignMgmtRemote {

	public boolean login(String username, String password);
	public void saveCampaign(long acctId, String companyName, URL target) throws AlreadyExists;
	public URL getRedirect( long acctId) throws MalformedURLException, NotFound ;
	public String getPublicId( long acctId) throws  NotFound;
	public String recordHit(String publicId, String xidIn) throws MalformedURLException,NotFound;
    
	public String acquireImpression(long acctId, String xidIn, String email, String emailType, String phoneNumber,String keywords) throws MalformedURLException,NotFound;


}
 