package net.mongonet.webservices;
import java.util.List;

import javax.ejb.Stateless;
import javax.jws.WebMethod;
import javax.jws.WebService;

import net.mongonet.beans.ejb.CampaignWS;
import net.mongonet.beans.ejb.Users;
import net.mongonet.beans.ejb.CampaignWS.NotFound;
import net.mongonet.beans.ejb.CampaignWS.AlreadyExists;

import org.jboss.seam.Component;
//import org.jboss.seam.annotations.Logger;
//import org.jboss.seam.log.Log;
import org.jboss.seam.security.Identity;

import java.net.MalformedURLException;
import java.net.URL;
import org.jboss.seam.log.*;
/*
 * Implement Soap Service
 */
@Stateless
@WebService(name = "CampaignService", serviceName = "CampaignService")
public class CampaignMgmt implements CampaignMgmtRemote {
	
//	@Logger private Log log;
//	private static Logger log = org.jboss.seam.log.Logging. getLogger(CampaignMgmt.class);
    private static final LogProvider log = Logging.getLogProvider(CampaignMgmt.class);

	@WebMethod
	public boolean login(String username, String password)
	{
		Identity.instance().setUsername(username);
		Identity.instance().setPassword(password);
		Identity.instance().login();
		return Identity.instance().isLoggedIn();
	}

	@WebMethod
	public String recordHit(String publicId, String xidIn) throws MalformedURLException,NotFound
	{
		log.info("recordHit " + publicId + " ," + xidIn);
		CampaignWS campaignHome = (CampaignWS) Component.getInstance("campaignWS",true);
		return campaignHome.recordHit(publicId, xidIn);
	}

	@WebMethod
	public URL getRedirect(long acctId) throws MalformedURLException, NotFound {
		
		log.info("getCurrent called with " + acctId);
		CampaignWS campaignHome = (CampaignWS) Component.getInstance("campaignWS",true);
		return campaignHome.getRedirect(acctId);
	}

	@WebMethod
	public String getPublicId(long acctId) throws NotFound {
		
		log.info("getCurrent called with " + acctId);
		CampaignWS campaignHome = (CampaignWS) Component.getInstance("campaignWS",true);
		return campaignHome.getPublicId(acctId);
	}
	
	@WebMethod
	public void saveCampaign(long acctId, String companyName, URL target) throws AlreadyExists {
		log.info("saveCampaign called with " + acctId + "  "+ target.toExternalForm());
		
		CampaignWS campaignHome = (CampaignWS) Component.getInstance("campaignWS",true);
		campaignHome.saveCampaign(acctId, companyName, target);
	}
	@WebMethod
	public String acquireImpression(long acctId, String xidIn, String email, String emailType, String phoneNumber, String keywords) throws MalformedURLException,NotFound
	{
		CampaignWS campaignHome = (CampaignWS) Component.getInstance("campaignWS",true);
		return campaignHome.acquireImpression(acctId, xidIn,email,emailType, phoneNumber,keywords);

	}


}
