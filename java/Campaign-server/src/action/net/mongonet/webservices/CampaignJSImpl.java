package net.mongonet.webservices;

import java.net.URL;

import net.mongonet.beans.ejb.CampaignWS;

import org.jboss.seam.Component;
import org.jboss.seam.security.Identity;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.log.Log;
import org.jboss.seam.security.Identity;
import org.jboss.seam.web.ServletContexts;

import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;
import org.jboss.seam.annotations.Factory;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.NoResultException;
import javax.servlet.http.HttpServletRequest;

import java.util.Calendar;
import java.util.Date;
import org.jboss.seam.annotations.Begin;
import org.jboss.seam.annotations.End;
import org.jboss.seam.annotations.remoting.WebRemote;

import net.mongonet.beans.ejb.CampaignWS.AlreadyExists;

import net.mongonet.beans.ejb.CampaignList;
import net.mongonet.beans.ejb.AccountList;
import org.jboss.seam.annotations.remoting.WebRemote;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.annotations.Transactional;
// Seam Remoting implementation (ie Javascript access)

@Transactional
@Name("CampaignJS")
public class CampaignJSImpl 
//implements CampaignJS 
{
	@Logger private Log log;

	protected void destroy() {
		log.info("Destroyed ...");
	}
	
	@Restrict("#{not identity.loggedIn}")
	public boolean login(String username, String password) {
		Identity.instance().setUsername(username);
		
		Identity.instance().setPassword(password);
		Identity.instance().login();
		return Identity.instance().isLoggedIn();
	}

	@Restrict("#{identity.loggedIn}")
	public void logout()
	{
		log.info("loggedOut ...");
	}

	@WebRemote
	@Restrict("#{identity.loggedIn}")
	public void saveCampaign(long acctId, String companyName, URL target) throws AlreadyExists {
		log.info("saveCampaign called with " + acctId + "  "+ target.toExternalForm());
		CampaignWS campaignHome = (CampaignWS) Component.getInstance("campaignWS",true);
		campaignHome.saveCampaign(acctId,companyName,target);
	} 

	// hibernate filtering will reduce the size of these returned
	@WebRemote(exclude={"account","domainRollups","campaignUsages","campaignImpressions","areacodeRollups","campaignRollups"})
	@Restrict("#{identity.loggedIn}")
	public List getCampaignList(){
		log.debug("getCampaignList ...");

		CampaignList list = (CampaignList) Component.getInstance("campaignList", true);
		 return  list.getResultList();
	 }

	 // hibernate filtering will reduce the size of these returned
	 @WebRemote(exclude={"campaigns","accountRollups","accountOptions","peraccountPhoneExclusionses","accountUserses","accountPhonenumberses"})
	 @Restrict("#{identity.loggedIn}")
	 public List getAccountList(){
		log.debug("getAccountList ...");
		AccountList list = (AccountList) Component.getInstance("accountList", true);
	 	return  list.getResultList();
 	}
}
