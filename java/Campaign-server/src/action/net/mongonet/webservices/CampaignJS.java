package net.mongonet.webservices;
import java.net.URL;
import java.util.Set;
import javax.ejb.Local;

import net.mongonet.beans.ejb.CampaignWS.AlreadyExists;

import org.jboss.seam.annotations.remoting.WebRemote;
import java.util.List;
@Local
public interface CampaignJS {

	 public void destroy();

	 @WebRemote public boolean login(String username, String password);
	 @WebRemote public void logout();

	 @WebRemote public void saveCampaign(long acctId, String companyName, URL target)  throws AlreadyExists ;
	 @WebRemote public void listCampaigns(long acctId);

	 // hibernate filtering will reduce the size of these returned
	 @WebRemote public List getCampaignList();

	 // hibernate filtering will reduce the size of these returned
	 @WebRemote public List getAccountList();

}
