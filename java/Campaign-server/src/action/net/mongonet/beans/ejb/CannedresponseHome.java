package net.mongonet.beans.ejb;

import net.mongonet.beans.ejb.*;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.framework.EntityHome;

@Name("cannedresponseHome")
public class CannedresponseHome extends EntityHome<Cannedresponse> {

	public void setCannedresponseCannedSeqid(Long id) {
		setId(id);
	}

	public Long getCannedresponseCannedSeqid() {
		return (Long) getId();
	}

	@Override
	protected Cannedresponse createInstance() {
		Cannedresponse cannedresponse = new Cannedresponse();
		return cannedresponse;
	}

	public void wire() {
	}

	public boolean isWired() {
		return true;
	}

	public Cannedresponse getDefinedInstance() {
		return isIdDefined() ? getInstance() : null;
	}

}
