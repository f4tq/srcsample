package net.mongonet.beans.ejb;

import org.jboss.seam.core.Expressions;
import org.jboss.seam.core.Expressions.ValueExpression;

import net.mongonet.beans.ejb.*;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Factory;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.framework.EntityQuery;
import java.util.List;
import java.util.Arrays;
import org.jboss.seam.security.Identity;

@Name("accountList2")
public class AccountList2 extends EntityQuery {

	private static final ValueExpression[] RESTRICTIONS = {Expressions.instance().createValueExpression("lower(account.companyName) like concat('%',concat(lower(#{accountList2.account.companyName}),'%'))"),
											Expressions.instance().createValueExpression(" account.acctSeqid like concat(#{accountList2.account.acctSeqid != 0 ? accountList2.account.acctSeqid : ''},'%')")};

	private Account account = new Account();
	
	@Override
	public String getEjbql() {
	    
	    
	    if (Identity.instance().isLoggedIn())
	    {
	    	if (Identity.instance().hasRole("support") || Identity.instance().hasRole("admin"))
	    		return "select account from Account account";
	    	else
	    		return "select account from Users users left join users.accountUserses bridge join bridge.account account where users.userSeqid =  #{currentUser.userSeqid}" 
	    				;
	    }
	    else
	    	return "select account from Account account where account.acctSeqid is null";
	}

	/*
	@Override
	public Integer getMaxResults() {
		
		return 25;
	}
	*/
	public AccountList2()
	{
		this.setMaxResults(25);
		this.setOrder("account.acctSeqid asc");
		
	}
	public Account getAccount() {
		return account;
	}
	@Override
	public List getResultList()
	{
		return super.getResultList();
	}
	
	@Override
	public List<ValueExpression> getRestrictions() {
		return Arrays.asList(RESTRICTIONS);
	}
	/*
	@Factory(value="pattern", scope=ScopeType.EVENT)
	public String getSearchPattern()
	{
		return searchString==null ?
				"%" : '%' + searchString.toLowerCase().replace('*', '%') + '%';
	}
	*/
	// flamingo support
	public void  setAcctSeqid( Integer id)
	{
		if (id != null)
			this.account.setAcctSeqid(id);
	}
	public void setCompanyName(String searchString)
	{
		this.account.setCompanyName( searchString );
	}


}
