package net.mongonet.beans.ejb;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import net.mongonet.beans.ejb.*;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.framework.EntityQuery;
import java.util.List;
import java.util.Arrays;

import javax.persistence.EntityManager;
import javax.transaction.UserTransaction;

@Scope(ScopeType.STATELESS)
@Name("helloAction")
public class FlamingoTest {
	public String hello(String name) {  
		return "Hello, " + name;  
		}

	public void setFilter(String matchMe)
	{
		XidsList x = (XidsList) Component.getInstance("xidsList",true);
		x.getXids().setXid(matchMe);
	}
	
	public List matches(String matchMe)
	{
		XidsList x = (XidsList) Component.getInstance("xidsList",true);
		x.getXids().setXid(matchMe);
		x.setMaxResults(5);
		List foo =  x.getResultList();
		return foo;
/*
 * 	
		UserTransaction userTx = null;
		boolean startedTx = false;
		userTx = (UserTransaction) Component.getInstance("org.jboss.seam.transaction.transaction");

		try {
			int status = userTx.getStatus();
			try {
				if ( status != javax.transaction.Status.STATUS_ACTIVE) {

					startedTx = true;
					userTx.begin();
				}
				EntityManager em = (EntityManager) Component.getInstance("entityManager");
			}
			catch (Exception ex) {
				try {
					if (startedTx) 
						userTx.rollback();

				} catch (Exception rbEx) {
					rbEx.printStackTrace();
				}
				catch (javax.transaction.SystemException e)
				{

				}
			}
			*/
		}
}  	
	
