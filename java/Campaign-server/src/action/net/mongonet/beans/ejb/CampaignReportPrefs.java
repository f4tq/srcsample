package net.mongonet.beans.ejb;

import net.mongonet.beans.ejb.*;

import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.framework.EntityQuery;
import java.util.List;
import java.util.Arrays;

import java.util.Calendar;
import java.util.Date;
import org.hibernate.validator.NotNull;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Basic;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.log.*;

@Name("campaignPreferences")
//@Scope(ScopeType.CONVERSATION)
@Scope(ScopeType.SESSION)
public class CampaignReportPrefs  {

	
	private Date start = new Date(0);
	private Date stop = Calendar.getInstance().getTime();
	private Character rollupType= 'd';
    private static final LogProvider log = Logging.getLogProvider(CampaignReportPrefs.class);
	
	@Create
	public void init()
	{
		Calendar cal = Calendar.getInstance();


		cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), 1, 0, 0, 0);
		cal.add(Calendar.DAY_OF_MONTH, -1);
		stop = cal.getTime();
		cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), 1, 0, 0, 0);
		start = cal.getTime();
		
		log.info("campaignPreferences.Create start:" + start + " end:" + stop );

	}
	
	@NotNull
	@Basic	@Temporal(TemporalType.TIMESTAMP)
	public void setStart(Date start)
	{
		this.start = start;
	}
	public Date getStart()
	{
		return this.start;
	}
	public void setStop(Date stop)
	{
		this.stop = stop;
	}

	@NotNull
	@Basic	@Temporal(TemporalType.TIMESTAMP)
	public Date getStop()
	{
		return this.stop;
	}
	public Character getRollupType() {
		return rollupType;
	}
	public void setRollupType(Character rollupType) {
		this.rollupType = rollupType;
	}
	
	
}
