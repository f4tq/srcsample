package net.mongonet.beans.ejb;

import org.jboss.seam.core.Expressions;
import org.jboss.seam.core.Expressions.ValueExpression;

import net.mongonet.beans.ejb.*;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.framework.EntityQuery;
import java.util.List;
import java.util.Arrays;

@Name("areacodeRollupDetailList")
public class AreacodeRollupPrefList extends EntityQuery {

	private static final ValueExpression[] RESTRICTIONS = {Expressions.instance().createValueExpression("lower(areacodeRollup.areacode) like concat(lower(#{areacodeRollupDetailList.areacodeRollup.areacode}),'%')"),
							  Expressions.instance().createValueExpression("lower(areacodeRollup.rollupType) like concat(lower(#{campaignPreferences.rollupType}),'%')"),
						      Expressions.instance().createValueExpression("areacodeRollup.campaign.cid like concat(#{areacodeRollupDetailList.campaignCid},'%')"),
						      Expressions.instance().createValueExpression("areacodeRollup.campaign.account.acctSeqid like concat(#{areacodeRollupDetailList.accountAcctSeqid},'%')"),
						      Expressions.instance().createValueExpression("areacodeRollup.transDate >= #{campaignPreferences.start}"),
						      Expressions.instance().createValueExpression("areacodeRollup.transDate <= #{campaignPreferences.stop}"),
	};

	private AreacodeRollup areacodeRollup = new AreacodeRollup();
	Long campaignCid;
	Long acctSeqid;
	
	public Long getCampaignCid()
	{
		return campaignCid != null && campaignCid != 0 ? campaignCid : null;
	}
	public void setCampaignCid(Long newOne)
	{
		campaignCid = newOne;
	}
	
	public Long getAccountAcctSeqid()
	{
		return acctSeqid != null && acctSeqid != 0 ? acctSeqid : null;
	}
	public void setAccountAcctSeqid(Long newOne)
	{
		acctSeqid = newOne;
	}

	@Override
	public String getEjbql() {
		return "select areacodeRollup from AreacodeRollup areacodeRollup";
	}

	@Override
	public Integer getMaxResults() {
		return super.getMaxResults();
	}

	public AreacodeRollup getAreacodeRollup() {
		return areacodeRollup;
	}

	@Override
	public List<ValueExpression> getRestrictions() {
		return Arrays.asList(RESTRICTIONS);
	}

}
