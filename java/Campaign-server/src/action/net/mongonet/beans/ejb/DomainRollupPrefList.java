package net.mongonet.beans.ejb;

import org.jboss.seam.core.Expressions;
import org.jboss.seam.core.Expressions.ValueExpression;

import net.mongonet.beans.ejb.*;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.framework.EntityQuery;
import java.util.List;
import java.util.Arrays;

@Name("domainRollupDetailList")
public class DomainRollupPrefList extends EntityQuery {

	private static final ValueExpression[] RESTRICTIONS = {
		Expressions.instance().createValueExpression("domainRollup.domainName like concat(upper(#{domainRollupDetailList.domainRollup.domainName}),'%')"),
		Expressions.instance().createValueExpression("domainRollup.rollupType like concat(lower(#{campaignPreferences.rollupType}),'%')"),
		Expressions.instance().createValueExpression("domainRollup.campaign.cid like concat(#{domainRollupDetailList.campaignCid},'%')"),
		Expressions.instance().createValueExpression("domainRollup.transDate >= #{campaignPreferences.start}"),
		Expressions.instance().createValueExpression("domainRollup.transDate <= #{campaignPreferences.stop}"),
	
	};

	private DomainRollup domainRollup = new DomainRollup();
	private Long campaignCid;
	public Long getCampaignCid()
	{
		return campaignCid != null && campaignCid != 0 ? campaignCid : null;
	}
	public void setCampaignCid(Long newOne)
	{
		campaignCid = newOne;
	}

	@Override
	public String getEjbql() {
		return "select domainRollup from DomainRollup domainRollup";
	}
	
	@Override
	public Integer getMaxResults() {
		return super.getMaxResults();
	}

	public DomainRollup getDomainRollup() {
		return domainRollup ;
	}

	@Override
	public List<ValueExpression> getRestrictions() {
		return Arrays.asList(RESTRICTIONS);
	
	}
	@Override
	public List getResultList()
	{
		List l = super.getResultList();
		
		return l;
	}

}
