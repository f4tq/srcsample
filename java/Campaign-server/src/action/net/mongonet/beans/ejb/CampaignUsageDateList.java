package net.mongonet.beans.ejb;

import org.jboss.seam.core.Expressions;
import org.jboss.seam.core.Expressions.ValueExpression;

import net.mongonet.beans.ejb.*;

import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.framework.EntityQuery;
import java.util.List;
import java.util.Arrays;

import java.util.Calendar;
import java.util.Date;
import org.hibernate.validator.NotNull;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Basic;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.log.Log;

@Name("campaignUsageDateList")
public class CampaignUsageDateList extends EntityQuery {

	@Logger private Log log;
	private static final ValueExpression[] RESTRICTIONS = 
	{	Expressions.instance().createValueExpression("campaignUsage.campaign.cid = #{campaignHome.id}"),
		Expressions.instance().createValueExpression("campaignUsage.clicked >= #{campaignPreferences.start}"),
		Expressions.instance().createValueExpression("campaignUsage.clicked <= #{campaignPreferences.stop}"),
		Expressions.instance().createValueExpression("lower(campaignUsage.ip) like concat(lower(#{campaignUsageList.campaignUsage.ip}),'%')"),};

	private CampaignUsage campaignUsage = new CampaignUsage();

	
	@Override
	public String getEjbql() {
		return "select campaignUsage from CampaignUsage campaignUsage";
	}

	@Override
	public Integer getMaxResults() {
		return 25;
	}

	public CampaignUsage getCampaignUsage() {
		return campaignUsage;
	}

	@Override
	public List<ValueExpression> getRestrictions() {
		return Arrays.asList(RESTRICTIONS);
	}
	
	@Override 
	public List getResultList()
	{
	//	log.info("Rendered sql #0" , super.getRenderedEjbql());
		
		List foo = super.getResultList();
		
		return foo;
	}
     
}
