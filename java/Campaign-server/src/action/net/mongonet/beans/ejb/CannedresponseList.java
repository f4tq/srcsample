package net.mongonet.beans.ejb;

import org.jboss.seam.core.Expressions;
import org.jboss.seam.core.Expressions.ValueExpression;

import net.mongonet.beans.ejb.*;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.framework.EntityQuery;
import java.util.List;
import java.util.Arrays;

@Name("cannedresponseList")
public class CannedresponseList extends EntityQuery {

	private static final ValueExpression[] RESTRICTIONS = {
			Expressions.instance().createValueExpression("lower(cannedresponse.subject) like concat(lower(#{cannedresponseList.cannedresponse.subject}),'%')"),
			Expressions.instance().createValueExpression("lower(cannedresponse.body) like concat(lower(#{cannedresponseList.cannedresponse.body}),'%')"),};

	private Cannedresponse cannedresponse = new Cannedresponse();

	@Override
	public String getEjbql() {
		return "select cannedresponse from Cannedresponse cannedresponse";
	}

	@Override
	public Integer getMaxResults() {
		return 25;
	}

	public Cannedresponse getCannedresponse() {
		return cannedresponse;
	}

	@Override
	public List<ValueExpression> getRestrictions() {
		return Arrays.asList(RESTRICTIONS);
	}

}
