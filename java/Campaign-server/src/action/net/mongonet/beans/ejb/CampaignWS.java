package net.mongonet.beans.ejb;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.log.Log;
import org.jboss.seam.security.Identity;
import org.jboss.seam.web.ServletContexts;

import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;
import org.jboss.seam.annotations.Factory;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.NoResultException;
import javax.servlet.http.HttpServletRequest;

import java.util.Calendar;
import java.util.Date;
import org.jboss.seam.annotations.Begin;
import org.jboss.seam.annotations.End;

import java.net.MalformedURLException;
import java.net.URL;

import javax.faces.context.FacesContext;
import org.jboss.seam.annotations.security.Restrict;


@Name("campaignWS")
public class CampaignWS {
	@Logger private Log log;

	@In (create=true) 
	AccountHome accountHome;

	@In (create=true) 
	CampaignHome campaignHome;

	@In (create=true) 
	CampaignList campaignList;

	@In(create=true)
	protected EntityManager entityManager;
	
	public class NotFound extends Exception
	{
		String msg= "Not found";
		public NotFound(){}
		public NotFound(String s)
		{
			msg = s;
		}
		public String toString()
		{
			return msg != null ? msg : super.toString();
		}
	};
	public class AlreadyExists extends Exception
	{
		String msg="Already Exists";
		public AlreadyExists(){}
		public AlreadyExists(String msg)
		{
			this.msg=msg;
		}
		public String toString()
		{
			return msg != null ? msg : super.toString();
		}
	}
	
	@Begin
	@Restrict("#{identity.loggedIn}")
	public URL getRedirect(long acct_seqid) throws MalformedURLException,NotFound
	{
		List messages  =  entityManager.createQuery(
				"select acc,cam from Account acc left join acc.campaigns as cam " +
				"where acc.acctSeqid=:a order by cam.startTime desc")
				
		.setParameter("a", acct_seqid)
		.setMaxResults(1)
		.getResultList();

		System.out.println( messages.size() + " campaigns found:" );
		java.util.ListIterator iter = messages.listIterator();
		if (iter.hasNext())
		{
			java.lang.Object lm [] = (java.lang.Object[]) iter.next();
			Account a = (Account) lm[0];
			Campaign r = (Campaign) lm[1];
			if (r == null)
				throw new NotFound("No campaign established for this account");
			return new URL(r.getLink());
		}
		else 
		{
			log.error("No such account #0 ",acct_seqid);
			throw new NotFound("No such account");
		}
	}
	@Begin
	@Restrict("#{identity.loggedIn}")

	public String getPublicId(long acct_seqid) throws NotFound
	{
		List messages  =  entityManager.createQuery(
				"select acc,cam from Account acc left join acc.campaigns as cam " +
				"where acc.acctSeqid=:a order by cam.startTime desc")
				
		.setParameter("a", acct_seqid)
		.setMaxResults(1)
		.getResultList();

//		System.out.println( messages.size() + " campaigns found:" );
		java.util.ListIterator iter = messages.listIterator();
		if (iter.hasNext())
		{
			java.lang.Object lm [] = (java.lang.Object[]) iter.next();
			Account a = (Account) lm[0];
			Campaign r = (Campaign) lm[1];
			if (a == null){
				log.error("No such account #0 ",acct_seqid);
				throw new NotFound("No such account");
			}
			else if (r == null)
			{
				log.error("No campaigns for this account #0 ",acct_seqid);
				throw new NotFound("No campaign exists for this account");
			}
			return r.getPublicId();
		}
		else 
		{
			log.error("No such account #0 ",acct_seqid);
			throw new NotFound();
		}
	}
	@Begin
	@End
	@Restrict("#{identity.loggedIn}")

	public String recordHit(String publicId, String xidIn) throws MalformedURLException,NotFound
	{
		HttpServletRequest req =null;
		if (FacesContext.getCurrentInstance() != null && FacesContext.getCurrentInstance().getExternalContext() != null)
			req = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
		if (req == null)
			req = ServletContexts.getInstance().getRequest();
		
		boolean justRedirect = false;
		
		Xids  x=null;
		try {
			x =  (Xids) entityManager.createQuery(
					"select xid from Xids  xid  " +
			"where xid.xid=:x")
			.setParameter("x", xidIn)
			.getSingleResult();
		}
		catch (NoResultException e )
		{
			x = new Xids();
			x.setXid(xidIn);
			entityManager.persist(x);
		}
		catch (Exception e)
		{
			log.info("Unknown exception finding Xid. Not recording but attempting redirect anyway" );
			justRedirect = true;
		}
		List messages  =  entityManager.createQuery(
				"select acc,cam from Campaign cam join cam.account acc " +
				"where cam.publicId = :a order by cam.startTime desc")
		.setParameter("a", publicId)
		.setMaxResults(1)
		.getResultList();
		
		java.util.ListIterator iter = messages.listIterator();
		if (iter.hasNext())
		{
			java.lang.Object lm [] = (java.lang.Object[]) iter.next();
			Account a = (Account) lm[0];
			Campaign r = (Campaign) lm[1];

			if (justRedirect)
				return r.getLink();

			CampaignUsage use = new CampaignUsage();
			use.setCampaign(r);
			use.setXids(x);
			use.setClicked(Calendar.getInstance().getTime());
			use.setIp(req.getRemoteAddr());
			entityManager.persist(use);
			return r.getLink();
			
		}
		else{
			throw new NotFound();
		}
	}
	
	@Begin
	@Restrict("#{identity.loggedIn}")

	public String saveCampaign(long acctId, String companyName, URL target) throws AlreadyExists{
		
		Account a = null;
		try {
			a =  (Account) entityManager.createQuery(
					"select acc from Account acc  " +
				"where acc.acctSeqid=:a ")
			.setParameter("a", acctId)
			.getSingleResult();
		}
		catch (NoResultException e)
		{
			log.error("No such account #0 .  Making it.",acctId );
			
			a = new Account();
			a.setAcctSeqid(acctId);
			a.setCompanyName(companyName);
			entityManager.persist(a);
		}
		log.info("Acct Id #0 #1 located.  Has #2 campaigns" , acctId,a.getCompanyName(),a.getCampaigns().size() );
		
		Iterator ii = a.getCampaigns().iterator();
		boolean found=false;
		while (ii.hasNext())
		{
			Campaign c = (Campaign) ii.next();
			log.info("campaign #0 #1 #2 against #3" , c.getName(),c.getPublicId(),c.getStartTime(),target.toExternalForm());
			if (c.getLink().equals(target.toExternalForm()))
			{
				log.info("Campaign exists for target #0" , target);
				c.setStartTime(Calendar.getInstance().getTime());
				entityManager.persist(c);
				throw new AlreadyExists("Campaign Already Exists");
			}
		}
		Campaign c2 = new Campaign();
		c2.setAccount(a);
		c2.setName(target.toExternalForm());
		c2.setLink(target.toExternalForm());
		c2.setStartTime(Calendar.getInstance().getTime());
		entityManager.persist(c2);
		entityManager.flush();
		entityManager.refresh(c2);
		return c2.getPublicId();
	}
//	@Restrict("#{identity.loggedIn}")
	public String acquireImpression(long acct_seqid, String xidIn, String email, String emailType, String phoneNumber, String keywords) throws MalformedURLException,NotFound
	{
		try {
			HttpServletRequest req =null;
			if (FacesContext.getCurrentInstance() != null && FacesContext.getCurrentInstance().getExternalContext() != null)
				req = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
			if (req == null)
				req = ServletContexts.getInstance().getRequest();

			List messages  =  entityManager.createQuery(
					"select acc,cam from Account acc left join acc.campaigns as cam " +
			"where acc.acctSeqid=:a order by cam.startTime desc")

			.setParameter("a", acct_seqid)
			.setMaxResults(1)
			.getResultList();

			java.util.ListIterator iter = messages.listIterator();
			if (iter.hasNext())
			{
				java.lang.Object lm [] = (java.lang.Object[]) iter.next();
				Account a = (Account) lm[0];
				Campaign r = (Campaign) lm[1];
				EmailAddresses e=null;
				Xids x=null;

				if (a == null){
					log.error("recordCampaign: No such account #0 ",acct_seqid);
					throw new NotFound("No such account");
				}
				else if (r == null)
				{
					log.error("recordCampaign: No campaigns for this account #0 ",acct_seqid);
					throw new NotFound("No campaign exists for this account");
				}
				if (email != null)
				{
					email = email.toUpperCase();
					
					try {
						e = (EmailAddresses) entityManager.createQuery("select ema from EmailAddresses ema where emailAddress=:ca")
						.setParameter("ca", email)
						.getSingleResult();
					}	
					catch(javax.persistence.NoResultException ex3)
					{
						e = new EmailAddresses();
						e.setEmailAddress(email);
						entityManager.persist(e);
					}	
				}
				try {
					x = (Xids) entityManager.createQuery("select xids from Xids xids where xid=:ca")
					.setParameter("ca", xidIn)
					.getSingleResult();
				}	
				catch(javax.persistence.NoResultException ex3)
				{
					x = new Xids();
					x.setXid(xidIn);
					entityManager.persist(x);
				}
				Phonenumbers p=null;

				if (phoneNumber != null)
				{
					try {
						p = (Phonenumbers) entityManager.createQuery("select phone from Phonenumbers phone where phoneNumber=:ca")
						.setParameter("ca", phoneNumber)
						.getSingleResult();
					}	
					catch(javax.persistence.NoResultException ex3)
					{
						p = new Phonenumbers();
						p.setPhoneNumber(phoneNumber);
						entityManager.persist(p);
					}
				}

				CampaignImpression use = new CampaignImpression();
				use.setCampaign(r);
				use.setXids(x);
				use.setRequested(Calendar.getInstance().getTime());
			use.setIp(req.getRemoteAddr());
				use.setEmailAddresses(e);
				if (emailType != null && emailType.length() == 1)
					use.setEmailType(emailType.charAt(0));
				use.setPhonenumber(p);
				entityManager.persist(use);
				return r.getPublicId();
			}
			else 
			{
				log.error("No such account #0 ",acct_seqid);
				throw new NotFound();
			}
		}
	
		catch (Exception e)
		{
			log.info("Caught #0 #1. Satisfing PHP by returning null", e.getClass().toString(),e.getMessage());
			return null;
		}
	}
}
