package net.mongonet.beans.flamingo;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import net.mongonet.beans.ejb.*;
import org.jboss.seam.framework.EntityQuery;
import java.util.List;
import java.util.Arrays;

import javax.persistence.EntityManager;
import javax.transaction.UserTransaction;
import org.jboss.seam.annotations.security.Restrict;
import javax.ejb.Remove;
import javax.persistence.EntityManager;

import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Out;

import org.jboss.seam.annotations.Logger;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.log.Log;

import org.jboss.seam.annotations.Factory;
import java.util.ArrayList;
import java.util.List;
import java.util.Date;

@Scope(ScopeType.SESSION)
@Name("campaignAPI")
@Restrict("#{identity.loggedIn}")
public class CampaignAPI 
{
	@Factory("userID")
	public Users getLoggedInUser()
	{
		return (Users) Component.getInstance("currentUser");
	}
	// returns an Account given an id.  flamingo function necessitated by the improper
	// conversion by flamingo in flex of a long int to a short int on the jboss side.  this 
	// causes a method argument conversion error.  
	public Account getAccount(long ii)
	{
		EntityManager em = (EntityManager) Component.getInstance("entityManager", true);
		return (Account) em.createQuery("select account from Account account where account.acctSeqid = :inId")
		.setParameter("inId", ii)
		.getSingleResult();
	}
	// returns an Campaign given an id.  flamingo function necessitated by the improper
	// conversion by flamingo in flex of a long int to a short int on the jboss side.  this 
	// causes a method argument conversion error.  

	public Campaign getCampaign(long ii)
	{
		EntityManager em = (EntityManager) Component.getInstance("entityManager", true);
		return (Campaign) em.createQuery("select campaign from Campaign campaign where campaign.cid = :inId")
		.setParameter("inId", ii)
		.getSingleResult();
	}

	@Factory("loadCampaigns")
	public List getCampaigns(long acctSeqid)
	{
		EntityManager em = (EntityManager) Component.getInstance("entityManager", true);
		return em.createQuery("select camp from Account account join account.campaigns camp " +
				"where account.acctSeqid = :inId ")
		.setParameter("inId", acctSeqid)
		.getResultList();
	}
	public List getCampaignAreacodes(long cid, Date start, Date end)
	{
		EntityManager em = (EntityManager) Component.getInstance("entityManager", true);
		return em.createQuery("select distinct rollup.areacode from Campaign campaign join campaign.areacodeRollups rollup " +
				"where campaign.cid = :inId ")
		.setParameter("inId", cid)
		.setParameter("start", start)
		.setParameter("end", end)
		.getResultList();
	}
	public List getAccountAreacodes(long inAcct, Date start, Date end)
	{
		EntityManager em = (EntityManager) Component.getInstance("entityManager", true);
		return em.createQuery("select distinct rollup.areacode from Account account join account.campaigns campaign join campaign.areacodeRollups rollup " +
				"where campaigns.cid = :inId ")
		.setParameter("inId", inAcct)
		.setParameter("start", start)
		.setParameter("end", end)
		.getResultList();
	}

	// given cid, date range
	// returns areacode rollup records ordered by areacode,transDate
	public List getDailyAreacodeSet(/* Campaign */ long inCid, Date start, Date end)
	{
		EntityManager em = (EntityManager) Component.getInstance("entityManager", true);

		return em.createQuery("select rollup from Campaign campaign  join campaign.areacodeRollups rollup " +
				"where campaign.cid = :inId " +
				"and rollup.rollupType='d' " +
				"and rollup.transDate between :start and :end order by rollup.areacode,rollup.transDate ")
		.setParameter("inId", inCid)
		.setParameter("start", start)
		.setParameter("end", end)
		.getResultList();
	}
	// 
	// given cid and date range
	// returns cid,transDate summarized (rollup fields set). (NOT rollup records!)
	// NOTE: this disregards intermediate areacode rollup records in the result set
	//
	public List getCampaignDailySummarySet(/* Campaign */ long inCid, Date start, Date end)
	{
		AreacodeRollup r ;
	
		EntityManager em = (EntityManager) Component.getInstance("entityManager", true);
		return em.createQuery("select campaign.cid, rollup.transDate " +
				" ,sum(rollup.totalCharges) as totalCharges" +
				" ,sum(rollup.totalClicks) as totalClicks" +
				" ,sum(rollup.totalEmails) as totalEmails " +
				" ,sum(rollup.totalMinutes) as totalMinutes " +
				" ,sum(rollup.totalPages) as totalPages " +
				" , sum(rollup.totalTx) as totalTx" +
				" , sum(rollup.totalUniqEmails) as totalUniqEmails" +
				" from Campaign campaign join campaign.areacodeRollups rollup " +
				" where campaign.cid = :inId " +
				" and rollup.rollupType='d' " +
				" and rollup.transDate between :start and :end " +
				" group by campaign.cid,rollup.transDate " +
				" order by campaign.cid,rollup.transDate ")
		.setParameter("inId", inCid)
		.setParameter("start", start)
		.setParameter("end", end)
		.getResultList();
	}
	// 
	// given cid and date range
	// returns areacode, summarized (rollup fields set). (NOT rollup records!)
	// NOTE: this disregards intermediate areacode rollup records in the result set
	//
	public List getCampaignPeriodTotals(/* Campaign */ long inCid, Date start, Date end)
	{
		AreacodeRollup r ;
	
		EntityManager em = (EntityManager) Component.getInstance("entityManager", true);
		return em.createQuery("select campaign.cid " +
				" ,sum(rollup.totalCharges) as totalCharges" +
				" ,sum(rollup.totalClicks) as totalClicks" +
				" ,sum(rollup.totalEmails) as totalEmails " +
				" ,sum(rollup.totalMinutes) as totalMinutes " +
				" ,sum(rollup.totalPages) as totalPages " +
				" , sum(rollup.totalTx) as totalTx" +
				" , sum(rollup.totalUniqEmails) as totalUniqEmails" +
				" from Campaign campaign join campaign.areacodeRollups rollup " +
				" where campaign.cid = :inId " +
				" and rollup.rollupType='d' " +
				" and rollup.transDate between :start and :end " +
				" group by campaign.cid " +
				" order by campaign.cid ")
		.setParameter("inId", inCid)
		.setParameter("start", start)
		.setParameter("end", end)
		.getResultList();
	}
	// 
	// given cid and date range
	// returns areacode, summarized (rollup fields set). (NOT rollup records!)
	// NOTE: this disregards intermediate areacode rollup records in the result set
	//
	public List getCampaignAreacodePieSet(/* Campaign */ long inCid, Date start, Date end)
	{
		AreacodeRollup r ;
	
		EntityManager em = (EntityManager) Component.getInstance("entityManager", true);
		return em.createQuery("select rollup.areacode " +
				" ,sum(rollup.totalCharges) as totalCharges" +
				" ,sum(rollup.totalClicks) as totalClicks" +
				" ,sum(rollup.totalEmails) as totalEmails " +
				" ,sum(rollup.totalMinutes) as totalMinutes " +
				" ,sum(rollup.totalPages) as totalPages " +
				" , sum(rollup.totalTx) as totalTx" +
				" , sum(rollup.totalUniqEmails) as totalUniqEmails" +
				" from Campaign camp join camp.areacodeRollups rollup " +
				" where camp.cid = :inId " +
				" and rollup.rollupType='d' " +
				" and rollup.transDate between :start and :end " +
				" group by rollup.areacode " +
				" order by rollup.areacode ")
		.setParameter("inId", inCid)
		.setParameter("start", start)
		.setParameter("end", end)
		.getResultList();
	}
	
	// returns domain_rollup records ordered by domain,transdate
	public List getDailyDomainSet(/* Campaign */ long inCid, Date start, Date end)
	{
		EntityManager em = (EntityManager) Component.getInstance("entityManager", true);
		return em.createQuery("select rollup from Campaign campaign  join campaign.domainRollups rollup " +
				"where campaign.cid = :inId " +
				"and rollup.rollupType='d' " +
				"and rollup.transDate between :start and :end " +
				" order by rollup.domain,rollup.transDate ")
		.setParameter("inId", inCid)
		.setParameter("start", start)
		.setParameter("end", end)
		.getResultList();
	}
	// give acct and date range
	// returns all rollup records 
	// ordered by acctSeqid,cid,areacode,transDate
	// NOTE: returns multiple cids
	public List getAcctCampAreacodeRollups(/* Account */ long inAcct, Date start, Date end)
	{
		EntityManager em = (EntityManager) Component.getInstance("entityManager", true);
		
		return em.createQuery("select rollup from Account account join account.campaigns campaigns join campaigns.areacodeRollups rollup " +
				"where account.acctSeqid = :inId " +
				"and rollup.rollupType='d' " +
				"and rollup.transDate between :start and :end order by account.acctSeqid, campaigns.cid,rollup.areacode,rollup.transDate ")
		.setParameter("inId", inAcct)
		.setParameter("start", start)
		.setParameter("end", end)
		.getResultList();
	}

	// given acct, date range
	// returns acctSeqid,cid,transDate, and the sum(rollup fields)
	// ordered acctSeqid/cid/transDate  
	// sums individual areacode rollup records
	public List getAcctCampaignSummarySet(long inAcctId, Date start, Date end)
	{
		EntityManager em = (EntityManager) Component.getInstance("entityManager", true);
		return em.createQuery("select account.acctSeqid, campaign.cid, rollup.transDate" +
				" ,sum(rollup.totalCharges) as totalCharges" +
				" ,sum(rollup.totalClicks) as totalClicks" +
				" ,sum(rollup.totalEmails) as totalEmails " +
				" ,sum(rollup.totalMinutes) as totalMinutes " +
				" ,sum(rollup.totalPages) as totalPages " +
				" , sum(rollup.totalTx) as totalTx" +
				" , sum(rollup.totalUniqEmails) as totalUniqEmails" +
				" from Account account join account.campaigns campaign join campaign.areacodeRollups rollup  " +
				" where account.acctSeqid = :inId " +
				" and rollup.rollupType='d' " +
				" and rollup.transDate between :start and :end " +
				" group by account.acctSeqid, campaign.cid, rollup.transDate " +
				" order by account.acctSeqid, campaign.cid, rollup.transDate ")
		.setParameter("inId", inAcctId)
		.setParameter("start", start)
		.setParameter("end", end)
		.getResultList();
	}
		

	// given acct,date range
	// returns acct,transDate, and sum(rollup fields)
	// NOTE:  disregards cid in group instead focusing on transDate
	// USES: used by pdf generators.  this *skips* the cid summing (use getAcctCampaignSummarySet for that)
	public List getAcctSet(long inAcctId, Date start, Date end)
	{
		EntityManager em = (EntityManager) Component.getInstance("entityManager", true);
		return em.createQuery("select account.acctSeqid, rollup.transDate" +
				" ,sum(rollup.totalCharges) as totalCharges" +
				" ,sum(rollup.totalClicks) as totalClicks" +
				" ,sum(rollup.totalEmails) as totalEmails " +
				" ,sum(rollup.totalMinutes) as totalMinutes " +
				" ,sum(rollup.totalPages) as totalPages " +
				" , sum(rollup.totalTx) as totalTx" +
				" , sum(rollup.totalUniqEmails) as totalUniqEmails" +
				" from Account account join account.campaigns campaign join campaign.areacodeRollups rollup  " +
				" where account.acctSeqid = :inId " +
				" and rollup.rollupType='d' " +
				" and rollup.transDate between :start and :end " +
				" group by account.acctSeqid, rollup.transDate " +
				" order by account.acctSeqid, rollup.transDate ")
		.setParameter("inId", inAcctId)
		.setParameter("start", start)
		.setParameter("end", end)
		.getResultList();
	}
	// given Date Range
	// return rollup sum(*) fields ordered by transDate
	// NOTE: disregards acct,cid,and areacodes to focus on the date
	public List getAllPeriodSummary( Date start, Date end)
	{
		EntityManager em = (EntityManager) Component.getInstance("entityManager", true);
		return em.createQuery("select rollup.transDate" +
				" ,sum(rollup.totalCharges) as totalCharges" +
				" ,sum(rollup.totalClicks) as totalClicks" +
				" ,sum(rollup.totalEmails) as totalEmails " +
				" ,sum(rollup.totalMinutes) as totalMinutes " +
				" ,sum(rollup.totalPages) as totalPages " +
				" , sum(rollup.totalTx) as totalTx" +
				" , sum(rollup.totalUniqEmails) as totalUniqEmails" +
				" from  AreacodeRollup rollup  " +
				" where rollup.rollupType='d' " +
				" and rollup.transDate between :start and :end " +
				" group by rollup.transDate " +
				" order  rollup.transDate ")
		.setParameter("start", start)
		.setParameter("end", end)
		.getResultList();
	}

	// given Date Range
	// return rollup sum(*) fields grouped/ordered by areacode
	// NOTE: disregards acct,cid,and transDate to focus on the areacode performance for the period 
	public List getAreacodePeriodSummary( Date start, Date end)
	{
		EntityManager em = (EntityManager) Component.getInstance("entityManager", true);
		return em.createQuery("select rollup.areacode" +
				" ,sum(rollup.totalCharges) as totalCharges" +
				" ,sum(rollup.totalClicks) as totalClicks" +
				" ,sum(rollup.totalEmails) as totalEmails " +
				" ,sum(rollup.totalMinutes) as totalMinutes " +
				" ,sum(rollup.totalPages) as totalPages " +
				" , sum(rollup.totalTx) as totalTx" +
				" , sum(rollup.totalUniqEmails) as totalUniqEmails" +
				" from  AreacodeRollup rollup  " +
				" where rollup.rollupType='d' " +
				" and rollup.transDate between :start and :end " +
				" group by rollup.areacode " +
				" order  rollup.areacode ")
		.setParameter("start", start)
		.setParameter("end", end)
		.getResultList();
	}
	
	public EmailAddresses resolveEmailV13 (long id)
	{
//		EmailAddresses em; em.getFaxjobEmail()
		//em.getSmtpPieces();
		//SmtpPiece p; p.getEmailAddresses(); p.getSmtplogDatas(); p.getSmtplog();
		// Faxjob f; f.getFaxjobEmailaddresseses(); f.getFaxjobDatas(); f.getFaxjobErrorses();
		
		EntityManager em = (EntityManager) Component.getInstance("entityManager", true);
		try {
			return (EmailAddresses) em.createQuery("select  em from EmailAddresses em " +
					" join fetch em.faxjobEmail fjm " +
					" join fetch fjm.faxjob fj " +
					" join fetch fj.xids " +
//					" join fetch fj.faxjobEmailaddresseses " +
					" where em.emailSeqid = :inId")
					.setParameter("inId", id)
					.getSingleResult();
		}
		catch (javax.persistence.NoResultException e)
		{
			return null;
		}
	}
	public Smtplog resolveXidSmtp (long id)
	{
		//EmailAddresses em;
		//em.getSmtpPieces();
		//SmtpPiece p; p.getEmailAddresses(); p.getSmtplogDatas(); p.getSmtplog();
		// Faxjob f; f.getFaxjobEmailaddresseses(); f.getFaxjobDatas(); f.getFaxjobErrorses();
		//Xids x; x.getSmtplogs();
		//Smtplog s; s.getSmtpSeqid() ; s.getSmtpPieces(); 
		//SmtplogData sd;
		//sd.getClientSmtpserver();
		
		EntityManager em = (EntityManager) Component.getInstance("entityManager", true);
		try {
			return (Smtplog) em.createQuery("select  x from " +
					" Xids x join fetch x.smtplogs em " +
					" left join fetch em.smtpPieces sp " +
					" left join fetch sp.smtplogDatas ad " +
					" join fetch sp.emailAddresses " +
					" join fetch sp.clientSmtpserver" +
					" where em.smtpSeqid = :inId")
					.setParameter("inId", id)
					.getSingleResult();
		}
		catch (javax.persistence.NoResultException e)
		{
			return null;
		}
	}

	public EmailAddresses resolveEmailV14 (long id)
	{
//		EmailAddresses em; 
//		Faxjob f;
//		SmtpPiece p; p.g
//		SmtplogData d; d.g 
		
		
		EntityManager em = (EntityManager) Component.getInstance("entityManager", true);
		try {
			return (EmailAddresses) em.createQuery("select  em from EmailAddresses em " +
				" join fetch em.smtpPieces sp " +
				" join fetch sp.smtplogDatas " +
				" join fetch em.faxjobEmail fjm " +
				" join fetch fjm.faxjob fj " +
				" join fetch fj.xids " +
				" left join fetch em.reservationEmailses " +
				" left join fetch em.reservations " +
				" where  em.emailSeqid = :inId")
				.setParameter("inId", id)
				.getSingleResult();
		}
		catch (javax.persistence.NoResultException e)
		{
			return null;
		}
	}

	public Xids resolveXid (long id)
	{
		//EmailAddresses em; 
		//Xids e;
		try {
			EntityManager em = (EntityManager) Component.getInstance("entityManager", true);
			return (Xids) em.createQuery("select x from Xids x join fetch x.faxjobs " +
				" join fetch x.smtplogs " +
				" join fetch x.bounces " +
				" join fetch x.campaignUsages " +
				" where  x.xidSeqid = :inId")
			.setParameter("inId", id)
			.getSingleResult();
		}
		catch (javax.persistence.NoResultException e)
		{
			return null;
		}
	}

	

}  	
	
