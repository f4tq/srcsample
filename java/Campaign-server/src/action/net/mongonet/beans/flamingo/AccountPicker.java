package net.mongonet.beans.flamingo;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import net.mongonet.beans.ejb.*;
import org.jboss.seam.framework.EntityQuery;
import java.util.List;
import java.util.Arrays;

import javax.persistence.EntityManager;
import javax.transaction.UserTransaction;
import org.jboss.seam.annotations.security.Restrict;
import javax.ejb.Remove;

import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Out;

import org.jboss.seam.annotations.Logger;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.log.Log;
import javax.persistence.PersistenceContext;
import org.jboss.seam.annotations.datamodel.DataModel;
import org.jboss.seam.security.Identity;
import org.jboss.seam.annotations.Factory;
import static javax.ejb.TransactionAttributeType.REQUIRES_NEW;
import static org.jboss.seam.ScopeType.SESSION;
import javax.ejb.TransactionAttribute;
import javax.ejb.Stateful;
import static javax.persistence.PersistenceContextType.EXTENDED;

//@TransactionAttribute(REQUIRES_NEW)
@Scope(ScopeType.SESSION)
@Name("accountPicker")
@Restrict("#{identity.loggedIn}")
public class AccountPicker {
	private String searchString;

	protected String queryCountStr()
	{
		String baseSearch;
		if (Identity.instance().hasRole("admin"))
		{
			baseSearch = "select count(account)  from Account account where acctSeqid like #{pattern}";
		}
		else
		{
			baseSearch = "select count(account) from Users users left join " +
					"users.accountUserses bridge join bridge.account " +
					"where users.userSeqid =  #{currentUser.userSeqid}" +
					"and bridge.account.acctSeqid like #{pattern}";
		}
		return baseSearch;
	}

	protected String queryString()
	{
		String baseSearch;
		if (Identity.instance().hasRole("admin"))
		{
			baseSearch = "select account from Account account where acctSeqid like #{pattern}";
		}
		else
		{
			baseSearch = "select account from Users users left join " +
					"users.accountUserses bridge join bridge.account " +
					"where users.userSeqid =  #{currentUser.userSeqid}" +
					"and bridge.account.acctSeqid like #{pattern}";
		}
		return baseSearch;
	}
	public int  count()
	{
		EntityManager em = (EntityManager)Component.getInstance("entityManager", true);
		
		return (Integer) em.createQuery(queryCountStr()).getSingleResult();
			
	}
	public List queryAccounts(int start, int limit)
	{
		EntityManager em = (EntityManager)Component.getInstance("entityManager", true);
		
		return em.createQuery(queryString())
		.setMaxResults(limit)
		.setFirstResult( start )
		.getResultList();
	}


	@Factory(value="pattern", scope=ScopeType.EVENT)
	public String getSearchPattern()
	{
		return searchString==null ?
				"%" : '%' + searchString.toLowerCase().replace('*', '%') + '%';
	}
	public String getSearchString()
	{
		return searchString;
	}
	public void setSearchString(String searchString)
	{
		this.searchString = searchString;
	}
	
	@Remove
	public void destroy() {}
	
}

		
/*
 * 	
		UserTransaction userTx = null;
		boolean startedTx = false;
		userTx = (UserTransaction) Component.getInstance("org.jboss.seam.transaction.transaction");

		try {
			int status = userTx.getStatus();
			try {
				if ( status != javax.transaction.Status.STATUS_ACTIVE) {

					startedTx = true;
					userTx.begin();
				}
				EntityManager em = (EntityManager) Component.getInstance("entityManager");
			}
			catch (Exception ex) {
				try {
					if (startedTx) 
						userTx.rollback();

				} catch (Exception rbEx) {
					rbEx.printStackTrace();
				}
				catch (javax.transaction.SystemException e)
				{

				}
			}
			*/

	
