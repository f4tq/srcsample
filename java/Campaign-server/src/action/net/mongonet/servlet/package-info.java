/**
 * Seam components for use in a servlet environment.
 */
@Namespace(value="http://mongonet.net/products/campaign/servlet", prefix="net.mongonet.servlet")
@AutoCreate
package net.mongonet.servlet;

import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Namespace;
