package net.mongonet.servlet;

import org.jboss.seam.web.AbstractFilter;
import javax.servlet.http.HttpServletResponseWrapper;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.List;
import java.util.ArrayList;
import java.util.regex.Pattern;
import java.util.regex.Matcher;


import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Install;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.intercept.BypassInterceptors;
import org.jboss.seam.annotations.web.Filter;
import org.jboss.seam.security.Identity;
import org.jboss.seam.security.Credentials;

import org.jboss.seam.log.Logging;
import org.jboss.seam.log.LogProvider;

/**
 * The Class CacheHeaderFilter.
 *
 * @author chris fortescue
 */

import java.util.HashMap;
import java.util.Map;

@Scope(ScopeType.APPLICATION)
@Name("net.mongonet.seam.CacheHeader")
@BypassInterceptors
@Filter(around="org.jboss.seam.web.ajax4jsfFilter")
@Install(classDependencies="org.apache.log4j.Logger", 
         dependencies="org.jboss.seam.security.identity",
         precedence=Install.BUILT_IN)

public class CacheHeaderFilter extends AbstractFilter {

    /** The Constant EXPIRES_HEADER. */
    private static final String EXPIRES_HEADER = "Expires";

    /** The Constant LAST_MODIFIED_HEADER. */
    private static final String LAST_MODIFIED_HEADER = "Last-Modified";
    /**
     * The Constant MILLISECONDS_IN_SECOND.
     */
    private static final int MILLISECONDS_IN_SECOND = 1000;


    private static LogProvider log = Logging.getLogProvider(CacheHeaderFilter.class);
    
	java.util.regex.Pattern regexp;

    List<Pattern> uriPatterns = null;
    HashMap<Pattern,String> patternToCache = new HashMap();
    HashMap<Pattern,String> patternToExpires = new HashMap();
    
    Long maxAge=new Long(3600);
    String cacheScope = "public";
    
    public List<String> getUriPatterns()
    {
    	List<String> tmp = new ArrayList<String>();
    	for (Pattern ii : uriPatterns)
    	{
    		tmp.add(ii.pattern());
    	}
    	return tmp;
    }
    
    public void setUriPatterns(List<String> patterns)
    {
    	uriPatterns = new ArrayList<Pattern>();
    	for (String i : patterns)
    	{
    		Pattern pattern = java.util.regex.Pattern.compile(i,
    				java.util.regex.Pattern.CASE_INSENSITIVE
    		);
    		uriPatterns.add(pattern);
    	}
    }


    private String strip(String fullPath, String contextPath) {
        if (fullPath.startsWith(contextPath)) {
            return fullPath.substring(contextPath.length());
        } else {
            return fullPath;
        }
    }
    public void setPatternToCache(Map<String, String> mapping)
    {
    	this.patternToCache = new HashMap<Pattern,String>();
    	
    	for (String key : mapping.keySet())
    	{
    		Pattern pattern = java.util.regex.Pattern.compile( key ,
    				java.util.regex.Pattern.CASE_INSENSITIVE
    		);
    		this.patternToCache.put(pattern, mapping.get(key));
    	}
    }
    public Map<String,String> getPatternToCache()
    {
    	Map<String,String> retval = new HashMap<String,String>();
    	
    	for (Pattern key : this.patternToCache.keySet())
    	{
    		retval.put(key.pattern(), this.patternToCache.get(key));
    	}
    	return retval;
    }

    
    public void setPatternToExpires(Map<String, String> mapping)
    {
    	this.patternToExpires = new HashMap<Pattern,String>();
    	
    	for (String key : mapping.keySet())
    	{
    		Pattern pattern = java.util.regex.Pattern.compile( key ,
    				java.util.regex.Pattern.CASE_INSENSITIVE
    		);
    		this.patternToExpires.put(pattern, mapping.get(key));
    	}
    }
    public Map<String,String> getPatternToExpires()
    {
    	Map<String,String> retval = new HashMap<String,String>();
    	
    	for (Pattern key : this.patternToExpires.keySet())
    	{
    		retval.put(key.pattern(), this.patternToExpires.get(key));
    	}
    	return retval;
    }

    
    Long getMaxAge()
    {
    	return this.maxAge;
    }
    void setMaxAge( Long newAge)
    {
    	this.maxAge = newAge;
    }
    String getCacheScope()
    {
    	return this.cacheScope;
    }
    void setCacheScope(String scope)
    {
//    	String pieces = scope.split(arg0)
    	this.cacheScope = (scope == null  ? "public" : scope);    
	}

    /**
     * Do filter.
     *
     * @param pRequest the request
     * @param pResponse the response
     * @param pChain the chain
     *
     * @throws IOException Signals that an I/O exception has occurred.
     * @throws ServletException the servlet exception
     *
     * @see javax.servlet.Filter#doFilter(javax.servlet.ServletRequest, javax.servlet.ServletResponse,
     *      javax.servlet.FilterChain)
     */
    public void doFilter(final ServletRequest pRequest, final ServletResponse pResponse, final FilterChain pChain)
            throws IOException, ServletException {
        // Apply the headers
    	if (uriPatterns == null || uriPatterns.size() == 0)
    	{
    		pChain.doFilter(pRequest, pResponse);
    		return;
    	}
    	
    	HttpServletRequest request = (HttpServletRequest) pRequest;
        String fullPath = request.getRequestURI();

        String localPath = strip(fullPath, request.getContextPath());
       

    	boolean doCache = false;
    	boolean doExpires = false;

    	String cacheControlVal=null;
    	long expiresVal=-1;
    	
    	for (Pattern p:  this.patternToCache.keySet())
    	{
    		Matcher matcher = p.matcher(localPath);
    		if (matcher.matches()){
    			doCache = true;
    			cacheControlVal = this.patternToCache.get(p);
    			break;
    		}
    	}
    	for (Pattern p:  this.patternToExpires.keySet())
    	{
    		Matcher matcher = p.matcher(localPath);
    		if (matcher.matches()){
    			doExpires = true;
    			expiresVal = Long.valueOf(this.patternToExpires.get(p));
    			break;
    		}
    	}
    	
    	if (doCache || doExpires)
    	{
    		if (pResponse instanceof HttpServletResponse)
    		{
    			HttpServletResponse response = (HttpServletResponse)pResponse;
    			if (doCache)
    				response.setHeader("Cache-Control", cacheControlVal);
    			if (doExpires)
    			{
    				
    	            final long now = System.currentTimeMillis();
    	            final DateFormat httpDateFormat = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss z", Locale.US);
    	            httpDateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
    	            response.addHeader(LAST_MODIFIED_HEADER, httpDateFormat.format(new Date(now)));
    	            response.addHeader(EXPIRES_HEADER, httpDateFormat.format(new Date(now
    	                    + (expiresVal * MILLISECONDS_IN_SECOND))));
    			}

    		}
    	}
   		pChain.doFilter(pRequest, pResponse);
    }

    /**
     * Destroy all humans!
     *
     * @see javax.servlet.Filter#destroy()
     */
    public void destroy() {
    }

}
