package net.mongonet.servlet; 



import javax.servlet.http.HttpServlet;

import javax.servlet.ServletException;
import javax.servlet.ServletConfig;
import java.io.IOException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;
import javax.crypto.*;

import javax.servlet.ServletException;
import javax.servlet.ServletContext;
import javax.servlet.ServletConfig;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jboss.seam.servlet.ContextualHttpServletRequest;
import org.jboss.seam.util.Resources;
import org.jboss.seam.core.Manager;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Image;
import com.lowagie.text.Font;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.pdf.*;
import com.lowagie.text.pdf.codec.TiffImage;
import com.lowagie.text.Image;

import javax.servlet.ServletOutputStream;
import javax.sql.*;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.*;

import net.mongonet.V14.beans.session.ECPBean;

import org.jboss.seam.log.LogProvider;
import org.jboss.seam.log.Logging;
import org.jboss.seam.Seam;
import org.jboss.seam.Component;
import org.jboss.seam.contexts.ServerConversationContext;

import org.jboss.seam.ScopeType;
import net.mongonet.V14.beans.session.ECP;
import net.mongonet.beans.ejb.*;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.transaction.UserTransaction;
import net.mongonet.V14.beans.session.CookieIfc;
import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.faces.FacesMessages;

import java.net.URLEncoder;
import java.net.URLDecoder;

import java.net.URL;
import java.net.URLConnection;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import org.jboss.seam.security.Identity;


import java.util.List;
import java.util.Properties;

import java.util.ArrayList;
import org.jboss.seam.core.Events;
import org.jboss.seam.web.Parameters;
import org.jboss.seam.faces.Renderer;
import java.net.URLEncoder;
import org.jboss.seam.servlet.ContextualHttpServletRequest;
import org.jboss.seam.core.Conversation;
import org.jboss.seam.core.ConversationPropagation;

/**
 * Servlet Class
 *
 * @web.servlet              name="Redirector"
 *                           display-name="Name for Redirector"
 *                           description="Description for Redirector"
 * @web.servlet-mapping      url-pattern="/Redirector"
 * @web.servlet-init-param   name="A parameter"
 *                           value="A value"
 */
public class GeoHelper extends HttpServlet {
    private static final LogProvider log = Logging.getLogProvider(GeoHelper.class);

	private ServletContext context;

	protected synchronized Properties getProperties(String rezPath) throws ServletException
	{
		Properties prop=null;;
		if (prop==null)
		{
			InputStream jk = context.getResourceAsStream(rezPath);
			prop = new java.util.Properties();
			try {
				prop.load(jk);
			}
			catch(Exception e ) 
			{ 
				prop = null;
				throw new ServletException("Couldn't load properties: "+rezPath,e);
			}
		}
		return prop;
	}
	
	@Override
	public void init(ServletConfig config) throws ServletException
	{
		super.init(config);
		context = config.getServletContext();

	}

	public GeoHelper() {
		// TODO Auto-generated constructor stub
	}


	protected void service(HttpServletRequest req, HttpServletResponse resp)
		throws ServletException,
		IOException {
		// TODO Auto-generated method stub
		super.service(req, resp);
	}

	public void destroy() {
		// TODO Auto-generated method stub
		super.destroy();
	}

	public String getServletInfo() {
		// TODO Auto-generated method stub
		return super.getServletInfo();
	}
	public void currentAccountCQL(final HttpServletRequest request, final HttpServletResponse response)
	throws ServletException,
	IOException 
	{
		request.setAttribute("action", "currentAccount");
		log.info("Request: " + request.getRequestURI() + "?" + request.getQueryString()  + " action=currentAccount" );

		doGet(request,response);
	}
	public void currentCampaignCQL(final HttpServletRequest request, final HttpServletResponse response)
	throws ServletException,
	IOException 
	{
		request.setAttribute("action", "currentCampaign");
		log.info("Request: " + request.getRequestURI() + "?" + request.getQueryString()  + " action=currentCampaign" );

		doGet(request,response);
	}
	public void cql(final HttpServletRequest request, final HttpServletResponse response)
	throws ServletException,
	IOException 
	{
		//		request.setAttribute("action", "cql");
		String action = (String) request.getAttribute("action");
		log.info("Request: " + request.getRequestURI() + "?" + request.getQueryString()  + " action=" + action);
// + " conversation=" + Conversation.instance().getId()
		if (action.indexOf(",") != -1)
		{
			String raw="";
			
			String actions[] = action.split("[,]");
			for (String a: actions)
			{
				request.setAttribute("action", a);
				
				doGet(request,response);
				String rez = (String) request.getAttribute(a + ".response");
				if (raw.length()==0)
				{
					raw += rez;
				}
				else {
					raw += " and " + rez;
				}
			}
			request.setAttribute(action + ".result", URLEncoder.encode(raw,"UTF-8"));
		}
		else
			doGet(request,response);
		try {
			log.info("Request: " + request.getRequestURI() + "?" + request.getQueryString() 
//					+ " conversation=" + Conversation.instance().getId() 
					+ " result=" + (String) request.getAttribute(action + ".result"));
			
		}
		catch (Exception e )
		{
			log.info("No result for :" + "Request: " + request.getRequestURI() + "?" + request.getQueryString() 
						+ " action=" + action);
			// + " conversation=" + Conversation.instance().getId() 
		}
	}
	/*
	 * Create accout branding.
	 *  User specifies url // campaign comes later
	 *  //php form target call
	 *    form target php call java
	 *    via remote soap call with account seqid and optional campaign name
	 *    
	 *    
	 * (non-Javadoc)
	 * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	@Override
	protected void doGet(final HttpServletRequest request, final HttpServletResponse response)
	throws ServletException,
	IOException 
	{
		new  ContextualHttpServletRequest(request)
		{
	        @Override
	        protected void restoreConversationId() {
	        	final String conversationName="cid";
	        	Map parameters = request.getParameterMap();


	        	Object object = parameters.get(conversationName);
	            String id;
	            if (object==null)
	            {
	               return ;
	            }
	            else
	            {
	               if ( object instanceof String )
	               {
	                  //when it comes from JSF it is (usually?) a plain string
	                  id =  (String) object;
	               }
	               else
	               {
	                  //in a servlet it is a string array
	                  String[] values = (String[]) object;
	                  if (values.length!=1)
	                  {
	                     throw new IllegalArgumentException("expected exactly one value for " + conversationName + " request parameter");
	                  }
	                  id =  values[0];
	               }
	            }

	        	
	            ConversationPropagation.instance().setConversationId(id);


	        }

	        @Override
	        protected void handleConversationPropagation() {
	        }

			@Override
			public void process() throws ServletException, IOException
			{
				if (!Identity.instance().isLoggedIn())
				{
					log.warn("Not Logged In Request Rejected: " + request.getRequestURI() + "?" + request.getQueryString()  + " action=currentCampaign" );

					throw new org.jboss.seam.security.NotLoggedInException();
				}
				// restore conversations
	        	Map parameters = request.getParameterMap();

	            Object acctId1=parameters.get("accountAcctSeqid");
	        	if (acctId1 != null )
	        	{
	        		String acctIdO = (acctId1 instanceof String ? (String) acctId1 : ((String[]) acctId1)[0]);	        		
	        		try {
	        			AccountHome accountHome = (AccountHome) Component.getInstance("accountHome", ScopeType.CONVERSATION,true);
	        			
	        			accountHome.setId( Long.valueOf(acctIdO));
	        		}
	        		catch(Exception e)
	        		{
	        			log.error("Couldn't set acctId: " + ((String) acctIdO) );
	        		}
	        	}
	        	Object campId1=parameters.get("campaignCid");
	        	if (campId1 != null )
	        	{
	        		String campIdO = (campId1 instanceof String ? (String) campId1 : ((String[]) campId1)[0]);	        		

	        		try {
	        			CampaignHome campaignHome = (CampaignHome) Component.getInstance("campaignHome", ScopeType.CONVERSATION,true);
	        			campaignHome.setId( Long.valueOf( campIdO));
	        		}
	        		catch(Exception e)
	        		{
	        			log.error("Couldn't set campaigId: " + ((String) campIdO) );
	        		}
	        	}

				
				String action = (String) request.getAttribute("action");
				if (action.equalsIgnoreCase("checkLogin"))
				{
					return ;
				}
				else if (action.equalsIgnoreCase("hasRole"))
				{
					String roleStr = (String) request.getAttribute("roles");

					String roles[] = roleStr.split("[,]");
					boolean ok=false;
					for (String r: roles)
					{
						if (Identity.instance().hasRole(r)){
							ok = true;
							break;
						}
					}
					if (!ok)
					{
						log.warn("Not Authorized Request Rejected: " + request.getRequestURI() + "?" + request.getQueryString()  );

						throw new org.jboss.seam.security.AuthorizationException("You don't have permission to do this");

					}
					return;
				}
				else if (action.equalsIgnoreCase("currentUser"))
				{
					Users user = (Users) Component.getInstance("currentUser", false);
					String raw="user_seqid=" + user.getUserSeqid();
					request.setAttribute(action + ".result", URLEncoder.encode(raw,"UTF-8"));
					return;
				}
				else if (action.equalsIgnoreCase("accountList"))
				{
					// hibernate filter applies
					try {
//						AccountList accountList = (AccountList) Component.getInstance("accountListShortLife", ScopeType.EVENT,true);

						org.jboss.seam.framework.EntityQuery accountList = (org.jboss.seam.framework.EntityQuery) Component.getInstance("accountListShortLife", ScopeType.EVENT,true);
						String raw="";
						accountList.setFirstResult(0);
						do 
						{
							List list = accountList.getResultList();
							ListIterator iter = list.listIterator();
							while (iter.hasNext())
							{
								Account c = (Account) iter.next();
								if (raw.length() > 0)
									raw += " or ";
								else
									raw += "(";
								raw +="acct_id=" + c.getAcctSeqid();
							}
							if (accountList.isNextExists())
								accountList.setFirstResult(accountList.getNextFirstResult());
						}
						while (accountList.isNextExists());
						if (raw.length() > 0)
							raw += ")";
						
						log.info("cql = " + raw);
						
						request.setAttribute(action + ".result", URLEncoder.encode(raw,"UTF-8"));

					}		
					catch (Exception e)
					{
						request.setAttribute(action + ".result", "");
						return;
					}
				}	
				else if (action.equalsIgnoreCase("campaignList"))
				{
					// hibernate filter applies
					try {
						CampaignList campaignList = (CampaignList) Component.getInstance("campaignListShortLife", ScopeType.EVENT,true);
						String raw="";
						campaignList.setFirstResult(0);
						do 
						{
							List list = campaignList.getResultList();
							ListIterator iter = list.listIterator();
							while (iter.hasNext())
							{
								Campaign c = (Campaign) iter.next();
								if (raw.length() > 0)
									raw += " or ";
								else
									raw += "(";

								raw +="cid=" + c.getCid();
							}
							campaignList.setFirstResult(campaignList.getNextFirstResult());
						}
						while (campaignList.isNextExists());

						if (raw.length() > 0)
							raw += ")";

						log.info("cql = " + raw);
						
						request.setAttribute(action + ".result", URLEncoder.encode(raw,"UTF-8"));

					}		
					catch (Exception e)
					{
						request.setAttribute(action + ".result", "");
						return;
					}
				}	
				else if (action.equalsIgnoreCase("currentCampaign"))
				{
					// hibernate filter applies
					try {
						CampaignHome campaignHome = (CampaignHome) Component.getInstance("campaignHome", ScopeType.CONVERSATION,false);
						log.info("currentCampaign=" + campaignHome.getId());
						request.setAttribute(action + ".result", URLEncoder.encode("cid=" + campaignHome.getId(),"UTF-8"));
					}
					catch (Exception e)
					{
						request.setAttribute(action + ".result", "cid=-1");
						return;
					}
					
				}
				else if (action.equalsIgnoreCase("currentAccount"))
				{
					// hibernate filter applies
					try {
						AccountHome accountHome = (AccountHome) Component.getInstance("accountHome", ScopeType.CONVERSATION,false);
						log.info("accountHome=" + accountHome.getId());
						request.setAttribute(action + ".result", URLEncoder.encode("acct_id=" + accountHome.getId(),"UTF-8"));
					}
					catch (Exception e)
					{
						request.setAttribute(action + ".result", "acct_id=-1");
						return;
					}
					
				}
				else if (action.equalsIgnoreCase("prefDates"))
				{
					// hibernate filter applies
					try {
						CampaignReportPrefs prefs = (CampaignReportPrefs) Component.getInstance("campaignPreferences",false);
						log.info("campaignPrefs [" +  prefs.getStart() + " - " + prefs.getStop() + "]");
					
						request.setAttribute(action + ".result", URLEncoder.encode("(trans_date>= timestamp '" + java.text.DateFormat.getDateInstance().format(prefs.getStart()) 
								+ "' and trans_date <= timestamp '" +  java.text.DateFormat.getDateInstance().format(prefs.getStop()) + "')","UTF-8"));
					}
					catch (Exception e)
					{
						request.setAttribute(action + ".result", "acct_id=-1");
						return;
					}
				}
				else{
					request.setAttribute(action + ".result", "");
				}
			}
		}.run();
	}
}
