
package net.mongonet.servlet; 



import javax.servlet.http.HttpServlet;

import javax.servlet.ServletException;
import javax.servlet.ServletConfig;
import java.io.IOException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.bouncycastle.util.encoders.Hex;


import javax.servlet.ServletContext;
import org.jboss.seam.servlet.ContextualHttpServletRequest;
import java.io.InputStream;
import java.util.*;

import org.jboss.seam.log.LogProvider;
import org.jboss.seam.log.Logging;
import org.jboss.seam.Component;
import net.mongonet.beans.ejb.*;

import javax.persistence.EntityManager;
import javax.transaction.UserTransaction;

import java.util.Properties;

/**
 * Servlet Class
 *
 * @web.servlet              name="Redirector"
 *                           display-name="Name for Redirector"
 *                           description="Description for Redirector"
 * @web.servlet-mapping      url-pattern="/Redirector"
 * @web.servlet-init-param   name="A parameter"
 *                           value="A value"
 */
public class Redirector extends HttpServlet {
    /**
	 * 
	 */
	private static final long serialVersionUID = 3992470631018303975L;

	private static final LogProvider log = Logging.getLogProvider(Redirector.class);

	private ServletContext context;

	protected synchronized Properties getProperties(String rezPath) throws ServletException
	{
		Properties prop=null;;
		if (prop==null)
		{
			InputStream jk = context.getResourceAsStream(rezPath);
			prop = new java.util.Properties();
			try {
				prop.load(jk);
			}
			catch(Exception e ) 
			{ 
				prop = null;
				throw new ServletException("Couldn't load properties: "+rezPath,e);
			}
		}
		return prop;
	}
	
	@Override
	public void init(ServletConfig config) throws ServletException
	{
		super.init(config);
		context = config.getServletContext();

	}

	public Redirector() {
		// TODO Auto-generated constructor stub
	}


	protected void service(HttpServletRequest req, HttpServletResponse resp)
		throws ServletException,
		IOException {
		// TODO Auto-generated method stub
		super.service(req, resp);
	}

	public void destroy() {
		// TODO Auto-generated method stub
		super.destroy();
	}

	public String getServletInfo() {
		// TODO Auto-generated method stub
		return super.getServletInfo();
	}
	public void stage1(final HttpServletRequest request, final HttpServletResponse response)
	throws ServletException,
	IOException 
	{
		doGet(request,response);
	}
	public void stage2(final HttpServletRequest request, final HttpServletResponse response)
	throws ServletException,
	IOException 
	{
		doGet(request,response);
		
	}

	protected void version1(final HttpServletRequest request, final HttpServletResponse response)
	throws ServletException,
	IOException 
	{
		String ref = "/ref/";
		if (request.getRequestURI().indexOf(ref)>=0 )
		{
			if (request.getRequestURI().endsWith(ref))
				response.sendRedirect("/error.mongo");
			else 
			{
				int pos = request.getRequestURI().indexOf(ref);
				String hexargs = request.getRequestURI().substring(pos + ref.length());

				if (hexargs.endsWith("/"))
				{
					hexargs = hexargs.substring(0,hexargs.length()-1);
				}

				try {
					String sp[] = hexargs.split("[;]");

					if (sp.length != 2)
					{
						log.error("Incorrect redirect form for  " + request.getRequestURI() + " . crazy ref " + hexargs);
						response.setStatus(404);
						return;
					}
					save(request,response, sp[0],sp[1]);
					

				}
				catch (Exception e)
				{
					log.error("Redirect failed for   " + request.getRequestURI() + " exception:"  + e.getMessage()  );
					response.setStatus(404);
					return;
				}
			}
		}
	}

	protected void version2(final java.util.HashMap<String, String> map, final HttpServletRequest request, final HttpServletResponse response)
	throws ServletException,
	IOException 
	{
		String theXid = map.get("x");
		String theCampaign = map.get("c");
		String theEmail = map.get("e");
		String etype = map.get("t");
		
		save(request,response,theCampaign,theXid,new String(Hex.decode(theEmail)), etype.charAt(0));
		
	}

	protected void save(final HttpServletRequest request, final HttpServletResponse response, String campaignKey, String xid)
	{
		save(request,response,campaignKey,xid,null,null);
		
	}
	
	protected void save(final HttpServletRequest request, final HttpServletResponse response, String campaignKey, String theXid, String email, Character emailType)
	{
		UserTransaction userTx = null;
		boolean startedTx = false;
		try {
			userTx = (UserTransaction)org.jboss.seam.Component.getInstance("org.jboss.seam.transaction.transaction");

			if (userTx.getStatus() != javax.transaction.Status.STATUS_ACTIVE) {
				startedTx = true;
				userTx.begin();
			}

			EntityManager em = (EntityManager) Component.getInstance("entityManager");
			Campaign c;
			Xids x;
			EmailAddresses e=null;
			email = email.toUpperCase();
			
			try {
				c = (Campaign) em.createQuery(
				"select campaign from Campaign campaign where publicId=:ca")
				.setParameter("ca", campaignKey)
				.getSingleResult();
			}
			catch(javax.persistence.NoResultException ex2)
			{
				log.error("No such campaign " + campaignKey);
				response.setStatus(404);
				return;
			}							
			try {
				x = (Xids) em.createQuery("select xids from Xids xids where xid=:ca")
				.setParameter("ca", theXid)
				.getSingleResult();
			}	
			catch(javax.persistence.NoResultException ex3)
			{
				x = new Xids();
				x.setXid(theXid);
				em.persist(x);
			}
			

			if (email != null)
			{
				try {
					e = (EmailAddresses) em.createQuery("select ema from EmailAddresses ema where emailAddress=:ca")
					.setParameter("ca", email)
					.getSingleResult();
				}	
				catch(javax.persistence.NoResultException ex3)
				{
					e = new EmailAddresses();
					e.setEmailAddress(email);
					em.persist(e);
				}	
			}

			
			CampaignUsage usage = new CampaignUsage(0,x,c,Calendar.getInstance().getTime(),	request.getRemoteAddr(),0);
			usage.setEmailAddresses(e);
			usage.setEmailType(emailType);
			
			em.persist(usage);
			userTx.commit();
			log.info("Redirected  " + request.getRequestURI() + " to " + c.getLink()  );
			request.setAttribute("F", c.getLink());
			//								response.sendRedirect(c.getLink());
			return;
		} 
		catch (Exception ex) {
			try {
				log.info("redirector failed because: " + ex.getMessage());
				if (startedTx) userTx.rollback();
			} 
			catch (Exception rbEx) {
				log.error("/EnterpriseRef redirect transaction failed for  " + rbEx.getMessage());
			}
			response.setStatus(404);
			return;
		}

	}
	/*
	 * Create accout branding.
	 *  User specifies url // campaign comes later
	 *  //php form target call
	 *    form target php call java
	 *    via remote soap call with account seqid and optional campaign name
	 *    
	 *    
	 * (non-Javadoc)
	 * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	@Override
	protected void doGet(final HttpServletRequest request, final HttpServletResponse response)
	throws ServletException,
	IOException 
	{
		new  ContextualHttpServletRequest(request)
		{
			@Override
			public void process() throws ServletException, IOException
			{
				log.info("Request: " + request.getRequestURI());
				String ref = "/ref/";
				if (request.getRequestURI().indexOf(ref)>=0 )
				{
					if (request.getRequestURI().endsWith(ref))
						response.sendRedirect("/error.mongo");
					else 
					{
						int pos = request.getRequestURI().indexOf(ref);
						String hexargs = request.getRequestURI().substring(pos + ref.length());
						String data[] = hexargs.split(new String("/"));
						if (data.length > 1)
						{
							java.util.HashMap<String, String> dat = new java.util.HashMap<String, String>();
							dat.put("c", data[0]);
							
							for ( int ii=1; ii < data.length;ii++)
							{
								String nvraw[] = data[ii].split(new String("="));
								if (nvraw.length != 2)
								{
									response.setStatus(404);
								}
								dat.put(nvraw[0], nvraw[1]);
							}
							if (dat.containsKey("v"))
							{
								String version = dat.get("v");
								if (version.equalsIgnoreCase("2"))
									version2(dat,request,response);
								else
								{
									log.error("Unknown key passed to redirector.  no version" + hexargs);
									response.setStatus(404);
									return;
								}
							}
							else
							{
								log.error("Unknown key passed to redirector");
								response.setStatus(404);
							}
						}
						else
						{
							version1(request,response);
							return;
						}
							
					}
				}
				else{
					response.setStatus(404);
					log.error("Redirected  " + request.getRequestURI() + " don't understand"   );
					return;
				}
			}
		}.run();

	}


}
