
<?php

class Login {
  function Login($a ,$b) {
    $this->arg0 = $a;
    $this->arg1 = $b;
  }
  public $arg0; //name
  public $arg1; // password
};
class Hit {
  function Hit($a ,$b) {
    $this->arg0 = $a;
    $this->arg1 = $b;
  }
  public $arg0; // publicId
  public $arg1; // Xid
};

class NewCampaign {
  function NewCampaign($a ,$b, $c) {
    $this->arg0 = $a;
    $this->arg1 = $b;
    $this->arg2 = $c;
  }

  
  public $arg0; // acctId
  public $arg1; // companyName
  public $arg2; // URL
  function getAcctId(){ return $this->arg0;}
};

class Impression {
  function Impression($a ,$b, $c,$d,$e,$f) {
    $this->arg0 = $a;
    $this->arg1 = $b;
    $this->arg2 = $c;
    $this->arg3 = $d;
    $this->arg4 = $e;
    $this->arg5 = $f;
  }
  public $arg0; // acctId
  public $arg1; // Xid
  public $arg2; // email
  public $arg3; // emailType
  public $arg4; // phoneNumber
  public $arg5; // keywords

};



class Account {
  function Account($a ) {
    $this->arg0 = $a;
  }
  public $arg0; // acct_seqid
  
};

$wsdlURL="http://localhost:8080/campaign-campaign/CampaignMgmt?wsdl";

$acctId = 17595;
$companyName = "Chris Fortescue LTD";
$url = "http://www.fortescue.net";
$xid = "2008.07.31.19.08.52-3232238251-335";
$email = "CHRIS@MONGONET.NET";
$phone = "14159715237";
$emailType = 'f';
$keywords="real estate disclosure";

$campaign = new NewCampaign($acctId, $companyName, $url);

try {
  //  $client = new SoapClient ("http://localhost:8080/campaign-campaign/CampaignMgmt?wsdl", array('classmap' => array('login' => 'Login','recordHit' =>'Hit', 'getPublicId' =>'Account','saveCampaign' => 'NewCampaign')));
  $client = new SoapClient ($wsdlURL, array('classmap' => array('login' => 'Login','recordHit' =>'Hit', 'getPublicId' =>'Account','saveCampaign' => 'NewCampaign','acquireImpression' => 'Impression')));
}
catch (Exception $e) {
    echo "Client setup failed.<br />";
    echo $e -> getMessage ();
}

try {

  $rez  = $client -> login (new Login("engineering@mongonet.net","foobar"));
  $v  = $rez->return;
  if ($v)
    echo "Valid Credentials! $v\n";
  else 
    echo "InValid Credentials! $v\n";
}

catch (Exception $e) {
    echo "Error!<br />\n";
    echo $e -> getMessage ();
}

try {
	$rez = $client-> saveCampaign($campaign);
	print_r($rez);
  	$pubKey = $rez->return;
    echo "saveCampaign's getPublicId succeeded! $pubKey\n";
}
catch (Exception $e) {
    echo "Error! saveCampaign failed!\n <br/>";
    echo $e -> getMessage ();
}

// make an impression
try {
    $impress = new Impression($campaign->getAcctId(),$xid,$email,$emailType,$phone,$keywords);

  $rez = $client->acquireImpression($impress);
	print_r($rez);
  	$pubKey = $rez->return;
    echo "acquireImpression's getPublicId succeeded! $pubKey\n";
}
catch (Exception $e) {
    echo "Error! acquireImpresssion failed!\n <br/>";
    echo $e -> getMessage ();
}


try {

  $rez = $client -> getPublicId (new Account($campaign->getAcctId()));
  print_r($rez);
  $pubKey = $rez->return;
    echo "getPublicId for " . $campaign->getAcctId() . " succeeded! $pubKey\n";
}
catch (Exception $e) {
    echo "Error! getPublicId failed!\n <br/>";
    echo $e -> getMessage ();
}

try {

    $rez = $client -> recordHit ( new Hit( $pubKey, '2008.02.28.15.25.14-3232235798-000') );
    $url = $rez->return;
    print "recordHit succeeded : redirecting to $url\n";
}
catch (Exception $e) {
    echo "Error! recordHit failed\n<br />";
    echo $e -> getMessage ();
}

?>