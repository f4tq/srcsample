drop table areacode_rollup;
drop table domain_rollup;
drop table campaign_usage;
drop table campaign;

create table campaign
(
	cid          serial8  NOT NULL 
	, acct_id int8 not null
	, public_id varchar(255) 
	, name         varchar(255) NOT NULL default ''  -- campaign name
	, description  text        -- campaign description
	, start_time   timestamp(6) with time zone default current_timestamp
	, link text not null
	, presentation text
	, primary key(cid)
	, constraint fk_campaign_01 foreign key(acct_id) references account(acct_seqid)
);

create index ix_campaign_01 on campaign(start_time);
create index ix_campaign_02 on campaign(acct_id);
create index ix_campaign_03 on campaign(public_id);

grant select,update,insert,update on campaign to pgpdb;
grant select,update,insert,update on campaign to mongoscan;

grant select, update on campaign_cid_seq to pgpdb;
grant select, update on campaign_cid_seq to mongoscan;

create table campaign_usage
(
 	cuid serial8 not null

	, cid_ref int8 NOT NULL default '0'    -- Campaign group for this click
	, clicked timestamp(6) with time zone default current_timestamp
	, ip varchar(128) NOT NULL default '' -- IP Address
	, uid int8 NOT NULL default '0'       --  User ID, if logged in
	, xid_ref int8 not null default '0'
	, primary key(cuid)
	, constraint fk_campaign_usage_02  foreign key(xid_ref) references xids(xid_seqid)
	, constraint fk_campaign_usage_03  foreign key(cid_ref) references campaign(cid)
);


create index ix_campaign_usage_02 on campaign_usage(cid_ref);
create index ix_campaign_usage_03 on campaign_usage(xid_ref);
create index ix_campaign_usage_04 on campaign_usage(clicked);
create index ix_campaign_usage_05 on campaign_usage(uid);

grant select,update,insert,update on campaign_usage to pgpdb;
grant select,update,insert,update on campaign_usage to mongoscan;

grant select,update on campaign_usage_cuid_seq to pgpdb;
grant select,update on campaign_usage_cuid_seq to mongoscan;

create table domain_rollup
(
	domain_rollup_seqid serial8
	, cid_ref int8 not null
	, rollup_type int8 not null
	, start_period timestamp(6) with time zone not null
	, end_period timestamp(6) with time zone not null
	, domain_name text not null

	, total_clicks int8 not null default 0
	, total_tx int8 not null default 0
	, total_emails int8 not null default 0
	, total_uniq_emails int8 not null default 0
	, primary key(domain_rollup_seqid)
	, constraint  fk_domain_rollup_01 foreign key(cid_ref) references campaign(cid)
);

create index ix_domain_rollup_01 on domain_rollup(cid_ref);

grant select,update,insert,delete on domain_rollup to pgpdb;
grant select,update,insert,delete on domain_rollup to mongoscan;

grant select,update on domain_rollup_domain_rollup_seqid_seq to pgpdb;
grant select,update on domain_rollup_domain_rollup_seqid_seq to mongoscan;

create table areacode_rollup
(
	areacode_rollup_id serial8 not null
	, cid_ref int8 not null
	, rollup_type int8 not null
	, start_period timestamp(6) with time zone not null
	, end_period timestamp(6) with time zone not null
	, areacode varchar(3) not null

	, total_clicks int8 not null default 0
	, total_tx int8 not null default 0
	, total_emails int8 not null default 0
	, total_uniq_emails int8 not null default 0
	, primary key(areacode_rollup_id)
	, constraint  fk_areacode_rollup_01 foreign key(cid_ref) references campaign(cid)
);
create index ix_areacode_rollup_01 on areacode_rollup(cid_ref);
create index ix_areacode_rollup_02 on areacode_rollup(start_period);

grant select,update,insert,delete on areacode_rollup to pgpdb;
grant select,update,insert,delete on areacode_rollup to mongoscan;

grant select,update on areacode_rollup_areacode_rollup_id_seq to pgpdb;
grant select,update on areacode_rollup_areacode_rollup_id_seq to mongoscan;

create or replace function generate_campaign_id() returns trigger as
$$
declare
	rid text;

begin
	if (TG_OP = 'INSERT') then
		rid := encode(digest(convert_data(new.acct_id || new.cid || new.start_time ),'sha1'),'hex');
		new.public_id := rid;
		return  new;

	elsif (TG_OP = 'UPDATE') then
		
		return new;
	end if;
end;
$$
LANGUAGE 'plpgsql';

create trigger tr_reservation_01 before insert or update on  campaign
	for each row execute procedure public.generate_campaign_id();
