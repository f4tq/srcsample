class ReservationForm
	constructor: (@formId) ->
		#alert "found form id=" + @formId 
		current_name = ''
		$('input[name*="_emails_attributes"],input[name*="email_address"]').each ->
			unless current_name == this.name
				# alert this.name
				$(this).addClass('email')
				current_name = this.name
			return
		@formId = "#" + @formId

		$(@formId).children(':first').find(':input:first').focus()


	reservationHandler: (form) =>
		alert 'reservationHandler called ' + @formId
		form.submit()			
		return
			
	enable_remove: ->
		$("a.remove_email").bind 'click', (ev) =>
			that= this
			link = ev.target
			container = $(link).closest("div.item")
			persist=false
			if $(container).find('input[id*="_id"]').length > 0
				$(container).find('input[id*="_destroy"]').val("true")
				$(container).hide()
				persist = true
			else
				if $(container).closest("div.field").find('div.item:not(:hidden):not(:has(input[id*="_id"]))').length > 1
				
					$(container).hide()	
				else 
					# don't erase the last field, clear it
					$("#"+@formId).resetForm()
		
	

$ ->
	return if $('#reservations_controller').length == 0  and $('#registrations_controller').length == 0 
#	alert("reservations controller")

	rez_form = $("form[id*='reservation']").first()
	if rez_form.length > 0
		window.rezForm = new ReservationForm $(rez_form).attr('id')

		# form submit handler
		$(rez_form).bind 'submit', (ev) ->
			check_clearError()
			eml = $(rez_form).find("input.email:text")
			count = 0
			invalid = 0
			eml.each ->
				tmp = $(this).val()
				if tmp.length > 0
					typ = $(this).attr('id')
					if ! typ.match(/_to_/) 
						if ! typ.match(/_cc_/)
							if $(this).attr('readonly') != undefined
								return false 
					count++
					if ! check_email(tmp)
						$(this).addClass("error")
						invalid++	
			if invalid
				check_errorMsg("email address is incorrect format")
			if count < 1
				check_errorMsg("You must provide at least one recipient")
				invalid++
			if invalid
				ev.stopImmediatePropagation()
				return false

			return true

		# add button, Add To, Add Cc click handler
		addbutt = $(rez_form).find(".addbutt")		
		if addbutt.length > 0
			clean_up = (eml) ->
				check_clearError()
				if( $(eml).hasClass('error') )
					$(eml).removeClass('error')

			remove_row = (target) ->	
				container = $(target).closest("div.item").remove()
				# rebind the rows so that one row will always remain
				removal_bind()

			erase_row = (target) ->
				eml = $(target).closest("div.item").find('input[type=text]')
				if eml.length > 0
					eml.first().val("")
					mbutts = $(target).closest(".remove_email")
					mbutts.unbind 'click'
					mbutts.removeAttr('href')

			removal_bind = ->
				for etype_ids in [ "#to_box" , "#cc_box" ]
					do (etype_ids) ->
						mbutts = $(rez_form).find(etype_ids + " .remove_email")
						mbutts.unbind 'click'
						if mbutts.length > 1
							mbutts.attr("href","#")
							mbutts.bind 'click', (ev) ->
								remove_row(ev.target)
								ev.stopImmediatePropogation()
								return false
						else
							eml = mbutts.first().closest("div.item").find('input[type=text]')
							content = eml.first().val()
							if content.length > 0 
								mbutts.bind 'click', (ev) ->
									erase_row(ev.target)
									ev.stopImmediatePropogation()
									return false
								mbutts.attr("href","#")
							else
								mbutts.removeAttr('href')

			add_row = (target) ->
				check_clearError()
				association = target.attr('name').substring(0,2)
				eml = $("input.email[id*='_" + association  + "']")
				invalid = false
				if( eml.length < 1 )
					invalid = true
				else 
					addr =  $(eml).last().val()
					if( addr.length > 0 )
						if ! check_email(addr)
							invalid = true
					else
						invalid = true
					if invalid
						$(eml).last().addClass("error")
						check_errorMsg("invalid email address")
				if ! invalid
					where = "#" + association + "_box"
					obj_time = new Date().getTime() 
					if( association == "to" )
						etype = "T"
					else if ( association == "cc" )
						etype = "C"
					id_base = "reservation_" + association + "_links_attributes_" + obj_time 
					name_base = "reservation[" + association + "_links_attributes][" + obj_time + "]"
					inp1 = "<input id=\"" + id_base  + "_email_type\" type=\"hidden\" value=\"" + etype + "\" name=\"" + name_base + "[email_type]\">"
					inp2 = "<input id=\"" + id_base + "_email_addresses_attributes_email_address\" class=\"email\" type=\"text\" size=\"30\" name=\"" + name_base + "[email_addresses_attributes][email_address]\">"
					inp3 = "<input id=\"" + id_base + "_destroy\" type=\"hidden\" value=\"false\" name=\"" + name_base + "[_destroy]\">"
					inp4 = "<a href=\"#\" class=\"remove_email\"><span id=\"minusbutton\"></span></a>"
					html = "<div class=\"item\">" + inp1 + inp2 + inp3 + inp4 + "</div>"
					$(html).appendTo($(where)) 
					$(rez_form).find(".email").bind 'keyup', (ev) ->
						if $(this).hasClass('error') 
							clean_up( $(this) )
					removal_bind()

			$(addbutt).bind  'click', (ev) ->
				if( $(ev.target).attr('id') == 'plusbutton')
					add_row( $(ev.target).parent() )
				else
					add_row( $(ev.target) )
				ev.stopImmediatePropogation()
				return false

			$(rez_form).find(".email").bind 'keyup', (ev) ->
				if $(this).hasClass('error') 
					clean_up( $(this) )
			
			removal_bind()
							

	if $('#instructions').length > 0
		show_instructions = (ev) ->
			$('#instructions .contracted').fadeIn(2222, 
				-> 
					$('#instructions').css color: 'red'
					$('#instructions .expanded').hide()
			)

		hide_instructions = (ev) ->
			$('#instructions .contracted').fadeOut(2222, -> $('#instructions .expanded').show() )

		$('#show_instructions').click (ev) -> 
			show_instructions(ev)
		$('#hide_instructions').click (ev) -> 
			hide_instructions(ev) 


	if $('#document_password_help').length > 0 
		pwd_help_div = $('#document_password_help')
		$(pwd_help_div).dialog({
				modal: true,
				autoOpen: false,
				position: [ 'center', 0] ,
				height: 350,
				width: 542,
				title: 'Setting a Document Password',
				show: 'blind',
				hide: 'explode'
			})

		show_pwd_help = (ev) ->
			$('#document_password_help').dialog({title: 'Document Password Help'})
			$('#document_password_help').show()
			$('#document_password_help').dialog('open')
		
			
		if $('#DocumentPasswordHelpLink').length > 0 
			$('#DocumentPasswordHelpLink').click (ev) -> 
				show_pwd_help(ev)



	if $('#approval_help').length > 0 
		approval_help_div = $('#approval_help')
		$(approval_help_div).dialog({
				modal: true,
				autoOpen: false,
				position: [ 'center', 0] ,
				height: 350,
				width: 542,
				title: 'Approving a Fax',
				show: 'blind',
				hide: 'explode'
			})

		show_approval_help = (ev) ->
			$('#approval_help').dialog({title: 'Approving a Fax'})
			$('#approval_help').show()
			$('#approval_help').dialog('open')
		
			
		if $('#ApprovalHelpLink').length > 0 
			$('#ApprovalHelpLink').click (ev) -> 
				show_approval_help(ev)



	if $('#no_ecp_help').length > 0 
		noecp_help_div = $('#no_ecp_help')
		$(noecp_help_div).dialog({
				modal: true,
				autoOpen: false,
				position: [ 'center', 0] ,
				height: 350,
				width: 542,
				title: 'Exclude the Coversheet from a fax PDF',
				show: 'blind',
				hide: 'explode'
			})

		show_no_ecp_help = (ev) ->
			$('#no_ecp_help').dialog({title: 'Exclude the Covershee from a fax PDF'})
			$('#no_ecp_help').show()
			$('#no_ecp_help').dialog('open')
		
			
		if $('#NoECPHelpLink').length > 0 
			$('#NoECPHelpLink').click (ev) -> 
				show_no_ecp_help(ev)


	
	wrapper = $('div.reservations_wrapper')

	if wrapper.length > 0
		view_regexp = new RegExp("XXX", "g");


		link = wrapper.find('div.reservation_data').first().text()


		view_url = wrapper.find('div.show_url').first().text()


		table = wrapper.find('table.display')

#		alert ("tab link "+ link + " show_pdf_link: " + view_url)

		args = 
			"bJQueryUI" : true,
			"bLengthChange" : false,
			"iDisplayLength" : 8,
			"bPaginate" : true,
			"sScrollX": "100%",
			"bScrollCollapse": true,
			"bStateSave" : true,
			"bServerSide" : true,
			"sAjaxSource": link,
			"sCookiePrefix": "rez_table_",
			'aaSorting' :[[3,'desc']],
			"bAutoWidth": false

		args["aoColumnDefs"] = [
				{
					sWidth : '15%'
					aTargets: [0]
					sClass: "rid_date"
				},
				{
					sWidth : '15%'
					aTargets :[ 1]
					sClass: "recipients"
					bSortable: false
				},
				{
					sWidth : '30%'
					sClass: "subject"
					aTargets: [2]

				},
				{
					sWidth : '15%'
					sClass: "filename"
					aTargets: [3]
					bSortable: false
				},
				{
					sClass: "locator"
					aTargets: [4]
					bSortable: false
				}
			]
		
		oTable = $(wrapper).find('table.display').dataTable( args )
		$('tbody',oTable).on 'click','tr', (ev) ->
			ev.stopPropagation()
#			$('.actions a',this).trigger('click')
#			alert(" view_url: " +  view_url + " locator: " + $('.locator',this).text())
			location = view_url.replace(view_regexp,$('.locator',this).text())
			document.location = location



